//	ishipshape.js

'use strict';

class CSplineEdit {
	constructor(id,width,height,exposedParms){
		console.log('CSplineEdit::constructor('+id+','+width+','+height+')');
		this._exposedParms		= exposedParms;
		this._mouseOverPosition	= this.mouseOverPosition.bind(this);
		this._mouseDownPosition	= this.mouseDownPosition.bind(this);
		this._mouseMovePosition	= this.mouseMovePosition.bind(this);
		this._mouseUpPosition	= this.mouseUpPosition.bind(this);
		this.bezier = 
			[[ 0, 2/3, 1/3,   0 ]
			,[ 0, 1/3, 2/3,   0 ]
			,[ 0, 1/6, 2/3, 1/6 ]
			];
		
		this.nodeSVG = i.svg('',{id:id,style:'position:absolute;width:'+width+'px;height:'+height+'px;left:0;top:2em;border:0px;'});
		//	create path first so the pullpoint are above it (like z-order which is available since SVG2.0
		this.shape = i.path('',{class:'line',fill:'none',stroke:'steelblue','stroke-width':exposedParms.shapeStrokeWidth+'px','z-index':-1});
		this.nodeSVG.appendChild(this.shape);
		this.nodePoints = [];
		exposedParms.pullPoints.myforEach(function(pt,key,arr,that){
			var nodepoint = i.circle('',
				{cx:pt[0] * width / 540
				,cy:pt[1] * height / 840
				,r:6.5
				,stroke:				exposedParms.color.pullPointStroke
				,'stroke-width':	exposedParms.color.pullPointStrokeWidth+'px'
				,fill:				exposedParms.color.pullPointFill
				,'fill-opacity':	exposedParms.color.pullPointFillOpacity
				,cursor:'move'
				,'z-index':1
				});
			that.nodeSVG.appendChild(nodepoint);
			that.nodePoints.push(nodepoint);
			nodepoint.addEventListener("mouseover",that.mouseOverPosition.bind(that));
			},this);

	// resize(width,height)
			
		var test = this.linear();
		//this.shape = i.path('',{class:'line',fill:'none',stroke:'steelblue','stroke-width':exposedParms.shapeStrokeWidth+'px',d:this.linear()});
		this.shape.setAttribute("d",this.linear());
		
		if (undefined!=width)	this.resize(width,height);
		
		this.draw(this.rotation,width/2,height/2);
		return this;
		}
	destruct(){
		console.log('CSplineEdit::destruct()');	
		this.nodeSVG.remove();
		var pt; while (pt=this.nodePoints.shift()){
			pt.remove();
			}
		}
	svg(){
		return this.nodeSVG;
		}
	resize(width,height/*,exposedPoints*/){
		console.log('resize('+width+','+height+')');
		if (0==width) {
			return;	//	this.resize(200,200);
			}
		this.nodePoints.myforEach(function(pt,ii,arr,that){
			var factor =	{x: parseFloat(that.nodeSVG.style.width)
								,y: parseFloat(that.nodeSVG.style.height)};
			var t1 = parseFloat(width)  / (factor.x>0?factor.x:parseFloat(width));
			var t2 = parseFloat(pt.getAttribute('cx'))+0.499;
			
			factor =	{x: Math.min(parseInt(parseFloat(width)  / (factor.x>0?factor.x:parseFloat(width))	* parseFloat(pt.getAttribute('cx'))+0.4999),width)
						,y: Math.min(parseInt(parseFloat(height) / (factor.y>0?factor.y:parseFloat(height))	* parseFloat(pt.getAttribute('cy'))+0.4999),height)};
			// console.log('resize factor=('+factor.x+','+factor.y+')');	
			that._exposedParms.pullPoints[ii][0] = factor.x;			pt.setAttribute('cx', factor.x);
			that._exposedParms.pullPoints[ii][1] = factor.y;			pt.setAttribute('cy', factor.y);
			},this);
		this.nodeSVG.style.width = width;
		this.nodeSVG.style.height = height;
		this.draw();	
		}	
	mouseOverPosition(evt){
		// console.log('CSplineEdit::mouseOverPosition(evt)');
		evt.currentTarget.addEventListener("mousedown", this._mouseDownPosition, false);
		}	
	mouseDownPosition(evt){
		// console.log('CSplineEdit::mouseDownPosition(evt)'+JSON.stringify({x:evt.clientX,y:evt.clientY}));
		this.dragPoint = evt.currentTarget;
		var cx = parseInt(this.dragPoint.getAttribute('cx'));
		var cy = parseInt(this.dragPoint.getAttribute('cy'));
		this.dragPoint.startDrag = {x:cx-evt.clientX,y:cy-evt.clientY};
		window.addEventListener("mousemove", this._mouseMovePosition, false);
		window.addEventListener("mouseup", this._mouseUpPosition, false);
		}	
	mouseMovePosition(evt){
		// console.log('CSplineEdit::mouseMovePosition(evt)'+JSON.stringify({x:evt.clientX,y:evt.clientY}));
		this.dragPoint.setAttribute('cx',this.dragPoint.startDrag.x+evt.clientX); 
		this.dragPoint.setAttribute('cy',this.dragPoint.startDrag.y+evt.clientY);
		this.draw();
		}	
	mouseUpPosition(evt){
		window.removeEventListener("mousemove", this._mouseMovePosition, false);
		window.removeEventListener("mouseup", this._mouseUpPosition, false);
		}	
	d3_svg_lineDot4(a, b) {
		return Math.round(a[0] * b[0] + a[1] * b[1] + a[2] * b[2] + a[3] * b[3],3);
		}
	d3_svg_lineBasisBezier(path, x, y) {
		path.push("C"
			, this.d3_svg_lineDot4(this.bezier[0], x), ","
			, this.d3_svg_lineDot4(this.bezier[0], y), ","
			, this.d3_svg_lineDot4(this.bezier[1], x), ","
			, this.d3_svg_lineDot4(this.bezier[1], y), ","
			, this.d3_svg_lineDot4(this.bezier[2], x), ","
			, this.d3_svg_lineDot4(this.bezier[2], y));
		}
	d3_svg_lineBasis(points) {
		if (points.length < 3) return this.d3_svg_lineLinear(points);
		var ii = 1
		, n = points.length
		, pi = points[0]
		, x0 = pi[0]
		, y0 = pi[1]
		, px = [ x0, x0, x0, (pi = points[1])[0] ]
		, py = [ y0, y0, y0, pi[1] ]
		, path = [ x0, ",", y0, "L"
			, this.d3_svg_lineDot4(this.bezier[2], px), ","
			, this.d3_svg_lineDot4(this.bezier[2], py) ];
		points.push(points[n - 1]);
		while (++ii <= n) {
			pi = points[ii];
			px.shift();			px.push(pi[0]);
			py.shift();			py.push(pi[1]);
			this.d3_svg_lineBasisBezier(path, px, py);
			}
		points.pop();
		path.push("L", pi);
		return 'M'+path.join("");
		}
	linear(){
		// console.log('linear()');
		var d3points = [];
		var mirror = [];
		try {
			var wh = {	w:this.nodeSVG.width.baseVal.value	
						,	h:this.nodeSVG.height.baseVal.value	};
			}
		catch(err){
			// pak maar een width en height
			var wh = {	w:parseInt(this.nodeSVG.style.width)
						,	h:parseInt(this.nodeSVG.style.height) };
			}
		// console.log('linear() wh: '+JSON.stringify(wh));
		if (0==wh.w) return;
		this.nodePoints.myforEach(function(pt,key,arr,that){
			d3points.push([parseInt(pt.getAttribute('cx')),parseInt(pt.getAttribute('cy'))]);
			mirror.push([wh.w-parseInt(pt.getAttribute('cx')),parseInt(pt.getAttribute('cy'))]);
			},this);	
		d3points = d3points.concat(mirror.reverse());
		// console.log('linear() d3points: '+d3points.length);
		var rc = this.d3_svg_lineBasis(d3points);
		return rc;		
		}
	draw(){
		this.shape.setAttribute('d',this.linear());
		}
	};
class CInstrument_shipshape extends CInstrument {
	constructor(obj,exposedParms,active,idx){
		super(idx,obj);
		if (undefined!=exposedParms)
			this.exposedParms = exposedParms;
		else
			this.exposedParms = //	todo deze moeten door de conning designer kunnen worden gezet, nier overrulen hier dan
				{test:['a','b']
				,globalAlpha:					1.0
				,pullPoints:
					[[270,10]
					,[  6,278]
					,[ 16,720]
					,[  6,800]
					]
				,shapeStrokeWidth: 10	
				,color:
					{pullPointStroke:'rgba(40,40,40,0.6)'
					,pullPointStrokeWidth:1.5
					,pullPointFill:'rgba(255,255,255,0.6)'
					,pullPointFillOpacity:0.6
					}
				};
				
		this.active = (undefined==active)? false: active;
		if (this.active){
			// this.divclick = this.divClick.bind(this);
			// this.div.addEventListener("click", this.divclick, false);
			// this.div.click();
			if (0==this.div.offsetWidth && 0==this.div.offsetHeight)
				var wh = {w:100,h:100};
			else
				var wh = {w:this.div.offsetWidth,h:this.div.offsetHeight};
			this.splineEditor = new CSplineEdit('_shape',wh.w,wh.h,this.exposedParms);
			this.div.appendChild(this.splineEditor.svg());
			this.draw();
			}
		}
	destruct(){
		this.splineEditor.destruct();
		}
	// divClick(evt){
		// // todo determine width and height of div
		// evt.target.removeEventListener("click", this.divclick, false);
		// }
	resize(width,height){
		// console.log('shipshape resize('+width+','+height+')');
		this.splineEditor.resize(width,height,this.pullPoints);
		}
	draw(ship) {
		}
	};

CInstrument.addInstrumentToSettings('./ishipshape.js',			'shipshape',	'div',		CInstrument_shipshape,	'shows ship shape under instruments');
