//	aiscpamatrix.js
'use strict';
// moved to zeroisim.js var aislist = [];	//	global variable
function getaiscpamatrix(divtable){
	//	todo met reverse een mooiere matrix maken
	//	todo put own ship in list
	var cpyaislist = aislist;
	// cpyaislist.reverse();// werkt niet, andersom itereren door array
	var row0 = [];
	var rows = [];
	rows[0] = [];	//	horizontale scheepsnamen
	rows[1] = [];	//	posities van schepen
	var rc = [];

	//	maak tweedim array met daarin col[0] en row[0] is schepen, inhoud is de cpa en tcpa tussen deze schepen 
	rows[0].push(h.nbsp);	//rows[0].push('me');
	rows[1].push(h.nbsp);	//rows[1].push(h.nbsp);
	var cols = 0;	
	for (var ii in aislist){		
		var schip = aislist[ii];
		if (!zeroisim.isFn(schip)){
			// rows[0].push(schip.Name+((schip.MMSI == ship0.mmsi)?' *':''));
			rows[0].push(schip.Name+((schip.ownship==true)?' *':''));
			var aispos = new zeroisim.LatLon({latdeg: schip.Latitude, londeg: schip.Longitude});
			rows[1].push
				(h.nbsp+aispos.latdegmindec+'<br/>'
				+aispos.londegmindec+'<br/>'
				+'HDGt='+schip.HDGt+h.deg+' rot='+schip.ROT+h.deg+h.br
				+'SOG ='+schip.SOG
				);
			aispos = undefined;
			cols++;
			}
		}
	var row = cols+1;	
	for (var ii in cpyaislist){
		var schip = cpyaislist[ii];
		if (!zeroisim.isFn(schip)){
			rows[row] = [];
			rows[row][0] = schip.Name+((schip.MMSI == ship0.mmsi)?' *':'');
			--row;
			}
		}
	row = cols+1;	
	var rc='';
	// bepaal mijn midden adhv VDO
	for (var ii in aislist){
		var schip0 = aislist[ii];
		if (!zeroisim.isFn(schip0)){
			var s0 = {hdt:  schip0.COG, spd: schip0.SOG, lat: schip0.Latitude, lon:schip0.Longitude};	
			for (var jj in aislist){
				if (jj==ii){
					rows[row].push(h.nbsp);	//	todo hier mag je eigenlijk uit deze loop springen
					}
				else {
					var schip1 = aislist[jj];
					if (!zeroisim.isFn(schip1)){
						var s1 = {hdt:  schip1.COG, spd: schip1.SOG, lat: schip1.Latitude, lon:schip1.Longitude};	
						var cpa = tcpa(s0,s1);
						rows[row].push
							('cpa='+cpa.cpa.toFixed(2)+' NM<br/>'
							+'tcpa='+Math.floor(cpa.tcpa/60).toFixed(0)+':'+Math.floor((cpa.tcpa*60)%60).toFixed(0)+':'+Math.floor((cpa.tcpa*3660)%60).toFixed(0)+'<br/>'
							+'brg='+cpa.bearing+'<br/>'
							+'dst='+cpa.range
							//	todo bowcrossing distance and time
							);
						}
					}
				}
			--row;
			}
		}
	var stophere;	
	var rc='';
	row = 0;
	for (var row in rows){
		if (!zeroisim.isFn(row)){
			var trow='';
			for (var col in rows[row]){
				if (!zeroisim.isFn(rows[row][col])){
					trow += h.td(rows[row][col]);
					}
				}	
			rc += h.tr(trow);	
			}
		}
	rc = h.table(rc,'border="1px" style="'+settings.style_aistable+'"');
	setValue(divtable,rc);
	}
function getaistable(divtable){
	setValue(divtable,asTable2(aislist,'border="1px" style="'+settings.style_aistable+'"',
	['Name'
	,'CallSign'
	,'MMSI'
	,'TimeStamp'
	,'ShipType'
	// ,'Longitude','Latitude'	//	degrees, decimals not so readable by sailors
	,'lat_dmd','lon_dmd'	//	these are the nice formatted ones
	,'COG'
	,'HDGt'	//	'TrueHeading'
	,'IMOnumber'
	,'dimRefPoint'	
	,'destination'
	,'ETA'
	,'draught'
	,'ROT'
	,'SOG'
	]));
	}
var asTable2=function(obj,att,firstrow){
	var table='';
	if (undefined!=firstrow) {
		var row='';
		firstrow.myforEach(function(td){
			row += h.td(td);
			});
		table += h.tr(row);	
		
		for (ii in obj) {
			var row='';
			firstrow.myforEach(function(fr,jj,arfr,objt){
				row += h.td(objt[fr]);
				},obj[ii]);
			table += h.tr(row);	
			}
		}
	else {	
		for (var ii in obj) {
			var row='';
			for (var jj in obj[ii]){
				row += h.td(obj[ii][jj]);
				}
			table += h.tr(row);	
			}
		}
	return h.table(table,att);	
	}	
	

