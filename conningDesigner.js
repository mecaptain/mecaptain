//	conningDesigner.js
'use strict';

//	todo maak instrumenten pas aktief naar op design slepen. Verplaatsen: eerst destructen en daarna opnieuw constructor aanroepen
// voor symbolen zie https://www.w3schools.com/charsets/ref_utf_geometric.asp
//	voor pointers https://www.w3schools.com/cssref/playit.asp?filename=playcss_cursor

class CTabConningDesigner extends C0isimTab {
	constructor(settings){
		super();
		this.settings = settings;
		this.modePreview = false;
		this.btnPreview = i.button('preview',{click:this.hPreview.bind(this),id:'cd_preview'});
		this.cd_header = i.div(
			[i.span('&#9776;',{selectable:'off',click:this.menuclick.bind(this)})
			,i.span('mode',{id:'cd_mode',selectable:'off',style:'text-align:center;'})
			,this.cd_menucontainer=i.div(
				[i.button('Open',{click:this.hOpen.bind(this),id:'cd_Open'})
				,i.button('Save',{click:this.hSave.bind(this),id:'cd_Save'})
				,i.button('Properties',{click:this.hDesignerProperties.bind(this),id:'cd_Designerproperties'})
				,this.btnPreview
				],{id:'cd_menucontainer'})
			],{id:'cd_header'});
		this.cd_toolbar = i.div('',
			{id:'cd_toolbar',
			selectable:'off',class:'split split-horizontal'
			});
		this.cd_editor = i.div('drop here the draggable instruments from the left panel to create an instrument layout.',
			{id:'cd_editor'
			,draggable:'false'
			,dragover:this.editor_dragover.bind(this)
			,drop:this.editor_drophandler.bind(this)
			,class:'split split-horizontal'
			});
		this.cd_propedit = i.div('click on &#9776; symbol of instrument to edit parameters here',
			{id:'cd_propedit'
			,class:'split split-horizontal'
			});	
		if (true && 'file://'==window.location.href.substr(0,7)){
			console.log('conningDesigner, installed handler for interwindow comms via localstorage');
			window.addEventListener('storage', function(evt) {  //	werkt niet onder chrome, geen idee waarom niet
				// console.log('conningDesigner, sto_event key='+evt.key);
				// console.log('conningDesigner, sto_event old='+evt.oldValue);
				// console.log('conningDesigner, sto_event new='+evt.newValue);		
				//	todo eigenlijk checken of wid in externalWindowIids zit
				var stochange = JSON.parse(evt.newValue);	
				if (undefined!=stochange.cmd){
					if ('rxcb'==stochange.cmd){
						zeroisim.busTX(stochange.parms.key,stochange.parms.data);	//	todo misschien nog checken of dit storage event van een gespawnt window afkomt
						}
					}
				});
			}
		var dependencies = [];	//	add all of instruments dependencies
		this.settings.instrumentList.myforEach(function(instr,ii,ar,that){
			var container = CInstrument.makeContainer4(instr,{w:400,h:400});
			that.cd_toolbar.appendChild(that.createToolbarInstrument
				(instr.file	//	file
				,instr.name	//	caption
				,container	//	dom(space for the instrument, typically a canvas)
				,instr.name	//	name
				,instr.flyo	//	flyo(flyover description of the instrument)
				));
			if (undefined!=instr.dependencies){
				dependencies = dependencies.concat(instr.dependencies);	
				}
			},this);
		dependencies = dependencies.zeroisimValuesUnique();	
		var ls = new CLoadScript();
		ls.loadScripts({scripts:dependencies});	
		}
	onload(evt){
		document.getElementById(this._tabdivname).appendChild(i.div(
			[this.cd_header
			,this.cd_toolbar
			,this.cd_editor
			,this.cd_propedit
			],{class:'tmpcontainer', style:'height:1000px;width:100%;position:absolute;background-color:#DDD;'})
			);
		this.hResizer = this.resize.bind(this);
		this.hResizerStop = this.stopResize.bind(this);
		// this.hresizeWindowMove = this.resizeWindowMove.bind(this);
		// this.hresizeWindowStop = this.resizeWindowStop.bind(this);
		this.externalWindowIids = [];	//	registrate this external window to facilitate communication with window over localstorage
		this.splitter = Split(['#cd_toolbar', '#cd_editor', '#cd_propedit'], 
			{sizes: [23, 60, 17]
			,minSize: 200
			});
		// window_resizer.addEventListener('mousedown', this.hresizeWindowStart.bind(this), false);
		// this.btnPreview.click();
		if (settings.conning){	//	load conning from webserver
			var urlconning=zeroisim.gup('conning');
			console.log('nmea_scripts/conning.js, conning='+urlconning);
			loadScript(urlconning,function(that){
				that.FileOnLoad(conning);
				that.btnPreview.click();
				},[this]);
			}
		else {
			this.btnPreview.click();
			this.btnPreview.click();
			}
		}
	applyBehaviours(obj,exposedParameters,active) {	//	todo ik denk dat dat beter meteen bij creation kan
		if (true===active){
			// test alleen de instrumentbar drag handlers te geven
			var divbar = obj.querySelector('.instrumentbar');
			divbar.addEventListener('dragstart', this.dragstart_handler.bind(this), false);
			divbar.addEventListener('dragend', this.dragend_handler.bind(this), false);
			divbar.draggable = true;
			obj.draggable = false;
			}
		else {	
			obj.addEventListener('dragstart', this.dragstart_handler.bind(this), false);
			obj.addEventListener('dragend', this.dragend_handler.bind(this), false);
			}
		// var resize = obj.querySelector('.resizer');
		// var close = obj.querySelector('.close');
		obj.querySelector('.resizer').addEventListener('mousedown', this.resizeStart.bind(this), false);
		obj.querySelector('.extwin').addEventListener('mousedown', this.hExtwin.bind(this), false);
		obj.querySelector('.close').addEventListener('click',function(){
			var instrument = this.parentNode.parentNode;
			instrument.externObj.destruct();	//	unsubscribes cbRxNmea
			instrument.parentNode.removeChild(instrument);
			}, false);
		obj.querySelector('.properties').addEventListener('click', this.hClickParameter.bind(this), false);	//	todo do this upon creation
		this.addExternInstrument(obj,exposedParameters,active);
		}
	hDesignerProperties(evt){
		//	todo let properties appear in property editor
		var designerProperties 
		=	{gridPosition:0
			,gridSize:0
			,colorBackground:'rgba(200,200,200,0.8)'
			};	//	geef designer window exposed parameters
		//	maak dit this.currentInstrument 
		var etable = new CeditableTable(designerProperties,undefined,undefined,this.parameterEdited.bind(this));
		etable.style['font-size'] = '0.6em';	//	}{style:'font-size:0.6em;'}
		zeroisim.replaceNode
			(document.getElementById('cd_propedit')
			,[i.div('designer'),etable]
			);	
		}
	hClickParameter(evt) {	//	instrument parameter menu clicked
		var instrument = evt.currentTarget.parentNode.parentNode;
		var caption = instrument.getAttribute('caption');
		this.currentInstrument = instrument;
		// console.log('settings clicked on ' + instrument.outerText);
		if (undefined!=instrument.externObj){
			var parms = instrument.externObj.getExposedParameters();
			var etable = new CeditableTable(parms,undefined,undefined,this.parameterEdited.bind(this));
			etable.style['font-size'] = '0.6em';	//	}{style:'font-size:0.6em;'}
			zeroisim.replaceNode
				(document.getElementById('cd_propedit')
				,[i.div(caption),etable]
				);	
			}
		}	
	//	todo verhuis meeste van deze code naar CInstrument	
	hExtwin(evt){	//	start instrument in external window
		var instrument = evt.currentTarget.parentNode.parentNode.externObj;
		var wh = 
			{w:instrument.html?	instrument.html.offsetWidth:	instrument.div.offsetWidth
			,h:instrument.html?	instrument.html.offsetHeight:	instrument.div.offsetHeight
			};
		var wid = evt.currentTarget.parentNode.parentNode.id;
		
		localStorage.setItem(wid,JSON.stringify(instrument.getExposedParameters()));	//	put ExposedParameters in localstorage, to much information for a url
		this.externalWindowIids.push(wid);	//	registrate this external window to facilitate communication with window over localstorage
		var extwin = window.open
			('iwin.html?'+encodeURIComponent
				('extwin=1&idx='+instrument.idxInInstrumentList
				+'&w='+wh.w
				+'&h='+wh.h
//				+'&exp='+JSON.stringify(instrument.getExposedParameters()))
				+'&wid='+wid)	//	key to localstorage where exposedParms are stored
			,'_blank'	//	target=_blank?
			,'height='+wh.h
				+',width='+wh.w
				+',left=20,top=20,toolbar=no,location=no,scrollbars=no,status=no,resizable=yes,fullscreen=no'
			);
		extwin.focus(); //	focus to the external window
		var close = evt.currentTarget.parentNode.parentNode.querySelector('.close');
		close.click();	//	now close the conning instrument
		}
	parameterEdited(instr,parm,newval){
		// console.log('parameterEdited(evt), nw value='+evt.target.value);
		// this.currentInstrument.externObj.exposedParms[evt.target.id] = evt.target.value;
		this.recreateInstrument(this.currentInstrument,this.currentInstrument.externObj.exposedParms);
		}
	menuclick(evt){
		this.cd_menucontainer.style.display = (this.cd_menucontainer.style.display == 'block')? 'none': 'block'
		}	
	hPreview(evt){
		this.modePreview = this.modePreview? false: true;
		// var test = document.body.querySelectorAll('.clone');
		// document.body.querySelectorAll('.clone').myforEach(function(instr,ii,ar,that)
		Array.prototype.forEach.call(document.body.querySelectorAll('.clone'),function(instr){
			var bar=instr.querySelector('.instrumentbar');
			var resizer=instr.querySelector('.resizer');
			var extwin=instr.querySelector('.extwin');
			if (this.modePreview){	// if (bar.style.display=='none'){
				bar.parentNode.style.top = bar.parentNode.offsetTop + bar.offsetHeight + "px";
				bar.style.display = 'none';
				resizer.style.display = 'none';
				// extwin.style.display = 'none';
				instr.style.border = 'none';
				}
			else {
				bar.style.display = 'block';
				resizer.style.display = 'block';
				extwin.style.display = 'block';
				instr.style.border = '2px solid silver';	//	todo Wim, hoe vraag ik deze waarden op?
				bar.parentNode.style.top = bar.parentNode.offsetTop - bar.offsetHeight + "px";
				}
			},this);	
		if (this.modePreview){
			evt.currentTarget.value = evt.currentTarget.innerHTML;
			evt.currentTarget.innerHTML = h.checkmark + h.nbsp + evt.currentTarget.innerHTML;
			setValue('cd_mode',' Preview mode');
			// todo split de toolbar en de properties weg
			this.splitter.collapse(0);
			this.splitter.collapse(2);
			}
		else {
			evt.currentTarget.innerHTML = evt.currentTarget.value;
			setValue('cd_mode',' Design mode');
			this.splitter.setSizes([23, 60, 17]);
			}
		this.cd_menucontainer.style.display = 'none';
		}	
	addExternInstrument(obj,exposedParms,active) {
		//	find instrument in directory
		var idx = this.settings.instrumentList.findIndex(function(needle){
			return (obj.classList.contains(needle.name));
			});
		if (-1===idx)	return;	//	instrument not found
		obj.externObj = new this.settings.instrumentList[idx].clas(obj,exposedParms,active,idx);	//	instantiate class
		}
	/* not needed if native resizer works resize:both; */	
	resizeStart(ev) {
		console.log('resizeStart');
		var evt = ev || window.event;
		evt.preventDefault();
		if (0) {
			evt.target.addEventListener("mousemove", this.hResizer, false);
			evt.target.addEventListener("mouseup", this.hResizerStop, false);
			evt.target.addEventListener("mouseout", this.hResizerStop, false);
			evt.target.resizeStartX = evt.clientX, evt.target.resizeStartY = evt.clientY;
			}
		else {	
			// nieuwe poging
			window.addEventListener("mousemove", this.hResizer, false);
			window.addEventListener("mouseup", this.hResizerStop, false);
			// window.addEventListener("mouseout", this.hResizerStop, false);
			evt.target.resizeStartX = evt.clientX, evt.target.resizeStartY = evt.clientY;
			this.resizingResizer = evt.target;
			}
		}
	resize(ev) {
		console.log('conningDesigner resize() ');
		var e = ev || window.event;
		if (0){
			//	e.target is de span.resizer
			var dx = e.clientX - e.target.resizeStartX;
			var dy = e.clientY - e.target.resizeStartY;
			e.target.resizeStartX = e.clientX, e.target.resizeStartY = e.clientY;
			var instrument = e.target.parentNode.parentNode;
			//	e.target.parentNode is div.instrumentbody
			//	e.target.parentNode.parentNode is div.cd_instrument_16.instrument.compass
			}
		else {	
			var dx = e.clientX - this.resizingResizer.resizeStartX;
			var dy = e.clientY - this.resizingResizer.resizeStartY;
			this.resizingResizer.resizeStartX = e.clientX, this.resizingResizer.resizeStartY = e.clientY;
			var instrument = this.resizingResizer.parentNode.parentNode;
			}
		var wh = {w:(instrument.offsetWidth)+dx,h:(instrument.offsetHeight)+dy};
		instrument.style.width = wh.w + "px";
		instrument.style.height = wh.h + "px";
		if (undefined!=instrument.externObj)
			instrument.externObj.resize(wh.w,wh.h);
		// console.log('new wh:'+instrument.style.width+','+instrument.style.height);
		}
	stopResize(evt) {	//	todo het lijkt of deze bij resize twee keer achter elkaar runt, dat moet maar een keer zijn
		console.log('resizeStop');
		evt = evt || window.event;
		if (0){
			var suc0 = evt.target.removeEventListener("mousemove", this.hResizer,false);
			var suc1 = evt.target.removeEventListener("mouseup", this.hResizerStop,false);
			var instrument = evt.target.parentNode.parentNode;
			}
		else {
			var suc0 = window.removeEventListener("mousemove", this.hResizer,false);
			var suc1 = window.removeEventListener("mouseup", this.hResizerStop,false);
			var instrument = this.resizingResizer.parentNode.parentNode;
			}
		this.recreateInstrument(instrument,instrument.externObj.getExposedParameters());	//	redraw the instrument
		//	todo maybe with new wh dimensions
		}
	recreateInstrument(instrument,exposedParms,active){
		var canvas = instrument.querySelector("canvas");
		if (undefined!=canvas){
			//	instead of scaling the canvas, discard the canvas, recreate it and pass it to externObj
			var new_canvas =
				{id:canvas.id
				// ,style:canvas.style
				,itype:canvas.getAttribute('cd_itype')
				// ,width:parseInt(instrument.style.width)-24 + 'px'
				,height:parseInt(instrument.style.height)-44 + 'px'	//	todo trek style margin van instrument af
				,width:canvas.width
				// ,height:canvas.height
				};
			var parentCanvas = canvas.parentNode;
			parentCanvas.removeChild(canvas);	//	todo does this removeEvent handlers?
			parentCanvas.appendChild(i.canvas(new_canvas));
			}
		if (undefined!=instrument.externObj){
			instrument.externObj.destruct();	//	unsubscribes cbRxNmea
			instrument.externObj = undefined;
			this.addExternInstrument(instrument,exposedParms,true);
			}
		}
	/**/	
	dragstart_handler(ev) {
		var obj = ev.currentTarget;
		console.log('dragstart_handler: '+obj.className);
		if ('instrumentbar'==obj.className)			obj = ev.target.parentNode;	// drag instrument on instrumentbar
		ev.dataTransfer.setData("text", obj.id);
		obj.startX = ev.pageX - obj.offsetLeft, obj.startY = ev.pageY - obj.offsetTop;
		obj.classList.contains("clone") ? ev.effectAllowed = "move" : ev.effectAllowed = "copy";
		}
	dragend_handler(ev) {
		console.log('dragend_handler');
		}
	editor_dragover(evt){
		console.log('editor_dragover ');
		/*
		console.log('	offset('+evt.offsetX+','+evt.offsetY+')');
		console.log('	screen('+evt.screenX+','+evt.screenY+')');
		console.log('	page  ('+evt.pageX+','+evt.pageY+')');
		console.log('	layer ('+evt.layerX+','+evt.layerY+')');
		console.log('	client('+evt.clientX+','+evt.clientY+')');
		console.log('	xy    ('+evt.x+','+evt.y+')');
		*/
		evt.preventDefault();
		evt.currentTarget.style.background = "lightblue";
		}
	editor_drophandler(evt){	
		console.log('editor_drophandler');
		/*
		console.log('	offset('+evt.offsetX+','+evt.offsetY+')');
		console.log('	screen('+evt.screenX+','+evt.screenY+')');
		console.log('	page ('+evt.pageX+','+evt.pageY+')');
		console.log('	layer('+evt.layerX+','+evt.layerY+')');
		console.log('	client('+evt.clientX+','+evt.clientY+')');
		console.log('	xy    ('+evt.x+','+evt.y+')');
		*/
		evt.currentTarget.style.background = "white";
		evt.preventDefault();
		var dt = evt.dataTransfer;
		
		if (dt.files.length > 0) {	//	
			var fileReader = new FileReader();
			// fileReader.onload = this.FileOnLoad.bind(this);
			fileReader.onload = this._FileOnLoad.bind(this);
			var file = dt.files[0]; //negeer overige files, todo juist niet, zo kun je sets instrumenten bij elkaar laden
			fileReader.readAsText(file);	//todo: valideer file
			return;
			}
		var id = dt.getData("text");
		var node = document.getElementById(id);
		if (undefined==node)	return;
		console.log('editor_drophandler: '+node.classList);

		if (node.classList.contains("clone")) {
			node.style.left = evt.clientX - node.startX + "px";
			node.style.top = evt.clientY - node.startY + "px";
			console.log('	Dropped "'+node.className+'" at ('+node.style.left+','+node.style.top+')');
			}
		else {
			var clone = node.cloneNode(true);
			clone.classList.add("clone");
			clone.id = "cd_instrument_" + newId().toString();
			// clone.style.left	= (evt.clientX - evt.currentTarget.offsetLeft - node.startX).toString() + "px";
			clone.style.left	= (evt.clientX - node.startX).toString() + "px";
			// clone.style.top	= (evt.clientY - evt.currentTarget.offsetTop	- node.startY).toString() + "px";
			clone.style.top	= (evt.clientY - node.startY - this.cd_toolbar.scrollTop).toString() + "px";
			clone.querySelector(".resizer").style.display = "block";
			clone.querySelector(".extwin").style.display = "block";
			// node.startX = evt.clientX;
			// node.startY = evt.clientY;
			this.applyBehaviours(clone,undefined,true);
			if ('<'!=evt.target.innerHTML[0])	//	clean up help text
				evt.target.innerHTML = "";
			evt.target.appendChild(clone);
			// console.log('	Dropped at     ('+clone.style.left+','+clone.style.top+')');
			}
		}
	hOpen(evt){
		this.cd_menucontainer.style.display = 'none';
		var opener = i.input('',{type:'file',change:this.handleFileSelect.bind(this)});
		opener.click();
		}
	handleFileSelect(evt) {
		if (!evt.target.files) return;
		var f = evt.target.files[0];
		var fileReader = new FileReader();
		fileReader.onload = this._FileOnLoad.bind(this);
		fileReader.readAsText(f);
		}
	_FileOnLoad(evt){	//	todo ook in drophandler
		var oInstruments = JSON.parse(evt.target.result);
		this.FileOnLoad(oInstruments);	
		}
	FileOnLoad(oInstruments){	//	todo must be exposed
		this.cd_editor.innerHTML = "";	//	todo misschien kun je huidige instrumenten mixen met te laden instrumenten
		/**/
		// todo resolv dependencies
		//	todo create an array of instruments en dan met replace parent vullen
		for (var oInstrument in oInstruments) {
			var elm = oInstruments[oInstrument];
			var parent = document.createElement("div"); //tijdelijke container, weet nog niet hoe anders
			parent.innerHTML = elm.html;
			this.cd_editor.appendChild(parent);
			var instrument = parent.firstChild;
			this.cd_editor.replaceChild(instrument, parent);
			this.applyBehaviours(instrument,elm.exposedParms,true);
			instrument.startX = elm.left;
			instrument.startY = elm.top;
			}
		}
	hSave(evt){
		this.cd_menucontainer.style.display = 'none';
		var instruments = this.cd_editor.querySelectorAll(".clone");
		var oInstruments = {};
		for (var ii=0; ii<instruments.length; ii++) {
			var oInstrument = {};
			oInstrument.id = instruments[ii].id;
			oInstrument.height = instruments[ii].offsetHeight;
			oInstrument.width = instruments[ii].offsetWidth;
			oInstrument.top = instruments[ii].offsetTop;
			oInstrument.left = instruments[ii].offsetLeft;
			oInstrument.html = instruments[ii].outerHTML;
			oInstrument.exposedParms = instruments[ii].externObj.getExposedParameters();
			oInstruments[ii] = oInstrument;
			}
		var blob = new Blob([JSON.stringify(oInstruments)], { type: "text/plain" });
		// var b = new Blob('var conning='+[JSON.stringify(oInstruments)]+';', { type: "text/plain" });
		var file_name = "myConning.json";	//	todo let user choose name
		if (false && window.navigator && window.navigator.msSaveOrOpenBlob) { // for IE
			window.navigator.msSaveOrOpenBlob(blob, file_name);
			} 
		else {
			var a = document.createElement("a");	//	todo met i.a
			a.style = "display: none";
			a.href = URL.createObjectURL(blob);
			a.target = "_blank";
			a.download = file_name;
			a.click();
			}
		}
	createToolbarInstrument(file,caption,container,xtraClass,flyover){
		// container.setAttribute('cd_replaceble', true);
		if (undefined==container){
			console.error('Instrument in file: "'+file+'" is of unknown class. Probably lacks the function call "addInstrumentToSettings()" after its class definition.')
			return;
			}
		if (undefined!=xtraClass){
			container.setAttribute('cd_itype', xtraClass);
			xtraClass = ['instrument',xtraClass].join(' ');
			}
		else	
			xtraClass = 'instrument';
		
		var rc = i.div(
			[i.div(
				[i.a('&#9776;',	{href:'#',class:'properties',title:'edit properties'})	//	properties
				,i.span(caption,	{style:'font-size:12px;'})		//	text beschrijving instrument
				,i.a('&#9634;',	{href:'#',class:'extwin',title:'run in separate browser window'})
				,i.a('&#10006;',	{href:'#',class:'close',title:'discard this component'})		//	close kruisje
				],{class:'instrumentbar',draggable:'true'})
			,i.div(
				[container
				// ,i.span('&#9727',{class:"resize"})	.resizer
				// ,i.span('&#9634',{class:"extwin",	title:'run in separate browser window',style:'font-size:200%;height:50px; width:50px; position:absolute; bottom:-15px;  left:-6px; 	cursor:crosshair;		color:silver; z-index:100; display:block;'})
				,i.span('&#9727',{class:"resizer",	title:'resize instrument',					style:'font-size:200%;height:50px; width:50px; position:absolute; bottom:-12px; right:-25px;	cursor:nwse-resize;	color:silver; z-index:100; display:block;'})
				],{class:"instrumentbody"})
			],
			{id:'cd_instr_'+newId().toString()
			,caption:caption
			,class:xtraClass
			,draggable:'true'	//	in toolbar draggable, in editor only window is draggable
			,title:flyover
			});
		
		this.applyBehaviours(rc);
		return rc;
		}
	}	//	CTabConningDesigner

// supply counter	
var newId = (function () {
	var counter = 0;
	return function () { 
		return counter += 1; 
		};
	})();
