//	iVstepMediumControlBox.js
//	provides readout and control of MCB

'use strict';

class CInstrument_mcb extends CInstrument {
	constructor(obj,exposedParms,active,idx){
		super(idx,obj);
		if (undefined!=exposedParms)
			this.exposedParms = exposedParms;
		else
			this.exposedParms = //	todo deze moeten door de conning designer kunnen worden gezet, nier overrulen hier dan
				{active:				0
				,noThruster:		0
				,arduino:			-1
				,rudder_PRC:		0
				,globalAlpha:		0.9
				,aziScale:			0.009		// 0.010 would be use full circle of azi picture, todo misschien afhankelijk van schaal
				,colorBackground:	'rgba(255,255,255,0.5)'
				,colorRings:		'rgba(  0,  0,255,0.6)'
				,colorCross:		'rgba(  0,  0,  0,0.6)'
				,colorCommand:		'rgba(  0,  0,255,0.6)'
				,colorRespons:		'rgba(255,  0,  0,0.6)'
					
				};	
		this.canvas = obj.querySelector("canvas");
		this.cv = this.canvas.getContext("2d")
		this.cv.globalAlpha = this.exposedParms.globalAlpha;
		this.arduino = this.exposedParms.arduino;
		this.bAziShip = false;
		this.deg2rad = Math.PI/180.0;
		this.azi_readout = 'azi_readout';
		this.hasFocus = true;
		/*
		this.imgPropellor = new Image();
		this.imgPropellor.this = this;
		this.imgPropellor.src = './img/propellor_azi_push.png';
		this.imgPropellor.onload = function(evt) {}	//	image loads slow
		*/
		this.thrusterDataC = {azi:0,rpm:0};
		this.thrusterDataD = {azi:0,rpm:0};
		if (0!=this.exposedParms.active){
			this.canvas.addEventListener("mousedown",this.mousedown.bind(this),false);
			this.canvas.addEventListener("mouseup",this.mouseup.bind(this),false);
			this.canvas.addEventListener("mousemove",this.mousemove.bind(this),false)
			super.cbRxNmea('TRC',function(rx,that){	//	install callback handler upon rx of TRC
				if (undefined==rx.data)	return;
				if (rx.data.not==that.exposedParms.noThruster)
					that.trc_rxd(rx);
				},this);
			super.cbRxNmea('TRD',function(rx,that){	//	install callback handler upon rx of TRD
				if (undefined==rx.data)	return;
				if (rx.data.not==that.exposedParms.noThruster)
					that.trd_rxd(rx);
				},this);
			// if (this.exposedParms.arduino!=-1){
			this.arduino = -1;	//	for subscription
			setInterval(this.updateHWInput.bind(this), 1000 / this.fps);
			this.draw();
			}
		this.fps = 1;
		}
	updateHWInput(){
		if (this.arduino!=this.exposedParms.arduino){
			if (-1<this.exposedParms.arduino){
				console.log('arduino old-new='+this.arduino+'-'+this.exposedParms.arduino);
				this.arduino=this.exposedParms.arduino;
				if (undefined!=wscontrol){
					//	todo check with wscontrol if arduino and axis are available
					console.log('subscribe_mediumControlbox('+this.arduino+','+this.arduino+')');
					wscontrol.subscribe_mediumControlbox(this.arduino,this.fnArduino.bind(this));
					}
				}
			}
		}
	fnArduino(data){
		console.log('iMCB fnArduino(data) azi='+JSON.stringify(data));
		/*
		nmeadefs['TRC'][0].rxcb.myforEach(function(cb,el,ar,p){cb.fun(p,cb.payload)},{data: 
			{not: this.exposedParms.noThruster
			, rpm: data.eot
			, pitch: data.eot
			, azi: data.azi
			}});
		*/	
		}
	trc_rxd(rx){	//	thruster command
		this.bAziShip = true;
		var data=rx.data;
		if (this.hasFocus) {
			//console.log('thruster data received; thruster: '+data.not+', but no focus');
			this.thrusterDataC.rpm = data.rpm;
			this.thrusterDataC.rpmi = data.rpmi;
			this.thrusterDataC.pitch = data.pitch;
			this.thrusterDataC.ptm = data.ptm;
			this.thrusterDataC.azi = data.azi;
			this.thrusterDataC.oloc = data.oloc;
			this.thrusterDataC.stat = data.stat;
			this.draw();
			}
		}
	trd_rxd(rx){	//	thruster response
		var data=rx.data;
		if (this.hasFocus) {
			this.thrusterDataD.rpm = data.rpm;
			this.thrusterDataD.rpmi = data.rpmi;
			this.thrusterDataD.pitch = data.pitch;
			this.thrusterDataD.ptm = data.ptm;
			this.thrusterDataD.azi = data.azi;
			//this.thrusterDataD.oloc = data.oloc;
			//this.thrusterDataD.stat = data.stat;
			// }
			this.draw();
			}
		}
	rudder_prc(azi,rpm){
		rudder(azi);
		}
	sendTRC(azi,rpm,color){
		var not = this.exposedParms.noThruster
		tx.TRC.not = not;
		tx.TRC.rpm = rpm;
		tx.TRC.azi = azi;
		tx.TRC.rpmi =	this.thrusterDataC.rpmi;
		tx.TRC.pitch = this.thrusterDataC.pitch;
		tx.TRC.ptm =	this.thrusterDataC.ptm;
		tx.TRC.oloc =	this.thrusterDataC.oloc;
		tx.TRC.stat =	this.thrusterDataC.stat;
		if (this.exposedParms.rudder_PRC)
			return this.rudder_prc(azi,rpm);
		else
			ws_talk_listen.sendNMEA(ship0.remote_protocol+'://'+ship0.remote_ip+':'+settings.talk_port,buildNMEA('TRC',1,1));
		this.showAziGraphic(color,{azi:azi,rpm:rpm});	//	command
		}
	mousedown(evt){
		//var azi = this.getRpmAzi(evt);
		}
	mouseup(evt){
		// console.log('mouseup');
		var azi = this.getRpmAzi(evt);
		this.sendTRC(azi.azi,azi.rpm,this.exposedParms.colorCommand);
		}
	mousemove(evt){
		this.predict = this.getRpmAzi(evt);
		}
	getRpmAzi(evt){
		// convert mouse coordinates in canvas to rpm and azi-angle
		var rect = evt.currentTarget.getBoundingClientRect();
		var pp = {x: evt.clientX - rect.left - evt.currentTarget.width/2, y: -1*(evt.clientY - rect.top - evt.currentTarget.height/2)};
		var rpm = Math.sqrt(Math.pow(pp.x,2) + Math.pow(pp.y,2));
		rpm /= evt.currentTarget.width/2;
		rpm /= this.exposedParms.aziScale;
		rpm = rpm.toFixed(1);

		var azi = Math.atan(pp.x/pp.y) / this.deg2rad;
		if (pp.x<0.0)	if (azi>0.0)	azi -= 180.0;
		if (pp.x>0.0)	if (azi<0.0)	azi += 180.0;
		azi = azi.toFixed(1);
		return {rpm: rpm, azi: azi};
		}
	draw(){
		if (!this.hasFocus) return;
		var not = this.exposedParms.noThruster
		this.cv.clearRect(0,0,this.canvas.width,this.canvas.height);
		this.cv.fillStyle = this.exposedParms.colorBackground;
		this.cv.fillRect(0,0,this.canvas.width,this.canvas.height);
		
		// cross
		this.cv.beginPath();
		this.cv.strokeStyle = this.exposedParms.colorCross;
		this.cv.moveTo(this.canvas.width/2,this.canvas.height);			this.cv.lineTo(this.canvas.width/2,0);
		this.cv.moveTo(this.canvas.width,this.canvas.height/2);			this.cv.lineTo(0,this.canvas.height/2);
		this.cv.stroke();

		//	rings
		var rings=[0.5,1.0];
		var smaller = 0.99;
		for (var ring in rings) {
			this.cv.beginPath();
			this.cv.strokeStyle = this.exposedParms.colorRings;
			this.cv.arc(this.canvas.width/2, this.canvas.height/2, smaller*this.canvas.width/2 * rings[ring], 0, 2 * Math.PI);
			this.cv.stroke();
			}
			
		// aziposition	
		if (this.exposedParms.active){
			this.showAziGraphic(this.exposedParms.colorCommand,	this.thrusterDataC);	//	command
			this.showAziGraphic(this.exposedParms.colorRespons,	this.thrusterDataD);	//	response
			}
		}
	showAziGraphic(color,thrusterData){
		var cv = this.cv;
		var ballsize = 10;
		if (undefined!=thrusterData){
			cv.beginPath();
			cv.strokeStyle = cv.fillStyle = color;
			cv.arc
			( this.canvas.width /2*(1+this.exposedParms.aziScale*thrusterData.rpm * Math.sin(thrusterData.azi * this.deg2rad))
			, this.canvas.height/2*(1-this.exposedParms.aziScale*thrusterData.rpm * Math.cos(thrusterData.azi * this.deg2rad))
			, ballsize, 0, 2 * Math.PI);
			cv.fill();
			cv.stroke();
			}
		//this.cv.drawImage(this.imgPropellor,this.canvas.width/2,this.canvas.height/2);
		}
	};
