	//	tab_trainer.js
'use strict';

class CTabTeacher extends C0isimTab {
	constructor(){
		super();
		settings.ws_addr=zeroisim.gup('ws_addr','ws://bert-toshsat:12120');
		this.world=zeroisim.gup('world','abc');
		this.loginpanel = i.table(
			[i.tr(i.td([i.span('Username: '),i.input('',{type:'text',id:'theUI',value:'ui'})]))	//	todo autofocus
			,i.tr(i.td([i.span('Password: '),i.input('',{type:'password',id:'thePW',value:'invalid'})]))
			,i.tr(i.td(i.button('login',{id:'btn_login',click:this.login.bind(this)})))
			]);
		this.teacherMenu = i.table(
			[i.tr(i.td('teachermenu'))
			,i.tr(i.td(i.button('set world',{id:'btn_setworld',click:this.klik_setworld.bind(this)})))
			,i.tr(i.td(i.button('my account',{id:'btn_myaccount',click:this.klik_myaccount.bind(this)})))
			,i.tr(i.td(i.button('manage exercises',{id:'btn_manageexercises',click:this.klik_manageexercises.bind(this)})))
			,i.tr(i.td(i.button('manage trainees',{id:'btn_managetrainees',click:this.klik_managetrainees.bind(this)})))
			// ,i.tr(i.td(i.button('edit environment radar aspect',{id:'btn_editenvradar',click:this.klik_editenvradar.bind(this)}))))
			,i.tr(i.td(i.button('Radar Aspect Edit',{click:this.envedit.bind(this)})))	//ui-id-13
			,i.tr(i.td(i.button('Conning design',{click:this.envedit.bind(this)})))	//ui-id-13
			
			,i.tr(i.td(i.slider('',{id:'btn_myslider',input:this.hslider.bind(this),min:0,max:100,step:5,value:50,style:'width: 270px;'})))
			]);
		this.exerciseMenu = i.table(
			[i.tr([i.td('excercise-menu'),	i.td(i.button('back',{click:this.backToTeacherMenu.bind(this)}))])
			,i.tr(i.td(i.button('unPause',{click:this.backToTeacherMenu.bind(this)})))
			// ,i.tr([i.td(i.span('latitude')),		i.td(i.input('',{type:'text',id:'ex_lat',change:this.chlat.bind(this)}))])
			// ,i.tr([i.td(i.span('longitude')),	i.td(i.input('',{type:'text',id:'ex_lon',change:this.chlon.bind(this)}))])
			]);
		this.setWorldMenu = i.table(
			[i.tr([i.td('set world-menu'),		i.td(i.button('back',{click:this.backToTeacherMenu.bind(this)}))])
			,i.tr([i.td(i.span('world')),			i.td(i.input('',{value:this.world,type:'text',id:'ex_world',change:this.chworld.bind(this)}))	])
			,i.tr([i.td(i.span('latitude')),		i.td(i.input('',{value: '52',type:'text',id:'ex_lat',change:this.chlat.bind(this)}))])
			,i.tr([i.td(i.span('longitude')),	i.td(i.input('',{value:  '4',type:'text',id:'ex_lon',change:this.chlon.bind(this)}))])
			]);
		this.radio = {onclick:"document.querySelector(\'#posplot_mouseaction\').attributes.posplot_mode.value=this.id;",name:"posplot_trainer_radio"};
		this.pptab_targets = i.table(
			[i.tr(i.td(i.radio(	'define route',	zeroisim.merge_objects(this.radio,{id:"div_posplot_mode_route"})),{colspan:3}))
			,i.tr(i.td(i.div(		'left click in map to add waypoints',{id:"de_route"}),{colspan:3}),{disabled:null})
			,i.tr(
				[i.td(i.span('load:'))
				,i.td(i.button('from Server',
					{click:this.listRoutesTX.bind(this)
					,title:'Load route from server using your credentials.'
					// ,disabled:null
					,id:'ppllload'
					}))
				,i.td(i.button('from Disk',
					{click:this.listRoutesLocalTX.bind(this)
					,title:'Load route from local storage device.'
					// ,disabled:null
					,id:'ppllrloadlocal'
					}))
				])
			,i.tr(i.td(i.radio(	'define target',	zeroisim.merge_objects(this.radio,{id:'div_posplot_mode_target'})),{colspan:3}))
			,i.tr(i.td(i.div(		'left click in map to add an object',{id:'de_target'}),{colspan:3}))
			,i.tr(i.td(i.radio(	'radar reflectiveness',	zeroisim.merge_objects(this.radio,{id:'div_posplot_mode_radarrefl'})),{colspan:3}))
			,i.tr(i.td(i.div(		'here will reflectiveness tools go',{id:'de_radarrefl'}),{colspan:3}))
			]); 
		}
	onload(evt){
		document.getElementById(this._tabdivname).appendChild(this.loginpanel);
		}
	hslider(evt){
		console.log('tab_teacher.js slider value: '+evt.target.value);
		}
	envedit(evt){
		document.getElementById('ui-id-13').click();
		}
	login(evt){
		//	todo nicer would be an encrypted transmission of credentials
		var ui=document.getElementById('theUI').value;
		var pw=document.getElementById('thePW').value;
		console.log('tab_teacher.js CTabTeacher login ui:pw="'+ui+'":"'+pw+'"');
		this.wsTeacher(settings.ws_addr,ui,pw);
		}
	wsTeacher(ws_addr,ui,pw){
		console.log('tab_teacher.js CTabTeacher wsTeacher...');
		// open connection to socketserver and he will give you the nmea coming from settings.listen_addr
		// this.ws_teacher = new wsconnect(ws_addr,'0isimTeacher'
		this.ws_teacher = new wsconnect(ws_addr,'nmea-bridge'
			,function(ws){	//	cbconnect
				// console.log('tab_teacher.js WebSocket protocol="0isimTeacher" connected to: '+ ws_addr);
				console.log('tab_teacher.js WebSocket protocol="nmea-bridge" connected to: '+ ws_addr);
				tab_settings.showConnected(true);
				ws.zend(JSON.stringify(
					{action:'login'
					,ui:			ui
					,pw:			pw
					}));
				}
			,function(ws,data,that){	//	cbdata
				// console.log('tab_teacher.js WebSocket protocol="nmea-bridge" data rx: '+ data);
				data = JSON.parse(data);
				if (undefined!=data.resp){
				switch (data.resp){
					case 'login':
						if (undefined!=data.err && 0===parseInt(data.err)){
							that.token = data.token;
							zeroisim.replaceNode(document.getElementById(that._tabdivname),that.teacherMenu);
							if (undefined!=posplotobj){
								//	enhance the posplot tab a bit
								zeroisim.replaceNode(document.getElementById('ppll_tab2'),that.pptab_targets);
								posplotobj.deTrainer = that;	//	dit is euvel todo
								that.chworld({target:{value:that.world}});	// set world to server
								
								// install handler
								cbRxNmea('MC0',function(rxd,that){	//	when mesh arrives, load it
									if (mc0.createObj == rxd.data.req){
										that.createObject(rxd.data.obj);
										}
									},that);
								cbRxNmea('MC0',function(rxd,that){	//	please propose on an object to be created. TODO untested
									if ((mc0.giveObj) == rxd.data.req){
										zeroisim.busTX('MC0',{req:mc0.giveObj+mc0.response,de_object:
											{world:'abc'	//	todo
											,hdg:0
											// ,position:{latdeg:rxd.data.position.latdeg,londeg:rxd.data.position.londeg}
											,position:rxd.data.position
											,stw:3.4
											,navStatus:1
											,name:'Nordic Nora'
											,mmsi:Math.floor((Math.random() * 1000000000) + 1)
											,callsign:'PAV'+Math.floor((Math.random() * 1000) + 1)
											,IMO:Math.floor((Math.random() * 10000000) + 1)
											,shipType:73
											,bow:11,stern:22,port:33,starboard:44
											,draught:14.6
											,posSystem:2
											,ETA:'11161254'
											,destination:'New York'
											// },position:rxd.data.de_object.position});
											}});
										}	
									},that);	
								}
							}
						else {	
							setValue(that._tabdivname,'login was not successful' + h.br + that.loginpanel);
							that.onload();
							}
						break;
						
					case 'addRoute':	
						console.log(data.msg);
						break;

					case 'listRoutes':	
						that.listRoutesRX(data,'de_route');
						break;
						
					case 'loadRoute':	
						that.loadRouteRX(data);
						break;
						}
					}
				}
			,function(ws,that){	//	cbclose
				// console.info('WebSocket protocol="0isimTeacher" disconnected');
				console.info('WebSocket protocol="nmea-bridge" disconnected');
				// setValue('div_trainertab','login was not successful' + h.br + that.loginpanel);
				}
			,this	
			);
		}
	replaceHTML(rpl){
		zeroisim.replaceNode(document.getElementById(this._tabdivname),rpl);
		// var parentNode = document.getElementById(this._tabdivname);
		// while (parentNode.firstChild) {	// remove old html
			// parentNode.removeChild(parentNode.firstChild);
			// }
		// parentNode.appendChild(rpl);	//	set new html
		}
	backToTeacherMenu(evt){
		// this.replaceHTML(this.teacherMenu);
		zeroisim.replaceNode(document.getElementById(this._tabdivname),this.teacherMenu);
		}
	klik_setworld(evt){	
		console.log('tab_teacher.js klik_setworld token='+this.token);
		// this.replaceHTML(this.setWorldMenu);
		zeroisim.replaceNode(document.getElementById(this._tabdivname),this.setWorldMenu);
		}
	klik_managetrainees(evt){
		console.log('tab_teacher.js klik_managetrainees token='+this.token)
		}
	klik_manageexercises(evt){
		console.log('tab_teacher.js klik_manageexercises token='+this.token);
		// this.replaceHTML(this.exerciseMenu);
		zeroisim.replaceNode(document.getElementById(this._tabdivname),this.exerciseMenu);
		}
	klik_myaccount(evt){
		console.log('tab_teacher.js klik_myaccount token='+this.token)
		}
	klik_editenvradar(evt){
		console.log('tab_teacher.js klik_editenvradar token='+this.token)
		}
	chworld(evt){
		console.log('tab_teacher.js chworld');
		this.ws_teacher.zend(JSON.stringify({world:evt.target.value}));
		}
	chlat(evt){
		ship0.latlon.latdeg = evt.target.value;
		}
	chlon(evt){
		ship0.latlon.londeg = evt.target.value;
		}
		
	//todo obsolete, should be done by route.js, have yet to find solution for the token-parameter
	saveRoute(evt){
		//	NB this is CRoute, CRoute.deTrainer is the CTabTeacher
		console.log('tab_teacher.js saveRoute');
		var upload = JSON.stringify(	//	upload this route to this trainer
			{action:'addRoute'
			,route:this.asJSON()
			,token:this.deTrainer.token
			});
		this.deTrainer.ws_teacher.zend(upload);
		}
	saveRouteLocal(evt){
		console.error('dit moet door route.js gedaan worden');
		//	NB this is CRoute, CRoute.deTrainer is the CTabTeacher
		}
	listRoutesTX(evt){	//	send a listroutes request to server
		console.log('tab_teacher.js listRoutesTX()');
		var upload = JSON.stringify(	//	upload this route to this trainer
			{action:'listRoutes'
			,token:this.token
			});
		this.ws_teacher.zend(upload);
		}
	listRoutesLocalTX(evt){
		console.log('tab_teacher.js listRoutesLocalTX()');
		}
	listRoutesRX(data){	//	a list of routes is received from server
		console.log('tab_teacher.js '+data.routes);
		var routes = JSON.parse(data.routes);
		var nroutes = [];
		routes.myforEach(function(route,idx,rts,that){	//	verrijk met knopje
			nroutes.push(
				{knop:i.button('load',{click:that.loadRouteTX.bind(that),route:JSON.stringify(route)})
				,name:route.name
				,created:route.created
				});
			},this)
		zeroisim.setiValue('de_route', zeroisim.iasTable(nroutes));
		}		
	loadRouteTX(evt){
		var route = evt.currentTarget.getAttribute('route');
		if (undefined!=route){
			route = JSON.parse(route);
			var upload = JSON.stringify(	//	upload this request for a route to server
				{action:'loadRoute'
				,token:this.token
				,name:route.name
				,created:route.created
				});
			this.ws_teacher.zend(upload);
			}
		}
	loadRouteRX(data){
		console.log('tab_teacher.js loadRouteRX: '+data);
		var routedata = JSON.parse(data.routedata);
		this.route = new CRoute(this,data.routename);
		routedata.myforEach(function(data,idx,rts,that){
			that.route.addWaypoint(new zeroisim.LatLon({latrad:data.lat,lonrad:data.lon}));
			},this);
		// $toclient->routecreated = $parms->created;
		this.route.asTable('de_route');
		// todo pass route to leaflet and render on canvaslayer
		}
	createObject(objectToCreate){
		console.log('tab_teacher.js createObject');
		this.ws_teacher.zend(JSON.stringify({createTargetShipClient:objectToCreate}));
		// this.ws_teacher.zend(JSON.stringify({createTargetShipWorld:target}));	
		}
	}	//	CTabTeacher

	
	/*
	dit hier is vast niet de bedoeling
				cbRxNmea('MC0',function(rxd,that){	//	please propose on an object to be created
				if ((mc0.giveObj) == rxd.data.req){
					zeroisim.busTX('MC0',{req:mc0.giveObj+mc0.response,de_object:
						{world:'abc'	//	todo
						,hdg:0
						,position:{latdeg:rxd.data.position.latdeg,londeg:rxd.data.position.londeg}
						,stw:3.4
						,navStatus:1
						,name:'Nordic Nora'
						,mmsi:Math.floor((Math.random() * 1000000000) + 1)
						,callsign:'PAV'+Math.floor((Math.random() * 1000) + 1)
						,IMO:Math.floor((Math.random() * 10000000) + 1)
						,shipType:73
						,bow:11,stern:22,port:33,starboard:44
						,draught:14.6
						,posSystem:2
						,ETA:'11161254'
						,destination:'New York'
						},position:rxd.data.position});
					}	
				},this);	
			*/	
