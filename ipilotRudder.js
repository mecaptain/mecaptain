// ipilotRudder.js
'use strict';

// todo maak een optie dat het knopje na een commando blijf blinken totdat de engine het benodigde rpm of pitch bereikt

class CInstrument_pilotRudder extends CInstrument_pilotHorizontal {
	constructor(obj,exposedParms,active,idx){
		super(obj,exposedParms,active,idx);
		if (undefined!=exposedParms)
			this.exposedParms = exposedParms;
		else
			this.exposedParms =
				{nots:'[0]'	//	number of thruster; value or array if coupled, in JSON format, may be array
				,oploc:	'B'	//	operation location in NMEA
				,colorBackground:	'rgba(255,255,255,0.0)'	//	gray
				,colorModeKnobOff:'rgba(224,224,224)'
				,colorModeKnobOn:	'rgba(0,255,0)'
				,tableBorder:		'3px solid rgba(255,255,255,0.4)' // 3px solid #FFF;
				,stops:	//	todo dit misschien in JSON formaat
					[{txt:'hard<br/>PS',value:-35}
					,{txt:'PS<br/>25',value:-25}
					,{txt:'PS<br/>20',value:-20}
					,{txt:'PS<br/>15',value:-15}
					,{txt:'PS<br/>10',value:-10}
					,{txt:'PS<br/>5',value:-5}
					,{txt:'midships',value:0}
					,{txt:'SB<br>5',value:5}
					,{txt:'SB<br>10',value:10}
					,{txt:'SB<br>15',value:15}
					,{txt:'SB<br>20',value:20}
					,{txt:'SB<br>25',value:25}
					,{txt:'hard<br>SB',value:35}
					]
				};
		}
	resize(w,h){		}
	draw(){
		super.draw();
		if (this.active!=true){
			zeroisim.replaceNode(this.div,i.span(i.center('RUDDER')));
			return;
			}
		}
	txValue(value){
		var nots = JSON.parse(this.exposedParms.nots);
		if (!Array.isArray(nots))	nots = [nots];
		var ror = 
			{srud:	value				//	'Starboard (or single) rudder order in degrees, "-" = port:0.0]
			,svalid:	'A'				//	'Starboard rudder status, A=Data valid, V=Data invalid:'A']
			,prud:	value				//	'Port rudder order in degrees , "-" = port:0.0]
			,pvalid:	'A'				//	'Port rudder status, A=Data valid, V=Data invalid:'A']
			,src:		'P'				//	Command source location 'B|P|S|C|E|W'
			,from:	'ipilotrudder'
			,talker: nmeadefs.ROR[0].ti
			};
		super.busTX('ROR',ror);
		}
	}

CInstrument.addInstrumentToSettings
	('./ipilotRudder.js'
	,'pilotRudder'
	,CInstrument.domTypeDiv
	,CInstrument_pilotRudder
	,'rudder'
	// todo default width and height
	);
