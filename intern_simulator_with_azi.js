//	intern_simulator.js
//	uses posplot.js for LatLon
//	simulates ship movement
//	also provides a loopback 'websocket'
//
//	todo maak een optie om echte NMEA data te versturen naar de loopback. Zo kun je dan de huidige scripts draaaien en de parser testen
//	todo zorg dat je ook een bepaalde set sentences naar de echte websocket kunt sturen zodat je multiship kunt enof OpenCPN kunt gebruiken.
//	bijvoorbeel VDO zodat je ais plot kunt maken en multiuser kunt maken
//
simulator_tab_page = function(){
	if (undefined==document.bert.simtab){
		document.bert.simtab = new Object();
		}
	document.bert.simtab.that = this;		//	todo misschien gaat dit fout als je meerdere instances van dit object gaat maken
	window.addEventListener("load",
		function(evt){	//	nu pas kun je bij de nmeadefs,	todo voeg sentences to aan simulator outputs
			var opts = [''];
			for (nmeadef in nmeadefs){
				opts.push(nmeadef);
				}
			setValue('sim_outputs',
				h_select('select_sim_outputs',opts)
				);
			}
		, false);

	return tag('table'
		,	tr(td('intern simulator')+td(onoffswitch('is_engage','start_intern_simulator()')))
		+	tr(td(tag('button','request session','onclick=is.requestSession()'))+td(tag('div','<session>','id="intsim_session"')))
		+	tr
			(td(tag('div','ship','id="intern_simulator_ship"'))
			+td(tag('fieldset',tag('legend','output NMEA','id="intern_simulator_onmea"')+tag('div','hier komt de lijst met nmeaoutputs','id="sim_outputs"')))
			)
		,	'style="width:100%; white-space:nowrap;"'
		);
	}

wsloopback = function(ip,prot,cbconnect,cbdata,cbclose,that,divdebug){
	var mythat = that;
	var mythis = this;
	var counter = 0;
	this.counter = 0;
	/*
	if (divdebug!=undefined){
		var dbgdiv = document.getElementById('debug');
		dbgdiv.innerHTML = 'WS='+ip;
		}
	*/
	this.open = function(){
		this.onopen();
		}
	this.onopen = function(){
		console.log('Websocket CONNECTED.');
		cbconnect(mythis,mythat);
		};
	this.onmessage = function(evt){
		// console.log('wsloopback rx: ' + evt.data );
		if (cbdata!=undefined){
			cbdata(mythis,evt.data,mythat);
			}
		};
	this.onclose = function(mythis,mythat){					// websocket is closed.
		console.log('websocket connection is closed...');
		if (cbclose!=undefined){
			cbclose(mythis,mythat);
			}
		};
	this.onerror = function(){};	// todo implementeren
	this.zend = function(str){
		//	misschien niet direct een call naar onmessage doen maar met een settimeout()
		// console.log('wsloopback zend()');
		if (this.counter++) {	//	first tx is listening address, we do not use this
			var data = JSON.parse(str);
			data.msg = data.message;
			data.fromip = 'ip_loopback';
			data.fromport = 'port_loopback';
			data.toip = 'ip_loopback';
			data.toport = 'port_loopback';
			this.onmessage({data:data});
			}
		}
	}

function is_onfocus(){	if (undefined!=is)	is.simulator_tab_onfocus();}
function is_onblur(){	if (undefined!=is)	is.simulator_tab_onblur();}

function start_intern_simulator(){
	//	todo als hij al draait moet je de simulator ook stoppen. Misschien de setInterval() stoppen.
	//	set ws to loopback, install evthndlr on ROR, TRC & start simulator
	//	This ws is a global variable, ws connect to real ship or other simulator, or, for now, to internal simulator
	ws = new wsloopback(settings.ws_addr,'nmea-talk-listen'
		,function(ws){
			if (log) log.info('WebSocket connected');
			var div_connected = document.getElementById('div_connected');
			div_connected.className = 'check_input_ok';
			ws.zend(JSON.stringify(
				{listen_addr:			settings.listen_addr
				}));
			count_tx++;
			}
		,function(ws,data,that){
			return nmea_sentence_received(data,that);
			}
		,function(/*ws*/){
			if (log) log.info('disconnected');
			var div_connected = document.getElementById('div_connected');
			div_connected.className = 'check_input_nok';
			}
		,{		grr: document.getElementById("grr")
			,	talkerids: talker_ids
			,	ship0: ship0
			}
		);
	is.init();	//	initialize ship
	ws.open();	//	start loopback socket server
	cbRxNmea('PRC',function(rxd){	//	install callback for prc
		if (undefined==rxd.data)	return;
		var rdval = parseFloat(rxd.data.rdval);	//
		var pdval = parseFloat(rxd.data.pdval);	//
	//	is.ship.cmdTelegraph = pdval;	//	eigenlijk waarschijnlijk this niet mogelijk hier
		is.update({cmdTelegraph: pdval,immediate_return: true});	//	kick the model, just setting telegraph is probably faster
		//	console.log('..................................................... telegraph: '+parseFloat(rxd.data.pdval));
		});
	cbRxNmea('ROR',function(rxd){	//	install callback for rudder order
		if (undefined==rxd.data)	return;
		var srud = parseFloat(rxd.data.srud);		//
		var prud = parseFloat(rxd.data.prud);		//
		is.update({cmdRudder: prud,immediate_return: true});	//	kick the model, just above lines is probably more performant
		//	console.log('..................................................... rudder: '+parseFloat(rxd.data.prud));
		});
	
	nmeadefs.VDMjson = [{title:'Autopilot External Controller'}		//	nautis specific
			,['SetPoint',floatingp,'value of set point autopilot parameter']
			,['AutoPilotMode',literal('[ROT|Compass]'),'value which represents an autopilot mode ( e.g: ROT , Compass )']
			,['Rudder',floatingp,'value of AutoPilot rudder parameter']
			,['Trim',floatingp,'value of autopilot trim parameter']
			,['Damping',floatingp,'value of damping range autopilot parameter']
			,['Sensitivity',floatingp,'value of autopilot parameter sensitivity']		
			];
	cbRxNmea('VDMjson',function(rxd){	//	install callback for VDMjson
		if (undefined==rxd.data)	return;
		add2aistable(rxd.data,false);	
		});
		
	//	start a timer to kick the model now and then.
	setInterval(function(){ is.update({}); }, 50);
	}

var positions =
	{'haven ouddorp':	{latdeg:51+47.6/60.0,londeg:3+56.6/60.0}
	,'grevelingen - vlieger':{latdeg:51+45.7/60.0,londeg:3+59.6/60.0}
	,'nulnul':{latdeg:-0.1/60.0,londeg:-0.1/60.0}							//	test equator and meridian passage
	,'vuurtoren hellevoetsluis':{latdeg:51.8196905,londeg:4.1278264}
	,'maasgeul in':{latdeg:51+59.32/60,londeg:3+24/60}
	,'Sunter':{latdeg:-1*(6+(8+15.1/60)/60),londeg:106+(52+32.4/60)/60}	//	sunter maritime academy
	};
var is = {	//	intern simulator object	start with simple ship propellor rudder
	ws_sim: undefined,	//	if undefined, simulator runs in server mode, simulator tells server the wereabouts of me and I am told wereabouts of other ships
	outputs:['VDS'],
	_count: 0,
	defaultShip: {
		// name:'Bliss',MMSI: 244750467, CallSign: 'PF4365'
		// name:'Zingwind',MMSI: 244750466, CallSign: 'PF4364'
		tcTelegraph:0.1	//	timeconstant, smaller is a faster response on commanded rpm
		,propulsion:'singleShaftFixedPitch'
		,tcRudder:0.1		//	timeconstant, smaller is a faster response on rudder order, might depend on how many steering pumps are running
		,cmdRudder:[0],		actualRudder:[0]
		,cmdTelegraph:[0],	actualTelegraph:[0]
		,maxRudder:35
		,rpm:[0], pitch: [25]
		,rpm2thrust:341800
		,mass: 100000000		//	kg
		,speed: 0.0				//	knots
		,factor_speedresistance: 120000.0	//
		,speed_tau: 20			//	bigger=faster reponding
		,rot: 0					//	degrees/minute
		,drot: 0					//	degrees/minute/second
		,rudder_tau: 1/1200		//
		,factor_ruddermoment: 4000.0	//
		,factor_turnresistance: 10000.0
		,autopilot_settings:{
		//	 Kp: 2.00,Ki: 0.01,Kd: 6.00	//	beetje veel overshoot
			 Kp: 0.90,Ki: 0.01,Kd: 10.20	//	beetje traag op koers
			}
		,course:83
		,latlon: new LatLon(positions[
			// 'haven ouddorp'
			// 'grevelingen - vlieger'
			// 'nulnul'
			// 'vuurtoren hellevoetsluis'
			// 'maasgeul in'
			'Sunter'
			]
			)
		,vdo_interval: 1030	//	sent every .. milliseconds a vdo1 to server
		,vdm_interval: 1010	//	request every .. milliseconds a vdm from server
		},
	aziShip: {
		// name:'Bliss',MMSI: 244750467, CallSign: 'PF4365'
		// name:'Zingwind',MMSI: 244750466, CallSign: 'PF4364'
		tcTelegraph:0.1	//	timeconstant, smaller is a faster response on commanded rpm
		,tcRudder:0.1		//	timeconstant, smaller is a faster response on rudder order, might depend on how many steering pumps are running
		,propulsion:'asd'
		,cmdRudder:[0,0],		actualRudder:[0,0]
		,cmdTelegraph:[0,0],	actualTelegraph:[0,0]
		,maxRudder:false
		,rpm:[0,,0], pitch: [25,25]
		,rpm2thrust:15000
		,mass: 10000000		//	kg
		,speed: 0.0				//	knots
		,factor_speedresistance: 120000.0	//
		,speed_tau: 20			//	bigger=faster reponding
		,rot: 0					//	degrees/minute
		,drot: 0					//	degrees/minute/second
		,rudder_tau: 1/1200		//
		,factor_ruddermoment: 4000.0	//	werkt dit met azi?
		,factor_turnresistance: 10000.0
		,autopilot_settings:{
		//	 Kp: 2.00,Ki: 0.01,Kd: 6.00	//	beetje veel overshoot
			 Kp: 0.90,Ki: 0.01,Kd: 10.20	//	beetje traag op koers
			}
		,course:83
		,latlon: new LatLon(positions[
			// 'haven ouddorp'
			// 'grevelingen - vlieger'
			// 'nulnul'
			// 'vuurtoren hellevoetsluis'
			'maasgeul in'
			]
			)
		,vdo_interval: 1030	//	sent every .. milliseconds a vdo1 to server
		,vdm_interval: 1010	//	request every .. milliseconds a vdm from server
		},
	simulator_tab_onfocus: function(){
		setValue('intern_simulator_ship',this.objAsTable(this.ship?this.ship:this.ship));	//	laat de eigenschappen van het schip zien
		//	todo dit in een editable grid en een save button
		},
	simulator_tab_onblur: function(){
		},
	init: function(){
		this.__id = 'intern_simulator';
		console.log(this.__id+'.init()');
		this.deg2rad = Math.PI / 180.0;
		var d = new Date();		var tim = d.getTime();		var dt = tim - this.lasttime;		this.lasttime = tim;
		this.lastWereaboutsSent = this.lastVdmRequest = tim;
		
		this.ship = this.defaultShip;
		// this.ship = this.aziShip;
		if (BrowserDetect.browser=='Chrome'){	//	todo dit is alleen voor testen
			this.ship.name='Bliss';this.ship.MMSI=244750467;	this.ship.CallSign='PF4365';
			}
		else{
			this.ship.name='Zingwind';this.ship.MMSI=244750466;this.ship.CallSign='PF4364';
			}
		
		//	call nmea RX functions
		nmeadefs['RSA'][0].rxcb.myforEach(function(cb,el,ar,p){cb.fun(p,cb.payload)},{data:{srud: this.ship.actualRudder, prud: this.ship.actualRudder}});
		nmeadefs['VTG'][0].rxcb.myforEach(function(cb,el,ar,p){cb.fun(p,cb.payload)},{data:{cogT: this.ship.course.toFixed(2), sogN: this.ship.speed}});
		nmeadefs['RMC'][0].rxcb.myforEach(function(cb,el,ar,p){cb.fun(p,cb.payload)},{data:{cog: this.ship.course.toFixed(2)}});
		nmeadefs['ROT'][0].rxcb.myforEach(function(cb,el,ar,p){cb.fun(p,cb.payload)},{data:{rot: this.ship.rot.toFixed(2)}});
		nmeadefs['THS'][0].rxcb.myforEach(function(cb,el,ar,p){cb.fun(p,cb.payload)},{data:{hdgT: this.ship.course.toFixed(2)}});
		nmeadefs['GGA'][0].rxcb.myforEach(function(cb,el,ar,p){cb.fun(p,cb.payload)},{data:{lat: 0, NS: 'N', lon: 0, EW: 'E'}});
		
		//	render table with ships particulars
		this.simulator_tab_onfocus();
		},
	update: function(parm) {	//	dt time since last call in millisecs
		var d = new Date();		var tim = d.getTime();		var dt = tim - this.lasttime;		this.lasttime = tim;
		/*debug*/var hhmmss = d.toISOString();
		//	console.log(this.__id+'.update()');
		
		var fps = 1000/dt;
		this._count++;
		//	pick up ordered rudder, misschien hier niet meer nodig
		if (undefined!=parm.cmdRudder){
		if (!$.isArray(parm.cmdRudder))	parm.cmdRudder = [parm.cmdRudder];
		for (var rr in cmdRudder) {
			if (parm.cmdRudder != this.ship.cmdRudder) {
				this.ship.cmdRudder = parm.cmdRudder;
				//	apply rudder limits
				if (this.ship.maxRudder!=false){
					if (this.ship.cmdRudder> this.ship.maxRudder)	this.ship.cmdRudder =  this.ship.maxRudder;
					if (this.ship.cmdRudder<-this.ship.maxRudder)	this.ship.cmdRudder = -this.ship.maxRudder;
					}
				else {
						if (this.ship.cmdRudder[rr]> 180)	this.ship.cmdRudder[rr] =  this.ship.cmdRudder-360;
						if (this.ship.cmdRudder[rr]<-180)	this.ship.cmdRudder[rr] =  this.ship.cmdRudder+360;
						}
					}
				}
			}
		//	pick up ordered thruster rpm
		if (undefined!=parm.cmdTelegraph){
		if (!$.isArray(parm.cmdTelegraph))	parm.cmdTelegraph = [parm.cmdTelegraph];
			for (var rr in parm.cmdTelegraph) {
				if (parm.cmdTelegraph[rr] != this.ship.cmdTelegraph[rr]){
					this.ship.cmdTelegraph[rr] = parm.cmdTelegraph[rr];
					}
				}
			}

		if (undefined!=parm.immediate_return)	return;	//	voorkom dat je recursief gaat

		// calculate actual rudder based on cmd and fps
		for (var rr in this.ship.cmdRudder) {
			if (this.ship.actualRudder[rr] != this.ship.cmdRudder[rr]) {
				var dRudder = this.ship.cmdRudder-this.ship.actualRudder[rr];
				if (Math.abs(dRudder)<1)	//	damping of last steps
					dRudder = this.ship.tcRudder * fps * 2/Math.abs(dRudder);
				else
					dRudder = this.ship.tcRudder * fps;

				//	console.log('ordered:'+this.ship.cmdRudder+' actualRudder='+this.ship.actualRudder);
				this.ship.actualRudder[rr] += Math.sign(this.ship.cmdRudder[rr]-this.ship.actualRudder[rr]) / dRudder;
				}
			}

		// calculate actual telegraph based on cmd and fps
		for (var rr in this.ship.cmdTelegraph) {
			if (this.ship.cmdTelegraph[rr] != this.ship.rpm[rr]){
				if (Math.abs(this.ship.cmdTelegraph[rr]-this.ship.rpm[rr])<0.4)	//	damping of last steps
					this.ship.rpm[rr] += Math.sign(this.ship.cmdTelegraph[rr]-this.ship.rpm[rr]) / this.ship.tcTelegraph[rr] / fps / 10 *Math.abs(this.ship.cmdTelegraph[rr]-this.ship.rpm[rr]);
				else
					this.ship.rpm[rr] += Math.sign(this.ship.cmdTelegraph[rr]-this.ship.rpm[rr]) / this.ship.tcTelegraph[rr] / fps;
				}
			}
		//	determine dROT;
		// positive factors: high rudder angle, speed, rpm, timeslice
		// negative factors: high rate of turn, high mass
		var ruddermoment = 123;
//		for (var rr in this.ship.actualRudder) {
		ruddermoment = this.ship.actualRudder.myforEach(function(cb,rr,ar,p){ todo hier this doorgeven EN ruddermoment
			return p + this.ship.factor_ruddermoment * this.ship.actualRudder[rr] * this.ship.rpm[rr] * this.ship.speed * Math.sign(this.ship.speed);	//	normaal 1.0
			},ruddermoment);

		this.turnresistance = this.ship.factor_turnresistance * this.ship.rot * this.ship.rot * Math.sign(this.ship.rot);	//	weerstand kwadratisch
		this.ship.drot = (ruddermoment - this.turnresistance) / this.ship.mass;

		// now determine ROT depending on time and dROT
		this.ship.rot += this.ship.drot / fps / 0.1;
		this.ship.course += this.ship.rot / 60 / fps / 1;
		if (this.ship.course<0)
			this.ship.course+=360;
		else if (this.ship.course>360)
			this.ship.course-=360;
			
		//	speed
		this.dSpeed = 0;
		//	determine dSpeed;
		// positive factors: rpm
		// negative factors: high rudder angle, high mass, high speed
		var thrust = 0;
		for (var rr in this.ship.rpm) {
			thrust += this.ship.rpm2thrust * this.ship.rpm[rr];
			}
		this.resistance = this.ship.factor_speedresistance * this.ship.speed * this.ship.speed * Math.sign(this.ship.speed);	//	weerstand kwadratisch

		//	acceleration = netto thrust / mass
		this.dSpeed = (this.thrust - this.resistance) / this.ship.mass;	//	F=m.a => a=F/m

		// now determine speed depending on timeconstant, time and dSpeed
		this.ship.speed += this.ship.speed_tau * this.dSpeed / fps / 1;

		// position
		this.ship.latlon.latdeg += this.ship.speed * Math.cos(this.ship.course *this.deg2rad) / 3600 / fps / 60;
		this.ship.latlon.londeg += this.ship.speed * Math.sin(this.ship.course *this.deg2rad) / 3600 / fps / 60 / Math.cos(this.ship.latlon.latrad);

		//	send nmea
		// preliminary, the easy way; call the nmeacallback directly without forming the nmea
		var rsa = {srud: this.ship.actualRudder, prud: this.ship.actualRudder};
		var vtg = {cogT: this.ship.course.toFixed(2), sogN: this.ship.speed};
		var rmc = {cog: this.ship.course.toFixed(2)};
		var rot = {rot: this.ship.rot.toFixed(2)};
		var ths = {hdgT: this.ship.course.toFixed(2)};
		var gga = {lat: this.ship.latlon.latgll, NS: this.ship.latlon.latsgn, lon: this.ship.latlon.longll, EW: this.ship.latlon.lonsgn};
		nmeadefs['RSA'][0].rxcb.myforEach(function(cb,el,ar,p){cb.fun(p,cb.payload)},{data: rsa});
		nmeadefs['VTG'][0].rxcb.myforEach(function(cb,el,ar,p){cb.fun(p,cb.payload)},{data: vtg});
		nmeadefs['RMC'][0].rxcb.myforEach(function(cb,el,ar,p){cb.fun(p,cb.payload)},{data: rmc});
		nmeadefs['ROT'][0].rxcb.myforEach(function(cb,el,ar,p){cb.fun(p,cb.payload)},{data: rot});
		nmeadefs['THS'][0].rxcb.myforEach(function(cb,el,ar,p){cb.fun(p,cb.payload)},{data: ths});
		nmeadefs['GGA'][0].rxcb.myforEach(function(cb,el,ar,p){cb.fun(p,cb.payload)},{data: gga});
		for (var rr in this.ship.rpm) {
			var trd = {not: rr, rpmr: this.ship.rpm[rr].toFixed(1), ptcr: this.ship.pitch[rr]};
			nmeadefs['TRD'][0].rxcb.myforEach(function(cb,el,ar,p){cb.fun(p,cb.payload)},{data: trd});
			}
		// here, form real nmea-sentences
		this.outputs.myforEach(function(cb,el,ar,p){
			var test = cb;	//	todo make compose nmea sentence functions for a ship
			// cb.fun(p,cb.payload)
			}
			,this
			);
		if (undefined!=this.ws_sim){	// if in contact with server
			if ((tim-this.lastWereaboutsSent) > this.ship.vdo_interval){	// reken in millisekonden
				this.lastWereaboutsSent = tim;
				var vdo = new Object();
				vdo.req = 'vdo';	
				vdo.id_session = this.id_session;
				vdo.ais = new Object();	//	follow AIS standard for fields
				vdo.ais.UserID = this.ship.MMSI;
				vdo.ais.Name = this.ship.name;
				vdo.ais.CallSign = this.ship.CallSign;
				vdo.ais.COG = this.ship.course.toFixed(2);
				vdo.ais.SOG = this.ship.speed;
				vdo.ais.ROT = this.ship.rot.toFixed(2);
				vdo.ais.TrueHeading = this.ship.course.toFixed(2);
				vdo.ais.Latitude = this.ship.latlon.latdeg;
				vdo.ais.Longitude = this.ship.latlon.londeg;
				this.ws_sim.zend(JSON.stringify(vdo));
				add2aistable(vdo.ais,true);	
				}
			else if ((tim-this.lastVdmRequest) > this.ship.vdm_interval){	//	in else want anders overlapt vdo vdm
				this.lastVdmRequest = tim;
				var aisrequest = new Object();
				aisrequest.req = 'vdm';
				aisrequest.id_session = this.id_session;
				this.ws_sim.zend(JSON.stringify(aisrequest));
				// console.log(hhmmss+' VDM requested');
				}
			}
		},
	objAsTable: function(obj,att,firstrow){
		var table='';
		if (undefined!=firstrow) {
			var row='';
			for (var ii in firstrow){
				row += tag('td',firstrow[ii]);
				}
			table += tag('tr',row);
			for (ii in obj) {
				var row='';
				for (var jj in firstrow){
					row += tag('td',obj[ii][firstrow[jj]]);
					}
				table += tag('tr',row);
				}
			}
		else {
			for (var ii in obj) {
				var value= obj[ii];
				// if (typeof value==="LatLon")	value = {lat: value.latdegmindec, lon: value.londegmindec};	//	todo vind uit hoe je bepaalt of value van het type LatLon is.
				if (typeof value==="object" && (undefined!=value.latdegmindec))	value = {lat: value.latdegmindec, lon: value.londegmindec};
				if (typeof value==="object")	value = this.objAsTable(value,att,firstrow);
				table += tag('tr',tag('td',ii)+tag('td',value));
				}
			}
		return tag('table',table,att);
		},
	requestSession:function(){
		setValue('intsim_session','queriing...');
		this.ws_sim = new wsconnect(settings.ws_addr
			,'getsession'						//	connect to websocketserver 'qrcode' service
			,function(ws,that){				//	connected
				ws.zend(JSON.stringify({req:'req_session',world:'Ouddorp',shipname:that.ship.name}));		//	todo bepaal in commandline welke world je wilt
				}
			,function(ws,data,that){		//	data received
				var rx = eval('['+data+']');	rx = rx[0];
				if (undefined!=rx.id_session){
					that.id_session = rx.id_session;
					setValue('intsim_session',that.id_session);
					}
				else if (undefined!=rx.ans){
					switch (rx.ans){
						case 'vdm':	//	received ais object data
							// console.log(hhmmss+' VDM received');
							for (var ii in rx.aislist){	//	zend alle vdm's naar een VDMjson receiver
								var aisobj = rx.aislist[ii];
								if (!isFn(aisobj)){
									nmeadefs['VDMjson'][0].rxcb.myforEach(function(cb,el,ar,p){cb.fun(p,cb.payload)},{data: aisobj});
									}
								}
							break;
						default:
							alert('unsupported ans from simulator server: '+rx.ans);
							break;
						}
					}
				else {
					alert('intern_simulator received: '+data)
					}
				}
			,function(evclose,thatws){		//	disconnected
				thatws = undefined;
				}
			,this
			);
		},
	};
