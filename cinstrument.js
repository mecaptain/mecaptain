//	cinstrument.js

'use strict';

class CInstrument {
	constructor(idx,obj){
		this.idxInInstrumentList = idx;	//	deterumine index in list
		this._counter_cbRxNmea = 0;		//	todo in nmeadefs stoppen
		this.div = this.getDiv(obj,this.containerName(this));	//	a div is created for div-based instruments (not canvas-based instruments)
		}
	getExposedParameters(){	//	handle exposed parameters here
		return this.exposedParms;
		}
	resize(){
		console.log('cinstrument.js: Warning: this instrument should overload the resize()-method.');
		}
	//	todo denk aan een destructor, misschien moeten bv cbRxNmea() registraties ongedaan gemaakt worden	
	destruct(){
		console.log('CInstrument::destruct()');	
		if (undefined!=this.nmeaSubscriptions){
			this.nmeaSubscriptions.myforEach(function(obj,key,arr,that){
				cbRxNmeaUnsubscribe(obj.counter,obj.key);
				},this);
			this.nmeaSubscriptions = undefined;
			}
		//	todo remove event handlers
		}
	cbRxNmea(key,fun,payload){
		//	registrate cbRxNmea-subscriptions for the sake of deregistrate during destruction
		if (undefined==this.nmeaSubscriptions)
			this.nmeaSubscriptions = [];
		this.nmeaSubscriptions.push({counter:cbRxNmea(key,fun,payload,zeroisim.gup('extwin',0)),key:key});	//	running in a tab
		// console.log('cbRxNmea(key,fun,payload) key='+key+', counter=');
		}
	getDiv(obj,divid){
		var rc;
		this.div = obj.querySelector('div.instrumentbody');	//	zoek naar een childNode div.instrumentbody
		if (undefined!=this.div){
			var idx = Array.from(this.div.childNodes).findIndex(node => node.id==divid);
			if (undefined!=idx){
				rc = this.div.childNodes[idx];
				}
			}
		else {	//	this happens in external window mode
			rc = this.div = obj.querySelector('div#'+divid);	//	zoek naar een childNode div.instrumentbody
			}
		return rc;	
		}
	//	for an instrument to be on the installable list, this function must be called	
	static addInstrumentToSettings(file,name,domt,clas,flyo,dependencies){	
		var idx = settings.instrumentList.findIndex(function(needle){return (file==needle.file);});
		if (-1===idx)	
			console.log('instrument file: "'+file+'" not found in settings.instrumentList; '+name+' not added to installable instruments.');
		else {
			settings.instrumentList[idx].name = name;
			settings.instrumentList[idx].domt = domt;
			settings.instrumentList[idx].clas = clas;
			settings.instrumentList[idx].flyo = flyo;
			settings.instrumentList[idx].dependencies = dependencies;
			settings.instrumentList[idx].idxInInstrumentList = idx;
			}
		}
	static get domTypeDiv(){		return 'div';}
	static get domTypeCanvas(){	return 'canvas';}	
	containerName(instr){
		var name = settings.instrumentList[this.idxInInstrumentList].name;
		switch (settings.instrumentList[this.idxInInstrumentList].domt){	// todo deze kan beter een static member van cinstrument zijn, ook conningdesigner gebruikt hem
			case 'canvas':	return 'cd_'+name+'_cv';
			case 'div':		return 'cd_'+name+'_div';
			}
		}
	static makeContainer4(instr,wh){
		switch (instr.domt){	// todo deze kan beter een static member van cinstrument zijn, ook conningdesigner gebruikt hem
			case 'canvas':	return i.canvas({id:'cd_'+instr.name+'_cv', style:'width:100%;height:100%;',width:wh.w+'px',height:wh.h+'px'});	break;
			case 'div':		return i.div('',{id:'cd_'+instr.name+'_div',style:'width:100%;height:100%;',width:wh.w+'px',height:wh.h+'px'});	break;
			}
		return undefined;
		}
	busTX(key,obj){
		zeroisim.busTX(key,obj,this.wid);
		}
	};
	
function cbNmeaRX(rxd){	//	running in external window, called from main program
	// console.log('cbNmeaRX('+JSON.stringify(rxd)+')');
	nmeadefs[rxd.key][0].rxcb.myforEach(function(cb,el,ar,p){
		cb.fun(p,cb.payload);
		},rxd);
	}
function fromExternalInstrument(externalWindow,what) {	//	running in main program, called from external window
	// document.getElementById("dbg").innerHTML += what + "<br />";
	switch (typeof what){
		case 'string':
			console.log('fromExternalInstrument, string: '+what);
			break;
		case 'object':	//	expect cmd,parms
			// console.log('fromExternalInstrument, cmd: '+what.cmd);
			switch (what.cmd) {
				case 'cbRxNmea':	//	expect parms is key
					// console.log('   key: '+what.parms);
					return cbRxNmea(what.parms,function(rxd,wdw){	
						wdw.cbNmeaRX(rxd);
						},externalWindow);
					break;
				case 'cbRxNmeaUnsubscribe':
					console.log('fromExternalInstrument(): ERROR cbRxNmeaUnsubscribe unsupported yet.');
					break;
					
				case 'rxcb':	//	call NMEA subscribers
					// console.log('fromExternalInstrument(): call NMEA subscribers');
					nmeadefs[what.parms.key][0].eachRxcb.go(what.parms);
					break;
					
				default:	
					console.log('fromExternalInstrument(): ERROR unsupported cmd: '+what.cmd);
					break;
				}
			break;
		}
	}
