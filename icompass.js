//	icompass.js

// todo waarden updaten met cbRxNmea(), redraw met timer

'use strict';

class CInstrument_compass extends CInstrument {
	constructor(obj,exposedParms,active,idx){
		super(idx,obj);
		this.canvas = obj.querySelector("canvas");
		this.cv = this.canvas.getContext("2d");
		if (undefined!=exposedParms)
			this.exposedParms = exposedParms;
		else
			this.exposedParms = //	todo deze moeten door de conning designer kunnen worden gezet, nier overrulen hier dan
				{colorCanvasBackground:'rgba(255,255,255,0.0)'
				,COGsize :10,COGthick :5,COGcolor :		'rgba(0,240,240,0.6)'	//	course over ground
				,RSAsize : 4,RSAthick :5,RSAcolor :		'rgba(242,147,15,0.5)'	//	rudder sensor angle
				,RORSsize: 1,RORSthick:5,RORScolor:		'rgba(242,147,15,0.5)'	//	single or starboard rudder order
				,RORPsize: 1,RORPthick:5,RORPcolor:		'rgba(242,147,15,0.5)'	//	port rudder order
				,HDGOsize: 7,HDGOthick:5,HDGOcolor:		'rgba(240,0,240,0.6)'	//	AP ordered HDG
				,ROTOsize: 7,ROTOthick:5,ROTOcolor:		'rgba(240,0,240,0.6)'	//	AP ordered ROT
				,WINDTsize:4,WINDTthick:5,WINDTcolor:	'rgba(0,255,0,0.5)'		//	wind true
				,WINDRsize:4,WINDRthick:5,WINDRcolor:	'rgba(0,0,0,0.5)'			//	wind relative
				,AP_Psize:4,AP_Pthick:4,AP_Pcolor:		'rgba(250,0,0,0.5)'		//	
				,AP_Isize:4,AP_Ithick:4,AP_Icolor:		'rgba(0,250,0,0.5)'		//	
				,AP_Dsize:4,AP_Dthick:4,AP_Dcolor:		'rgba(0,0,250,0.5)'		//	
				
				,ROTstarboard:'#0F0',	ROTport:'#F00',	ROTtextpos:-0.48
				
				,colorC360:'rgba(0,0,255,0.8)'
				,colorC010:'rgba(0,0,255,0.8)'
				,colorHdgText:'rgba(0,0,255,0.8)'
				,colorLubber360:'rgba(255,0,0,0.5)'
				,colorLubber010:'rgba(255,0,0,0.5)'
				
				,diamond:[[1,-1],[0,-2],[-1,-1],]
				,nmea:	
					{compassHDG: {s:'THS',f:'hdgT'}
					,compassROT: {s:'ROT',f:'rot'}
					,compassRSA: {s:'RSA',f:'srud'}
					,compassAPN: {s:'APN',f:'mode'}
					,compassCOG: {s:'RMC',f:'cog'}	//	may be VTG.cogT or RMC.cog
					}
				};
		this.ship0 = {hdt:0,ror:{}};	//	geen gebruik meer maken van global		
		this.mso = {hdt:0};	//	geen gebruik meer maken van global		
		this.ap_mode = 'off';
		this.ap_setpoint = 0;
		this.active = (undefined==active)? false: active;
		if (this.active){
			super.cbRxNmea(this.exposedParms.nmea.compassHDG.s,function(rxd,that){	//	install callback for compass and autopilot
				if (undefined==rxd.data)	return;
				that.ship0.hdt = parseFloat(rxd.data[that.exposedParms.nmea.compassHDG.f]);
				that.draw(that.ship0,180);
				},this);
			super.cbRxNmea('MSO',function(rxd,that){	//	install callback weather
				if (undefined==rxd.data)	return;
				that.mso = rxd.data;
				},this);
			super.cbRxNmea('ROR',function(rxd,that){	//	install callback weather
				if (undefined==rxd.data)	return;
				if ('A'==rxd.data.svalid)		that.ship0.ror.s = rxd.data.srud;
				if ('A'==rxd.data.pvalid)		that.ship0.ror.p = rxd.data.prud;
				},this);
			super.cbRxNmea(this.exposedParms.nmea.compassRSA.s,function(rxd,that){	//	install callback weather
				if (undefined==rxd.data)	return;
				that.ship0.rsa = parseFloat(rxd.data[that.exposedParms.nmea.compassRSA.f]);
				},this);
			super.cbRxNmea(this.exposedParms.nmea.compassCOG.s,function(rxd,that){	//	install callback weather
				if (undefined==rxd.data)	return;
				that.ship0.cog = parseFloat(rxd.data[that.exposedParms.nmea.compassCOG.f]);
				},this);
			super.cbRxNmea(this.exposedParms.nmea.compassROT.s,function(rxd,that){	//	install callback weather
				if (undefined==rxd.data)	return;
				that.ship0.rot = parseFloat(rxd.data[that.exposedParms.nmea.compassROT.f]);
				},this);
			super.cbRxNmea(this.exposedParms.nmea.compassAPN.s,function(rxd,that){	//	install callback for compass and autopilot
				if (undefined==rxd.data)	return;
				/*
				switch (rxd.data.control){
					case 'OFF':	that.setAP({mode:'off',setpoint:0});	break;
					case 'HDG':	that.setAP({mode:'hdg',setpoint:rxd.data.orderedhdg});	break;
					case 'ROT':	that.setAP({mode:'rot',setpoint:rxd.data.rot_rad});		break;
					case 'RAD':	that.setAP({mode:'rad',setpoint:rxd.data.rot_rad});		break;
					case 'TRK':	that.setAP({mode:'trk',setpoint:rxd.data.orderedhdg});	break;	//	todo
					}
				*/	
				that.setAP(rxd.data);
				that.draw(that.ship0,180);
				},this);
			}
		this.pi_180 = Math.PI / 180.0;
		var init2_onrdy = 0;
		this.cb_onreadyImgs = init2_onrdy;	//	dit:init2_onrdy; was het onload event
		//	bouw de kompasrozen
		this.kompasresize(this.canvas.width,this.canvas.height,init2_onrdy);
		this.canvas.that = this;
		
		//	todo dit zou voor ieder instrument-canvas moeten gelden
		this.canvas.addEventListener("click", this.canvasClick.bind(this), false);	
		this.canvas.addEventListener("mousedown", this.canvasMouseDown.bind(this), false);	
		this.canvas.addEventListener("mousemove", this.canvasMouseMove.bind(this), false);	
		this.canvas.addEventListener("mouseup", this.canvasMouseUp.bind(this), false);	
		}
	canvasMouseUp(ev){
		if (ev.currentTarget.classList.contains('resizing')){
			var hw = 
				{w:ev.currentTarget.width-(ev.currentTarget.zeroisimStart.w-ev.offsetX)
				,h:ev.currentTarget.height-(ev.currentTarget.zeroisimStart.h-ev.offsetY)};
			ev.currentTarget.height = hw.h;
			ev.currentTarget.width = hw.w;
			this.kompasresize(ev.currentTarget.width,ev.currentTarget.height);
			ev.currentTarget.classList.remove('resizing');
			ev.currentTarget.zeroisimStart = undefined;
			console.log('Stop resizing ('+hw.w+','+hw.h+')');
			}
		if (ev.currentTarget.classList.contains('moving')){
			ev.currentTarget.classList.remove('moving');
			// console.log('Stop moving');
			}
		}
	canvasMouseMove(ev){
		//	uitdaging: als de muis buiten het canvas beweegt, krijgt deze handler geen events meer
		if (ev.currentTarget.classList.contains('resizing')){
			//	todo draw rubberband
			var hw = 
				{w:ev.currentTarget.width-(ev.currentTarget.zeroisimStart.w-ev.offsetX)
				,h:ev.currentTarget.height-(ev.currentTarget.zeroisimStart.h-ev.offsetY)};
			console.log('resizing ('+hw.w+','+hw.h+')');
			}
		if (ev.currentTarget.classList.contains('moving')){
			console.log('moving');
			}
		}
	canvasMouseDown(ev){
		//	if down in resize position, resize
		//	if down in move position, move
		if ((ev.offsetX > ev.currentTarget.width-20)
		&&  (ev.offsetY > ev.currentTarget.height-20)){
			ev.currentTarget.classList.add('resizing');
			ev.currentTarget.classList.remove('moving');	//	vd zekerheid
			ev.currentTarget.zeroisimStart = {w:ev.offsetX,h:ev.offsetY};
			console.log('Start resizing');
			}
		else {
			ev.currentTarget.classList.add('moving');
			ev.currentTarget.classList.remove('resizing');	//	vd zekerheid
			// console.log('Start moving');
			}			
		}
	canvasClick(ev){
		}
	resize(w,h){
		this.kompasresize(w,h);
		}
	kompasresize(w,h){
		var rc = {width:this.canvas.width, height:this.canvas.height};
		if (undefined!=this.img360) delete this.img360;
		if (undefined!=this.img010) delete this.img010;

		this.canvas.width = this.w = this.canvas.height = this.h = Math.min(w,h);	//	cards must be round, not ellips
		this.midden = {x: this.canvas.width/2, y:this.canvas.height/2};
		this.r_half = this.canvas.width/2;	//	todo let op aspectratio
		//	define settings
		var cs = 
			{c360:this.exposedParms.colorC360	//	'rgba(  0,  0,255,0.3)'
			,c010:this.exposedParms.colorC010	//	'#00E'
			,hdgtxt:this.exposedParms.colorHdgText	//	'#00D'
			,lubber360:this.exposedParms.colorLubber360	//	'rgba(255,0,0,0.5)'
			,lubber010:this.exposedParms.colorLubber010	//	'rgba(255,0,0,0.5)'
			,cv:this.exposedParms.colorCanvasBackground	//	'#EEE'
			};
		this.settings = 
			{	card360:{routside:0.92,rinside:0.87,r10s:0.95,r5s:0.978,rtexts:0.87,color:cs.c360,font:'10px Verdana'}
			,	card010:{routside:0.42,rinside:0.37,r10s:0.90,r5s:0.950,rtexts:0.78,color:cs.c010,font:'10px Verdana'}
			,	heading:{color:cs.hdgtxt,font:'bold 20px sans-serif',pos:{x:0,y:-0.65*this.midden.y}}
			,	lubber360:{color:cs.l360,outside:-0.96*this.midden.y,inside:-0.82*this.midden.y,thick:3}
			,	lubber010:{color:cs.l010,outside:-0.46*this.midden.y,inside:-0.35*this.midden.y,thick:3}
			,	canvas:{bgcolor:cs.cv}
			,	diamond:	{cog:{size:	this.exposedParms.COGsize,color:	this.exposedParms.COGcolor,thick:	this.exposedParms.COGthick}	//	course over ground
							,rsa:{size:	this.exposedParms.RSAsize,color:	this.exposedParms.RSAcolor,thick:	this.exposedParms.RSAthick}	//	rudder sensor angle	
							,rorp:{size:this.exposedParms.RORPsize,color:this.exposedParms.RORPcolor,thick:	this.exposedParms.RORPthick}	//	ordered port rudder				
							,rors:{size:this.exposedParms.RORSsize,color:this.exposedParms.RORScolor,thick:	this.exposedParms.RORSthick}	//	ordered starboard rudder
							,ohdg:{size:this.exposedParms.HDGOsize,color:this.exposedParms.HDGOcolor,thick:	this.exposedParms.HDGOthick}	//	ordered heading
							,orot:{size:this.exposedParms.ROTOsize,color:this.exposedParms.ROTOcolor,thick:	this.exposedParms.ROTOthick}	//	ordered rot			
							,windT:{size:this.exposedParms.WINDTsize,color:this.exposedParms.WINDTcolor,thick:	this.exposedParms.WINDTthick}	//	wind true
							,windR:{size:this.exposedParms.WINDRsize,color:this.exposedParms.WINDRcolor,thick:	this.exposedParms.WINDRthick}	//	wind relative
							,ap_P:{size:this.exposedParms.AP_Psize,color:this.exposedParms.AP_Pcolor,thick:	this.exposedParms.AP_Pthick}	//	autopilot P
							,ap_I:{size:this.exposedParms.AP_Isize,color:this.exposedParms.AP_Icolor,thick:	this.exposedParms.AP_Ithick}	//	autopilot I
							,ap_D:{size:this.exposedParms.AP_Dsize,color:this.exposedParms.AP_Dcolor,thick:	this.exposedParms.AP_Dthick}	//	autopilot D
							}
			,	rrot: 0.59,	drot: 0.04	//	radius and thickness of rot marker
			};
		// zet de kleur van background op canvas
		this.canvas.style.backgroundColor = this.settings.canvas.bgcolor;

		//	bouw de kompasrozen
		//	todo misschien geheugenlek eruithalen
		this.cv.clearRect(0,0,this.w,this.h);
		if (Math.min(this.w,this.h) < 120) {
			//	todo, wat ook lauk is is om het grote plaatje te houden en het te tonen met een translatie
			if (undefined!=this.img360)	this.img360 = undefined;
			if (undefined!=this.img010)	this.img010 = undefined;
			this.readyCreatingCards();
			}
		else if (Math.min(this.w,this.h) < 240) {
			//	todo, wat ook lauk is is om het grote plaatje te houden en het te tonen met een translatie
			zeroisim.createImages
				(this
				,this.canvas
				,[{name:'img360',fun:this.gradenboog_proxy,parm:{that:this,canvas: this.cv,p1:{ticks: 72,short:1,long: 2,ptext: 6,angledivider:0.2},p2:this.settings.card360,m:this.midden}}
				 ,{name:'img010',fun:this.gradenboog_proxy,parm:{that:this,canvas: this.cv,p1:{ticks:100,short:5,long:10,ptext:10,angledivider: 10},p2:this.settings.card010,m:this.midden}}
				 ]
				,this.readyCreatingCards.bind(this) 
				);
			}
		else if (Math.min(this.w,this.h) < 310) {
			//	todo, wat ook lauk is is om het grote plaatje te houden en het te tonen met een translatie
			zeroisim.createImages(this,this.canvas,
				[{name:'img360',fun:this.gradenboog_proxy,parm:{that:this,canvas: this.cv,p1:{ticks: 72,short:1,long: 2,ptext: 4,angledivider:0.2},p2:this.settings.card360,m:this.midden}}
				,{name:'img010',fun:this.gradenboog_proxy,parm:{that:this,canvas: this.cv,p1:{ticks:100,short:5,long:10,ptext:10,angledivider: 10},p2:this.settings.card010,m:this.midden}}
				 ]
				,this.readyCreatingCards.bind(this) 
				);
			}
		else {	
			//	todo misschien moeten de images zichzelf kunnen tekenen met een soort cb routine
			zeroisim.createImages(this,this.canvas,
				[{name:'img360',fun:this.gradenboog_proxy,parm:{that:this,canvas: this.cv,p1:{ticks:360,short:5,long:10,ptext:10,angledivider:  1},p2:this.settings.card360,m:this.midden}}
				,{name:'img010',fun:this.gradenboog_proxy,parm:{that:this,canvas: this.cv,p1:{ticks:100,short:5,long:10,ptext:10,angledivider: 10},p2:this.settings.card010,m:this.midden}}
				 ]
				,this.readyCreatingCards.bind(this) 
				);
			}
		return rc;	
		}
	readyCreatingCards(){
		this.imgsReady = true;
		this.draw(this.ship0,180);
		if ((undefined!=this.cb_onreadyImgs) && zeroisim.isFn(this.cb_onreadyImgs))
			this.cb_onreadyImgs.apply();
		}
	setAP(ap){
		// if (undefined!=ap.mode)			this.ap_mode = ap.mode;
		if (undefined!=ap.setpoint)	this.ap_setpoint = ap.setpoint;
		if (undefined!=ap.control)		this.ap_mode = ap.control.substring(ap.control.length-3).toLowerCase();	//	off, hdg, rot, rad, trk
		if (undefined!=ap.P) 			this.ap_P = ap.P;	
		if (undefined!=ap.I) 			this.ap_I = ap.I;
		if (undefined!=ap.D) 			this.ap_D = ap.D;
		}		
	draw(ship,ord) {
		if (true!==this.imgsReady)	return;
		this.hdg = ship.hdt;
		if (undefined==this.img010)	return;

		if (undefined!=ship.rot)	this.rot = ship.rot;		else	this.rot = 0;	//	rate of turn
		if (undefined!=ship.cog)	this.cog = ship.cog;		else	this.cog = 0;	//	course over ground
		if (undefined!=ship.rsa)	this.rsa = ship.rsa;		else	this.rsa = 0;	//	rudder sensor angle
		if (undefined!=ship.ror){
		if (undefined!=ship.ror.p)	this.rorp = ship.ror.p;	else	this.rorp = 0;	//	ordered rudder angle
		if (undefined!=ship.ror.s)	this.rors = ship.ror.s;	else	this.rors = 0;	//	ordered rudder angle
		}
		this.cv.save();
		this.cv.clearRect(0,0,this.w,this.h);
		this.cv.fillStyle = this.exposedParms.colorCanvasBackground; 	//	this.canvas.style.backgroundColor;
		this.cv.fillRect(0,0,this.w,this.h);
		this.cv.translate(this.midden.x,this.midden.y);

		//	draai naar de koers en zet plaatje op het canvas
		if (undefined!=this.img360){
			this.cv.save();
			this.cv.rotate( -this.hdg * this.pi_180);
			if (undefined!=this.img360.m)
				this.cv.drawImage(this.img360,-this.img360.m.x,-this.img360.m.y);
			this.cv.restore();	// this.cv.rotate( +this.hdg * this.pi_180);	//	todo uitvinden wat sneller is, terugroteren of save()-restore()
			}
		//	teken vergroting van het kompas
		if (undefined!=this.img010.m){
			this.cv.save();
			this.cv.rotate( 36 * -this.hdg * this.pi_180);
			if (undefined!=this.img010.m)
				this.cv.drawImage(this.img010,-this.img010.m.x,-this.img010.m.y);
			this.cv.restore();	// this.cv.rotate( 36 * +this.hdg * this.pi_180);
			}
		//	schrijf koers
		this.cv.textAlign="center";
		this.cv.fillStyle = this.settings.heading.color;
		this.cv.font = this.settings.heading.font;
		this.cv.fillText(zeroisim.prefixpad(this.hdg.toFixed(1),3)+h.deg,this.settings.heading.pos.x,this.settings.heading.pos.y);

		//	teken buitenste zeilstreep
		this.cv.beginPath();
		this.cv.strokeStyle = this.settings.lubber360.color;	this.cv.lineWidth = this.settings.lubber360.thick;
		this.cv.moveTo(0,this.settings.lubber360.outside);		this.cv.lineTo(0,this.settings.lubber360.inside);
		
		//	teken binnenste zeilstreep
		this.cv.strokeStyle = this.settings.lubber010.color;	this.cv.lineWidth = this.settings.lubber010.thick;
		this.cv.moveTo(0,this.settings.lubber010.outside);		this.cv.lineTo(0,this.settings.lubber010.inside);
		this.cv.stroke();
		this.cv.closePath();

		//	draw rot
		if (Math.round(Math.abs(this.rot))>0){
			this.cv.fillStyle = this.exposedParms.ROTstarboard;
			var rotX = +14;
			if (this.rot<0){
				this.cv.fillStyle = this.exposedParms.ROTport;
				rotX *= -1;
				}
			this.cv.strokeStyle = this.cv.fillStyle;
			this.cv.fillText(Math.abs(this.rot).toFixed(0), rotX,this.exposedParms.ROTtextpos*this.midden.y);
			}
		
		// var boog = {straal:0.59*this.midden.y,dikte:0.04*this.midden.y};
		var boog = {straal:this.settings.rrot*this.midden.y,dikte:this.settings.drot*this.midden.y};
		this.cv.beginPath();		this.cv.fillStyle=this.cv.fillStyle;
		this.arc(this.cv,0,0,boog.straal,boog.dikte,0,this.rot,this.rot>0);
		this.cv.fill();			this.cv.stroke();
		
		//	draw marks around compasscard
		this.diamond(this.settings.diamond.cog,	this.settings.card360.routside,this.cog-this.hdg);	//	draw cog
		this.diamond(this.settings.diamond.rsa,	this.settings.card360.routside,this.rsa	);	//	draw rudder sensor angle
		this.diamond(this.settings.diamond.rorp,	this.settings.card360.routside,this.rorp	);	//	draw rudder order port
		this.diamond(this.settings.diamond.rors,	this.settings.card360.routside,this.rors	);	//	draw rudder order starboard
		
		if (undefined != this.mso.windspd && undefined != this.mso.winddir){
			this.diamond(this.settings.diamond.windT,	this.settings.card360.routside,mso.winddir	);	//	draw true wind
			//	calculate relative or apparent wind. NB Real ships experience apparent wind and have to calculate true wind.
			
			}
		
		if (this.ap_mode=='hdg'){
			var dhdg = zeroisim.deltaDirection(this.hdg,this.ap_setpoint,1);
			var rr = this.settings.card360.routside;
			if (Math.abs(dhdg)<8){	//	move marker to magnified ring
				rr = this.settings.card010.routside;
				dhdg *=36;
				}
			this.diamond(this.settings.diamond.ohdg,rr,dhdg);	//	draw ordered heading

			if (undefined!=this.ap_P){
				dhdg = this.ap_P;
				if (Math.abs(dhdg)<8){	//	move marker to magnified ring
					rr = this.settings.card010.routside;
					dhdg *=36;
					}
				else
					rr = this.settings.card360.routside;
				this.diamond(this.settings.diamond.ap_P,rr,dhdg);	//	draw ordered heading
				}
			if (undefined!=this.ap_I){
				dhdg = zeroisim.deltaDirection(this.ap_I);
				if (Math.abs(dhdg)<8){	//	move marker to magnified ring
					rr = this.settings.card010.routside;
					dhdg *=36;
					}
				else
					rr = this.settings.card360.routside;
				this.diamond(this.settings.diamond.ap_I,rr,dhdg);	//	draw ordered heading
				}
			if (undefined!=this.ap_D){
				dhdg = this.ap_D;
				if (Math.abs(dhdg)<8){	//	move marker to magnified ring
					rr = this.settings.card010.routside;
					dhdg *=36;
					}
				else
					rr = this.settings.card360.routside;
				this.diamond(this.settings.diamond.ap_D,rr,dhdg);	//	draw ordered heading
				}
			
			
			// todo consider using tween (http://learningthreejs.com/blog/2011/08/17/tweenjs-for-smooth-animation/)
			}
		else if (this.ap_mode=='rot'){
			this.diamond(this.settings.diamond.orot,this.settings.rrot,this.ap_setpoint);	//	draw ordered heading
			}
		else {
			// todo ondersteun ook track en radius
			}
		this.cv.restore();	// sluit het tekenen af en zet oude matrix aktief
		}
	gradenboog_proxy(){
		zeroisim.gradenboog(this);			
		}
	arc(cv,xm,ym,r,dik,starthoek,eindhoek,rolo){
		var boog = {a:-Math.PI/2*(1-starthoek/90), b:Math.PI/2*(eindhoek/90-1)};
		cv.arc(xm,ym,r+dik,boog.a,boog.b,!rolo);
		cv.arc(xm,ym,r,    boog.b,boog.a, rolo);
		}
	diamond(laf,r,angle){
		zeroisim.doPolair(this.cv,function(cv,x,y,laf,that){
			cv.beginPath();
			cv.moveTo(x,y);
			for (var ii in that.exposedParms.diamond){
				var node=that.exposedParms.diamond[ii];
				cv.lineTo(x+laf.size*node[0],y+laf.size*node[1]);
				}
			cv.lineTo(x,y);
			cv.strokeStyle = laf.color;
			cv.lineWidth = laf.thick;
			cv.closePath();
			cv.stroke();
			},angle,r,laf,this.midden,this);
		}
	}
CInstrument.addInstrumentToSettings('./icompass.js',				'compass',		'canvas',	CInstrument_compass,		'shows values measured by gyro compass');
