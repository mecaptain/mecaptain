'use strict';
var tab_settings = {
	// verplichte drie members om de tabs interface te ondersteunen
	html: function(){
		this.bInDom = true;
		this.div_connected = 'div_connected';
		window.addEventListener("load", this.onload.bind(this), false);	//	todo use bind() everywhere on load event
		return h.table(
				h.tr(h.td('address to listen','title="a port is opened under this protocol and bound to this ipaddress. Leave ipaddress NULL to open socket on \'hostname\' of websocketserver.\nSupported NMEA sentences: see RX or TX tab"')
					+h.td(h.input('','type="text" value="'+settings.listen_addr+'" id="talkaddr" style="" onchange="change_settings(\'listen_addr\',this.value)"'),'colspan="3"'))
			+	h.tr(h.td('address of websocketserver','title="middleware, webpages themselves cannot perform plain socket i/o"')+h.td(h.input('','type="text" value="'+settings.ws_addr+'" id="inp_ws_addr" style=""'))
				+	h.td(h.table(
					h.tr(	h.td(h.button('connect','onclick=wsStartListenToNmeaShip0() title="click to connect. The box \'connected\' should become green. If it is green, this button does nothing."'))
						+	h.td(h.div('connected','id="'+this.div_connected+'" class="check_input_nok"'))
						))
					)
				)
			+	h.tr(h.td('port to send to','title="ship, equipment or simulation opens this port to receive NMEA data, protocol and ip-address are derived from incoming data."')+h.td(h.input('','type="text" value="'+settings.talk_port+'" id="talkport" style="" onchange="change_settings(\'talk_port\',this.value)"'),'colspan="3"'))
			+	h.tr(h.td('received/transmitted sentences')+h.td(h.div('','id="count_rx"'))+h.td(h.div('','id="count_tx"'),'colspan="2"'))
			+	h.tr(h.td('file with NMEA to process')+h.td(h.input('','type="file" id="files" name="files[]" multiple onchange=handleFileSelect(event)'),'colspan="3"'))
				+	h.tr(h.td(h.nbsp)+h.td('file processing time')+h.td(h.table(
					 h.tr(h.td(h.input('realtime',				'type="radio" onclick="check(this,function(that){settings.file_processing_time=that.id.substring(13);})" name="posplot_radio" id="div_fileproc_rt"')))
					+h.tr(h.td(h.input('as fast as possible',	'type="radio" onclick="check(this,function(that){settings.file_processing_time=that.id.substring(13);})" name="posplot_radio" id="div_fileproc_afap"')))
					+h.tr(h.td(h.input('10x',						'type="radio" onclick="check(this,function(that){settings.file_processing_time=that.id.substring(13);})" name="posplot_radio" id="div_fileproc_t10"')))
					),'id="posplot_mouseaction" posplot_mode="nothing"'))
			+	h.tr(h.td('ws verbose')+h.td(h.input('','type="checkbox" id="verboseinput" onclick="check(this)"'+(settings.ws_verbose===true?' checked=true':'')),'colspan="3"'))
			+	h.tr(h.td('check checksum')+h.td(h.input('','type="checkbox" id="chinput" onclick="check(this)"'+(settings.check_checksum===true?' checked=true':'')),'colspan="3"'))	//	todo niet goed
			+	h.tr(h.td('check patterns')+h.td(h.input('','type="checkbox" id="chpatt" onclick="check(this)"'+(settings.check_pattern===true?' checked=true':'')),'colspan="3"'))
			+	h.tr(h.td('received NMEA to js varspace')+h.td(h.input('','type="checkbox" id="chrxnmea_tojs" onclick="check(this)"'+(settings.chrxnmea_tojs===true?' checked=true':'')),'colspan="3"'))
			+	h.tr(h.td('talker id separate in js varspace')+h.td(h.input('','type="checkbox" id="talkerid_tojs" onclick="check(this)"'+(settings.talkerid_tojs===true?' checked=true':'')),'colspan="3"'))
			+	h.tr(h.td('show raw nmea sentence on rx-pages')+h.td(h.input('','type="checkbox" id="showraw" onclick="check(this)"'+(settings.showraw===true?' checked=true':'')),'colspan="3"'))
			+	h.tr(h.td('decode 6bit parts')+h.td(h.input('','type="checkbox" id="decode_6bit" onclick="check(this)"'+(settings.decode_6bit===true?' checked=true':'')),'colspan="3"'))
			+	h.tr(h.td('set rudder midships upon leaving autopilot')+h.td(h.input('','type="checkbox" id="rudder0_on_ap_off" onclick="check(this)"'+(settings.rudder0_on_ap_off===true?' checked=true':'')),'colspan="3"'))
			+	h.tr(h.td('style of aislist')+h.td(h.input('','type="text" value="'+settings.style_aistable+'" id="style_aistable" style="" onchange="change_settings(\'style_aistable\',this.value)"'),'colspan="3"'))
			+	h.tr(h.td('diameter of aisplot')+h.td(h.input('','type="text" value="'+settings.aisgraph_diameter+'" id="aisgraph_diameter" style="" onchange="change_settings(\'aisgraph_diameter\',this.value)"'),'colspan="3"'))
			+	h.tr(h.td('version')+h.td('20170309')+h.td('(c) BGTuijl','colspan="2"'))
			+	h.tr(h.td(h.table(h.tr(h.td('link in qrcode')+h.td(h.button('get QRCode','onclick="getQRCode(window.location.href,7);"'))),	'style="width:100%; white-space:nowrap;"'))+h.td(h.div('','id="qrcodepng"'))+h.td(h.div('','id="qrcodetxt" style="font-family: Courier New; font-size: 0.6em"')))
			+	h.tr(h.td(h.button('hide tab-bar',"id='tab_settings_hide_tab_bar'")))
			,	'style="width:100%; white-space:nowrap;"'
			);
		}
	,hasFocus: false
	,onfocus:function(){
		this.hasFocus=true;
		}
	,onblur:function(){
		this.hasFocus=false;
		}
	,onload: function(evt){
		document.getElementById('tab_settings_hide_tab_bar').addEventListener("click", this.zettabsuit.bind(this), false);
		setInterval(function(){	// start rxtx counter
			setValue('count_rx',count_rx);
			setValue('count_tx',count_tx);
			// todo add an age counter to NMEA RX table
			} ,1000);
		}
	,draw: function(){
		}
	,zettabsuit: function(){	//	todo ctrl+tab should bring you to next tab and back with shift+ctrl+tab
		var yy = document.getElementsByClassName('ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all');
		if (yy[0].style['display']=='none')
			yy[0].style['display'] = '';
		else
			yy[0].style['display'] = 'none';
		}
	,showConnected: function(tf){
		if (true===this.bInDom){
			if (true===tf)
				setValue(this.div_connected,'check_input_ok','className');
			else
				setValue(this.div_connected,'check_input_nok','className');
			}
		}
	};
