//	iengordertelegraph.js

// obsolete, this instrument worked on global variable ship0, it should instead use NMEA for 
// communication to be able to work in an external window

'use strict';

class CInstrument_eot extends CInstrument {
	constructor(obj,exposedParms,active,idx){
		super(idx,obj);
		var canvas = obj.querySelector("canvas");
		this.cv = canvas.getContext("2d")
		this.midden = {x: canvas.width/2, y:canvas.height/2};
		this.deg2rad = Math.PI / 180.0;
		this.shiftkey = false;
		this.mdown = false;
		this.__id = 'CInstrument_eot';
		
		if (undefined!=exposedParms)
			this.exposedParms = exposedParms;
		else
			this.exposedParms = //	todo deze moeten door de conning designer kunnen worden gezet, nier overrulen hier dan
				{globalAlpha:					1.0
				,schaal: 10.0
				,joystick:	-1	//	should be a dropdown list
				,axis:		-1
				,colorBackground:			'rgba(255,255,255,0.0)'	//	gray
				// ,schaal: canvas.height/12.0
				,telegraaf:    [[0,0],[1,0],[1,10],[-1,10],[-1,0],[1,0]]
				,telegraaf_handle: [[-3,5],[3,5]]
				,colorTelegraaf:	'rgba(0,0,0,1.0)'		//	zwart
				,color_telegraaf_handle_cmd: 'rgba(0,255,0,0.9)'	//	groen
				,color_telegraaf_handle_act: 'rgba(0,0,0,1.0)'		//	zwart
				,actualTelegraphFont: 'bold 30px sans-serif'
				,cmdTelegraphFont: 'bold 12px sans-serif'
				};	
		this.ship0 = {};		
		this.cv.globalAlpha = this.exposedParms.globalAlpha;
		this.joystick=this.exposedParms.joystick;
		this.axis=this.exposedParms.axis;
		// canvas.addEventListener("keydown",		this.keydown.bind(this), false);
		// canvas.addEventListener("keyup",			this.keyup.bind(this), false);
		// canvas.addEventListener("mousemove",	this.mousemove.bind(this), false);
		canvas.addEventListener("mousedown",	this.mousedown.bind(this), false);
		// canvas.addEventListener("mouseup",		this.mouseup.bind(this), false);
		
//		window.addEventListener("keydown", this.keydown.bind(this), false);
//		window.addEventListener("keyup", this.keyup.bind(this), false);

		// ship0.cmdTelegraph = 0;
		this.tcTelegraph = 0.1;	//	timeconstant, smaller is faster
		this.bMoveRight = false;
		this.bMoveLeft = false;
		this.bMoveUp = false;
		this.bMoveDown = false;
		this.telegraphzero = false;
		this.hMouseMover = this.mousemove.bind(this);
		this.hMouseMoverStop = this.mouseup.bind(this);
		this.fps = 10;
		setInterval(this.update.bind(this), 1000 / this.fps);
		}
	update() {
		if (this.bMoveUp === true)		this.ship0.cmdTelegraph += 1 *10/this.fps;
		if (this.bMoveDown === true)	this.ship0.cmdTelegraph -= 1 *10/this.fps;
		if (this.telegraphzero === true){
			this.telegraphzero = false;
			this.ship0.cmdTelegraph = 0;
			}
		var tmp = this.ship0.eng[0];
		tmp = parseFloat(tmp.rpm);
		this.actualTelegraph = tmp;
		//	todo make this only active after a takeover with original telegraph
		// this.telegraph(this.ship0.cmdTelegraph,this.ship0.cmdTelegraph);
		if (this.joystick!=this.exposedParms.joystick){
			console.log('joystick old-new='+this.joystick+'-'+this.exposedParms.joystick);
			this.joystick=this.exposedParms.joystick;
			if (-1 != this.axis){
				if (undefined!=wscontrol){
					//	todo check with wscontrol if joystick and axis are available
					console.log('wscontrol.subscribe_joystick_axis('+this.joystick+','+this.axis+')');
					wscontrol.subscribe_joystick_axis(this.joystick,this.axis,this.fnJoystick.bind(this));
					}
				}
			}
		if (this.axis!=this.exposedParms.axis){
			console.log('axis old-new='+this.axis+'-'+this.exposedParms.axis);
			this.axis=this.exposedParms.axis;
			if (-1 != this.joystick){
				if (undefined!=wscontrol){
					//	todo check with wscontrol if joystick and axis are available
					//	todo unsubscribe old joystick and axis
					console.log('wscontrol.subscribe_joystick_axis('+this.joystick+','+this.axis+')');
					wscontrol.subscribe_joystick_axis(this.joystick,this.axis,this.fnJoystick.bind(this));
					}
				}
			}
		this.draw();	
		}
	fnJoystick(data){
		// console.log('iEOT fnJoystick(data) value='+data.value);
		this.ship0.cmdTelegraph = -100*data.value;
		this.draw();
		}
	draw() {
		this.cv.clearRect(0,0,2*this.midden.x,2*this.midden.y);
		this.cv.save();
		this.cv.fillStyle = this.exposedParms.colorBackground;	this.cv.fillRect(0,0,2*this.midden.x,2*this.midden.y);

		this.cv.translate(this.midden.x,50);
		this.cv.lineWidth = 5;

		//	type telegraaf
		this.cv.textAlign="center";
		this.cv.fillStyle = '#000';
		this.cv.font = this.exposedParms.actualTelegraphFont;
		this.cv.fillText(this.actualTelegraph.toFixed(1), 0,-10);

		//	draw telegraaf
		this.drawcanvaslines(this.exposedParms.telegraaf,this.exposedParms.colorTelegraaf);

		//	draw desired telegraafhandle
		this.cv.strokeStyle = this.exposedParms.color_telegraaf_handle_cmd;	//	groen
		this.cv.beginPath();
		for (var idxlijn in this.exposedParms.telegraaf_handle) {
			var lijn = this.exposedParms.telegraaf_handle[idxlijn];
			this.cv.lineTo( this.exposedParms.schaal * lijn[0], this.exposedParms.schaal * (lijn[1]-this.ship0.cmdTelegraph/10) );
			if (this.actualTelegraph != this.ship0.cmdTelegraph) {
				//	type telegraaf
				this.cv.textAlign="center";
				this.cv.fillStyle = this.cv.strokeStyle;
				this.cv.font = this.exposedParms.cmdTelegraphFont;
				this.cv.fillText(this.ship0.cmdTelegraph.toFixed(1), 50, this.exposedParms.schaal * (lijn[1]-this.ship0.cmdTelegraph/10));
				}
			}
		this.cv.stroke();
		
		//	draw actual telegraafhandle
		this.cv.strokeStyle = this.exposedParms.color_telegraaf_handle_act;
		this.cv.beginPath();
		for (idxlijn in this.exposedParms.telegraaf_handle) {
			var lijn = this.exposedParms.telegraaf_handle[idxlijn];
			this.cv.lineTo( this.exposedParms.schaal * lijn[0], this.exposedParms.schaal * (lijn[1]-this.actualTelegraph/10) );
			}
		this.cv.stroke();
		this.cv.restore();
		}
	keydown(evt) {
		this.shiftkey = evt.shiftKey;
		switch (evt.key){
			// case 'ArrowRight':	this.bMoveRight = true;	break;
			// case 'ArrowLeft':		this.bMoveLeft = true;	break;
			case 'ArrowUp':		this.bMoveUp = true;	break;
			case 'ArrowDown':		this.bMoveDown = true;	break;
			}
		}
	keyup(evt) {
		this.shiftkey = evt.shiftKey;
		switch (evt.key){
			// case 'ArrowRight':	this.bMoveRight = false;	break;
			// case 'ArrowLeft':		this.bMoveLeft = false;	break;
			case 'ArrowUp':		this.bMoveUp = false;	break;
			case 'ArrowDown':		this.bMoveDown = false;	break;
			case '0':				this.rudderzero = true;	break;
			}
		}
	drawcanvaslines(lines,strokeStyle){
		this.cv.beginPath();
		this.cv.strokeStyle = strokeStyle;
		for (var idxlijn in lines) {
			this.cv.lineTo( this.exposedParms.schaal * lines[idxlijn][0], this.exposedParms.schaal * lines[idxlijn][1] );
			}
		this.cv.stroke();
		}
	mousemove(evt){
		var e = evt || window.event;
		console.log(this.__id+'.mousemove() y='+(this.mdown.y-e.clientY)/this.exposedParms.schaal);
		if (this.mdown!==false){
			this.ship0.cmdTelegraph = 10*(this.mdown.y-e.clientY)/this.exposedParms.schaal;
			}
		}
	mousedown(evt){
		console.log(this.__id+'.mousedown()');
		var e = evt || window.event;
		this.mdown = {x:e.clientX,y:e.clientY+this.ship0.cmdTelegraph};
		e.preventDefault();
		e.target.addEventListener("mousemove", this.hMouseMover, false);
		e.target.addEventListener("mouseup", this.hMouseMoverStop, false);
		}
	mouseup(evt){
		console.log(this.__id+'.mouseup()');
		var e = evt || window.event;
		e.target.removeEventListener("mousemove", this.hMouseMover, false);
		e.target.removeEventListener("mouseup", this.hMouseMoverStop, false);
		if (this.mdown!==false){
			this.mdown = false;
			}
		}
	telegraph(shaft0,shaft1){	//	todo move to pilo
		if (log) log.info('telegraph shaft0: '+shaft0+'; shaft1: '+shaft1);
		if (0){
			tx.PRC.rdval = tx.PRC.pdval = 1.0*shaft0;
			tx.PRC.engshft = 0;
			var nmea_shaft0 = buildNMEA('PRC',1,1);
			tx.PRC.rdval = tx.PRC.pdval = 1.0*shaft1;
			tx.PRC.engshft = 1;
			//	todo ws_talk_listen moet parameter zijn, niet meer global
			if (undefined!=ws_talk_listen)
				ws_talk_listen.sendNMEA(this.ship0.remote_protocol+'://'+this.ship0.remote_ip+':'+settings.talk_port,nmea_shaft0+buildNMEA('PRC',1,1));
			}
		else {
			tx.TRC.not	=	0;				//',		integer(),		'Number of thruster (thruster index)']
			tx.TRC.rpm	=	shaft0;		//',		floatingp,		'RPM demand value (percentage)']
			tx.TRC.rpmi	=	'P';			//',		literal('P'),	'RPM mode indicator (indicates percentage)']
			tx.TRC.pitch =	shaft0;		//',		floatingp,		'Pitch demand value (percentage)']
			tx.TRC.ptm	=	'P';			//',		literal('P'),	'Pitch mode indicator (indicates percentage)']
			tx.TRC.azi	=	undefined;	//',		floatingp,		'Azimuth demand (0-360, follow-up)']
			tx.TRC.oloc	=	'B';			//',		literal('.'),	'Operating location indicator']
			tx.TRC.stat	=	'A';			//',		literal('A|V'),'Sentence status flag']
			if (undefined!=ws_talk_listen)
				ws_talk_listen.sendNMEA(this.ship0.remote_protocol+'://'+this.ship0.remote_ip+':'+settings.talk_port,buildNMEA('TRC',1,1));
			}
		}
	}

CInstrument.addInstrumentToSettings('./iengordertelegraph.js',	'eot',			'canvas',	CInstrument_eot,			'Engine Order Telegraph, might be connected to an USB-input');
	