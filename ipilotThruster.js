// ipilotThruster.js
'use strict';

// todo maak een optie dat het knopje na een commando blijf blinken totdat de engine het benodigde rpm of pitch bereikt

class CInstrument_pilotEOT extends CInstrument_pilotHorizontal {
	constructor(obj,exposedParms,active,idx){
		super(obj,exposedParms,active,idx);
		if (undefined!=exposedParms)
			this.exposedParms = exposedParms;
		else
			this.exposedParms =
				{nots:'[0]'	//	number of thruster; value or array if coupled, in JSON format, may be array
				,oploc:	'B'	//	operation location in NMEA
				,colorBackground:	'rgba(255,255,255,0.0)'	//	gray
				,colorModeKnobOff:'rgba(224,224,224)'
				,colorModeKnobOn:	'rgba(0,255,0)'
				,tableBorder:		'3px solid rgba(255,255,255,0.4)' // 3px solid #FFF;
				,stops:
					[{txt:'PS<br/>100%',value:-100}
					,{txt:'PS<br/>50%',value:-50}
					,{txt:'PS<br/>25%',value:-25}
					,{txt:'stop',value:0}
					,{txt:'SB<br>25%',value:25}
					,{txt:'SB<br>50%',value:50}
					,{txt:'SB<br>100%',value:100}
					]
				};
		}
	resize(w,h){		}
	draw(){
		super.draw();
		if (this.active!=true){
			zeroisim.replaceNode(this.div,i.span(i.center('THRUSTER')));
			return;
			}
		}
	txValue(value){
		var nots = JSON.parse(this.exposedParms.nots);
		if (!Array.isArray(nots))	nots = [nots];
		nots.myforEach(function(not,ii,ar,that){
			var prc = 
				{ldpos:	value	//	Lever demand position (-100% to 100%):100.0]
				,ldstat:	'A'	//	Lever demand status, A=Data valid, V=Data invalid:'A']
				,rdval:	value	//	RPM demand value (-100% to 100%):100.0]
				,rmind:	'P'	//	RPM mode indicator; Percentage Rpm inValid:'P']
				,pdval:	value	//	Pitch demand value (-100% to 100%):100.0]
				,pmind:	'P'	//	Pitch mode indicator; Percentage Degrees inValid:'P']
				,oploc:	'B'	//	Operating location indicator; Bridge Portwing Starboardwing engineControlroom Engineside Wing:'W']
				,engshft:not	//	Number of engine or propeller shaft, zero based (numeric) 0=single or center, odd=starboard, even=port:0]
				,from:	'ipilotThruster'
				,talker: nmeadefs.PRC[0].ti
				}
			zeroisim.busTX('PRC',prc,that.wid);
			},this);
		}
	}
	
CInstrument.addInstrumentToSettings
	('./ipilotThruster.js'
	,'pilotThruster'
	,CInstrument.domTypeDiv
	,CInstrument_pilotEOT	
	,'bow or sternthruster'
	);
