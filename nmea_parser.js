//	nmea_parser.js
//	(c) BGTuijl 2015
//	Als je eenmaal de definitie van een sentence hebt, nmea_defs.js, is het parsen niet moeilijk meer.
//	Het parsen:
//		maak van de nmea-string een array met de splice(',') funktie
//		haal uit de eerste zes bytes eruit en bepaal hiervan de talker-id en de sentence key.
//		haal uit de nmea definities de definitie van de gevonden key
//		loop het array door en stop iedere waarde in een javascript variabele met de naam die je in de definitie vindt
//		bit64 data zoals in VDO en VDM sentences worden hier niet geparst, 
'use strict';
var tablelize2 = function(strs,date,callback){
	if (undefined!=strs.msg){
		var data = strs.msg;
		var from = strs.fromip+':'+strs.fromport;
		var to = strs.toip+':'+strs.toport;
		strs = data;
		data = undefined;
		}

	var checkto = '';
	var parm = 'nothingy'
	var regex_options = '';
	var strar = strs.split('\n');
	for (var jj in strar){
		var str = strar[jj];
		if (str.length>4){
			var valid = false;
			var split = str.split(',');
			var key = split[0];
			var srcid = key.substring(1,3);
			key = key.substring(3);
			var nmeadef = nmeadefs[key];
			if (undefined==nmeadef){
				parm = 'unsupported NMEA sentence; key="'+key+'"';
				callback({sentence: key, unsupported: key});
				}
			else {
				rx[key] = {};	//	add this nmea sentence to js variable space
				// todo if undefined, unknown nmeastring, key=key
				split.splice(1,0,srcid);	//	talker is not separated by comma so just add to array in position [1]
				if (settings.talkerid_tojs)	rx[key][srcid] = {};	//	add this nmea sentence to js variable space

				if (undefined!=from && undefined!=to) {
					//	add from and to addresses to js variable space
					if (settings.talkerid_tojs) {	//	moet aanstaan
						rx[key][srcid]['from'] = from;
						rx[key][srcid]['to'] = to;
						}
					}
				var chksum_value = split[split.length-1].split('*');	//	last element is postfixed by a '*' and a checksum, change data and push checksum
				split[split.length-1] = chksum_value[0];
				chksum_value = chksum_value[1];
				split.push(chksum_value);
				rx[key][srcid]['checksum'] = chksum_value;
				var ndata = split.length;
				//	todo misschien ruwe data in varspace zetten
				for (var ii in split){
					if (undefined==nmeadef[ii]){
						}
					else if (zeroisim.isFn(nmeadef[ii])){
						}
					else {
						checkto = nmeadef[ii][1];
						valid = h.nbsp;
						if (0==ii){
							var summary = nmeadef[ii][0];
							var field = key;
							}
						else{
							var summary = nmeadef[ii][2];
							var field = nmeadef[ii][0];
							}
						var word=split[ii];
						if (ii==ndata-1){
							//	laatste element bevat ook sterretje en checksum
							if (settings.check_checksum){
								var chksum_calculated = nmeaChecksum(strs);
								valid = (parseInt('0'+chksum_calculated)==parseInt(chksum_value));
								}
							else {
								var chksum_calculated = h.nbsp;
								}
							checkto = chksum_calculated;
							rx[key][srcid]['timeRX'] = date.toLocaleTimeString();	//	add timestamp
							}
						else if (settings.check_pattern) {
							var regex = new RegExp(nmeadef[ii][1],regex_options);
							valid = regex.test(word);
							}
						if (settings.talkerid_tojs)
							rx[key][srcid][field] = split[ii];	//	add this nmea sentence to js variable space
						else
							rx[key][field] = split[ii];	//	add this nmea sentence to js variable space

						regex = undefined;	//	ga geheugenlek tegen
						}
					}
				// 20161013 add raw sentence
				rx[key]['raw'] = str;
				callback({sentence: key, table: parm});
				}
			}
		}
	}
var nmeaChecksum= function(sentence){
	var len = sentence.length;
	var rc = 0;
	for (var ii=0;ii<len;ii++){
		var ord = sentence[ii];
		if ('$'==ord){
			}
		else if ('!'==ord){
			}
		else if ('*'==ord){
			break;
			}
		else {
			rc ^= ord.charCodeAt(0);
			}
		}
	if (rc>0xF)	//	convert to 2-character HEX
		return rc.toString(16).toUpperCase();
	else
		return '0'+rc.toString(16).toUpperCase();
	}
/****	
breek= function(str,cb,parm){
	var split = str.split(',');
	var key = split[0];
	for (var ii in split){
		parm = cb(parm,split[ii]);
		}
	return parm;
	}
******/