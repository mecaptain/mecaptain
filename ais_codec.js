// ais_decoder.js
//	todo limit vdm5 to 256 characters, return array of strings to put in nmea msg
//	todo add more msg types
//	todo zinloze .enc() and .dec() s opruimen
//	testable with http://www.maritec.co.za/?page_id=1051
//	bugs
//	msgtype 5: ETA beter regelen, gaat nu fout, misschien met object doorgeven, dan geen verwarring over day month minute sec
//	msgtype 5: message id moet 5 zijn
//	see http://nmearouter.com/docs/ais/ais_decoder.html

//Table 269 - AIS Message 1, 2 and 3 Position Reports
//From ITU-R M.1371 AIS Messages 1,2,3: Position Reports
'use strict';
var aismsg123 = 	//	from NMEA 0183, table 269, From ITU-R M.1371 AIS Messages 1,2,3: Position Reports
//	parameter				#bits		conversion function
[{name:'MessageID',bits:6}	// Identifier for this message 1, 2 or 3
,{name:'RepeatIndicator',bits:2}	//  Used by the repeater to indicate how many times a message has been repeated. Refer to ITU-R M.1371 Section 4.6.1; 0 - 3; default = 0; 3 = do not repeat any more.
,{name:'UserID'	,bits:30}	//  MMSI
,{name:'NavigationalStatus',bits:4	//  0 = under way using engine, 1 = at anchor, 2 = not under command, 3 = restricted manoeuvrability, 4 = Constrained by her draught, 5 = Moored, 6 = Aground, 7 = Engaged in Fishing, 8 = Under way sailing, 9 = reserved for future amendment of Navigational Status for HSC, 10 = reserved for future amendment of Navigational Status for WIG, 11 - 14 = reserved for future use, 15 = not defined = default
	,table:
		['under way using engine'					//	 0
		,'at anchor'									//	 1
		,'not under command'							//	 2
		,'restricted manoeuvrability'				//	 3
		,'Constrained by her draught'				//	 4
		,'Moored'										//	 5
		,'Aground'										//	 6
		,'Engaged in Fishing'						//	 7
		,'Under way sailing'							//	 8
		,'reserved for future amendment of Navigational Status for HSC'	//	 9
		,'reserved for future amendment of Navigational Status for WIG'	//	10
		,'reserved for future use'					//	11
		,'reserved for future use'					//	12
		,'reserved for future use'					//	13
		,'reserved for future use'					//	14
		,'not defined'									//	15
		]
	,dec:function(rc){
		return this.table[rc];
		}
	,enc:function(rc){
		return this.table.indexOf(rc);
		}
	}
,{name:'ROT',bits:8	//  ± 127 (–128 (80 hex) indicates not available, which should be the default). Coded by ROT [AIS] =4.733 SQRT (ROT [IND]) degrees/min ROT [IND] is the Rate of Turn (720 degrees per minute), as indicated by an external sensor. + 127 = turning right at 720 degrees per minute or higher; - 127 = turning left at 720 degrees per minute or higher
	,dec:function(rc){
		if (-128==rc)	return 'Not avl';
		var sgn = -1;
		if (0x80 & rc)
			rc -= 0x100;
		else
			sgn = +1;
		return sgn * Math.pow((rc/4.733),2);
		}
	,enc:function(rc){
		var sgn = Math.sign(rc);
		rc = Math.round(4.733*Math.sqrt(Math.abs(rc)));
		if (rc>127)	rc = 127;
		rc *= sgn;
		return rc;
		}
	}
,{name:'SOG',bits:10	//  Speed over ground in 1/10 knot steps (0-102.2 knots) 1023 = not available, 1022 = 102.2 knots or higher
	,dec:function(rc){
		return rc/10.0;
		}
	,enc:function(rc){
		rc	= Math.round(rc * 10);
		if (rc>1022)
			rc = 1022;
		return rc;
		}
	}
,{name:'PositionAccuracy',bits:1	//  1 = high (< 10 m; Differential Mode of e.g. DGNSS receiver), 0 = low (> 10 m; Autonomous Mode of e.g. GNSS receiver or of other Electronic Position Fixing Device), default = 0
	,table:
		['low'	//	> 10 m; Autonomous Mode of e.g. GNSS receiver or of other Electronic Position Fixing Device
		,'high'	//	< 10 m; Differential Mode of e.g. DGNSS receiver
		]
	,dec:function(rc){
		return this.table[rc];
		}
	,enc:function(rc){
		return this.table.indexOf(rc);
		}
	}
,{name:'Longitude',bits:28	//  Longitude in 1/10 000 min (±180 degrees, East = positive, West = negative. 181 degrees (6791AC0 hex) = not available = default)
	,dec:function(rc){
		if (0x6791AC0==rc) return 'not available';
		if(rc & 0x08000000)	rc |= 0xf0000000;	//	west, 2s complement van een 28 bits waarde
		return rc / 10000 / 60;	//	todo maybe format into nice longitude
		}
	,enc:function(rc){
		return Math.round(rc * 60 * 10000);	
		}
	}
,{name:'Latitude',bits:27	//  Latitude in 1/10 000 min (±90 degrees, North = positive, South = negative, 91 degrees (3412140 hex) = not available = default)
	,dec:function(rc){
		if (0x3412140==rc) return 'not available';
		if (rc & 0x04000000)	rc |= 0xf8000000;	//	south
		return rc / 10000 / 60;	//	todo maybe format into nice latitude
		}
	,enc:function(rc){
		return Math.round(rc * 60 * 10000);	
		}
	}
,{name:'COG',bits:12	//  Course over ground in 1/10º (0-3599). 3600 (E10 hex)= not available = default; 3601 – 4095 should not be used
	,dec:function(rc){
		if (0xE10==rc) return 'not available';
		return rc / 10;
		}
	,enc:function(rc){
		return Math.round(rc * 10);	
		}
	}
,{name:'TrueHeading',bits:9	//  Degrees (0-359) (511 indicates not available = default).
	,dec:function(rc){
		if (511==rc) return 'not available';
		return rc;
		}
	,enc:function(rc){
		return Math.round(rc);	
		}
	}
,{name:'TimeStamp',bits:6	//  UTC second when the report was generated (0-59,
									//	or 60 if time stamp is not available, which should also be the default value,
									//	or 62 if Electronic Position Fixing System operates in estimated (dead reckoning) mode,
									//	or 61 if positioning system is in manual input mode
									//	or 63 if the positioning system is inoperative)
	,dec:function(rc){
		switch (rc){
			case 60:	return 'not available';
			case 61:	return 'positioning system is in manual input mode';
			case 62:	return 'Electronic Position Fixing System operates in estimated (dead reckoning) mode';
			case 63:	return 'positioning system is inoperative';
			default:	return rc;
			}
		}
	}
,{name:'ResforRegApps',bits:4}	//  Reserved for definition by a competent regional authority. Should be set to zero, if not used for any regional application. Regional applications should not use zero.
,{name:'Spare',bits:1}				//  Not used. Should be set to zero
,{name:'RAIMFlag',bits:1}			//  RAIM (Receiver Autonomous Integrity Monitoring) flag of Electronic Position Fixing Device; 0 = RAIM not in use = default; 1 = RAIM in use)
,{name:'CommunicationState',bits:19	//  See ITU_R M.1371 Sections 3.3.7.2.2 and 3.3.7.3.2
	,table:
		['utc direct'
		,'utc indirect'				//	see 3.1.1.2
		,'sync to base station'		//	Station is synchronized to a base station (base direct)
		,'sync to another station'	//	Station is synchronized to another station based on the highest number of received stations or to another mobile station, which is directly synchronized to a base station
		]
	,dec:function(rc){
		var sync_state		= (rc & (0x60000)) >> 17;	//	bits 17,18		1100000000000000000
		var slot_timeout	= (rc & (0x1C000)) >> 14;	//	bits 14,15,16	0011100000000000000	Specifies frames remaining until a new slot is selected
		var sub_message	= (rc & (0x03FFF));			//	bits 14,15,16	0000011111111111111	The sub message depends on the current value in slot time-out as described in Table 19
		/****
		TABLE 19
		Slot time-out	Sub message Description
		3, 5, 7	Received stations 	Number of other stations (not own station) which the station
												currently is receiving (between 0 and 16 383).
		2, 4, 6	Slot number 			Slot number used for this transmission (between 0 and 2 249).
		1 			UTC hour and			If the station has access to UTC, the hour and minute should be
					minute					indicated in this sub message. Hour (0-23) should be coded in bits
												13 to 9 of the sub message (bit 13 is MSB). Minute (0-59) should be
												coded in bit 8 to 2 (bit 8 is MSB). Bit 1 and bit 0 are not used.
		0			Slot offset				If the slot time-out value is 0 (zero) then the slot offset should
												indicate the offset to the slot in which transmission will occur
												during the next frame. If the slot offset is zero, the slot should be
												de-allocated after transmission.
		****/
		switch (sync_state){
			case 0:	//	'utc direct';						see 3.1.1.1
				if (1==slot_timeout){
					var hour		= (rc>>9) & 0x001F;	//	utc hour is in bits 13-9	11111000000000
					var minute	= (rc>>2) & 0x007F;	//	utc minute is in bits 8-2	00000111111100 (bits 1 & 0 unused)
					rc = ((hour<10)? '0'+hour: hour) + ':' + ((minute<10)? '0'+minute: minute);
					}
				else {
					// rc = 'utc direct';
					rc = this.table[sync_state];	//	utc direct
					}
				break;
			case 1:	rc = this.table[sync_state];	break;	//	utc indirect, see 3.1.1.2
			case 2:	rc = this.table[sync_state];	break;	//	sync to base station, Station is synchronized to a base station (base direct)
			case 3:	rc = this.table[sync_state];	break;	//	sync to another station, Station is synchronized to another station based on the highest number of received stations or to another mobile station, which is directly synchronized to a base station
			}
		//	todo, dit gebeurt nog niet helemaal volgens bovenstaande definitie	
		return rc;
		}
	,enc:function(rc){
		if (undefined!=this.table.indexOf(rc))	
			return this.table.indexOf(rc);
		else
			// return '??';
			return rc;
		}
	}
	];
var aismsg5 = 	//	From ITU-R M.1371 table 52 AIS Message 5: Ship static and voyage related data
//	parameter				#bits		conversion function
[{name:'MessageID',bits:6}	// Identifier for this message 5
,{name:'RepeatIndicator',bits:2}	//  Used by the repeater to indicate how many times a message has been repeated. Refer to ITU-R M.1371 Section 4.6.1; 0 - 3; default = 0; 3 = do not repeat any more.
,{name:'UserID',bits:30}	//  MMSI
,{name:'AISversion',bits:2	//
	,dec:function(rc){
		switch (rc){
			case 0:	// station compliant with Recommendation ITU-R M.1371-1
			case 1:	// station compliant with Recommendation ITU-R M.1371-3 (or later)
			case 2:	// station compliant with Recommendation ITU-R M.1371-5 (or later)
			case 3:	// station compliant with future editions
			}
		return rc;
		}
	}
,{name:'IMOnumber',bits:30
	// ,dec:function(rc){
		// //log.info('IMOnumber = '+rc);
		// return rc;
		// }
	}
,{name:'CallSign',bits:42	//	7 characters
	,enc:function(rc){	
		return rc.toUpperCase();
		}
	// ,dec:function(rc){
		// // return string according to
		// for (var ii=0; ii<7; ii++) {
			// }
		// return rc;
		// }
	}
,{name:'Name',bits:120	//	todo naam wordt niet goed gecodeerd, callsign en destination ook niet
	,enc:function(rc){	
		return rc.toUpperCase();
		}
	// ,dec:function(rc){
		// //log.info('Name = '+rc);
		// return rc;
		// }
	}
,{name:'ShipType',bits:8}
,{name:'dimRefPoint',bits:30
	,dec:function(rc){
		//	reference point for reported position and overall dimensions of ship
		//	A	9	bit 21-29	meters from bow to posrefpoint
		//	B	9	bit 12-20	meters from stern to posrefpoint
		//	C	6	bit 06-11	meters from port beam to posrefpoint
		//	D	6	bit 00-05	meters from starboard beam to posrefpoint
		var rc =
			[	(rc & (0x3FE00000)) >> 21	//	A bits 21-29		111111111000000000000000000000
			,	(rc & (0x001FF000)) >> 12	//	B bits 12-20		000000000111111111000000000000
			,	(rc & (0x00000FC0)) >> 6	//	C bits 06-11		000000000000000000111111000000
			,	(rc & (0x0000003F))			//	D bits 00-05		000000000000000000000000111111
			];
		rc = '(A,B,C,D)=('+rc.join(',')+')';	//	log.info(rc);
		return rc;
		}
	,enc:function(rc){	
		var re = /\(([0-9]+),([0-9]+),([0-9]+),([0-9]+)\)/g, dimensions;
		if (dimensions=re.exec(rc)){
			rc = parseInt(dimensions[1]) << 21
				| parseInt(dimensions[2]) << 12
				| parseInt(dimensions[3]) << 6
				| parseInt(dimensions[4])
			}
		return rc;
		}	
	}
,{name:'PosFixDev',bits:4
	,table:
	['undefined'
	,'GPS'
	,'GLONASS'
	,'combined GPS/GLONASS'
	,'Loran-C'
	,'Chayka'
	,'integrated navigation system'
	,'surveyed'
	,'Galileo'
	,'not used'
	,'not used'
	,'not used'
	,'not used'
	,'not used'
	,'not used'
	,'internal GNSS'
	]
	,dec:function(rc){
		return this.table[rc];
		}
	,enc:function(rc){
		return this.table.indexOf(rc);
		}
	}
,{name:'ETA',bits:20
	,dec:function(rc){
		var mo = (rc & 0xF0000) >> 16;	//	month	bits 19-16	11110000000000000000	F0000
		var dd = (rc & 0x0F800) >> 11;	//	day	bits 15-11	00001111100000000000	0F800
		var hh = (rc & 0x007C0) >>  6;	//	hour	bits 10-6	00000000011111000000	007C0
		var mi = (rc & 0x0003F);			//	minute bits 5-0	00000000000000111111	0003F
		//return '14-06 12:34:56';
		rc = 	 zeroisim.prefixpad(mo,2)
		+	'-'+zeroisim.prefixpad(dd,2)
		+	' '+zeroisim.prefixpad(hh,2)
		+	':'+zeroisim.prefixpad(mi,2)
		+	' UTC';
		return rc;
		}
	,enc:function(rc){
		var re = /([0-9]+)\-([0-9]+) ([0-9]+):([0-9]+) UTC/g, eta;	
		if (eta=re.exec(rc)){
			rc = parseInt(eta[1]) << 16	//	month
				| parseInt(eta[2]) << 11	//	day
				| parseInt(eta[3]) << 6		//	hour
				| parseInt(eta[4])			//	minute
			}
		return rc;
		}	
	}
,{name:'draught',bits:8
	,dec:function(rc){
		return parseFloat(rc) / 10;
		// return parseFloat(rc);
		}
	,enc:function(rc){
		return 10*rc;
		}
	}
,{name:'destination',bits:120	
	,enc:function(rc){	
		return rc.toUpperCase();
		}
	}
,{name:'DTE',bits:1
	,dec:function(rc){
		return rc;
		}
	,enc:function(rc){
		return rc;
		}
	}
,{name:'spare',bits:1}
//	424
];

function internCheckAisMsg123(){
	var aismsg123_rcBits=168;
	var bitcount = 0;
	for (var part in aismsg123){
		bitcount += aismsg123[part].bits;
		}
	if (bitcount==aismsg123_rcBits){
		return 1;	//	ok
		}
	return 0;	//	nok
	}
function internCheckAisMsg5(){
	var aismsg5_rcBits=424;
	var bitcount = 0;
	for (var part in aismsg5){
		bitcount += aismsg5[part].bits;
		}
	if (bitcount==aismsg5_rcBits){
		return 1;	//	ok
		}
	return 0;	//	nok
	}
function char_2_bit6(car){		//	zie nmea-boek, table 11
	if (undefined==car) return 0;
	car = car.charCodeAt(0);
	car += 0x28;		//	00101000
	if (car > 0x80)	//	10000000
		car += 0x20;	//	00100000
	else
		car += 0x28;	//	00101000

	return car & 0x3F;	//	return 6 LSB
	}
function bit6_2_char(bit6){	//	zie nmea-boek, table 11
	var add = 0x38;
	if (bit6 < 0x28)		add = 0x30;
	return String.fromCharCode(bit6 + add);
	}
function getbit(bit,byte){	//	let op: bit=0 is LSB, bit7=MSB
	var rc = (byte & (1<<bit))>>bit;
	return rc;
	}
function giveBits(sentence,start,end,dbginfo){	//	todo, straks rc returnen, is nog niet helemaal identiek als voorbeeld in NMEA boek
	var start_masks =
		[	0x3F	//0	111111
		,	0x1F	//1	011111
		,	0x0F	//2	001111
		,	0x07	//3	000111
		,	0x03	//4	000011
		,	0x01	//5	000001
		];
	var idx_start = Math.trunc((start-1)/6);
	var bit_start = (start-1) % 6;
	var idx_end = Math.trunc((end-1)/6);
	var bit_end = (end-1) % 6;
	var rc = 0;
	var shift = 6;
	var bit6;
	if ((end - start) >32) {	// more bits than a word, return ascii string
		// todo
		var bitarray = [];
		rc = '';
		for (var idx=idx_start; idx<=idx_end; idx++){
			bit6 = parseInt(char_2_bit6(sentence[idx]));
			if (idx==idx_start) {	//	todo hier gaat iets fout
				//var tstbit6 = bit6 & start_masks[bit_start];	//	experimental
				for (var bit=bit_start; bit<6; bit++){
					bitarray.push(getbit(5-bit,bit6));
					}
				}
			else if (idx==idx_end)	{
				bit6 >>= 5-bit_end;
				shift = 1+bit_end;
				for (var bit=0; bit<=bit_end; bit++){
					bitarray.push(getbit(5-bit,bit6));
					}
				}
			else {
				for (var bit=0; bit<6; bit++){
					bitarray.push(getbit(5-bit,bit6));
					}
				}
			}
		for (idx=0; idx<(end-start)/6; idx++){		// put every bit in an array, later on we get the 6bit values from array and convert to characters
			var val=0;
			for (var ii=0; ii<6; ii++){
				val <<= 1;
				val += bitarray[6*idx+ii];
				}
			if (0x0000!=val){						// ignore the @-character
				if (val<0x20)	val |= 0x40;	// special 'ascii'-tabel: ITU-R M.1371-5 TABLE 47
				rc += String.fromCharCode(val);
				}
			}
		}	//	this was string processing
	else {
		for (var idx=idx_start; idx<=idx_end; idx++){
			bit6 = parseInt(char_2_bit6(sentence[idx]));
			if (idx==idx_start)		bit6 &= start_masks[bit_start];
			if (idx==idx_end)	{
				bit6 >>= 5-bit_end;
				shift = 1+bit_end;
				}
			rc <<= shift;
			rc += bit6;
			}
		}
	return rc;
	}
function decodeVDM(sentence){
	// var test = sentence[0];
	if (1==sentence.length){
		sentence = sentence[0];
		var bp=0;
		}
	else{
		var bp=0;
		}	
	switch (sentence[0]){
		case '1':
		case '2':
		case '3':	return decodeVDM_intern(aismsg123,sentence);
		case '5':	return decodeVDM_intern(aismsg5,sentence);
		default:		return 'unsupported msg: starting with ' + sentence[0];
		}
	}
function decodeVDM_intern(aismsg,sentence){
	var rc = new Object;
	var idx_aismsg = 0;
	for (var msg=0; msg<aismsg.length; msg++){
		var gb = giveBits(sentence,idx_aismsg+1,idx_aismsg+aismsg[msg].bits,aismsg[msg].name);
		if (undefined!=aismsg[msg].dec){	//	if decoding function exists for this field
			gb = (aismsg[msg].dec)(gb);	//	decode the bits
			}
		rc[aismsg[msg].name] = gb;
		idx_aismsg +=aismsg[msg].bits;
		}
	return rc;
	}
//	ENCODING
function encodeVDM(msgtype,msg){
	msg.MessageID=msgtype; 
	switch (msgtype){
		case 1:	
		case 2:	
		case 3:	return encodeVDM_intern(aismsg123,msg);
		case 5:	return encodeVDM_intern(aismsg5,msg);
		}
	}
function encodeVDM_intern(aismsg,msg){
	var rc = {uncoded:[],startbit:0};
	aismsg.myforEach(function(def,ii,that,rc){
		var value = msg[def.name];
		if (undefined!=def.enc){	//	if there is a encoding function for this field
			value = def.enc(value)	//	call the encoding function for this field
			}
		rc = encodeBits(rc,def,value);
		},rc);
	return bitsToAIS(rc);	//	todo moet meer parameters krijgen
	}
function isNumeric(n) {
	return parseFloat(n) == n;
	}
function encodeBits(sentence,def,value){
	if (!isNumeric(value)){
		return encodeBitsString(sentence,def,value);
		}
	else {
		return encodeBitsNumeric(sentence,def,value);
		}
	}
function encodeBitsString(sentence,def,value){
	// ik weet niet wat deze hier moet... if (NaN!=value) return '';
	var numberToWrite = 0;
	// var endBit = sentence.startbit + def.bits;
	for (var ii=0; ii<def.bits/6; ii++){
		numberToWrite = value.charCodeAt(ii);
		if (numberToWrite >= 0x60)	numberToWrite -=0x20;		// make uppercase
		if (numberToWrite >= 0x40 && numberToWrite <= 0x5F){	// 8-bit chars in range [0x40, 0x5F], [A..Z] and @,[,\,],^,-. (not include comma and dot)
			numberToWrite -= 0x40;
			}
		else if (numberToWrite < 0x20 && numberToWrite > 0x3F){	// anything outside [0x20, 0x3F] U [0x40, 0x5F]
			numberToWrite = 0x3F;
			}
		sentence = encodeBitsNumeric(sentence,{bits:6},numberToWrite);
		}
	return sentence;
	}
function encodeBitsNumeric(sentence,def,value){
	for (var ii=sentence.startbit+def.bits-1; ii>=sentence.startbit; --ii){
		var bitVal = ((value & 1)==1)? 1: 0;	// look at only the last bit
		sentence.uncoded[ii] = bitVal;
		value >>= 1;
		}
	sentence.startbit += def.bits;
	return sentence;
	}
function bitsToAIS(pp){
	var bitset = pp.uncoded;
	var bitCount = pp.startbit;
	var symbCount = (bitCount / 6>>0);
	var restSymbCount = bitCount % 6;
	var rc = '';

	for (var ii=0; ii<(6-restSymbCount); ++ii) {
		bitset[6*symbCount+ii+restSymbCount] = 0;	//	extra entries in de bitset zodat je er straks niet overheen gaat
		}
	
	for (var ii=0; ii<=symbCount; ++ii) {	//	encode to 6-bits characters
		var bb = ii * 6;
		rc += bit6_2_char(
			  (bitset[bb+0] << 5) 
			+ (bitset[bb+1] << 4)
			+ (bitset[bb+2] << 3)
			+ (bitset[bb+3] << 2)
			+ (bitset[bb+4] << 1)
			+  bitset[bb+5]);
		}
	return rc;
	}
	
// WScript.stdout.WriteLine('testing AIS codec');      //      onder cscript	
// WScript.stdout.WriteLine(decodeVDM('5<bH3j02@aST5O4tP008lu@u9T4<QC000000000t28B335hf@3TnA3QFH43lU0000000008'));      //      onder cscript	
