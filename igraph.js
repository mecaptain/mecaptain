//	igraph.js
// graphing with dygraph. Other nice lib is flot
'use strict';

class CInstrument_graph extends CInstrument {
	constructor(obj,exposedParms,active,idx){
		super(idx,obj);
		if (undefined!=exposedParms)
			this.exposedParms = exposedParms;
		else
			this.exposedParms =
				{something:0
				};
		// this.divGraph = i.div('hier komt de graph van te kiezen NMEA rx waarden.',{id:"divgraph",style:"width:1000px; height:400px;"});
		this.divGraph = i.div('hier komt de graph van te kiezen NMEA rx waarden.',{id:"divgraph",style:"width:100%; height:100%;"});
		this.html = i.table(i.tr(
			[i.td(this.divGraph)
			,i.td(i.table(
				[i.tr(i.td(i.input('graph on/off', {type:"checkbox",onclick:"check(this)",id:"graph_on"})))
				,i.tr(i.td(i.button('clear', {onclick:"graphicdata=[];"})))
				,i.tr(i.td(i.button('save', {onclick:"alert(\'yet to make\')"})))
				]))
			]));
		if (active){
			this.graphicdata = [];
			this.graphic = new Dygraph(this.divGraph, this.graphicdata,
				{drawPoints: true	//	showRoller: true
	//			,valueRange: [null, null, null, null],	labels: ['Time', 'ROT', 'rudder', 'dHDG' ],	ylabel: 'rot,rud,err'
	//			,valueRange: [null, null, null, null],	labels: ['Time', 'ROT' ],	ylabel: 'rot,rud,err'
	//			,valueRange: [null, null, null, null],	labels: ['11', '22','33','44' ],	ylabel: 'speed (knots)'
				,valueRange: [null, null,],				labels: ['Time', 'knots',],	ylabel: 'speed (knots)'
				,rollPeriod: 8
				,showRangeSelector: true
				});
			zeroisim.replaceNode(this.div,this.html);
			}
		else {
			zeroisim.replaceNode(this.div,i.span(i.center('GRAPH')));
			}
		}
	}
CInstrument.addInstrumentToSettings
	('./igraph.js'
	,'graph'
	,CInstrument.domTypeDiv
	,CInstrument_graph
	,'plotting'
	,['./incl/dygraph-combined.js']
	);
