//	tab_example.js
//	provides skeleton to add a tab to zeroisim userinterfacereadout
'use strict';
var exampletabobj = {
	// verplichte drie members om de tabs interface te ondersteunen
	html: function(){
		// console.log('initting exampletabObj');
		this.bSomeBoolean = false;
		//	this init is called before page is loaded. Defer initialisation, you need the canvas for it, to after pageload event
		window.addEventListener("load", this.onload.bind(this), false);	//	todo use bind() everywhere on load event
		return 'example tab' + h.canvas('id="exampletab_canvas" width="480" height="340"');
		}
	,onfocus:function(){	
		this.hasFocus=true;
		}
	,onblur:function(){	
		this.hasFocus=false;	
		}
	,onload: function(evt){
		var canvas = document.getElementById('exampletab_canvas');
		canvas.that = this;
		canvas.onmousedown= function(e){	e.currentTarget.that.mousedown(e);	}
		canvas.onmouseup	= function(e){	e.currentTarget.that.mouseup(e);		}
		canvas.onmousemove= function(e){	e.currentTarget.that.mousemove(e);	}
		canvas.onwheel		= function(e){	e.currentTarget.that.mousewheel(e);	}
		}
	,hasFocus: false
	,mousedown: function(evt){
		}
	,mouseup: function(evt){
		}
	,mousemove: function(evt){
		}
	,mousewheel: function(evt){
		}
	,draw: function(){
		}
	};
