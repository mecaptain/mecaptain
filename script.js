// script.js
// todo read all todo's
//	todo make drag and drop interface (like show_keymap4.html)	
'use strict';
var script_tab_obj = {
	html: function(){
		return h.fieldset('run script that contains main_once() and main_period() entrypoints'
			,	h.textarea('','rows="4" cols="100" id="scriptarea_onceperiod"')
			+	h.br
			+	h.table(h.tr
				(h.td(h.input('','type="file" id="scriptfiles_op" style="width:600px" onchange=handleScriptSelect_op(event)')) 
				+h.td(h.output('','id="filelist_op"'))
				+h.td(h.button('run','onclick=runscript()'))
				+h.td(h.select('',['3/s','2/s','1/s','1/2s']))
				+h.td(h.button('stop','onclick=runscript_onceperiodically_stop()'))
				+h.td(h.button('save','onclick=file_save(this)'))
				+h.td(h.button('cpy output to clipboard','onclick=zeroisim.copyTextToClipboard(getValue("script_output","textContent"))'))
				)))
			+	h.fieldset('script output',h.div('','id="script_output"'));
		}
	,onfocus: function(){}
	,onblur: function(){}
	}

// todo put in namespace
var periodically_running_script = undefined;

function handleScriptSelect_op(evt) {	
	return handleScriptSelect(evt, document.getElementById('scriptarea_onceperiod'));
	}
function handleScriptSelect(evt,scriptdiv) {
	var file = evt.target.files[0]; // FileList object, assume single selected file
	console.log('handleScriptSelect('+file+')');
	var reader = new FileReader();
	reader.onload = (function(/*theFile*/) {
		return function(e) {
			scriptdiv.innerText = e.target.result;
			};
		})(file);
	reader.readAsText(file);	//	read the script as text
	}

function runscript(){	//	runs a script that contains main_once() and main_period() entrypoints todo
	console.log("runscript()");
	var script = document.createElement('script');
	script.type = 'text/javascript';
	script.text = getValue('scriptarea_onceperiod');
	document.head.appendChild(script); 
	//	call main_once()
	console.log("runscript() is calling main_once()");
	if (undefined!=main_once)	//	test does not work...
		if (zeroisim.isFn(main_once))	
			main_once();
	// call main_period()
	periodically_running_script=setInterval(function(){
		console.log("runscript() is calling main_period()");
		var rc=main_period();
		if (rc===false){
			runscript_onceperiodically_stop();
			}
		},1000);	//	todo must be specified interval
	}	
function runscript_onceperiodically_stop(){
	if (undefined!=periodically_running_script){
		console.log("runscript_periodically_stop() kill the running script");
		clearInterval(periodically_running_script);
		periodically_running_script = undefined;
		}
	// todo remove script from document, like: 	document.body.removeChild(script);
	}
function print(txt){
	if (log) log.info(txt);
	setValue('script_output',txt,'innerText');
	}
