//	scriptload.js
//	todo in DOM (bv id) registreren welke scripts er geladen zijn, dan niet meer een tweede keer laden
'use strict';

class CLoadScript {
	constructor(cbFinished,parmFinished){
		this.cbFinished = cbFinished;	
		this.parmFinished = parmFinished;
		this.__loadscript_toload = 0;
		}
	loadScripts(parms){
		if (Array.isArray(parms.scripts)){
			var script = parms.scripts.shift();
			}
		else {
			var script = parms.scripts;
			parms.scripts = undefined;
			}
		if (undefined!=script){
			// console.log('loadScripts('+script+')');
			this._loadScript(script,this.loadScripts.bind(this),parms);
			}
		else {
			this.__loadscript_toload += 1;
			// console.log('_loadScript() return');
			if (undefined==parms.parms)
				parms.cb?parms.cb.call({niks:0},parms):0;
			else
				parms.cb?parms.cb.call({niks:0},parms.parms):0;
			return;	//	all scripts popped from array; ready
			}
		}
	loadScript(jsFilePath,cb,parms){
		var newParms = 
			{parms:parms
			,cb:cb
			,scripts:jsFilePath
			}
		return this.loadScripts(newParms);
		}
	_loadScript(jsFilePath,cb,parms){
		/**/console.log('_loadScript('+jsFilePath+')');
		var script = document.createElement('script');		//	todo naam geven gebaseerd op uri
		script.type = 'text/javascript';
		script.charset = 'utf-8';
		script.src = jsFilePath;
		if (undefined!=cb){
			script.payload = parms;
			script.funtocall = cb;
			}
		script.onreadystatechange = script.onload = this._loadScriptOnloadFunctionCaller.bind(this);	//todo kijk op welke change
		document.head.appendChild(script); 
		}	
	_loadScriptOnloadFunctionCaller(evt){
		if (undefined!=evt.target.funtocall){
			evt.target.funtocall.call({niks:0},evt.target.payload);	//	apply werkt hier niet
			}
		if (this.__loadscript_toload==0){
			if (undefined!=this.cbFinished)
				this.cbFinished(this.parmFinished);
			}
		}
	callScriptLoadFunction(which,bDelete){
		console.log('callScriptLoadFunction()');
		var slfun;
		var scripts = document.getElementsByTagName('script');
		for (var ii=0; ii<scripts.length; ii++) {	//	gebruik myforEach() niet want het break statement is nodig
			var script = scripts[ii];
			slfun = script.getAttribute(which);
			if (slfun){	//	roep de slfun function aan
				var slprm = script.getAttribute('parms');	//	this parameter must now be in global scope, todo must be any scope
				slprm = window[slprm];
				window[slfun](which,slprm);
				if (undefined!=bDelete && true==bDelete) {
					var test = 1;
					window[slfun] = function(){};
					}
				break;
				}
			}
		}
	loadStyle(jsFilePath,cb,parms){
		// console.log('loadStyle('+jsFilePath+')');
		var link = document.createElement('link');		//	todo naam geven gebaseerd op uri
		link.rel = 'stylesheet';
		link.href = jsFilePath;
		document.head.appendChild(link); 
		}	
	}
