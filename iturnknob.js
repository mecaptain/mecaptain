// draaiknop.js
//	heeft wim gemaakt, ik heb een beetje abstraktie toegevoegd
// voorbeeld:
/*
		window.onload = function () {
			var Button = new draaiKnop(document.getElementById("map"), rotateMe);
			var Button2 = new draaiKnop(document.getElementById("map2"), rotateMe);
			function rotateMe(elm,this.alpha) {
				console.log("rotation of '"+elm.id+"' is: " + this.alpha + "&deg;");
				}
			}
*/
'use strict';

class CInstrument_rotaryknob extends CInstrument {
// class draaiKnop {
	constructor(obj,exposedParms,active,idx){
		super(idx,obj);
		if (undefined!=exposedParms)
			this.exposedParms = exposedParms;
		else
			this.exposedParms =
				{img:'./img/propellor_azi_push_400.png'
				,widthheight: 400
				,colorBackground:	'rgba(255,255,255,0.0)'	//	gray
				,colorModeKnobOff:'#EEE'
				,colorModeKnobOn:	'#0F0'
				};
		this.active = (undefined==active)? false: active;
		if (!this.active) return;
		this.alpha = 0;	
		this.sigma = 0;	
		this.move = this.mouseMove.bind(this);
		this.moveTouch = this.touchMove.bind(this);
		this.remMove = this.remMove.bind(this);
		this.remMoveTouch = this.remMoveTouch.bind(this);
		this.click = this.click.bind(this);
		this.knob = i.div('',	
			{mousedown:	this.startMove.bind(this)
			,touchstart: this.hTouchstart.bind(this)
			,click:	this.click
			// ,style:'border-radius:20px;height:100%;width:100%;border:2px solid #999999;unselectable:on;unselectable: on;-webkit-user-select: none;-moz-user-select: none;-ms-user-select: none;user-select: none;'
			// ,style:'overflow:hidden;border:1px solid silver;background-image:url(./img/propellor_azi_push_400.png);background-repeat:no-repeat;background-position:50% 50%;width:400px;height:400px;'
			,style:'overflow:hidden;border:0px solid silver;background-image:url('
				+this.exposedParms.img
				+');background-repeat:no-repeat;background-position:50% 50%;width:'
				+this.exposedParms.widthheight
				+'px;height:'+this.exposedParms.widthheight+'px;'
			});
		this.knob.style.color = this.exposedParms.colorModeKnobOff;	
		zeroisim.replaceNode(this.div,this.knob);
		
		this.knob.click();	//	determine coordinates of element with respect to viewport
		// this.callBack = callBack;
		this.callBack = undefined;
		if (navigator.userAgent.indexOf('Edge') != -1){//MS Edge
			this.browser = 'edge';
			}
		else if (navigator.userAgent.indexOf('Firefox') != -1 && parseFloat(navigator.userAgent.substring(navigator.userAgent.indexOf('Firefox') + 8)) >= 3.6){//Firefox
			this.browser = 'ff';
			}
		else if (navigator.userAgent.indexOf('Chrome') != -1 && parseFloat(navigator.userAgent.substring(navigator.userAgent.indexOf('Chrome') + 7).split(' ')[0]) >= 15){//Chrome
			this.browser = 'chrome';
			}
		else if(navigator.userAgent.indexOf('Safari') != -1 && navigator.userAgent.indexOf('Version') != -1 && parseFloat(navigator.userAgent.substring(navigator.userAgent.indexOf('Version') + 8).split(' ')[0]) >= 5){//Safari
			this.browser = 'safari';
			}
		else {
			console.log('unsupported browser');
			}
		// Block
		// this.draw();
		}
	click(evt){
		//	this is to determine the midpoint of this element with respect to the viewport of the page
		if (undefined==this.halfTouch)
			this.halfTouch = {x:evt.layerX,y:evt.layerY};
		this.half = {x:evt.target.offsetWidth/2,y:evt.target.offsetHeight/2};
		evt.target.removeEventListener("click", this.click, false);
		// console.log('click, half='+JSON.stringify(this.half));
		}	
	resize(w,h){
		if (undefined==this.halfTouch)
			this.halfTouch = {x:w,y:h};
		this.half = {x:w/2,y:h/2};
		// console.log('resize, half='+JSON.stringify(this.half));
		}	
	startMove(evt) {
		// console.log('startMove(evt) '+evt.target.id);
		this.sigma = this.hoekFromPos(evt)-this.alpha; 
		// todo vraag je adhv het event af of er mouse of touch handlers geinstalleerd moeten worden
		this.knob.addEventListener("mousemove", this.move, false);
		this.knob.addEventListener("mouseup", this.remMove, false);
		this.knob.addEventListener("mouseout", this.remMove, false); 
		}
	hTouchstart(evt) {
		// console.log('hTouchstart(evt) '+evt.target.id);
		this.knob.addEventListener("touchmove", this.moveTouch, false);
		this.knob.addEventListener("touchend", this.remMoveTouch, false);
		this.startMove(this.touch2mouse(evt));
		}	
	remMove(evt){
		// console.log('remMove '+evt.target.id);
		this.knob.removeEventListener("mousemove", this.move, false);
		}
	remMoveTouch(evt){
		// console.log('remMoveTouch '+evt.target.id);
		this.knob.removeEventListener("touchmove", this.moveTouch, false);
		this.knob.removeEventListener("touchend", this.remMoveTouch, false);
		}
	mouseMove(evt) {
		var deg = this.hoekFromPos(evt) - this.sigma; //zou relatieve hoek moeten zijn t.o.v.
		while (deg < 0) { deg += 360 };
		this.draaiNaar(deg);
		}
	touch2mouse(evt) {
		this.halfTouch;
		var rc = {target: evt.target
			,layerX:evt.changedTouches[0].pageX + this.halfTouch.x
			,layerY:evt.changedTouches[0].pageY + this.halfTouch.y
			,offsetX:evt.changedTouches[0].pageX + this.halfTouch.x
			,offsetY:evt.changedTouches[0].pageY + this.halfTouch.y
			};
		// console.log('touch2mouse rc='+JSON.stringify(rc));
		return rc;
		}
	touchMove(evt) {
		this.mouseMove(this.touch2mouse(evt));
		}
	mouseOver(evt){	//	testing coordinaten
		var layer =	{X:evt.layerX,Y: evt.layerY};
		var client ={X:evt.clientX,Y:evt.clientY};
		var offset ={X:evt.offsetX,Y:evt.offsetY};
		console.log
			(this.prtCoordinates('layer',layer)
			+this.prtCoordinates('client',client)
			+this.prtCoordinates('offset',offset)
			);
		}		
	prtCoordinates(string,struct){	
		return string+'('+struct.X + ', ' + struct.Y+')';
		}		
	hoekFromPos(evt){
		var dX = evt.layerX - this.half.x;
		var dY = this.half.y - evt.layerY;
		// console.log(evt.target.id+': '+this.prtCoordinates('d(XY)',{X:dX,Y:dY}));
		switch (this.browser){
			case 'edge':
			case 'safari':
				dX = evt.offsetX - this.half.x;
				dY = this.half.y - evt.offsetY;
			case 'ff':
				return Math.atan2(dX, dY) * 180.0 / Math.PI + this.alpha;	//	firefox
				break;
			case 'chrome':
				return Math.atan2(dX, dY) * 180.0 / Math.PI;	//	chrome
				break;
			}
		}
	/*
	this.draaiNaarT = function(deg){		
		var t = this.alpha-deg;
		var a = this.alpha;
		var throttled = _.throttle(this.draaiNaar, 1000);
		for (var ii=0; ii<t; ii++) {
			throttled(a-ii);		
			}
		}
	*/
	draaiNaar(deg){		
		// console.log(this.knob.id+' draaiNaar('+deg+')');
		this.knob.style.transform = 'rotate(' + deg + 'deg)';
		// console.log('deg='+deg.toString()+', this.alpha='+this.alpha.toString()+', deg-this.alpha='+(deg-this.alpha).toString()+', Math.abs(deg-this.alpha)'+Math.abs(deg-this.alpha));
		if (Math.abs(deg-this.alpha) >= 0.1) {	//	stuur niet elk wissewasje door, todo dit instelbaar maken
			this.alpha = deg;
			// this.callBack(this.knob,Math.round(10.0*this.alpha)/10);	//	todo precisie instellen
			}
		}
	draai(deg){
		this.draaiNaar(this.alpha+deg);
		}
	}
CInstrument.addInstrumentToSettings('./iturnknob.js',				'rotary',		'div',		CInstrument_rotaryknob,	'Rotary knob, you can assign an action to it');
