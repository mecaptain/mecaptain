//	intern_simulator.js
//	simulates ship movement
//	also provides a loopback 'websocket'
//
//	todo betere class indeling, nmea dialoog dynamisch vullen en gereed maken
//	todo als er kontakt met de webserver is, dus (undefined!=oldws), dan moet er een 'world' gezonden worden naar de ws-server.
//		Deze koppelt dit dan aan de client, andere clients die VDO's zenden, krijgen dan VDM's van de ws-server.
//	todo maybe once support multiple udders
//	todo hefty calculations (update) in Worker()
//	todo geef schip instrumenten en voorstuwing bv ship.hdt wordt ship.instruments.gyro.hdt
//
'use strict';
var intern_simulator = {
	__id: 'intern_simulator'
	,qMC0: []
	,html: function(){
		//document.intern_simulator = this;
		window.addEventListener("load", this.onload.bind(this), false);
		cbRxNmea('MC0',function(rxd,that){	//	install callback mecaptain0
			if (undefined==rxd.data)	return;
			that.processMC0(rxd.data);
			},this);
		return h.table
			(	h.tr(h.td('choose ship:')+h.td(h.div('kies hier een schip uit','id="sim_ships"')))
			+	h.tr(h.td('intern simulator')+h.td(zeroisim.onoffswitch('is_engage','intern_simulator.start_intern_simulator()')))
			+	h.tr(h.td(h.button('request session','onclick=is.requestSession()'))+h.td(h.div('<session>','id="intsim_session"')))
			+	h.td(h.fieldset('output NMEA',h.div('hier komt de lijst met nmeaoutputs','id="list_nmea_output_channels"')),'colspan=2')
			+	h.tr(h.td(h.div('ship','id="intern_simulator_ship"'),'colspan=2'))
			);
		}
	,onfocus:function(){
		// setValue('list_nmea_output_channels',this.listNmeaOutputChannels());
		if (undefined!=this.is)	this.is.simulator_tab_onfocus();
		}
	,onblur:	function(){	if (undefined!=this.is)	this.is.simulator_tab_onblur();}
	,onload(){	//	todo als dit werkt, ook in tab_example doen
		// var that = evt.target.intern_simulator;
		var simship=zeroisim.gup('simship',false);
		if (false!==simship && this.setSimShip(simship)) {
			window.is_engage.click();	//	this.start_intern_simulator();
			}
		else {
			var ship_opts = [''];
			simShips.myforEach(function(cb,el,ar,p){
				ship_opts.push(cb.name)
				});
			setValue('sim_ships',
				h.select('select_sim_ships',ship_opts,'onchange="intern_simulator.setSimShip(this)"')
				);
			}
		if (undefined==mso.currentdir)	mso.currentdir = 0;	//	for instance if mso page is not loaded,
		if (undefined==mso.currentspd)	mso.currentspd = 0;	//	for instance if mso page is not loaded,
		}
	,processMC0(rxd_data){
		if (undefined===this.is.ship){	//	no ship is yet given to simulate, queue request to be processed when one is.
			switch (rxd_data.req){
				case mc0.getnames:
				case mc0.setpos:
				case mc0.sethdg:
				case mc0.getOwnShip:
				case mc0.get3d:
				case mc0.setONS:
					this.qMC0.push(rxd_data);	//	queue these requests until simulated ship is known
					return;
				}
			}
		switch (rxd_data.req){
			case mc0.getnames:
				zeroisim.busTX('MC0', 
					{req:rxd_data.req+mc0.response
					,name:this.is.ship.name
					,mmsi:this.is.ship.mmsi
					,callsign:this.is.ship.CallSign
					});
				break;
			case mc0.setship:
				this.setSimShip(rxd_data.ship);
				break;
			case mc0.setpos:
				this.is.ship.latlon = new zeroisim.LatLon({latrad:rxd_data.latrad,lonrad:rxd_data.lonrad});
				break;
			case mc0.sethdg:
				this.is.ship.hdt = rxd_data.hdgt;
				break;
			case mc0.setONS:
				this.is.ship.NmeaOutputChannels = rxd_data.sentences;
				break;
			case mc0.startsim:
				setValue('intern_simulator_ship',this.is.objAsTable(this.ship));	//	laat de eigenschappen van het schip zien
				window.is_engage.click();	//	start de simulator
				break;
			case mc0.getOwnShip:
				zeroisim.busTX('MC0', 
					{req:rxd_data.req+mc0.response
					,mesh:this.is.ship.dae.mesh
					,distance:this.is.ship.dae.distance
					// ,y_correction:this.is.ship.dae.y_correction
					,scaleMesh:this.is.ship.dae.scaleMesh
					,latlon:this.is.ship.latlon
					,hdt:this.is.ship.hdt
					//	todo 6 assige info (trim, slagzij, diepgang)
					});
				break;
			case mc0.getApK:
				zeroisim.busTX('MC0', 
					{req:rxd_data.req+mc0.response
					,ApK: this.is.ship.autopilot_values
					});
				break;
			// case mc0.:
				break;
			default:
				return;	//	unsupported or maybe called by myself
			}
		}
	,setSimShip: function(pp) {
		if (undefined==pp.selectedIndex){
			this.is.ship = simShips.find(function(el){return (el.name==pp);});
			if (undefined==this.is.ship) return false;
			setValue('sim_ships', this.is.ship.name);
			}
		else {
			this.is.ship = simShips[pp.selectedIndex-1];
			}
		setValue('intern_simulator_ship',this.is.objAsTable(this.is.ship));	//	laat de eigenschappen van het schip zien
		return true;
		}
	,createNmeaOutputChannel: function(divid){
		var newChannel = {};
		var protocol = '???';
		if (true===getValue("nmeachoutputprot_udp",'checked'))	protocol = 'udp';
		if (true===getValue("nmeachoutputprot_tcp",'checked'))	protocol = 'tcp';
		var host = getValue('nmeachoutputprot_host');
		var port = getValue('nmeachoutputprot_port');
		newChannel.address = protocol+'://'+host+':'+port;
		newChannel.sentences = [];
		this.ship.NmeaOutputChannels.push(newChannel);
		setValue('list_nmea_output_channels',this.listNmeaOutputChannels());
		}
	,protClick: function(btn){
		// setValue("nmeachoutputprot",btn.id);
		}
	,listNmeaOutputChannels: function(){
		//	define a 'create'-possibility
		var rc = '';
		rc += h.tr(h.td('udp')+h.td('tcp')+h.td('host')+h.td('port')+h.td('&plusmn;'));
		rc += h.tr
			(h.td(h.input('','type="radio" id="nmeachoutputprot_udp" name="nmeachoutputprot" onclick=protClick(this)'))
			+h.td(h.input('','type="radio" id="nmeachoutputprot_tcp" name="nmeachoutputprot" onclick=protClick(this)'))
			+h.td(h.input('','type="text"  id="nmeachoutputprot_host"'))
			+h.td(h.input('','type="text"  id="nmeachoutputprot_port"'))
			+h.td(h.button('&plus;','onclick="createNmeaOutputChannel(\'tr_nmouch\')"'))
			,'id="tr_nmouch"');
		rc = h.fieldset('create channel',h.table(rc));

		var rc1='';
		if (undefined!=this.ship && this.ship.NmeaOutputChannels.length>0) {
			var nmea_opts = [''];
			for (var nmeadef in nmeadefs){	//	make a list of possible sentences
				nmea_opts.push(nmeadef);		//	todo, sentences die al in dit outputChannel zitten, rood anders groen
				}
			rc1+=h.tr(h.td('address')+h.td('add')+h.td('sentences')+h.td('del'));
			//	show every output channel
			var ii=0;
			this.ship.NmeaOutputChannels.myforEach(function(cb,el,ar,p){
				rc1 += h.tr
					(h.td(cb.address)
					+h.td(h.select('select_sim_outputs',nmea_opts,'onchange=this.addSentence(this) _ch_index='+el))
					+h.td(cb.sentences.join(';'))
					+h.td(h.button('del','onclick=this.delNmeaOutputChannel(this)'))
					,'id="nmeaOutputChannel_'+ii+'"');
				ii++;
				});
			rc1 = h.fieldset('outputChannels',h.table(rc1));
			}
		return rc+rc1+h.button('asJson','onclick=nMmeaOutputChannelsAsJson(this)');
		}
	,delNmeaOutputChannel: function(pp){
		var id = pp.parentNode.parentNode.id;
		id = id.split('_');
		if (id[0]=='nmeaOutputChannel'){
			id = id[1];
			this.ship.NmeaOutputChannels[id] = undefined;	//	be sure to not leak
			this.ship.NmeaOutputChannels.splice(id,1);
			}
		setValue('list_nmea_output_channels',this.listNmeaOutputChannels());
		}
	,nMmeaOutputChannelsAsJson: function(){
		var rc = JSON.stringify(this.ship.NmeaOutputChannels);
		alert(rc);
		}
	,addSentence: function(pp) {
		var idx = pp.getAttribute('_ch_index');
		this.ship.NmeaOutputChannels[idx].sentences.push(pp.value);
		setValue('list_nmea_output_channels',listNmeaOutputChannels());
		}
	,is: {	//	intern simulator object	start with simple ship propellor rudder
		ws_sim: undefined,	//	if undefined, simulator runs in server mode, simulator tells server the wereabouts of me and I am told wereabouts of other ships
		__id: 'is',
		_count: 0,	//	todo must this be this._count?
		simulator_tab_onfocus: function(){
			// setValue('intern_simulator_ship',this.objAsTable(this.ship));	//	laat de eigenschappen van het schip zien
			},
		simulator_tab_onblur: function(){
			},
		init: function(parent){
			// console.log(this.__id+'.init()');
			this.parent = parent;
			this.deg2rad = Math.PI / 180.0;
			var d = new Date();		this.tim = d.getTime();		var dt = this.tim - this.lasttime;		this.lasttime = this.tim;
			this.ship.NmeaOutputChannels.myforEach(function(channel){	// rearrange NmeaOutputChannels
				if (undefined!=channel.sentences){
					channel.sentences.myforEach(function(sentence,ii,ar1){
						sentence.i = 1000/sentence.f;	//	interval in milliseconds
						sentence.l = d.getTime();	//	lastsent
						});
					}
				});
			},
		update: function(parm) {	//	dt time since last call in millisecs
			//	console.log(this.__id+'.update()');
			var d = new Date();		this.tim = d.getTime();		var dt = this.tim - this.lasttime;		this.lasttime = this.tim;
			//var hhmmss = d.toISOString();	//	for debugging
			var fps = 1000/dt;
			this._count++;
			//	pick up ordered rudder, misschien hier niet meer nodig
			if (undefined!=parm.rorp){
				if (parm.rorp != this.ship.ror.p) {
					this.ship.ror.p = parm.rorp;
					//	apply rudder limits
					if (this.ship.maxRudder!==false) {
						if (this.ship.ror.p> this.ship.maxRudder)	this.ship.ror.p =  this.ship.maxRudder;
						if (this.ship.ror.p<-this.ship.maxRudder)	this.ship.ror.p = -this.ship.maxRudder;
						}
					}
				}
			if (undefined!=parm.rors){
				if (parm.rors != this.ship.ror.s) {
					this.ship.ror.s = parm.rors;
					if (this.ship.maxRudder!==false) {	//	apply rudder limits
						if (this.ship.ror.s> this.ship.maxRudder)	this.ship.ror.s =  this.ship.maxRudder;
						if (this.ship.ror.s<-this.ship.maxRudder)	this.ship.ror.s = -this.ship.maxRudder;
						}
					}
				}

			//	pick up ordered thruster rpm
			if (undefined!=parm.cmdTelegraph){
				if (parm.cmdTelegraph != this.ship.cmdTelegraph){
					this.ship.cmdTelegraph = parm.cmdTelegraph;
					}
				}

			if (undefined!=parm.immediate_return)	return;	//	voorkom dat je recursief gaat

			// calculate actual rudder based on cmd and fps
			if (this.ship.rsa.p != this.ship.ror.p) {
				var dRudder = this.ship.ror.p-this.ship.rsa.p;
				if (Math.abs(dRudder)<1)	//	damping of last steps
					dRudder = this.ship.tcRudder * fps * 2/Math.abs(dRudder);
				else
					dRudder = this.ship.tcRudder * fps;

				//	console.log('ordered:'+this.ship.ror.p+' rsa.p='+this.ship.rsa.p);
				this.ship.rsa.p += Math.sign(this.ship.ror.p-this.ship.rsa.p) / dRudder;
				this.ship.rsa.s = this.ship.rsa.p;	//	todo maybe once support multiple udders
				}

			// calculate actual telegraph based on cmd and fps
			if (this.ship.cmdTelegraph != this.ship.rpm){
				if (Math.abs(this.ship.cmdTelegraph-this.ship.rpm)<0.4)	//	damping of last steps
					this.ship.rpm += Math.sign(this.ship.cmdTelegraph-this.ship.rpm) / this.ship.tcTelegraph / fps / 10 *Math.abs(this.ship.cmdTelegraph-this.ship.rpm);
				else
					this.ship.rpm += Math.sign(this.ship.cmdTelegraph-this.ship.rpm) / this.ship.tcTelegraph / fps * this.ship.rpm_tau;
				}
			//	determine dROT;
			// 	positive factors: high rudder angle, speed, rpm, timeslice
			// 	negative factors: high rate of turn, high mass
			this.ship.ruddermoment = this.ship.factor_ruddermoment * this.ship.rsa.p * (this.ship.rpm + this.ship.stwL * Math.sign(this.ship.stwL)/30);
			this.turnresistance = this.ship.factor_turnresistance * this.ship.rot * this.ship.rot * Math.sign(this.ship.rot);	//	weerstand kwadratisch
			this.ship.drot = (this.ship.ruddermoment - this.turnresistance) / this.ship.mass;

			// now determine ROT depending on time and dROT
			this.ship.rot += this.ship.drot / fps / 0.1;
			this.ship.hdt += this.ship.rot / 60 / fps / 1;
			if (this.ship.hdt<0)
				this.ship.hdt+=360;
			else if (this.ship.hdt>360)
				this.ship.hdt-=360;

			//	speed
			this.dSpeed = 0;
			//	determine dSpeed;
			// 	positive factors: rpm
			// 	negative factors: high rudder angle, high mass, high speed
			this.thrust = this.ship.rpm2thrust * this.ship.rpm;
			this.resistance = this.ship.factor_speedresistance * this.ship.stwL * this.ship.stwL * Math.sign(this.ship.stwL);	//	weerstand kwadratisch

			//	acceleration = netto thrust / mass
			this.dSpeed = (this.thrust - this.resistance) / this.ship.mass;	//	F=m.a => a=F/m

			// now determine speed depending on timeconstant, time and dSpeed
			this.ship.stwL += this.ship.stwL_tau * this.dSpeed / fps / 1;
			this.ship.stwT = 0.0;	//	todo 

			if (this.ship.stwL>100){
				var ditisopmerkelijk = 1;
				}

			/*if (0){
				var sog	= //	speed over ground	in NS and EW directions
					{ns:(this.ship.stwL * Math.cos(this.ship.hdt	*this.deg2rad)
					+	  mso.currentspd * Math.cos(mso.currentdir*this.deg2rad))//	take current into consideration; mso.currentdir,mso.currentspd
					,ew:(this.ship.stwL * Math.sin(this.ship.hdt	*this.deg2rad)
					+	  mso.currentspd * Math.sin(mso.currentdir*this.deg2rad))//	take current into consideration; mso.currentdir,mso.currentspd
					,pyt:0.0
					};
				sog.pyt = Math.sqrt(sog.ns*sog.ns+sog.ew*sog.ew);
				var alpha = this.ship.hdt*this.deg2rad - Math.atan2(sog.ew,sog.ns);	//	angle between ships heading and direction of movement
				this.ship.sog = 
					{L: +sog.pyt*Math.cos(alpha)	//	longitudinal
					,T: -sog.pyt*Math.sin(alpha)	//	transversal
					,pyt:	0.0//	total
					};
				this.ship.sog.pyt = Math.sqrt(this.ship.sog.L*this.ship.sog.L+this.ship.sog.T*this.ship.sog.T);
				this.ship.cogT = this.ship.hdt*this.deg2rad + Math.atan(this.safediv(this.ship.sog.T,this.ship.sog.L));
				this.ship.cogT /= this.deg2rad;
				}
			else {
			*/
				var sog = CShip.calcSOG(this.ship.stwL,this.ship.stwT,this.ship.hdt,{dir:mso.currentdir,speed:mso.currentspd});
				this.ship.sog = sog;
				this.ship.cogT = sog.cogT;
				// }
			// position, depending on ships speed and heading
			var sogCorrection = 3600 * fps * 60;
			this.ship.latlon.latdeg	+=sog.ns	/ sogCorrection;
			this.ship.latlon.londeg +=sog.ew	/ sogCorrection / Math.cos(this.ship.latlon.latrad);	//	correct for latitude

			this.ship.timestamp = d;
			/*	for performance not handy to calc each timeslice
			this.ship.timestamp
				= zeroisim.prefixpad(d.getUTCHours(),2)
				+ zeroisim.prefixpad(d.getUTCMinutes(),2)
				+ zeroisim.prefixpad(d.getUTCSeconds(),2)
				;
			*/

			//	send nmea
			// preliminary, the easy way; call the nmeacallback directly without forming the nmea
			var rsa = {srud: this.ship.rsa.s, prud: this.ship.rsa.p};
			var vtg = {cogT: this.ship.cogT.toFixed(2), sogN: this.ship.stwL};	//	todo current not calculated into cog and sog
			var rmc = {status:'A',lat:this.ship.latlon.latgll,slat:this.ship.latlon.latsgn,lon:this.ship.latlon.longll,slon:this.ship.latlon.lonsgn,cog: this.ship.cogT.toFixed(2), sog: this.ship.sog.L};
			var rot = {rot: this.ship.rot.toFixed(2)};
			var ths = {hdgT: this.ship.hdt.toFixed(2)};
			var gga = {lat: this.ship.latlon.latgll, NS: this.ship.latlon.latsgn, lon: this.ship.latlon.longll, EW: this.ship.latlon.lonsgn};
			var zda =
				{time:d.toLocaleTimeString()	//	d.getUTCHours()+':'+d.getUTCMinutes()+':'+d.getUTCSeconds()	//	hhmmss
				,day: d.getUTCDate()	//	'UTC day, 01 to 31',
				,month: d.getUTCMonth()+1	//	'UTC month 01 to 12'
				,year: d.getUTCFullYear()	//	'UTC year'
				,hzone: 0	//	'Local zone hours, 00 to � 13 hrs'
				,mzone: 0	//	,integer(),'Local zone minutes, 00 to +59'
				};
			var vbw =
				{longwat:this.ship.stwL.toFixed(2)		//							'Longitudinal water speed, knots "-" = astern'
				,transwat:this.ship.stwT.toFixed(2)		//	floatingp,			'Transverse water speed, knots "-" = port']
				,validwat:'A'									//	literal('A|V'),	'Status: Water speed, A = Data valid']
				,longgrd:this.ship.sog.L.toFixed(2)		//	floatingp,			'Longitudinal ground speed, knots "-" = astern']
				,transgrd:this.ship.sog.T.toFixed(2)	//	floatingp,			'Transverse ground speed, knots "-" = port']
				,statgrd:'A'									//	literal('A|V'),	'Status, Ground speed, A = Data valid']
				,sterntransw:this.ship.stwT.toFixed(2)	//	floatingp,			'Stern transverse water speed, knots "-" = port']
				,statsterntransw:'A'							//	literal('A|V'),	'Status, stern water speed, A = Data valid']
				,sterntransg:this.ship.sog.T.toFixed(2)//	floatingp,			'Stern transverse ground speed, knots "-" = port']
				,statsterntransg:'A'							//	literal('A|V'),	'Status, stern ground speed A = Data valid V = Invalid']
				};
			var trd = {not: 0, rpm: this.ship.rpm.toFixed(1), pitch: this.ship.pitch, azi: this.ship.rsa.p.toFixed(1)};
			zeroisim.busTX('RSA',rsa);
			zeroisim.busTX('VTG',vtg);
			zeroisim.busTX('RMC',rmc);
			zeroisim.busTX('ROT',rot);
			zeroisim.busTX('THS',ths);
			zeroisim.busTX('GGA',gga);
			zeroisim.busTX('ZDA',zda);
			zeroisim.busTX('VBW',vbw);
			zeroisim.busTX('TRD',trd);
			
			if (this.ship.azi){
				var trc = {not: 0, rpm: this.ship.cmdTelegraph.toFixed(1), pitch: this.ship.pitch, azi: this.ship.ror.p};
				zeroisim.busTX('TRC',trc);
				}
			},
		safediv(a,b){
			if (b!=0.0)
				return a/b;
			else
				return 0;
			},
		objAsTable: function(obj,att,firstrow){
			if (undefined==obj)	return '';
			var table='';
			if (undefined!=firstrow) {
				var row='';
				for (var ii in firstrow){
					row += h.td(firstrow[ii]);
					}
				table += h.tr(row);
				for (ii in obj) {
					var row='';
					for (var jj in firstrow){
						row += h.td(obj[ii][firstrow[jj]]);
						}
					table += h.tr(row);
					}
				}
			else {
				for (var ii in obj) {
					var value= obj[ii];
					if (typeof value==="function")	continue;
					if (typeof value==="object" && (undefined!=value.latdegmindec)){
						value = {lat: value.latdegmindec, lon: value.londegmindec};
						}
					else if (Array.isArray(value))
						value = '[array]';
					else if (typeof value==="object"){	
						if (value.length){
							console.log(ii);
							value = this.objAsTable(value,att,firstrow);
							}
						else{
							value = '[]';
							}
						}
					table += h.tr(h.td(ii)+h.td(value));
					}
				}
			return h.table(table,att);
			},
		requestSession:function(){
			setValue('intsim_session','queriing...');
			this.ws_sim = new wsconnect(settings.ws_addr
				,'getsession'						//	connect to websocketserver 'qrcode' service
				,function(ws,that){				//	connected
					ws.zend(JSON.stringify({req:'req_session',world:'Ouddorp',shipname:that.ship.name}));		//	todo bepaal in commandline welke world je wilt
					}
				,function(ws,data,that){		//	data received
					var rx = eval('['+data+']');	rx = rx[0];
					if (undefined!=rx.id_session){
						that.id_session = rx.id_session;
						setValue('intsim_session',that.id_session);
						}
					else if (undefined!=rx.ans){
						switch (rx.ans){
							default:
								alert('unsupported ans from simulator server: '+rx.ans);
								break;
							}
						}
					else {
						alert('intern_simulator received: '+data)
						}
					}
				,function(evclose,thatws){		//	disconnected
					thatws = undefined;
					}
				,this
				);
			},
		}	//	is
	,start_intern_simulator: function(){
		//	todo als hij al draait moet je de simulator ook stoppen. Misschien de setInterval() stoppen.
		//	set ws to loopback, install evthndlr on ROR, TRC & start simulator
		//	This ws is a global variable, ws connect to real ship or other simulator, or, for now, to internal simulator
		this.is.init(this);	//	initialize ship
		cbRxNmea('PRC',function(rxd,that){	//	install callback for prc
			if (undefined==rxd.data)	return;
			var rdval = parseFloat(rxd.data.rdval);	//
			var pdval = parseFloat(rxd.data.pdval);	//
			that.is.update({cmdTelegraph: pdval,immediate_return: true});	//	kick the model, just setting telegraph is probably faster
			//	console.log('..................................................... telegraph: '+parseFloat(rxd.data.pdval));
			},this);
		cbRxNmea('ROR',function(rxd,that){	//	install callback for rudder order
			if (undefined==rxd.data)	return;
			var srud = parseFloat(rxd.data.srud);		//
			var prud = parseFloat(rxd.data.prud);		//
			that.is.update({rorp: prud,immediate_return: true});	//	kick the model, just above lines is probably more performant
			//	console.log('..................................................... rudder: '+parseFloat(rxd.data.prud));
			},this);
		cbRxNmea('TRC',function(rxd,that){	//	install callback for trc, preliminary only respond azi-like with trd
			if (undefined==rxd.data)	return;
			if (rxd.data.rpm.length)
				that.is.update({cmdTelegraph: parseFloat(rxd.data.rpm),immediate_return: true});
			if ((undefined!=rxd.data.rpm.azi) && rxd.data.rpm.azi.length){
				switch (rxd.data.not) {
					case '0':	that.is.update({rorp: parseFloat(rxd.data.azi),immediate_return: true});	break;
					case '1':	that.is.update({rors: parseFloat(rxd.data.azi),immediate_return: true});	break;
					}
				}
			},this);
		cbRxNmea('GLL',function(rxd,that){
			if (undefined==rxd.data)	return;
			that.is.ship.latlon.interpretLatLonGGA(rxd.data);
			// console.log('rxGLL New position: '+that.is.ship.latlon.latdegmindec+that.is.ship.latlon.londegmindec);
			},this);
		cbRxNmea('THS',function(rxd,that){
			if (undefined==rxd.data)	return;
			that.is.ship.hdt = parseFloat(rxd.data.hdgT);
			// console.log('rxTHS New heading: '+that.is.ship.hdt);
			},this);
		// todo maybe a queue of MC0 requests exist, process it here 
		var rxd_data;
		while (rxd_data=this.qMC0.shift()){
			this.processMC0(rxd_data);
			}
		//	start a timer to kick the model now and then.
		setInterval(function(that){
			that.is.update({});
			}, 50,this);
		}
	}
