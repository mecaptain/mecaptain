'use strict';

function setValue(id,txt,val,ignore){
	var div=document.getElementById(id);
	/**/
    if (typeof div === "undefined" ){
        if (typeof ignore !== "undefined") return
		console.error('ERROR div.'+id+' not in DOM');
		return;
		}
	/**/
	if (undefined!=val)
		div[val] = txt;
	else
		div.innerHTML = txt;
	}
function getValue(id,what){
	if (undefined==what)	what = 'value';
	var obj = document.getElementById(id);
	// var val = document.getElementById(id)[what];
	var val =obj[what];
	return val;
	}
var h = {	//	html namespace
    tag: function (tagname, data, style_class) {
        if (undefined == data) data = '';
        if (undefined == style_class)
            style_class = '';
        else
            style_class = " " + style_class;
        return "<" + tagname + style_class + ">" + data + "</" + tagname + ">";
    }
    , quoted: function (str, quote) {
        if (quote == '"')
            return '"' + str + '"';
        else
            return "'" + str + "'";
    }
    , attributes: function (obj) {
        var rc = new Array();
        for (var ii in obj) {
            rc.push(ii + '=' + h.quoted(obj[ii], '"'));
        }
        return rc.join(' ');
    }
    , a: function (data, style_class) { return h.tag('a', data, style_class); }
    , br: '<br />'
    , button: function (data, style_class) { return h.tag('button', data, style_class); }
    , canvas: function (style_class) { return h.tag('canvas', h.tag('p', 'sorry, no browsersupport for canvas :-('), style_class); }
    , checkmark: '\u2713'
    , center: function (data, style_class) { return h.tag('center', data, style_class); }
    , circle: function (data, style_class) { return h.tag('circle', data, style_class); }
    , code: function (data, style_class) { return h.tag('code', data, style_class); }
    , deg: '\u00B0'
    , div: function (data, style_class) { return h.tag('div', data, style_class); }
    , ele: function (id) { return document.getElementById(id); }
    , fieldset: function (legend, data, style_class) { return h.tag('fieldset', h.legend(legend) + data, style_class); }
    , figure: function (data, style_class) { return h.tag('figure', data, style_class); }
    , figcaption: function (data, style_class) { return h.tag('figcaption', data, style_class); }
    , g: function (data, style_class) { return h.tag('g', data, style_class); }
    , h1: function (data, style_class) { return h.tag('h1', data, style_class); }
    , h2: function (data, style_class) { return h.tag('h2', data, style_class); }
    , h3: function (data, style_class) { return h.tag('h3', data, style_class); }
    , img: function (data, style_class) { return h.tag('img', data, style_class); }
    , input: function (data, style_class) { return h.tag('input', data, style_class); }
    , label: function (data, style_class) { return h.tag('label', data, style_class); }
    , legend: function (data, style_class) { return h.tag('legend', data, style_class); }
    , li: function (data, style_class) { return h.tag('li', data, style_class); }
    , link: function (data, style_class) { return h.tag('a', data, style_class); }
    , nbsp: '&nbsp;'
    , output: function (data, style_class) { return h.tag('output', data, style_class); }
    , option: function (data, value) { return h.tag('option', data, 'value="' + value + '"'); }
    , path: function (data, style_class) { return h.tag('path', data, style_class); }
    , span: function (data, style_class) { return h.tag('span', data, style_class); }
    , style: function (data, style_class) { return h.tag('style', data, style_class); }
    , sup: function (data, style_class) { return h.tag('sup', data, style_class); }
    , svg: function (data, style_class) { return h.tag('svg', data, style_class); }
    , table: function (data, style_class) { return h.tag('table', data, style_class); }
    , tbody: function (data, style_class) { return h.tag('tbody', data, style_class); }
    , td: function (data, style_class) { return h.tag('td', data, style_class); }
    , text: function (data, style_class) { return h.tag('text', data, style_class); }
    , textarea: function (data, style_class) { return h.tag('textarea', data, style_class); }
    , title: function (data, style_class) { return h.tag('title', data, style_class); }
    , tr: function (data, style_class) { return h.tag('tr', data, style_class); }
    , trtd: function (data, style_class) { return h.tag('tr', h.tag('td', data, style_class), style_class); }
    , ul: function (data, style_class) { return h.tag('ul', data, style_class); }
    , select: function (id, options, style_class) {
        var rc = '';
        for (var ii = 0; ii < options.length; ii++) {
            if (typeof options[ii] === 'object') {	//	item must have an .i and .a (item, attribute)
                rc += h.tag('option', options[ii].i, options[ii].a);
            }
            else
                rc += h.tag('option', options[ii]);
        }
        return h.select2(id, rc, style_class);
    }
    , select2: function (id, options, style_class) {
        var rc = '';
        if (undefined == style_class)
            style_class = '';
        else
            style_class = " " + style_class;
        rc += options;
        rc = h.tag('select', rc, 'id="' + id + '"' + style_class);
        return rc;
    }
    , select3: function (id, option_array, style_class) {
        var options = '';
        option_array.myforEach(function (option) {
            options += h.option(option);
        });
        return h.select2(id, options, style_class);
    }
    , recurs: function (el, field, sibling) {	//	handy for offsetLeft etc
        var rc = 0;
        do {
            rc += el[field];
            el = el[sibling];
        } while (undefined != el);
        return rc;
    }
    , node: function (data) {	//	return html of a domelement
        var rc;
        // var test = typeof data.toString();
        switch (data.toString()) {
            case '[object HTMLTableElement]':
                rc = h.table(h.node(data.childNodes), h.node(data.attributes));
                break;
            case '[object NodeList]':
                rc = [];
                data.forEach(function (el, idx, ar) {
                    rc.push(h.node(el));
                });
                rc = rc.join('');
                break;
            case '[object HTMLTableCellElement]':
                rc = h.td(h.node(data.childNodes), h.node(data.attributes));
                break;
            case '[object HTMLTableRowElement]':
                switch (data.nodeName) {
                    case 'TR':
                        rc = h.tr(h.node(data.childNodes), h.node(data.attributes));
                        break;
                    default:
                        rc = h.tr(h.td('HTMLTableRowElement'));
                        break;
                }
                break;
            case '[object NamedNodeMap]':
                if (0 === data.length)
                    rc = '';
                else
                    rc = '[object namednodemap]';
                break;
            case '[object HTMLInputElement]':
                rc = h.input(data.childNodes, data.attributes);
                break;
            case '[object HTMLFieldSetElement]':
                break;
            default:
                rc = '';
                console.log('utils.js h.node(): ERROR: "' + data.toString() + '" is not supported.');
                break;
        }
        return rc;
    }
};	//	h namespace
	
var i = //	html namespace, createElement stuff
{
    tagNS: function (typ, value, attrs) {
        return i.tag(typ, value, attrs, 1);
    }
    , tag: function (typ, value, attrs, ns) {
        if (undefined == ns) {
            var rc = window.document.createElement(typ);		//	console.log('window.document.createElement('+typ+');');
        }
        else {
            switch (typ) {
                case 'html': case 'HTML': ns = 'http://www.w3.org/1999/xhtml'; break;
                case 'circle':
                case 'line':
                case 'path':
                case 'polygon':
                case 'rect':
                case 'text':
                case 'svg': case 'SVG': ns = 'http://www.w3.org/2000/svg'; break;
                case 'xbl': case 'XBL': ns = 'http://www.mozilla.org/xbl'; break;
                case 'xul': case 'XUL': ns = 'http://www.mozilla.org/keymaster/gatekeeper/there.is.only.xul'; break;
            }
            var rc = window.document.createElementNS(ns, typ);		//	console.log('window.document.createElementNS('+ns+','+typ+');');
        }
        for (var ii in attrs) {
            switch (ii) {
                case 'blur':
                case 'change':
                case 'click':	//	install event handler, example: i.button('click me!',{id:'btn_clickme',click:{that:this,handler:this.klik}})
                case 'dblclick':
                case 'drag':
                case 'dragend':
                case 'dragenter':
                case 'dragexit':
                case 'dragleave':
                case 'dragover':
                case 'dragstart':
                case 'drop':
                case 'error':
                case 'focus':
                case 'input':	//	maybe for IE replace for 'change'
                case 'keyup':
                case 'keydown':
                case 'keypress':
                case 'load':
                case 'mouseover':
                case 'mouseout':
                case 'mousedown':
                case 'mouseup':
                case 'mousemove':
                case 'reset':
                case 'resize':
                case 'select':
                case 'submit':
                case 'unload':
                    rc.addEventListener(ii, attrs[ii], false);
                    break;
                default:	//	no events here
                    rc.setAttribute(ii, attrs[ii]);			//	console.log('   rc.setAttribute('+ii+','+attrs[ii]+');');
                    break;
            }
        }

        if (zeroisim.isString(value)) {
            // rc.textContent = value;	console.log('   rc.textContent = '+value);
            rc.innerHTML = value;	//console.log('   rc.innerHTML = '+value);
        }
        else if (zeroisim.isNumeric(value)) {
            rc.innerHTML = value;	//console.log('   rc.innerHTML = '+value);
        }
        else if (Array.isArray(value)) {
            value.myforEach(function (el, idx, ar, that) {
                rc.appendChild(el);			//console.log('   rc.appendChild('+el+');');
            });
        }
        else if (typeof value === 'Node') {  //wim Node is geen type
            rc.appendChild(value);			//console.log('   rc.appendChild('+value+');');
        }
        else if (undefined === value) { }	//	do nothing
        else {
            rc.appendChild(value);			//console.log('   rc.appendChild('+value+');');
        }
        return rc;
    }
    , a: function (data, attrs) { return i.tag('a', data, attrs); }
    , attributes: function (attrs) { return attrs; }
    , button: function (data, attrs) { return i.tag('button', data, attrs); }
    , canvas: function (attrs) { return i.tag('canvas', i.tag('p', 'sorry, no browsersupport for canvas :-('), attrs); }
    , center: function (data, attrs) { return i.tag('center', data, attrs); }
    , checkbox: function (data, attrs) { attrs.type = 'checkbox'; return [i.tag('input', '', attrs), i.span(data)]; }
    , circle: function (data, attrs) { return i.tagNS('circle', data, attrs); }
    , datalist: function (data, attrs) { return i.tag('datalist', data, attrs); }
    , div: function (data, attrs) { return i.tag('div', data, attrs); }
    , fieldset: function (legend, data, attrs) { return i.tag('fieldset', [i.legend(legend), data], attrs); }
    , font: function (data, attrs) { return i.tag('font', data, attrs); }
    , html: function (data, attrs) { return i.tagNS('html', data, attrs); }
    , input: function (data, attrs) { return i.tag('input', data, attrs); }
    , legend: function (data, attrs) { return i.tag('legend', data, attrs); }
    , label: function (data, attrs) { return i.tag('label', data, attrs); }
    , line: function (data, attrs) { return i.tagNS('line', data, attrs); }
    , nbsp: function () { return i.span('&nbsp;'); }
    , option: function (value, attrs) { return i.tag('option', undefined, Object.assign({ value: value }, attrs)); }
    , output: function (data, attrs) { return i.tag('output', data, attrs); }
    , path: function (data, attrs) { return i.tagNS('path', data, attrs); }
    , polygon: function (data, attrs) { return i.tagNS('polygon', data, attrs); }
    , radio: function (data, attrs) { attrs.type = 'radio'; return [i.tag('input', '', attrs), i.span(data)]; }
    , rect: function (data, attrs) { return i.tagNS('rect', data, attrs); }
    , script: function (data, attrs) { return i.tag('script', data, attrs); }
    , slider: function (data, attrs) { attrs.type = 'range'; return i.tag('input', data, attrs); }
    , span: function (data, attrs) { return i.tag('span', data, attrs); }
    , svg: function (data, attrs) { return i.tagNS('svg', data, attrs); }
    , table: function (data, attrs) { return i.tag('table', data, attrs); }
    , td: function (data, attrs) { return i.tag('td', data, attrs); }
    , text: function (data, attrs) { return i.tagNS('text', data, attrs); }
    , tr: function (data, attrs) { return i.tag('tr', data, attrs); }
    , trtd: function (data, attrs) { return i.tag('tr', i.tag('td', data, attrs), attrs); }
    //	more complex DOM elements:
    , select: function (id, options, style_class) {
        var rc = [];
        for (var ii = 0; ii < options.length; ii++) {
            if (typeof options[ii] === 'object') {
                if (undefined != options[ii].i && undefined != options[ii].a)		//	item must have an .i and .a (item, attribute)
                    rc.push(i.tag('option', options[ii].i, options[ii].a));
                else if (undefined != options[ii].t && undefined != options[ii].a)	//	item must have an .t and .a (text, attributes)
                    rc.push(i.tag('option', options[ii].t, options[ii].a));
            }
            else
                rc.push(i.tag('option', options[ii]));
        }
        return i.select2(id, rc, style_class);
    }
    , select2: function (id, options, style_class) {
        var rc = '';
        /*
                if (undefined==style_class)
                    style_class = '';
                else
                    style_class = " " + style_class;
                rc.push(options);
                rc = i.tag('select',rc,'id="'+id+'"'+style_class);
        */
        if (undefined == style_class)
            style_class = { id: id };
        else
            style_class = Object.assign(style_class, { id: id });

        rc = i.tag('select', options, style_class);

        return rc;
    }
};	//	i namespace
	
class C0isimTab {	//	todo all tabs should inherit from this one, later next to tabs, it might be accordeon, page or whatever
	constructor(){
		}
	html(tabdivname){
		this._tabdivname = tabdivname;
		window.addEventListener("load", this.onload.bind(this), false);
		return '';
		}
	onfocus(){	//	todo maybe to super
		this.hasFocus=true;
		}
	onblur(){	//	todo maybe to super
		this.hasFocus=false;	
		}
	onload(evt){
		console.log('C0isimTab onload, should be overloaded');
		}
	}
Array.prototype.myforEach = function (cb, p) {
    var rc;
    for (var ii = 0; ii < this.length; ii++) {
        // if (undefined!=this[ii])	//	holes in array may exist, due to delete (see nmea_defs.js)
        rc = cb(this[ii], ii, this, p);
    }
    return rc;
};
Array.prototype.myforEachSparse = function (cb, p) {
    var rc;
    for (var ii in this) {
        if (!zeroisim.isFn(this[ii])) {
            rc = cb(this[ii], ii, this, p);
        }
    }
    return rc;
};
/*	
function array_flip(trans){ // 
	var key, tmp_ar = {};
	for ( key in trans){
		if ( trans.hasOwnProperty( key ) ){
			tmp_ar[trans[key]] = key;
			}
		}
	return tmp_ar;
	}
*/	
Array.prototype.myValues = function () {	//	untested
    var rc = [];
    for (let ii = 0; ii < this.length; ii++) {
        rc.push(this[ii]);
    }
    return rc;
};
Array.prototype.zeroisimValuesUnique = function () {
    var rc0 = {};
    for (var ii = 0; ii < this.length; ii++) {
        rc0[this[ii]] = ii;
    }
    var rc = [];
    for (let ii in rc0) {
        rc.push(ii);
    }
    return rc;
};
function log10(val) {//log10 niet ge�mplementeerd in IE
	// Math.log10 = Math.log10 || function (x) { //log10 niet ge�mplementeerd in IE
	return Math.log(val) / Math.LN10;
	}
Math.sign = Math.sign || function (x) {
    x = +x;
    if (x === 0 || isNaN(x)) {
        return x;
    }
    return x > 0 ? 1 : -1;
};
Math.sinh = Math.sinh || function (x) { //polyfill niet geïmplementeerd in IE
    var y = Math.exp(x);
    return (y - 1 / y) / 2;
};
Math.cosdeg = function (x) {
    return Math.cos(x * Math.PI / 180.0);
};
Math.sindeg = function (x) {
    return Math.sin(x * Math.PI / 180.0);
};
Math.tandeg = function (x) {
    return Math.tan(x * Math.PI / 180.0);
};
var asTable = function (obj, att) {
    var table = '';
    for (var ii in obj) {	//	todo myforEach
        var row = '';
        for (var jj in obj[ii]) {
            row += h.td(obj[ii][jj]);
        }
        table += h.tr(row);
    }
    return h.table(table, att);
};	

var zeroisim = {};	//	start a clean namespace

zeroisim.scene = undefined;	//	global variable to reduce memory

zeroisim.setiValue = function (divname, node) {
    var parentNode = document.getElementById(divname);
    while (parentNode.firstChild) {	// remove old domtree
        parentNode.removeChild(parentNode.firstChild);
    }
    parentNode.appendChild(node);
    return parentNode;
};
zeroisim.clone = function (obj) {	//	from https://stackoverflow.com/questions/18829099/copy-a-variables-value-into-another
    // Handle the 3 simple types, and null or undefined
    if (null == obj || "object" != typeof obj) return obj;

    // Handle Date
    if (obj instanceof Date) {
        var copy = new Date();
        copy.setTime(obj.getTime());
        return copy;
    }

    // Handle Array
    if (obj instanceof Array) {
        var copy = [];
        for (var i = 0, len = obj.length; i < len; i++) {
            copy[i] = zeroisim.clone(obj[i]);
        }
        return copy;
    }

    // Handle Object
    if (obj instanceof Object) {
        var copy = {};
        for (var attr in obj) {
            if (obj.hasOwnProperty(attr)) copy[attr] = zeroisim.clone(obj[attr]);
        }
        return copy;
    }
    throw new Error("Unable to copy obj! Its type isn't supported.");
};
zeroisim.enrich = function (attrobj, obj) {	//	replace a dollar marked part of str with a field found in obj
    var rc = zeroisim.clone(attrobj);
    for (var ii in rc) {
        if (zeroisim.isString(rc[ii])) {
            var test = rc[ii].substr(0, 1);
            if ('$' == rc[ii].substr(0, 1)) {
                var regex = new RegExp('^.*(\\$(.*))\s*$');
                if (true === regex.test(rc[ii])) {
                    var srcrep = regex.exec(rc[ii]);
                    if (3 == srcrep.length) {
                        var replacement = obj[srcrep[2]];
                        rc[ii] = replacement;	//	todo rc[ii].replace(..,replacement)
                    }
                }
            }
        }
    }
    return rc;
};
zeroisim.iasTable = function (obj, table_att, tr_att) {
    var table = [];
    for (var ii in obj) {	//	todo myforEach
        var row = [];
        for (var jj in obj[ii]) {
            row.push(i.td(obj[ii][jj]));
        }
        var tr_att_enriched = zeroisim.enrich(tr_att, obj[ii]);
        table.push(i.tr(row, tr_att_enriched));
    }
    return i.table(table, table_att);
};	
zeroisim.roundNumber = function (num, dec) {	//	todo probably same as zeroisim.round
    return Math.round(num * Math.pow(10, dec)) / Math.pow(10, dec);	//	todo sign erbij
};
zeroisim.round = function(number,precision) {
	var factor = Math.pow(10,precision);
	var tempNumber = number * factor;
	var roundedTempNumber = Math.round(tempNumber);
	return roundedTempNumber / factor;
	};
zeroisim.isFn = function (f) {
    return !!(f && f.call && f.apply);
};
zeroisim.isString = function (f) {
    if (typeof f === 'string' || f instanceof String)
        return true;// it's a string
    else
        return false;	// it's something else	
};
zeroisim.isDefined = function (variable) {	//	dit werkt niet want undefd variabele kan nooit als argument meegegeven worden
    return (!(typeof variable === 'undefined' || variable === null));
};
zeroisim.merge_objects = function (obj1, obj2) {
    var rc = {};
    for (let attrname in obj1) { rc[attrname] = obj1[attrname]; }
    for (let attrname in obj2) { rc[attrname] = obj2[attrname]; }
    return rc;
};	
zeroisim.gup = function (name, defaultvalue) {
    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regexS = "[\\?&]" + name + "=([^&#]*)";
    var regex = new RegExp(regexS);
    var href = window.location.href;
    // begin new code; 
    href = decodeURIComponent((href + '').replace(/\+/g, '%20'));
    // end new code
    var results = regex.exec(href);
    if (results == null)
        return defaultvalue;
    else
        return results[1];
};
zeroisim.prefixpad = function (num, digits) {
    var pad = '';
    for (var power = 1; power < digits; power++) {
        var factor = Math.pow(10, power);
        if (Math.abs(num) < factor) {
            pad += '0';
        }
    }
    return pad + num;
};
zeroisim.deltaDirection = function (h0, h1, turnDirection, round) {	//	verschil in richting. Als turnDirection!=undefined, +=SB, -=BB (+=kloksgewijs, -=antiklok)
    var rc = h1 - h0;
    if (rc > +180.0) rc -= 360;
    if (rc < -180.0) rc += 360;
    if (undefined != turnDirection) {
        if (turnDirection < 0) {
            rc = -rc;
        }
    }
    if (undefined != round) {
        rc = Math.round(rc * round) / round;
    }
    return rc;	//	returns delta direction, positiv means clockwise turn, negativ	means anticlockwise turn
};
zeroisim.dec2Bin = function (dec) {	//	todo radar future
    return (dec >>> 0).toString(2);
};
//	truk, teken eerst mooi instrument, maak er een dataurl van, daarna maak er een image van.
zeroisim.createImages = function (that, canvas, imgs, onrdy) {
    if (imgs.length) {
        that.imgsReady = false;
        var img = imgs.shift();
        img.fun.apply(img.parm);
        that[img.name] = undefined;
        that[img.name] = new Image();
        that[img.name].that = that;
        that[img.name].canvas = canvas;
        that[img.name].onload = function (evt) {	//	image loads slow
            evt.currentTarget.m = { x: parseInt(this.width / 2), y: parseInt(this.height / 2) };
            var that = evt.currentTarget.that;
            that.cv.clearRect(0, 0, that.w, that.h);
            zeroisim.createImages(that, evt.currentTarget.canvas, imgs, onrdy);
        };
        that[img.name].src = canvas.toDataURL();
    }
    else {
        that.imgsReady = true;
        if (undefined != onrdy)
            onrdy.apply();
    }
};
zeroisim.setStyle = function(id,txt,val){
	var div=document.getElementById(id);
	zeroisim.setStyle_ (div,txt,val);
	}
zeroisim.setStyle_ = function(div,txt,val){
	/**/
	if (undefined==div){
		console.error('ERROR div.'+id+' not in DOM');
		return;
		}
	/**/
	if (undefined!=val)
		div.style[val] = txt;
	}
zeroisim.onoffswitch = function (id, cb, ch) {
    var oc = '';
    if (undefined != cb) oc = ' onchange=' + cb;
    if (undefined != ch) oc += ' checked';
    return h.div
        (h.input('', 'type="checkbox" name="onoffswitch" class="onoffswitch-checkbox"' + oc + ' id="' + id + '"')
        + h.label(h.span('', 'class="onoffswitch-inner"')
            + h.span('', 'class="onoffswitch-switch"')
            , 'class="onoffswitch-label" for="' + id + '"')
        , 'class="onoffswitch"');
};
zeroisim.ionoffswitch = function (id, cb, ch) {
    var oc = {};
    if (undefined != cb) oc = { change: cb };
    if (undefined != ch) zeroisim.merge_objects(oc, { checked: true });
    return i.div(
        [i.input('', zeroisim.merge_objects(oc, { type: 'checkbox', name: 'onoffswitch', class: 'onoffswitch-checkbox', id: id }))
            , i.label(
                [i.span('', { class: 'onoffswitch-inner' })
                    , i.span('', { class: 'onoffswitch-switch' })
                ], { class: 'onoffswitch-label', for: id })
        ], { class: 'onoffswitch' });
};
zeroisim.copyTextToClipboard = function(text) {
	var textArea = document.createElement("textarea");
	//
	// *** This styling is an extra step which is likely not required. ***
	//
	// Why is it here? To ensure:
	// 1. the element is able to have focus and selection.
	// 2. if element was to flash render it has minimal visual impact.
	// 3. less flakyness with selection and copying which **might** occur if
	//    the textarea element is not visible.
	//
	// The likelihood is the element won't even render, not even a flash,
	// so some of these are just precautions. However in IE the element
	// is visible whilst the popup box asking the user for permission for
	// the web page to copy to the clipboard.
	//

	// Place in top-left corner of screen regardless of scroll position.
	textArea.style.position = 'fixed';
	textArea.style.top = 0;
	textArea.style.left = 0;

	// Ensure it has a small width and height. Setting to 1px / 1em
	// doesn't work as this gives a negative w/h on some browsers.
	textArea.style.width = '2em';
	textArea.style.height = '2em';

	// We don't need padding, reducing the size if it does flash render.
	textArea.style.padding = 0;

	// Clean up any borders.
	textArea.style.border = 'none';
	textArea.style.outline = 'none';
	textArea.style.boxShadow = 'none';

	// Avoid flash of white box if rendered for any reason.
	textArea.style.background = 'transparent';

//	textArea.value = text;	werkt niet
	textArea.textContent = text;	//	werkt wel maar ongeformatteerd
	document.body.appendChild(textArea);
	textArea.select();
	try {
		var successful = document.execCommand('copy');
		var msg = successful ? 'successful' : 'unsuccessful';
		console.log('Copying text command was ' + msg);
		} 
	catch (err) {
		console.log('Oops, unable to copy');
		}
	document.body.removeChild(textArea);
	}
zeroisim.pi_180 = Math.PI / 180.0;
zeroisim.safediv = function (a, b) {
    if (b != 0.0)
        return a / b;
    else
        return 0;
};
zeroisim.gradenboog = function (args) {	//	todo ook excentrieke gradenboog maken, bv voor radarPPI als schip niet in het midden
    var binnen;
    var cv = args.canvas;
    cv.strokeStyle = args.p2.color;
    for (var tick = 0; tick < args.p1.ticks; tick++) {
        var hoek = 360.0 * tick / args.p1.ticks;
        if (!(tick % args.p1.short)) {
            if (!(tick % args.p1.long)) {
                binnen = args.p2.rinside * args.p2.r10s;
                if (!(tick % args.p1.ptext)) {
                    this.doPolair(cv, function (cv, x, y) {
                        cv.font = args.p2.font;
                        cv.textAlign = 'center';
                        cv.strokeText(tick / args.p1.angledivider, x, y);
                    }, hoek, args.p2.rinside * args.p2.rtexts, undefined, args.m);
                }
            }
            else {
                binnen = args.p2.rinside * args.p2.r5s;
            }
        }
        else {
            binnen = args.p2.rinside;
        }
        cv.beginPath();
        this.moveToPolair(cv, hoek, binnen, args.m);
        this.lineToPolair(cv, hoek, args.p2.routside, args.m);
        cv.stroke();
    }
};
zeroisim.doPolair = function (cv, cb, a, r, obj, m, that) {
    cv.restore();
    cv.save();
    cv.translate(m.x, m.y);
    cv.rotate(a * this.pi_180);
    cb(cv, 0, -r * m.y, obj, that);
    cv.restore();
};
zeroisim.moveToPolair = function (cv, a, r, m) {
    var vv = zeroisim.pol2xy(a, r, m);
    cv.moveTo(vv.x, vv.y);
};
zeroisim.lineToPolair = function (cv, a, r, m) {
    var vv = zeroisim.pol2xy(a, r, m);
    cv.lineTo(vv.x, vv.y);
};
zeroisim.pol2xy = function (a, r, m) {
    return {
        x: m.x * (1 + r * (Math.sin(a * this.pi_180)))
        , y: m.y * (1 - r * (Math.cos(a * this.pi_180)))
    };
};
zeroisim.bin = function (txt) {
    var rc = 0;
    for (var ii = 0; ii < txt.length; ii++) {
        rc <<= 1;
        rc += parseInt(txt.substr(ii, 1));
    }
    return rc;
};
class CeditableTable {
	constructor(obj,att,firstrow,cb_edit,id){
		return this._objAsTableEditable(obj,att,firstrow,cb_edit,id);	//	todo er moeten custom edit dingen gemaakt worden a la launcher
		// return this;
		}
	lastAtsign(str){
		var rc = str.split('@');
		return rc.pop();
		}
	_parameterEdited(evt){
		console.log('CeditableTable:_parameterEdited(evt), '+evt.target.id+'='+evt.target.value);
		var idxs = evt.target.id.split('@');
		var expr = "this.obj";
		idxs.myforEach(function(el,idx,arr,that){
			if (zeroisim.isNumeric(el))
				expr += '['+el+']';
			else
				expr += '.'+el;
			},this);
		expr += '="'+evt.target.value+'";';	//	werkt goed maar niet bij een array
		//expr += '=JSON.parse('+evt.target.value+');';
		
		//var test = expr + '='+JSON.stringify(JSON.parse(expr))+';';
		//var test = expr + '='+JSON.stringify(JSON.parse(evt.target.value))+';';
		//expr += '='+evt.target.value+';';
		//expr += '='+JSON.parse(evt.target.value)+';';
		
		eval(expr);	//	dirty rotten maar ik weet niet hoe het anders moet
		if (undefined!=this.cb_edit)
			this.cb_edit(this.obj,idxs,evt.target.value);
		}	
	funTableRow(ii,value){
		var typ = typeof value;
		if (typeof value==='object'){
			return i.tr(
				[i.td(i.span(this.lastAtsign(ii)))
				,i.td(value)
				]);
			}
		else
			return i.tr(
				[i.td(i.span(this.lastAtsign(ii)))
				,i.td(i.input('',{type:'text',change:this._parameterEdited.bind(this),value:value,id:ii}))
				]);
		}
	_objAsTableEditable(obj,att,firstrow,cb_edit,id){	//	todo er moeten custom edit dingen gemaakt worden a la launcher
		this.obj = obj;
		this.cb_edit = cb_edit;
		return this._objAsTable(i,obj,att,firstrow,this.funTableRow.bind(this));
		}
	objAsTable(obj,att,firstrow){
		return this._objAsTable(h,obj,att,firstrow,function(ii,value){
			return h.tr(h.td(ii)+h.td(value));
			});
		}
	_objAsTable(hi,obj,att,firstrow,cb,jj){
		if (undefined==obj)	return '';
		if (undefined==jj)	jj = '';
		if (i==hi)
			var table=[];
		else
			var table='';
		if (undefined!=firstrow) {
			if (i==hi)
				var row=[];
			else
				var row='';
			for (var ii in firstrow){
				if (i==hi)
					row.push(hi.td(firstrow[ii]));
				else
					row += hi.td(firstrow[ii]);
				}
			if (i==hi)
				table.push(hi.tr(row));
			else
				table += hi.tr(row);
			for (ii in obj) {
				var row='';
				for (var jj in firstrow){
					if (i==hi)
						row.push(cb(ii,obj[ii][firstrow[jj]]));
					else
						row += cb(ii,obj[ii][firstrow[jj]]);
					}
				if (i==hi)
					table.push(hi.tr(row));
				else
					table += hi.tr(row);
				}
			}
		else {
			for (var ii in obj) {
				var value=obj[ii];
				if (typeof value==="function")	
					continue;
				if (typeof value==="object" && (undefined!=value.latdegmindec))	
					value = {lat: value.latdegmindec, lon: value.londegmindec};
				if (typeof value==="object")	
					// value = this._objAsTable(hi,value,att,firstrow,cb,ii+'@');	//	todo bij diepere nesting dan 2 gaat iets mis
					value = JSON.stringify(value);
					// if (Array.isArray(value))	value = this._objAsTable(hi,value,att,firstrow,cb,ii+'@');
				if (i==hi)
					table.push(cb(jj+ii,value));
				else
					table += cb(jj+ii,value);
				}
			}
		// att = zeroisim.merge_objects({id:jj},hi.attributes(att));
		return hi.table(table,att);
		}
	}	//	editableTable
	
/*	
zeroisim.objAsString= function(obj){
	var rc = '';
	for (var ii in obj){
		rc += 'ii='
		}
	return rc;
	}
*/	
	
	
	
/*	
zeroisim.lastAtsign=function(str){
	var rc = str.split('@');
	return rc.pop();
	}
zeroisim.parameterEdited=function(evt){
	console.log('parameterEdited(evt), '+evt.target.id+'='+evt.target.value);
	// this.currentTarget[evt.target.id] = evt.target.value;
	var idxs = evt.target.id.split('@');
	// var parameter = this.currentTarget;
	var parameter = this.obj;
	idxs.myforEach(function(el,idx,that,thing){
		parameter = parameter[el];
		});
	parameter = evt.target.value;
	if (undefined!=this.cb_edit)
		this.cb_edit(this.obj,idxs);
	}	
zeroisim.objAsTableEditable=function(obj,att,firstrow,cb_edit){	//	todo er moeten custom edit dingen gemaakt worden a la launcher
	this.obj = obj;
	this.cb_edit = cb_edit;
	return zeroisim._objAsTable(i,obj,att,firstrow,function(ii,value){
		var typ = typeof value;
		if (typeof value==='object'){
			return i.tr(
				// [i.td(i.span(ii))
				[i.td(i.span(zeroisim.lastAtsign(ii)))
				,i.td(value)
				]);
			}
		else
			return i.tr(
				[i.td(i.span(zeroisim.lastAtsign(ii)))
				// ,i.td(i.input('',{type:'text',change:cb_edit,value:value,id:ii}))
				,i.td(i.input('',{type:'text',change:zeroisim.parameterEdited.bind(this),value:value,id:ii}))
				]);
		});
	}
*/	
zeroisim.objAsTable = function (obj, att, firstrow) {
    return zeroisim.__objAsTable(h, obj, att, firstrow, function (ii, value) {
        return h.tr(h.td(ii) + h.td(value));
    });
};
zeroisim.__objAsTable = function (hi, obj, att, firstrow, cb, jj) {
    if (undefined == obj) return '';
    if (undefined == jj) jj = '';
    if (i == hi)
        var table = [];
    else
        var table = '';
    if (undefined != firstrow) {
        if (i == hi)
            var row = [];
        else
            var row = '';
        for (var ii in firstrow) {
            if (i == hi)
                row.push(hi.td(firstrow[ii]));
            else
                row += hi.td(firstrow[ii]);
        }
        if (i == hi)
            table.push(hi.tr(row));
        else
            table += hi.tr(row);
        for (ii in obj) {
            var row = '';
            for (var jj in firstrow) {
                if (i == hi)
                    row.push(cb(ii, obj[ii][firstrow[jj]]));
                else
                    row += cb(ii, obj[ii][firstrow[jj]]);
            }
            if (i == hi)
                table.push(hi.tr(row));
            else
                table += hi.tr(row);
        }
    }
    else {
        for (var ii in obj) {
            var value = obj[ii];
            if (typeof value === "function")
                continue;
            if (typeof value === "object" && (undefined != value.latdegmindec))
                value = { lat: value.latdegmindec, lon: value.londegmindec };
            if (typeof value === "object")
                value = this.__objAsTable(hi, value, att, firstrow, cb, ii + '@');
            // if (Array.isArray(value))	value = this.__objAsTable(hi,value,att,firstrow,cb,ii+'@');
            if (i == hi)
                table.push(cb(jj + ii, value));
            else
                table += cb(jj + ii, value);
        }
    }
    return hi.table(table, att);	// return table;
};
zeroisim.replaceNode = function (parentNode, newNode) {	//	todo is this a duplicate of setivalue
    var rc = parentNode.firstChild;
    while (parentNode.firstChild) {	// remove old html
        var test = parentNode.firstChild.innerHTML;
        parentNode.removeChild(parentNode.firstChild);
    }
    if (Array.isArray(newNode)) {
        newNode.myforEach(function (node) {
            parentNode.appendChild(node);
        });
    }
    else
        parentNode.appendChild(newNode);	//	set new html
    return rc;
};
zeroisim.isNumeric = function (n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
};
/*
    <style type="text/css">
        .container {width: 300px; height: 400px; margin-left: auto; margin-right: auto; margin-top: 100px; }
        .tabheader {display:table-cell; width:25%; border:1px solid #ccc; background-color:#DDD; vertical-align:middle; border-top-right-radius:5px; border-top-left-radius:5px; cursor:pointer; transition: 0.3s; }
        .tabheader:hover {background-color: #888;}
        .tabheader.active {background-color: transparent; border-bottom:none;}
        .tabcontent {padding: 10px 10px; border: 1px solid #ccc;box-sizing:border-box; border-top: none; height:calc(100% - 40px);} 
    </style>
		  
		document.body.appendChild(new CTabBook('myFirstBook',
				[{hdr:'Tab1',content:'Tab1 content'}
				,{hdr:'Tab2',content:'Tab2 content'}
				,{hdr:'Tab3',content:'Tab3 content'}
				,{hdr:'Tab4',content:'Tab4 content'}
				,{hdr:'Tab5'}
				],{classContainer:'container',classHeader:'tabheader',classContent:'tabcontent'}).node);
		tab pages are referrable as: 	getElementById('myFirstBook@c@x') x:[0,n>
*/
class CTabBook {
	constructor(id,tabs,classes){
		this.id = id;
		var rc = [];
		this.focusFuns = [];

        tabs.myforEach(function (tab, idx, rts, that) {
            rc.push(i.div(tab.hdr,
                {
                 //class: classes.classHeader,
                 class: idx==0?classes.classHeader + ' active':classes.classHeader
				,id:that.id+'@h@'+idx
				,click:that.openTab.bind(that)
				}
            ));

			that.focusFuns.push({onactivate:tab.onfocus,ondeactivate:tab.onblur});
			},this);
        
        rc = [i.div(rc, { class: classes.headerDivname })];
		tabs.myforEach(function(tab,idx,rts,that){
			if (0==idx){
                var style = '';	//	show first tab
                that.activate(idx);
				}
			else
				var style = 'display:none';
			rc.push(i.div((tab.content || h.nbsp),{id:that.contentId(idx),class:classes.classContent,style:style}));
			},this);
		this.node = i.div(rc,{class:classes.classContainer});
		return this;
		}
	contentId(idx){
		return this.id+'@c@'+idx;
		}
	lastAtsign(str){
		var rc = str.split('@');
		return rc.pop();
		}
	activate(idx){
		var onactivate = this.focusFuns[idx].onactivate;
		if (undefined!=onactivate){
			if (zeroisim.isFn(onactivate))
				onactivate.apply();
			}
		}
	deactivate(idx){
		var ondeactivate = this.focusFuns[idx].ondeactivate;
		if (undefined!=ondeactivate){
			if (zeroisim.isFn(ondeactivate))
				ondeactivate.apply();
			}
		}
	openTab(evt) {
		var chosenTab = evt.currentTarget;
		var idx = this.lastAtsign(evt.currentTarget.id);	
		var parent = evt.currentTarget.parentNode;
		var pparent = parent.parentNode;
		for (var ii=1; ii<pparent.childNodes.length; ii++){
			var test1 = pparent.childNodes[ii];
			var test2 = parent.childNodes[ii-1];
			if (idx != (ii-1))	// doe de blur
				this.deactivate(ii-1);
			pparent.childNodes[ii].style.display = "none";	//	hide all content
			parent.childNodes[ii-1].classList.remove("active");	//	hide all tabs
		}
				
		this.activate(idx);
		chosenTab.classList.add("active");	//	show choosen tab
		// this.focusFuns.push({onactivate:tab.onfocus,ondeactivate:tab.onblur});
		/* dit is nieuw, bekijk even of het goed is...
		var onactivate = this.focusFuns[idx].onactivate;
		if (undefined!=onactivate){
			if (zeroisim.isFn(onactivate))
				onactivate.apply();
			}
		*/

		// chosenTab.style.display = "block";	//	show choosen content
        
		document.getElementById(this.contentId(idx)).style.display = "block";	//	show choosen content
		}
};	//	CTabBook

/*	Typical usage of CSwipeBook
	var sb = new CSwipeBook('subcontainer',	//	<font style='font-size:180px'>
		[{content:i.div(i.font('a',{style:'font-size:180px'}),{style:'background-color:aliceblue;'})}
		,{content:i.div(i.font('b',{style:'font-size:180px'}),{style:'background-color:aqua;'})}
		,{content:i.div(i.font('c',{style:'font-size:180px'}),{style:'background-color:aquamarine;'})}
		,{content:i.div(i.font('d',{style:'font-size:180px'}),{style:'background-color:beige;'})}
		,{content:i.div(i.font('e',{style:'font-size:180px'}),{style:'background-color:bisque;'})}
		,{content:i.div(i.font('f',{style:'font-size:180px'}),{style:'background-color:blanchedalmond;'})}
		,{content:i.div(i.font('g',{style:'font-size:180px'}),{style:'background-color:blue;'})}
		,{content:i.div(i.font('h',{style:'font-size:180px'}),{style:'background-color:blueviolet;'})}
		,{content:i.div(i.font('i',{style:'font-size:180px'}),{style:'background-color:brown;'})}
		,{content:i.div(i.font('j',{style:'font-size:180px'}),{style:'background-color:burlywood;'})}
		]);
	zeroisim.replaceNode(document.getElementById('container'),sb.node());
*/	
class CSwipeBook {
	constructor(id,pages){
		this.id = id;
		this.pages = [];
		pages.myforEach(function(rawpage,idx,rts,that){
			var page = rawpage.content;
			page.style.width = window.innerWidth + "px";
			page.style.height = '100%';
			page.style.float = 'left';
			that.pages.push(page);
			},this);
		this.sub = i.div(this.pages,
			{id:'_swipe@'+id
			,style:'position:absolute; height:100%;'
			});
		
		this.sub.style.width = pages.length * (screen.width+50) + "px"; //extra pixel speling per .page + 50, bij onvoldoende speling breken de pages uit hun flow
		this.sub.offsetX = 0;

		this._hSwipe = this.Swipe.bind(this);
		this._hstopSwipe = this.stopSwipe.bind(this);

		this.sub.addEventListener("mousedown", this.hMousedown.bind(this),false);
		this.sub.addEventListener("touchstart", this.hTouchstart.bind(this),false);
		//onderstaande events worden beiden afgevuurd bij schermrotatie, rotateHandler wordt dus 2x uitgevoerd, weglaten van 1 van de 2 werkt niet
		window.addEventListener("orientationchange", this.hOrientationchange.bind(this),false);
		window.addEventListener("resize", this.hResize.bind(this),false);
		}
	node(){
		return this.sub;
		}
	hMousedown(e) {
		e.preventDefault();
		this.sub.startX = e.clientX;
		window.document.addEventListener("mousemove", this._hSwipe, false);
		window.document.addEventListener("mouseup", this._hstopSwipe, false);
		}	
	hTouchstart(e) {
		e.preventDefault();
		this.sub.startX = e.changedTouches[0].pageX;
		window.document.addEventListener("touchmove", this._hSwipe, false);
		window.document.addEventListener("touchend", this._hstopSwipe, false);
		}
	hOrientationchange(e){
		this.rotateHandler(e);
		}
	hResize(e){ 
		console.log('hResize(e)');
		this.rotateHandler(e);
		}
	rotateHandler(e) {
		this.pages.myforEach(function(page,idx,rts,that){
			page.style.width = window.innerWidth + "px";
			},this);
		this.sub.offsetX = this.p * window.innerWidth;
		this.sub.style.left = this.sub.offsetX.toString() + "px"
		}
	Swipe(e) {
		if (e.clientX !== 0) {
			var x = e.clientX || e.changedTouches[0].pageX;
			var dx = x - this.sub.startX + this.sub.offsetX;
			this.sub.style.left = dx + "px";
			}
		}
	stopSwipe(e){
		this.sub.offsetX = this.sub.offsetLeft;
		this.p = Math.round(this.sub.offsetX / window.innerWidth);
		if (this.p <= -this.pages.length)	this.p++;
		if (this.p > 0) 							this.p=0 ;
		this.sub.offsetX = this.p * window.innerWidth;
		this.sub.style.left = this.sub.offsetX.toString() + "px"
		// var ori = "";
		// window.innerWidth > window.innerHeight ? ori = "landscape" : ori = "portrait";
		// console.log(ori + "  p: " + p + "  w: " + window.innerWidth + "  h:" + window.innerHeight + "  offset: " + this.sub.offsetX);
		// console.log("posXY1: " + this.sub.offsetLeft + "," + this.sub.offsetTop + " posXY2: " + (this.sub.offsetLeft + this.sub.offsetWidth) + ", " + (this.sub.offsetTop + this.sub.offsetHeight));
		// console.log("this.sub.offsetX after swipe: " + this.sub.offsetX);
		window.document.removeEventListener("touchmove", this._hSwipe, false);
		window.document.removeEventListener("mousemove", this._hSwipe, false);
		window.document.removeEventListener("mouseup", this._hstopSwipe, false);
		}
	};	//	CSwipeBook 
	
zeroisim.HEX=function(n){	//	Convert integer to hex digit literals.  
	var nibbles = ['0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'];
	var rc = '';
	rc	+= nibbles[(n)>>28 & 0xF];
	rc	+= nibbles[(n)>>24 & 0xF];
	rc	+= nibbles[(n)>>20 & 0xF];
	rc	+= nibbles[(n)>>16 & 0xF];
	rc	+= nibbles[(n)>>12 & 0xF];
	rc	+= nibbles[(n)>> 8 & 0xF];
	rc	+= nibbles[(n)>> 4 & 0xF];
	rc	+= nibbles[(n)     & 0xF];
	return rc;
  }
zeroisim.busTX=function(key,data,wid){	//	ease of use
	nmeadefs[key][0].eachRxcb.go({key:key,data:data},wid);
	} 
/* 
//	Convert integer to decimal digit literals.  
#define DEC(n)                   \
  ('0' + (((n) / 10000000)%10)), \
  ('0' + (((n) / 1000000)%10)),  \
  ('0' + (((n) / 100000)%10)),   \
  ('0' + (((n) / 10000)%10)),    \
  ('0' + (((n) / 1000)%10)),     \
  ('0' + (((n) / 100)%10)),      \
  ('0' + (((n) / 10)%10)),       \
  ('0' +  ((n) % 10))
*/	
/****************	
		arcPolair: function(a,r,radius,p1,p2){
			var vv = zeroisim.pol2xy(a,r,{x:this.r_half,y:this.r_half});
			this.cv.arc(vv.x,vv.y,radius,p1,p2);
			},
		strokeTextPolair: function(cv,text,a,r){
			cv.save();
			cv.translate(this.midden.y,this.midden.x);
			cv.rotate( a * this.pi_180);
			cv.textAlign = 'center';
			cv.strokeText(text,0,-r*this.midden.y);
			cv.restore();
			},
		xy2pol: function(xy){
			rc = 	{	a:	Math.atan((xy.x-this.r_half) / (xy.y-this.r_half)) / this.pi_180
					,	r:	Math.sqrt((xy.x-this.r_half)*(xy.x-this.r_half)+(xy.y-this.r_half)*(xy.y-this.r_half)) / this.r_half * this.range
					};
			if ((xy.y-this.r_half) > 0)
				rc.a = 180-rc.a;
			else {
				rc.a = 360-rc.a;
				if (rc.a > 360)	rc.a -= 360;
				}
			return rc;
			},
zeroisim.asTable3 = function(obj,att,firstrow){	//	zelfde als asTable2 maar swap(col,row) todo
	var table='';
	if (undefined!=firstrow) {
		var row='';
		for (var ii in firstrow){	//	todo myforEach
			row += h_td(firstrow[ii]);
			}
		table += h_tr(row);	
		for (ii in obj) {
			var row='';
			for (var jj in firstrow){
				row += h_td(obj[ii][firstrow[jj]]);
				}
			table += h_tr(row);	
			}
		}
	else {	
		for (var ii in obj) {
			var row='';
			for (var jj in obj[ii]){
				row += h_td(obj[ii][jj]);
				}
			table += h_tr(row);	
			}
		}
	return h_table(table,att);	
	}	
*****************/
