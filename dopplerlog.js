//	dopplerlog.js
// todo bruine en groene pijlen voor grond en water
//	todo op de tab_example manier en maak het dan een class zodat er twee van gemaakt kunnen worden, een grond en een watersnelheid
'use strict';
var createDopplerLog = function () {
	return {
		init: function(id_canvas){
			this.id_canvas = id_canvas;
			window.addEventListener("load", this.init2.bind(this), false);
			}
		,init2: function(){
			var canvas = document.getElementById(this.id_canvas);
			if (undefined==canvas) return;	//	no dopplerlog possible
			//console.log('createDopplerLog.init()');
			this.cv = canvas.getContext("2d")
			this.midden = {x: canvas.width/2, y:canvas.height/2};
			this.cv.textAlign="center";
			this.cv.fillStyle = '#000';
			this.arrow =
				{x:[1  ,1  ,2  , 2  , 1  , 1  ]
				,y:[1.2,0.6,0.6,-0.6,-0.6,-1.2]
				};
			ship0.doppler = {};
			this.mode = 'GROUND';
			canvas.addEventListener("click", this.canvasClick.bind(this), false);
			cbRxNmea('VBW',function(rxd,that){
				if (undefined==rxd.data)	return;
				if ('A'==rxd.data.validwat) {
					ship0.doppler.longwat = rxd.data.longwat;
					ship0.doppler.transwat = rxd.data.transwat;
					}
				if ('A'==rxd.data.statgrd) {
					ship0.doppler.longgrd = rxd.data.longgrd;
					ship0.doppler.transgrd = rxd.data.transgrd;
					}
				if ('A'==rxd.data.statsterntransw) {
					ship0.doppler.sterntransw = rxd.data.sterntransw;
					}
				if ('A'==rxd.data.statsterntransg) {
					ship0.doppler.sterntransg = rxd.data.sterntransg;
					}
				that.draw(ship0);
				},this);
			}
		,canvasClick: function(evt){
			if (this.mode=='WATER')
				this.mode = 'GROUND';
			else if (this.mode=='GROUND')
				this.mode = 'WATER';
			}
		,draw: function(ship) {
			this.cv.clearRect(0,0,2*this.midden.x,2*this.midden.y);
			this.cv.save();
			this.cv.fillStyle = 'rgba(0xB,0xB,0xB, 1.0)';
			this.cv.fillStyle = '#BBB';
			this.cv.fillRect(0,0,2*this.midden.x,2*this.midden.y);
			this.cv.translate(this.midden.x,this.midden.y);

			if (this.mode=='WATER'){
				this.cv.fillStyle = '#00F';	//	blue for water, brown for ground
				var trans		= ship.doppler.transwat		;
				var longs		= ship.doppler.longwat		;
				var sterntrans	= ship.doppler.sterntransw	;
				}
			else {	//	groundspeed
				this.cv.fillStyle = '#743';	//	blue for water, brown for ground
				var trans		= ship.doppler.transgrd		;
				var longs		= ship.doppler.longgrd		;
				var sterntrans	= ship.doppler.sterntransg	;
				}
			this.cv.font = '10px sans-serif';
			var YY = -100;	//	todo query font height
			this.cv.fillText(this.mode,0,YY);				YY+=45;
			this.cv.font = '40px sans-serif';
			this.cv.fillText(Math.abs(trans)		 ,0,YY);	YY+=75;
			this.cv.fillText(Math.abs(longs)		 ,0,YY);	YY+=75;
			this.cv.fillText(Math.abs(sterntrans),0,YY);	
			if (longs == 0) {}	//	niks tekenen
			else if (longs > 0) {
				this.drawArrow('#FFF',-80,-28,1,1,+Math.PI/2);	//	forward speed
				this.drawArrow('#FFF',+80,-28,1,1,+Math.PI/2);	//	forward speed
				}
			else {
				this.drawArrow('#F00',-80,+28,1,1,-Math.PI/2);	//	backward speed
				this.drawArrow('#F00',+80,+28,1,1,-Math.PI/2);	//	backward speed
				}

			if (trans == 0){}	//	niks tekenen
			else if (trans > 0)
				this.drawArrow('#0F0',+100,-76,-1,1);	//	starboard focsel, mirrored horizontal
			else
				this.drawArrow('#F00',-100,-76, 1,1);	//	port focsel

			if (sterntrans == 0){}	//	niks tekenen
			else if (sterntrans > 0)
				this.drawArrow('#0F0',+100,+76,-1,1);	//	starboard poop, mirrored horizontal
			else
				this.drawArrow('#F00',-100,+76, 1,1);	//	port poop

			this.cv.restore();
			}
		,drawArrow: function(color,x,y,sx,sy,rotate){
			var thick = 5;
			this.cv.save();
			this.cv.translate(x,y);
			this.cv.scale(sx,sy);
			if (undefined!=rotate)
				this.cv.rotate(rotate);
			var scale = 20;
			this.cv.beginPath();
			this.cv.strokeStyle = color;
			this.cv.lineWidth = thick;
			this.cv.moveTo(0,0);
			for (var ii=0; ii<6; ii++){
				this.cv.lineTo(20*this.arrow.x[ii],20*this.arrow.y[ii]);
				}
			this.cv.closePath();
			this.cv.stroke();
			this.cv.restore();
			}
		};
	};
