//	engine_panel.js
// teken allerlei instrument uitlezingen
//	todo doe op de tab_example manier
// todo op de instrument manier, instrumenten zijn stukjes canvas of andere divs die op allerlei paginas' geplakt kunnen worden of los startbaar
'use strict';
var createEnginePanel = function() {
	return {
		 hasFocus: false
		,onfocus:function(){	
			this.hasFocus=true;
			}
		,onblur:function(){	
			this.hasFocus=false;	
			}
		,init: function(id_canvas){
			this.id_canvas = id_canvas;	
			window.addEventListener("load", this.init2.bind(this), false);	
			},
		init2: function(){
			// console.log('createEnginePanel.init()');
			var canvas = document.getElementById(this.id_canvas);
			if (undefined==canvas)	return;	//	no enginepanel
			this.cv = canvas.getContext("2d")
			this.midden = {x: canvas.width/2, y:canvas.height/2};
			this.cv.textAlign ="center";
			this.cv.fillStyle = '#000';
			this.cv.font = '20px sans-serif';
			cbRxNmea('TRD',function(rx,that){	//	todo also TRC
				if (undefined==rx.data)	return;
				var not = rx.data.not;	//	number of thruster
				if (undefined==ship0.eng[not])	ship0.eng[not] = {rpm:0, pitch:0};	//	todo uitlaattemp, inlaattemp, oliedruk, ...
				ship0.eng[not].rpm = rx.data.rpm;
				ship0.eng[not].pitch = rx.data.pitch;
				that.draw(ship0);
				},this);
			//cbRxNmea('RPM',function(rx){	
			},
		draw: function(ship) {
			// if (!hasFocus)	return;
			this.cv.clearRect(0,0,2*this.midden.x,2*this.midden.y);
			this.cv.save();
			this.cv.fillStyle = 'rgba(125,125,125, 1.0)';
			this.cv.translate(this.midden.x,this.midden.y);
			this.cv.fillStyle = '#000';
			var YY = -40;	//	todo query font height
			ship.eng.myforEach
				(function(engine,nengine,engines,p){
					if (undefined!=engine)	p.cv.fillText(nengine+': rpm='+engine.rpm+' pitch='+engine.pitch,0,p.YY);		
					p.YY+=25;	//	dit lukt waarschijnlijk niet, oplossen met this.
					}
				,{ship:ship,cv:this.cv, YY:YY}
				);
			this.cv.restore();
			},
		};
	};