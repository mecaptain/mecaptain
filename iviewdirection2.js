//	iviewdirection2.js
//	todo nog een shipshape in het midden
'use strict';

class CViewdirGizmo {
	constructor(width,height,settings){
		settings = settings || {};
		this.detectBrowser();
		this.wh = {w:width,h:height};

		this.nodeHeadingSettings = 
			{radius:			this.or(settings.hdgRadius,40)
			,startAngle:	this.or(settings.hdgStartAngle,-10)
			,endAngle:		this.or(settings.hdgEndAngle,10)
			
			,stroke:			this.or(settings.hdgStrokeColor,'#f00')
			,strokewidtht:	this.or(settings.hdgStrokeWidth,20)
			,strokeopacity:this.or(settings.hdgStrokeOpacity,.3)
			};
		this.nodeSegmentSettings = 
			{radius_pct:	this.or(settings.segRadius,90)
			,startAngle:	this.or(settings.segStartAngle,-5)
			,endAngle:		this.or(settings.segEndAngle,5)
			
			,stroke:			this.or(settings.segStrokeColor,'#0f0')
			,strokewidtht:	this.or(settings.segStrokeWidth,20)
			,strokeopacity:this.or(settings.segStrokeOpacity,.6)
			};
		this.nodeSVG = i.svg('',{id:'svg_viewdir2',class:'gizmosvg',style:'position:relative;width:'+width+'px;height:'+height+'px;left:0;top:0;border:0px;'});
		this.nodeHeading = i.path('',{id:'gizmopath',class:'arc',d:'M 0 0 A 0 0 0 0 0 0 0',fill:'none',stroke:this.nodeHeadingSettings.stroke,'stroke-width':this.nodeHeadingSettings.strokewidtht,'stroke-opacity':this.nodeHeadingSettings.strokeopacity});
		// this.nodeLine = i.line('',{id:'gizmoline',x1:0,y1:-140,x2:0,y2:-300,stroke:'#f00','stroke-width':2,'stroke-opacity':.6});
		// this.nodeText = i.text('000'+h.deg,{id:"gizmotxt",x:0,y:0,fill:"#F00",selectable:"off"});
		this.nodeSegments = [];
		this.segNumber = settings.segNumber;
		for (var ii=0; ii<this.segNumber; ii++){
			this.nodeSegments[ii] = (i.path('',
				{id:'segment_'+parseFloat(360.0*parseFloat(ii)/parseFloat(this.segNumber)).toString()
				,class:'arc'
				,d:'M 0 0 A 0 0 0 0 0 0 0'
				,fill:'none'
				,stroke:this.nodeSegmentSettings.stroke
				,'stroke-width':this.nodeSegmentSettings.strokewidtht
				,'stroke-opacity':this.nodeSegmentSettings.strokeopacity
				}
				));			
			this.nodeSVG.appendChild(this.nodeSegments[ii]);
			if (0!=settings.segmentButtons)
				this.nodeSegments[ii].addEventListener("mousedown",this.mouseDown.bind(this));
			}
		
		this.nodeSVG.appendChild(this.nodeHeading);
		// this.nodeSVG.appendChild(this.nodeLine);
		// this.nodeSVG.appendChild(this.nodeText);
				
		if (0!=settings.mouseMoves)
			this.nodeHeading.addEventListener("mouseover",this.mouseOverGizmoRotation.bind(this));
		this._mouseDownGizmoRotation	= this.mouseDownGizmoRotation.bind(this);
		this._mouseMoveGizmoRotation	= this.mouseMoveGizmoRotation.bind(this);
		this._mouseUpGizmoRotation		= this.mouseUpGizmoRotation.bind(this);

		this.rotation = 0;
		if (undefined!=this.cb_onRotate) 
			this.cb_onRotate(this.rotation);

		this.draw(this.rotation,width/2,height/2);
		return this;
		}
	svg(){
		return this.nodeSVG;
		}
	or(firstOption,secondOption){
		return (undefined!=firstOption)? firstOption: secondOption;
		}
	detectBrowser(){
		if (navigator.userAgent.indexOf('Edge') != -1){//MS Edge
			this.browser = 'edge';
			}
		else if (navigator.userAgent.indexOf('Firefox') != -1 && parseFloat(navigator.userAgent.substring(navigator.userAgent.indexOf('Firefox') + 8)) >= 3.6){//Firefox
			this.browser = 'ff';
			}
		else if (navigator.userAgent.indexOf('Chrome') != -1 && parseFloat(navigator.userAgent.substring(navigator.userAgent.indexOf('Chrome') + 7).split(' ')[0]) >= 15){//Chrome
			this.browser = 'chrome';
			}
		else if(navigator.userAgent.indexOf('Safari') != -1 && navigator.userAgent.indexOf('Version') != -1 && parseFloat(navigator.userAgent.substring(navigator.userAgent.indexOf('Version') + 8).split(' ')[0]) >= 5){//Safari
			this.browser = 'safari';
			}
		else {
			console.log('unsupported browser');
			}
		}
	resize(width,height){
		console.log('CViewdirGizmo resize('+width+','+height+')');
		this.nodeSVG.style.width = width+'px';
		this.nodeSVG.style.height = height+'px';
		this.wh.w=width;
		this.wh.h=height;
		this.draw(this.rotation,width/2,height/2);
		}
	polarToCartesian(centerX, centerY, radius, angleInDegrees) {
		var angleInRadians = (angleInDegrees-90) * Math.PI/180.0;
		return	{ x: centerX + (radius * Math.cos(angleInRadians))
					, y: centerY + (radius * Math.sin(angleInRadians)) 
					};
		}
	svgArc(x, y, radius, startAngle, endAngle, anticlockwise){
		var start= this.polarToCartesian(x, y, radius, endAngle);
		var end	= this.polarToCartesian(x, y, radius, startAngle);
		var arcSweep = (endAngle - startAngle <= 180)? "0" : "1";
		return ["M", start.x, start.y, "A", radius, radius, 0, arcSweep, 0, end.x, end.y].join(" ");
		}
	draw(direction,x,y){
		this.xy = {x:x,y:y};
		this.rotation = direction;
		this.nodeHeading.setAttribute("d", this.svgArc
			(x, y
			// ,this.nodeHeadingSettings.radius
			,this.nodeHeadingSettings.radius * Math.min(this.wh.h,this.wh.w) / 280 - this.nodeHeadingSettings.strokewidtht/2
			,this.nodeHeadingSettings.startAngle
			,this.nodeHeadingSettings.endAngle
			));
		// this.nodeLine.setAttribute("x1",x);			this.nodeLine.setAttribute("y1",y-140);
		// this.nodeLine.setAttribute("x2",x);			this.nodeLine.setAttribute("y2",y-300);

		for (var ii=0; ii<12; ii++){
			this.nodeSegments[ii].setAttribute("d"
				, this.svgArc
					(x, y
					,this.nodeSegmentSettings.radius_pct * Math.min(this.wh.h,this.wh.w) / 200
					,this.nodeSegmentSettings.startAngle
					,this.nodeSegmentSettings.endAngle
					));
			var transform = "rotate(" + parseFloat(ii)*30.0 + ", "+this.xy.x+", "+this.xy.y+")";
			this.nodeSegments[ii].setAttribute("transform", transform);
			}
		return this.rotate(this.rotation);
		}
	rotate(direction){
		this.rotation = direction;
		var transform = "rotate(" + direction + ", "+this.xy.x+", "+this.xy.y+")";
		this.nodeHeading.setAttribute("transform", transform);
		// this.nodeLine.setAttribute("transform", transform);
		if (this.rotation<  0) this.rotation+=360;
		if (this.rotation>360) this.rotation%=360;
		return this;
		}
	onRotate(cb){
		this.cb_onRotate = cb;
		return this;
		}
	onExit(cb){
		this.cb_onExit = cb;
		return this;
		}
	mouseDown(evt){
		// console.log('mouseDown on segment: '+evt.currentTarget.id);
		this.rotate(parseFloat(evt.currentTarget.id.split('_')[1]));
		if (undefined!=this.cb_onRotate) 
			this.cb_onRotate(this.rotation);
		}
	mouseClickExit(evt){
		this.mouseUpGizmoRotation(evt);		// uninstall event handlers
		this.mouseUpGizmoPosition(evt);		// uninstall event handlers
		if (undefined!=this.cb_onExit)
			this.cb_onExit(this.rotation,this.xy.x,this.xy.y);
		}
	mouseOverGizmoRotation(evt){		// console.log('mouseOverGizmoRotation');
		this.nodeHeading.addEventListener("mousedown", this._mouseDownGizmoRotation, false);
		}
	mouseDownGizmoRotation(evt){		// console.log('mouseDownGizmoRotation');
		this.nodeHeading.addEventListener("mousemove", this._mouseMoveGizmoRotation, false);
		this.nodeHeading.addEventListener("mouseup", this._mouseUpGizmoRotation, false);
		}
	mouseUpGizmoRotation(evt){		// console.log('mouseDownGizmoRotation');
		this.nodeHeading.removeEventListener("mousemove", this._mouseMoveGizmoRotation, false);
		this.nodeHeading.removeEventListener("mouseup", this._mouseUpGizmoRotation, false);
		}
	mouseMoveGizmoRotation(evt){
		if (undefined!=this.cb_onRotate) {	//	if no callback, do not rotate
			var XY = {X:evt.offsetX,Y:evt.offsetY};		
			switch (this.browser){
				case 'ff':
					// todo dit is nog niet goed
					var angle = 90 + (180 / Math.PI) * Math.atan2
						(XY.Y - this.xy.y
						,XY.X - this.xy.x
						)+this.rotation;
					break;
				case 'edge':
				case 'safari':
					var angle = 90 + (180 / Math.PI) * Math.atan2
						(XY.Y - this.xy.y
						,XY.X - this.xy.x
						)+this.rotation;
					break;
				case 'chrome':
					var angle = 90 + (180 / Math.PI) * Math.atan2
						(XY.Y - this.xy.y
						,XY.X - this.xy.x
						);
					break;
				}
			this.rotate(angle);
			this.cb_onRotate(this.rotation);
			}
		}
	onPosition(cb){
		this.cb_onPosition = cb;
		return this;
		}
	mouseOverGizmoPosition(evt){		// console.log('mouseOverGizmoPosition');
		// this.nodePosition.addEventListener("mousedown", this._mouseDownGizmoPosition, false);
		}	
	mouseDownGizmoPosition(evt){		// console.log('mouseDownGizmoPosition');
		// this.nodePosition.addEventListener("mousemove", this._mouseMoveGizmoPosition, false);
		// this.nodePosition.addEventListener("mouseup", this._mouseUpGizmoPosition, false);
			}
	mouseUpGizmoPosition(evt){			// console.log('mouseDownGizmoPosition');
		// this.nodePosition.removeEventListener("mousemove", this._mouseMoveGizmoPosition, false);
		// this.nodePosition.removeEventListener("mouseup", this._mouseUpGizmoPosition, false);
		}
	mouseMoveGizmoPosition(evt){		// console.log('mouseMoveGizmoPosition');
		if (undefined!=this.cb_onPosition){
			this.draw(this.rotation,evt.offsetX,evt.offsetY);
			this.cb_onPosition(evt.offsetX,evt.offsetY);
			}
		}
	};	//	CViewdirGizmo

class CInstrument_viewdir2 extends CInstrument {
	constructor(obj,exposedParms,active,idx){
		super(idx,obj);
		if (undefined!=exposedParms)
			this.exposedParms = exposedParms;
		else
			this.exposedParms =
				{mouseMoves:1
				,segmentButtons:1
				,camids:[0,1,2]
				,globalAlpha:1.0
				,shapeStrokeWidth: 10	
				,hdgStrokeColor:'#00f'	
				,hdgStrokeWidth:130
				,hdgStrokeOpacity:0.3
				,hdgRadius:			108	
				,hdgStartAngle:	-60
				,hdgEndAngle:		 60

				,segNumber:12
				,segStrokeColor:'#0f0'	
				,segStrokeWidth:  60
				,segStrokeOpacity:0.7
				,segRadius:			 85
				,segStartAngle:	-13
				,segEndAngle:		 13

				};
				
		this.active = (undefined==active)? false: active;
		var wh = {w:this.div.offsetWidth,h:this.div.offsetHeight};
		if (this.active){
			this.gizmo = new CViewdirGizmo(wh.w,wh.h,this.exposedParms)
				.onRotate(this.rotate.bind(this))
				.onPosition(this.move.bind(this))
				.onExit(this.sendPostionReady.bind(this))
				;
			zeroisim.replaceNode(this.div,this.gizmo.svg());
			// this.div.appendChild(this.gizmo.svg());
			this.draw();
			}
		}
	destruct(){
		// todo this.gizmo.destruct();
		}
	resize(width,height){
		console.log('viewdirection resize('+width+','+height+')');
		this.gizmo.resize(width,height);
		}
	rotate(direction){
		// console.log('CInstrument_viewdir2 rotate:'+direction);
		this.exposedParms.camids.myforEach(function(camid,idx,arr,that){
			// console.log('CInstrument_viewdir2 rotate:'+JSON.stringify({camid:camid,direction:direction}));
			zeroisim.busTX('CAM',{camid:camid,rotY:direction},that.wid);
			},this);
		}
	move(x,y){
		console.log('rotate:');
		}
	sendPostionReady(){}
	draw(ship) {
		}
	};

CInstrument.addInstrumentToSettings('./iviewdirection2.js',		'viewdir2',		'div',		CInstrument_viewdir2,		'shows and defines where you are looking at');
