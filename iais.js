//	iais.js

'use strict';

class CInstrument_ais extends CInstrument {
	constructor(obj,exposedParms,active,idx){
		super(idx,obj);
		var canvas = obj.querySelector("canvas");
		this.cv = canvas.getContext("2d")
		this.midden = {x: canvas.width/2, y:canvas.height/2};
		if (undefined!=exposedParms)
			this.exposedParms = exposedParms;
		else
			this.exposedParms = //	todo deze moeten door de conning designer kunnen worden gezet, nier overrulen hier dan
				{globalAlpha:					1.0
				,colorBackground:			'rgba(200,200,200, 0.1)' //	gray	'#BBB'	
				,colorRings:				'rgba(  0,  0,220, 0.5)' //	'#00F'	blue
				,colorCross:				'rgba(  0,220, 70, 0.5)' //	'#0F0'	green
				,arrow:
					{x:[1  ,1  ,2  , 2  , 1  , 1  ]
					,y:[1.2,0.6,0.6,-0.6,-0.6,-1.2]
					}
				};	
		// this.cv.globalAlpha = this.exposedParms.globalAlpha;
		// setInterval(this.draw.bind(this), 500);
		this.draw();
		}
	resize(w,h){
		}
	draw() {
		// if (!this.hasFocus)	return;
		// this.course=course;
		// this.lat=lat;
		// this.lon=lon;
		
		// this.cv.clearRect(0,0,2*this.midden.x,2*this.midden.y);
		this.cv.save();
		this.cv.fillStyle = this.exposedParms.colorBackground;	this.cv.fillRect(0,0,2*this.midden.x,2*this.midden.y);
		// this.cv.strokeStyle = this.exposedParms.colorBackground;
		// this.cv.clearRect(0, 0, 2*this.midden.x, 2*this.midden.y);
		//	draw a cross
		this.cv.strokeStyle = this.exposedParms.colorCross;
		this.cv.beginPath();
		this.cv.moveTo(this.midden.x,2*this.midden.y);			this.cv.lineTo(this.midden.x,0);
		this.cv.moveTo(2*this.midden.x,this.midden.y);			this.cv.lineTo(0,this.midden.y);
		this.cv.stroke();

		var rings=[1/3, 2/3, 3/3];
		this.cv.strokeStyle = this.exposedParms.colorRings;
		for (var ring in rings) {
			this.cv.beginPath();
			this.cv.arc(this.midden.x, this.midden.y, this.midden.x * rings[ring], 0, 2 * Math.PI);
			this.cv.stroke();
			}
		// for (var aistarget in this.targets){
			// this.aisplot(this.targets[aistarget]);
			// }
		}
	}
CInstrument.addInstrumentToSettings
	('./iais.js'
	,'ais'
	,'canvas'
	,CInstrument_ais
	,'shows polar diagram of positions of objects from which an AIS message over VHF is received'
	);
	