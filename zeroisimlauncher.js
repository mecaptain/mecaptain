// zeroisimlauncher.js
//	(c) Bert Tuijl
//	constructs url parameters to start 0isim
//	todo radiobox
var config = 
	{mydocs:'./img/'
	,machines:
		['ws://192.168.20.13:12120'
		,'ws://bert-toshsat:12120'
		,'ws://lt015:12120'
		,'ws://bert-toshsat:12121'
		,'ws://127.0.0.1:12120'
		,'ws://127.0.0.1:80'
		,'ws://127.0.0.1:12121'
		,'ws://ws053:12120'
		]
	,send_sentences:	//	id & Hz
		[new nmea_send('ETL',1)
		,new nmea_send('GLL',1)
		,new nmea_send('HDT',1)
		,new nmea_send('MSO',1)
		,new nmea_send('RMC',1)
		,new nmea_send('ROT',3)
		,new nmea_send('RSA',5)
		,new nmea_send('THS',1)
		,new nmea_send('VDM1',1)
		,new nmea_send('VDM5',0.05)
		,new nmea_send('VBW',1)
		,new nmea_send('VTG',10)
		]
	// ,qrcode_url_prefix:'http://e-genda.nl/zeroisim/'	//	deze site gaat waarschijnlijk niet op deze computer draaien	
	,qrcode_url_prefix:'https://mecaptain.com/zeroisim/'	//	deze site gaat waarschijnlijk niet op deze computer draaien	
	,buttonstyle:
		['background-color:#8AA'
		,'xborder: 0px solid black'
		,'height: 65px'
		,'white-space:nowrap'
		,'text-align:center'
		,'-moz-border-radius: 8px'
		,'-webkit-border-radius: 8px'
		,'font-family: Arial, Helvetica, sans-serif'
		,'font-size: 1.9em'
		,'color: #EEE'
		,'min-width: 80px'
		,'border: 3px solid #FFF'
		,'margin: 0px'
		,'padding: 1px'
		,'unselectable: on'
		,'-webkit-user-select: none'	// Safari
		,'-moz-user-select: none'		// Firefox
		,'-ms-user-select: none'		// IE10+/Edge
		,'user-select: none'				// Standard
		]
	};	
var mijnlinkss = {
	rpc:
		{flyover:'just start the zeroisim and have it wait for instructions (like create an outside view)'
		,params:[
			{text:'websocket server IP address',urlparam:'ws_addr',dropdown:{list:config.machines}},
			]
		,script:['./nmea_scripts/rpc.js']
		,tabmask:	settings.tabmasks.show_env
		},
	location:
		{flyover:'ecdis base upon the gps of your device'
		,params:[
			// {text:'websocket server IP address',urlparam:'ws_addr',dropdown:{list:config.machines}},
			]
		// ,script:['./nmea_scripts/rpc.js']
		,tabmask:	settings.tabmasks.show_ecdis
					|	settings.tabmasks.show_geoloc
					|	settings.tabmasks.show_kompas
		},
	ControlBoxVSTEP:
		{flyover:'function for VSTEP controlbox'
		,params:[
			{text:'websocket conntrol',	urlparam:'ws_controladdr',dropdown:{list:config.machines}},
			{text:'websocket nmea',			urlparam:'ws_addr',dropdown:{list:config.machines}},
			{text:'Nautis NMEA output UDP ip:port',urlparam:'listen_addr',verstek:'udp://192.168.20.13:8888'},
			{text:'Nautis NMEA input UDP port',urlparam:'talk_port',verstek:'8011'},
			{text:'conning',verstek:'C:/Users/bert/Downloads/myConning (6).json',urlparam:'conning'},	//	todo filedialog
			]
		,script:['./nmea_scripts/nautis.js']
		,tabmask:	settings.tabmasks.show_ecdis
					|	settings.tabmasks.show_env
					|	settings.tabmasks.show_conningdesign
		},
	ConningDesigner:
		{flyover:'design a nice conningscreen'
		,params:[
			// {text:'websocket conntrol',	urlparam:'ws_controladdr',dropdown:{list:config.machines}},
			// {text:'websocket nmea',			urlparam:'ws_addr',dropdown:{list:config.machines}},
			// {text:'Nautis NMEA output UDP ip:port',urlparam:'listen_addr',verstek:'udp://192.168.20.13:8888'},
			// {text:'Nautis NMEA input UDP port',urlparam:'talk_port',verstek:'8011'},
			]
		,tabmask:zeroisim.bin('1000000000000000000000000')	//	see settings.js	nautis
		},
	RobsArduino:
		{flyover:'Act as HILI'
		,params:[
			{text:'websocket server IP address',urlparam:'ws_addr',dropdown:{list:config.machines}},
			{text:'IP Arduino',urlparam:'rob_remote_ip_port',verstek:'192.168.66.11:8888'},
			{text:'Arduino talk adres',urlparam:'listen_addr',verstek:'udp://192.168.66.10:9867'},
			]
		// ,script:'./nmea_scripts/tab_trainer.js'
		,tabmask:zeroisim.bin('0100000000000000000000000')	//	see settings.js
		},
	trainer:
		{flyover:'Teacher can log in, create scenario\'s'
		// {flyover:'by editing a spreadsheet, create a Collision Regulations Training exercise with some ghost-ships in it'
		,params:[
			{text:'websocket server IP address',urlparam:'ws_addr',dropdown:{list:config.machines}},
			{text:'world',param:'world',verstek:'abc',urlparam:'world'},
			{text:'startposition',dropdown:{list:positions},urlparam:'position'},
			{text:'startheading',verstek:'280',urlparam:'heading'},
			]
		// ,script:'./nmea_scripts/createGhosts.js'
		,tabmask:zeroisim.bin('1011111001010000011001111')	//	see settings.js
		},
	Nautis:
		{flyover:'0isim shows instruments driven by the Nautis simulator. Also you have some control like rudder, EOT and AP. You might send THS & GLL'
		,params:[	
			{text:'websocket server IP address',urlparam:'ws_addr',dropdown:{list:config.machines}},
			{text:'Nautis NMEA output UDP ip:port',urlparam:'listen_addr',verstek:'udp://192.168.1.15:8888'},
			{text:'Nautis NMEA input UDP port',urlparam:'talk_port',verstek:'8011'},
			]
		,script:'./nmea_scripts/nautis.js'
		,script:['./nmea_scripts/nautis.js']
		,tabmask: settings.tabmasks.show_settings
					|settings.tabmasks.show_rxtx
					|settings.tabmasks.show_kompas
					|settings.tabmasks.show_radar
					|settings.tabmasks.show_ecdis
					|settings.tabmasks.show_ais
					|settings.tabmasks.show_env
					|settings.tabmasks.show_conningdesign
		},
	NautisAzi:
		{flyover:'0isim shows instruments driven by the Nautis simulator. Please run Argus as you have AZI controls'
		,params:[	
			{text:'websocket server IP address',urlparam:'ws_addr',dropdown:{list:config.machines}},
			{text:'Nautis NMEA output UDP ip:port',urlparam:'listen_addr',verstek:'udp://127.0.0.1:8888'},
			{text:'Nautis NMEA input UDP port',urlparam:'talk_port',verstek:'8011'},
			]
		,script:'./nmea_scripts/nautis.js'
		,tabmask:zeroisim.bin('1000101001010000011101111')	//	see settings.js
		},
	Kompas:
		{flyover:'0isim shows only a compass driven by the Nautis simulator.'
		,params:[	
			{text:'websocket server IP address',urlparam:'ws_addr',dropdown:{list:config.machines}},
			{text:'NMEA input UDP ip:port',urlparam:'listen_addr',verstek:'udp://127.0.0.1:1500'},
			]
		,script:'./nmea_scripts/nautis.js'
		,tabmask:zeroisim.bin('000000000000000000100')	//	see settings.js
		},

	Multiple:
		{flyover:'0isim transmits its whereabouts to other ships in its world, have a look at your radar and AIS!'
		,params:[	//	todo textentry, radiobox, listbox
			{text:'websocket server IP address',urlparam:'ws_addr',dropdown:{list:config.machines}},
			{text:'ship',urlparam:'ship',dropdown:{list:simShips,field:'name'},img:config.mydocs+'atlassian.png'},
			{text:'world',param:'world',verstek:'abc',urlparam:'world'},
			{text:'startposition',dropdown:{list:positions},urlparam:'position'},
			{text:'startheading',verstek:'280',urlparam:'heading'},
			]
		,script:'./nmea_scripts/multiple.js'
		,	tabmask:	settings.tabmasks.show_radar
					|	settings.tabmasks.show_simulator
					|	settings.tabmasks.show_ecdis
					|	settings.tabmasks.show_ais
					|	settings.tabmasks.show_env
					|	settings.tabmasks.show_script
					|	settings.tabmasks.show_conningdesign
		},
	Simulator:
		{flyover:'No external communications, standalone simulator.'
		,params:[
			{text:'ship',urlparam:'ship',dropdown:{list:simShips,field:'name'},img:config.mydocs+'atlassian.png'},
			{text:'startposition',dropdown:{list:positions},urlparam:'position'},
			{text:'startheading',verstek:'85',urlparam:'heading'},
			// {text:'conning',verstek:'C:/Users/bert.tuijl/Downloads/myConning (11).json',urlparam:'conning'},	//	todo filedialog
			]
		,script:['./nmea_scripts/sim.js','./nmea_scripts/localghosts.js']
		,tabmask:	settings.tabmasks.show_simulator
					|	settings.tabmasks.show_ecdis
					|	settings.tabmasks.show_ais
					|	settings.tabmasks.show_env
					|	settings.tabmasks.show_conningdesign
					|	settings.tabmasks.show_view
					|	settings.tabmasks.show_ghostmenu
		},
	AISTargets:
		{flyover:'create some ships in the designated world'
		,params:[	//	todo textentry, radiobox, listbox
			{text:'websocket server IP address',urlparam:'ws_addr',dropdown:{list:config.machines}},
			{text:'world',param:'world',verstek:'abc',urlparam:'world'},
			]
		,script:'./nmea_scripts/aistargets.js'
		,tabmask:zeroisim.bin('1011111001010000011001111')	//	see settings.js
		}, 
	ECDIS:
		{flyover:'0isim runs a simulator that transmits its whereabouts to an ECDIS, accepts some NMEA messages'
		,params:[
			{text:'websocket server IP address',urlparam:'ws_addr',dropdown:{list:config.machines}},
			{text:'ship',urlparam:'ship',dropdown:{list:simShips,field:'name'},img:config.mydocs+'atlassian.png'},
			{text:'world',param:'world',verstek:'abc',urlparam:'world'},
			{text:'startposition',dropdown:{list:positions},urlparam:'position'},
			{text:'startheading',verstek:'280',urlparam:'heading'},
			{text:'NMEA to send',dropdown:{list:config.send_sentences,multiple:true,selected:['RMC','THS']},urlparam:'sentences'},
			// {text:'ECDIS address',verstek:'udp://localhost:1500',urlparam:'ecdisadress'},
			{text:'ECDIS address',verstek:'udp://127.0.0.1:8888',urlparam:'ecdisadress'},
			{text:'address for UDP commands',verstek:'udp://:8011',urlparam:'listen_addr'},
			]
		,script:'./nmea_scripts/ecdis.js'
		,tabmask:	settings.tabmasks.show_simulator
					|	settings.tabmasks.show_env
					|	settings.tabmasks.show_conningdesign
					|	settings.tabmasks.show_settings
		},
	Bahama:
		{flyover:'0isim transmits its whereabouts to an ECDIS and has a conning designer'
		,params:[
			{text:'websocket server IP address',urlparam:'ws_addr',dropdown:{list:config.machines}},
			{text:'ship',urlparam:'ship',dropdown:{list:simShips,field:'name'},img:config.mydocs+'atlassian.png'},
			{text:'world',param:'world',verstek:'abc',urlparam:'world'},
			{text:'startposition',dropdown:{list:positions},urlparam:'position'},
			{text:'startheading',verstek:'280',urlparam:'heading'},
			// {text:'NMEA',dropdown:{list:['GLL','HDT','MSO','RMC','ROT','RSA','THS','VDM','VTG',],multiple:true},urlparam:'sentences'},
			// {text:'NMEA',dropdown:{list:config.send_sentences,multiple:true},urlparam:'sentences'},
			{text:'ECDIS address',verstek:'udp://localhost:1500',urlparam:'ecdisadress'},
			]
		,script:'./nmea_scripts/bahama.js'
		,tabmask:zeroisim.bin('1000111001010000011001111')	//	see settings.js
		},

	nautisGLL_THS:
		{flyover:'Nautis is only visualizer for 0isim as 0isim sends position and heading to Nautis'
		,params:[
			{text:'websocket server IP address',urlparam:'ws_addr',dropdown:{list:config.machines}},
			]
		,script:'./nmea_scripts/ecdis.js'
		,tabmask:zeroisim.bin('111001010000011001111')	//	see settings.js
		},
		
	CreateGhosts:
		{flyover:'by editing a spreadsheet, create a Collision Regulations Training exercise with some ghost-ships in it'
		,params:[
			{text:'websocket server IP address',urlparam:'ws_addr',dropdown:{list:config.machines}},
			{text:'world',param:'world',verstek:'abc',urlparam:'world'},
			{text:'startposition',dropdown:{list:positions},urlparam:'position'},
			{text:'startheading',verstek:'280',urlparam:'heading'},
			]
		,script:'./nmea_scripts/createGhosts.js'
		,tabmask:zeroisim.bin('1111001010000011001111')	//	see settings.js
		},
		
	AutoPilot:
		{flyover:'control ship by receiving THS, ROT and sending ROR'
		,params:[
			{text:'websocket server IP address',urlparam:'ws_addr',dropdown:{list:config.machines}},	//	todo maybe not
			{text:'Ships NMEA output UDP ip:port',urlparam:'listen_addr',verstek:'udp://127.0.0.1:8888'},
			{text:'Ships NMEA input UDP port',urlparam:'talk_port',verstek:'8011'},
			]
		,script:'./nmea_scripts/autopilot.js'	//	todo dit script moet ervoor zorgen dat deze tab focus heeft en misschien de save-load logic hier
		,tabmask:zeroisim.bin('1000000000000000100')	//	see settings.js
		},
		
	USBControls:
		{flyover:'show values of USB connected joystick input'
		,params:[
			{text:'websocket server IP address',urlparam:'ws_addr',dropdown:{list:config.machines}},	//	todo maybe not
			]
		// ,script:'./nmea_scripts/ecdis.js'	//	todo dit script moet ervoor zorgen dat deze tab focus heeft en misschien de save-load logic hier
		,tabmask:zeroisim.bin('100000000000')	//	see settings.js
		},
	};

function on_load(){
	var buttonstyle = config.buttonstyle.join(';');
	for (var idx in mijnlinkss){
		var sectionName = 'n_' + idx;
		var sectionParagraph = 'p_' + idx;
		addChapter(sectionName,sectionParagraph,mijnlinkss[idx]);
		setValue(sectionName,idx );
		setValue(sectionParagraph
			,h.table
				(h.tr(h.td(asTable(idx,mijnlinkss[idx].params)))
				+h.tr(h.td(h.table(h.tr
					(h.td(h.button('start',h.attributes({onclick:'start0sim(this,0)',id:'deButton_'+sectionName,chapter:sectionName,style:buttonstyle})))
					+h.td(h.button('start in new window',h.attributes({onclick:'start0sim(this,1)',id:'deButton_'+sectionName,chapter:sectionName,style:buttonstyle})))
					+h.td(h.button('QRcode',h.attributes({onclick:'showQRcode(this,1)',id:'deQRButton_'+sectionName,chapter:sectionName,style:buttonstyle})))
					))),h.attributes({colspan:3}))
				+h.tr(h.td
					(h.svg(h.g('','id="qr_'+idx+'"'),h.attributes
						({xmlns:'http://www.w3.org/2000/svg'
						,'xmlns:xlink':'http://www.w3.org/1999/xlink'
						,width:'400',height:'400'
						,id:'qrsvg_'+idx,display:'none'
						}))
						)
				+h.tr(h.td(h.div(idx,'id=link_'+idx)))
				))
			);
		mijnlinkss[idx].qrcode = new QRCode(document.getElementById("qr_"+idx), {useSVG: true});
		// mijnlinkss[idx].qrcode = new QRCode(document.getElementById("qr_"+idx));	//	canvas doet het niet, iets met refresh
		}

	var acc = document.getElementsByClassName("accordion");
	for (var ii=0; ii<acc.length; ii++) {
		acc[ii].onclick = function(evt){	//	toggle active and next sibling show

			//	set every chapter inactive
			var accs = document.getElementsByClassName("accordion");
			for (var iii=0; iii<accs.length; iii++) {
				if (this!=accs[iii]){
					accs[iii].classList.remove("active");
					accs[iii].nextElementSibling.classList.remove("show");
					}
				}
			//	omdat het nogal veel data is, verwijder oude qrcodes
			for (var chapter in mijnlinkss){
				if (undefined != mijnlinkss[chapter].qrcode)
					mijnlinkss[chapter].qrcode.clear();
				}

			//	zet de qrcode erbij
			var chapter = this.getAttribute('chapter');
			// mijnlinkss[chapter].qrcode.makeCode(config.qrcode_url_prefix + createURL(chapter,1));
			this.classList.toggle("active");
			this.nextElementSibling.classList.toggle("show");
			// setValue('link_'+chapter,config.qrcode_url_prefix + createURL(chapter,1));
			}
		}
	}
function addChapter(sectionName,sectionParagraph,attr){
	var div0 = window.document.createElement('div');
	div0.setAttribute('id',sectionName);
	div0.setAttribute('title',attr.flyover);
	if (undefined!=attr.script) div0.setAttribute('script',JSON.stringify(attr.script));
	div0.setAttribute('tabmask',attr.tabmask);
	var btn = window.document.createElement('button');
	btn.setAttribute('class','accordion');
	btn.setAttribute('chapter',sectionName.substr(2));
	btn.appendChild(div0);

	var txt1 = document.createTextNode('hier komt nog een paragraaf...');
	var div1 = window.document.createElement('div');
	div1.setAttribute('id',sectionParagraph);
	var div2 = window.document.createElement('div');
	div2.setAttribute('class','panel');
	div2.appendChild(div1);

	document.body.appendChild(btn);	//	moet class='accordion' zijn
	document.body.appendChild(div2);	//	moet nextElementSibling zijn
	}
function asTable(name,mijnlinks){
	var tabels = '';
	mijnlinks.myforEach(function(mijnlink){
		if (undefined!=mijnlink.verstek)		value=h.input('','type="text" value="'+mijnlink.verstek+'"');	//	 
		if (undefined!=mijnlink.urlparam)	var urlparam=mijnlink.urlparam; else var urlparam='bertje';
		if (undefined!=mijnlink.dropdown){
			var value = h.nbsp;
			var attr = '';
			if (undefined!=mijnlink.dropdown.multiple && true==mijnlink.dropdown.multiple){
				attr = 'multiple';
				}
			if (typeof mijnlink.dropdown==='object' && undefined==mijnlink.dropdown.length){
				var values = [];
				if (undefined!=mijnlink.dropdown.field){
					mijnlink.dropdown.list.myforEach(function(item){
						values.push(item[mijnlink.dropdown.field]);
						},);
					}
				else{
					if (Array.isArray(mijnlink.dropdown.list)){
						if ('multiple'==attr){
							var values = [];
							mijnlink.dropdown.list.myforEach(function(item,idx,arr,obj){
								if (undefined!=mijnlink.dropdown.selected  && mijnlink.dropdown.selected.includes(item.s)){
									values.push({i:item,a:'selected'});
									}
								else
									values.push({i:item});
								});
							}
						else 	
							var values = Object.values(mijnlink.dropdown.list);
						}
					else
						var values = Object.keys(mijnlink.dropdown.list);
					}
				value=h.select('poep',values,attr)
				}
			else
				value=h.select('poep',mijnlink.dropdown,attr)
			}
		tabels+=h.tr
			(h.td(h.a(mijnlink.text))	//	todo iets van param toevoegen hier
			+	h.td(value)
			,'id="'+urlparam+'"');
		}
		,tabels);
	return h.table(h.tbody(tabels),'id="t_'+name+'" border="1px"');
	// return h.table(h.tbody(h.tr(tabels)),'id="t_'+name+'" border="1px"');
	}
function start0sim(that,bNewWindow){
	// var url = createURL(that.parentElement.id.substr(2));
	var test = that.getAttribute('chapter').substr(2);
	var url = createURL(that.getAttribute('chapter').substr(2));
	// var chapter = ;
	if (bNewWindow){
		var win = window.open(url);
		win.focus(); 
		}
	else	
		document.location = url;
	}
function showQRcode(that,bNewWindow){
	var chapter = that.getAttribute('chapter').substr(2);

	var test = document.getElementById('qrsvg_'+chapter);
	test.setAttribute('display','yes');
	
	var url = config.qrcode_url_prefix + createURL(chapter,1);
	mijnlinkss[chapter].qrcode.makeCode(url);
	// this.classList.toggle("active");
	// this.nextElementSibling.classList.toggle("show");
	setValue('link_'+chapter,url);
	}
function createURL(chapter){
	//	get the parameter values from the table and construct url
	var sep = '?';
	var url = 'nmea_presenter.html';
	var url = 'zeroisim.html';
	var script = 'n_' + chapter;
	script = document.getElementById(script);
	script = script.getAttribute('script');
	if (undefined!=script){
		url += sep + 'script=' + script;
		sep = '&';
		}
	var tabmask = 'n_' + chapter;
	tabmask = document.getElementById(tabmask);
	tabmask = tabmask.getAttribute('tabmask');
	url += sep + 'tabmask=' + tabmask;
	sep = '&';
	url += sep + 'tabmaskH=' + zeroisim.HEX(parseInt(tabmask));
	var id_params = 't_' + chapter;
	var params = document.getElementById(id_params);
	for (var ii=0; ii<params.rows.length;ii++) {
		if (undefined!=params.rows[ii].cells && 2==params.rows[ii].cells.length){
			// var td0 = params.rows[ii].cells[0].textContent;				//	not used
			if ('select-one'==params.rows[ii].cells[1].firstChild.type)
				var value = 	params.rows[ii].cells[1].firstChild.value;	//	from dropdown selection
			else if ('select-multiple'==params.rows[ii].cells[1].firstChild.type){
				var options = 	params.rows[ii].cells[1].firstChild.options;	
				var value = [];
				for (var iii=0; iii<options.length; iii++){
					if (options[iii].selected)	value.push(options[iii].value);
					}				
				value = JSON.stringify(value);
				}
			else if ('text'==	params.rows[ii].cells[1].firstChild.type)
				var value = 	params.rows[ii].cells[1].firstChild.value;	//	from textinput
			else
				var value = params.rows[ii].cells[1].textContent;			//	from ???
			var parm = params.rows[ii].id;
			url += sep + parm + '=' + value;
			}
		}
	// console.log('createURL('+chapter+'):'+url);
	return url;	
	}
