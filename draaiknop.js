// draaiknop.js
//	heeft wim gemaakt, ik heb een beetje abstraktie toegevoegd
// voorbeeld:
/*
		window.onload = function () {
			var Button = new draaiKnop(document.getElementById("map"), rotateMe);
			var Button2 = new draaiKnop(document.getElementById("map2"), rotateMe);
			function rotateMe(elm,this.alpha) {
				console.log("rotation of '"+elm.id+"' is: " + this.alpha + "&deg;");
				}
			}
*/
'use strict';

class draaiKnop {
	constructor(theElm, callBack) {
		this.alpha = 0;	
		this.sigma = 0;	
		this.half = {x:theElm.offsetWidth/2,y:theElm.offsetHeight/2};
		this.move = this.mouseMove.bind(this);
		this.moveTouch = this.touchMove.bind(this);
		this.remMove = this.remMove.bind(this);
		this.remMoveTouch = this.remMoveTouch.bind(this);
		this.click = this.click.bind(this);
		theElm.addEventListener("mousedown", this.startMove.bind(this), false);
		theElm.addEventListener("touchstart", this.hTouchstart.bind(this), false);
		// theElm.addEventListener("mousemove", this.mouseOver.bind(this), false);	//	debugging only
		theElm.addEventListener("click", this.click, false);
		theElm.click();	//	determine coordinates of element with respect to viewport
		this.theElm = theElm;
		this.callBack = callBack;
		if (navigator.userAgent.indexOf('Edge') != -1){//MS Edge
			this.browser = 'edge';
			}
		else if (navigator.userAgent.indexOf('Firefox') != -1 && parseFloat(navigator.userAgent.substring(navigator.userAgent.indexOf('Firefox') + 8)) >= 3.6){//Firefox
			this.browser = 'ff';
			}
		else if (navigator.userAgent.indexOf('Chrome') != -1 && parseFloat(navigator.userAgent.substring(navigator.userAgent.indexOf('Chrome') + 7).split(' ')[0]) >= 15){//Chrome
			this.browser = 'chrome';
			}
		else if(navigator.userAgent.indexOf('Safari') != -1 && navigator.userAgent.indexOf('Version') != -1 && parseFloat(navigator.userAgent.substring(navigator.userAgent.indexOf('Version') + 8).split(' ')[0]) >= 5){//Safari
			this.browser = 'safari';
			}
		else {
			console.log('unsupported browser');
			}
		// Block
		}
	click(evt){
		//	this is to determine the midpoint of this element with respect to the viewport of the page
		if (undefined==this.halfTouch)
			this.halfTouch = {x:evt.layerX,y:evt.layerY};
		evt.target.removeEventListener("click", this.click, false);
		}	
	startMove(evt) {
		// console.log('startMove(evt) '+evt.target.id);
		this.sigma = this.hoekFromPos(evt)-this.alpha; 
		// todo vraag je adhv het event af of er mouse of touch handlers geinstalleerd moeten worden
		this.theElm.addEventListener("mousemove", this.move, false);
		this.theElm.addEventListener("mouseup", this.remMove, false);
		this.theElm.addEventListener("mouseout", this.remMove, false); 
		}
	hTouchstart(evt) {
		// console.log('hTouchstart(evt) '+evt.target.id);
		this.theElm.addEventListener("touchmove", this.moveTouch, false);
		this.theElm.addEventListener("touchend", this.remMoveTouch, false);
		this.startMove(this.touch2mouse(evt));
		}	
	remMove(evt){
		// console.log('remMove '+evt.target.id);
		this.theElm.removeEventListener("mousemove", this.move, false);
		}
	remMoveTouch(evt){
		// console.log('remMoveTouch '+evt.target.id);
		this.theElm.removeEventListener("touchmove", this.moveTouch, false);
		this.theElm.removeEventListener("touchend", this.remMoveTouch, false);
		}
	mouseMove(evt) {
		var deg = this.hoekFromPos(evt) - this.sigma; //zou relatieve hoek moeten zijn t.o.v.
		while (deg < 0) { deg += 360 };
		this.draaiNaar(deg);
		}
	touch2mouse(evt) {
		this.halfTouch;
		var rc = {target: evt.target
			,layerX:evt.changedTouches[0].pageX + this.halfTouch.x
			,layerY:evt.changedTouches[0].pageY + this.halfTouch.y
			,offsetX:evt.changedTouches[0].pageX + this.halfTouch.x
			,offsetY:evt.changedTouches[0].pageY + this.halfTouch.y
			};
		// console.log('touch2mouse rc='+JSON.stringify(rc));
		return rc;
		}
	touchMove(evt) {
		this.mouseMove(this.touch2mouse(evt));
		}
	mouseOver(evt){	//	testing coordinaten
		var layer =	{X:evt.layerX,Y: evt.layerY};
		var client ={X:evt.clientX,Y:evt.clientY};
		var offset ={X:evt.offsetX,Y:evt.offsetY};
		console.log
			(this.prtCoordinates('layer',layer)
			+this.prtCoordinates('client',client)
			+this.prtCoordinates('offset',offset)
			);
		}		
	prtCoordinates(string,struct){	
		return string+'('+struct.X + ', ' + struct.Y+')';
		}		
	hoekFromPos(evt){
		var dX = evt.layerX - this.half.x;
		var dY = this.half.y - evt.layerY;
		// console.log(evt.target.id+': '+this.prtCoordinates('d(XY)',{X:dX,Y:dY}));
		switch (this.browser){
			case 'edge':
			case 'safari':
				dX = evt.offsetX - this.half.x;
				dY = this.half.y - evt.offsetY;
			case 'ff':
				return Math.atan2(dX, dY) * 180.0 / Math.PI + this.alpha;	//	firefox
				break;
			case 'chrome':
				return Math.atan2(dX, dY) * 180.0 / Math.PI;	//	chrome
				break;
			}
		}
	/*
	this.draaiNaarT = function(deg){		
		var t = this.alpha-deg;
		var a = this.alpha;
		var throttled = _.throttle(this.draaiNaar, 1000);
		for (var ii=0; ii<t; ii++) {
			throttled(a-ii);		
			}
		}
	*/
	draaiNaar(deg){		
		// console.log(this.theElm.id+' draaiNaar('+deg+')');
		this.theElm.style.transform = 'rotate(' + deg + 'deg)';
		// console.log('deg='+deg.toString()+', this.alpha='+this.alpha.toString()+', deg-this.alpha='+(deg-this.alpha).toString()+', Math.abs(deg-this.alpha)'+Math.abs(deg-this.alpha));
		if (Math.abs(deg-this.alpha) >= 0.1) {	//	stuur niet elk wissewasje door, todo dit instelbaar maken
			this.alpha = deg;
			// this.callBack(this.theElm,Math.round(10.0*this.alpha)/10);	//	todo precisie instellen
			}
		}
	draai(deg){
		this.draaiNaar(this.alpha+deg);
		}
	}
