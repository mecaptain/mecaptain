//	iazivector.js
//	provides readout of thrust of azithruster

'use strict';

class CInstrument_azivector extends CInstrument {
	constructor(obj,exposedParms,active,idx){
		super(idx,obj);
		if (undefined!=exposedParms)
			this.exposedParms = exposedParms;
		else
			this.exposedParms = //	todo deze moeten door de conning designer kunnen worden gezet, nier overrulen hier dan
				{active:				0
				,thrusters:			[0,1]
				,globalAlpha:		0.9
				,aziScale:			0.009		// 0.010 would be use full circle of azi picture, todo misschien afhankelijk van schaal
				,colorBackground:	'rgba(255,255,255,0.5)'
				,colorRings:		'rgba(  0,  0,255,0.6)'
				,colorCross:		'rgba(  0,  0,  0,0.6)'
				,colorCommand:		'rgba(  0,  0,255,0.6)'
				,colorRespons:		'rgba(255,  0,  0,0.6)'
				};	
		this.canvas = obj.querySelector("canvas");
		this.cv = this.canvas.getContext("2d")
		this.cv.globalAlpha = this.exposedParms.globalAlpha;
		this.bAziShip = false;
		this.deg2rad = Math.PI/180.0;
		this.azi_readout = 'azi_readout';
		this.hasFocus = true;
		this.thrusterDataC = [];
		this.thrusterDataD = [];
		this.exposedParms.thrusters.myforEach(function(not,ii,ar,that){
			that.thrusterDataC[not] = {rpm:0,azi:0};
			that.thrusterDataD[not] = {rpm:0,azi:0};
			},this);

		if (0!=this.exposedParms.active){
			super.cbRxNmea('TRC',function(rx,that){	//	install callback handler upon rx of TRC
				if (undefined==rx.data)	return;
				//	todo check of rx.data.not in de gewenste thrusters zit
				if (that.hasFocus) {
					that.thrusterDataC[rx.data.not].rpm = rx.data.rpm;
					that.thrusterDataC[rx.data.not].azi = rx.data.azi;
					that.draw();
					}
				},this);
			super.cbRxNmea('TRD',function(rx,that){	//	install callback handler upon rx of TRD
				if (undefined==rx.data)	return;
				//	todo check of rx.data.not in de gewenste thrusters zit
				if (that.hasFocus) {
					that.thrusterDataD[rx.data.not].rpm = rx.data.rpm;
					that.thrusterDataD[rx.data.not].azi = rx.data.azi;
					that.draw();
					}
				},this);
			this.draw();
			}
		}
	getRpmAzi(evt){
		// convert mouse coordinates in canvas to rpm and azi-angle
		var rect = evt.currentTarget.getBoundingClientRect();
		var pp = {x: evt.clientX - rect.left - evt.currentTarget.width/2, y: -1*(evt.clientY - rect.top - evt.currentTarget.height/2)};
		var rpm = Math.sqrt(Math.pow(pp.x,2) + Math.pow(pp.y,2));
		rpm /= evt.currentTarget.width/2;
		rpm /= this.exposedParms.aziScale;
		rpm = rpm.toFixed(1);

		var azi = Math.atan(pp.x/pp.y) / this.deg2rad;
		if (pp.x<0.0)	if (azi>0.0)	azi -= 180.0;
		if (pp.x>0.0)	if (azi<0.0)	azi += 180.0;
		azi = azi.toFixed(1);
		return {rpm: rpm, azi: azi};
		}
	draw(){
		if (!this.hasFocus) return;
		// teken de vectoren en hun resultante
		var not = this.exposedParms.noThruster
		this.cv.clearRect(0,0,this.canvas.width,this.canvas.height);
		this.cv.fillStyle = this.exposedParms.colorBackground;
		this.cv.fillRect(0,0,this.canvas.width,this.canvas.height);
		
		// cross
		this.cv.beginPath();
		this.cv.strokeStyle = this.exposedParms.colorCross;
		this.cv.moveTo(this.canvas.width/2,this.canvas.height);			this.cv.lineTo(this.canvas.width/2,0);
		this.cv.moveTo(this.canvas.width,this.canvas.height/2);			this.cv.lineTo(0,this.canvas.height/2);
		this.cv.stroke();
		
		var wh={w:this.canvas.width,h:this.canvas.height};
		
		if (this.thrusterDataC.length){
			var rc = this.showResultingVector(this.cv,wh,this.exposedParms.colorCommand,this.thrusterDataC);	//	command
			}
		if (this.thrusterDataD.length){
			var rr = this.showResultingVector(this.cv,wh,this.exposedParms.colorRespons,this.thrusterDataD);	//	respons
			// setValue('azi_res_force',rr.m);
			// setValue('azi_res_azimuth',rr.a/this.deg2rad);
			}
		}
	showResultingVector(cv,wh,color,azi){
		var vp0 = {x:9,y:+2,a:azi[0].azi*this.deg2rad,m:azi[0].rpm}; 
		var vp1 = {x:9,y:-2,a:azi[1].azi*this.deg2rad,m:azi[1].rpm};
		var rp = this.resultante(vp0,vp1);
		cv.save();	{
			cv.translate(wh.w/2-10*rp.y,wh.h/2+5*rp.x);
			// teken aangrijpingspunt voortstuwingsresultante
			cv.beginPath();	cv.strokeStyle = cv.fillStyle = color;	cv.arc(0,0,5,0,2*Math.PI);	cv.fill();	cv.stroke();
			cv.rotate(-rp.a);	
			// teken vector voortstuwingsresultante
			cv.beginPath();	cv.strokeStyle = color;	cv.moveTo(0,0);cv.lineTo(0,rp.m);	cv.stroke();
			}	cv.restore();
		return rp;	
		}		
	resultante(v0,v1){
		//	vector parameters moeten x,y,a,m bevatten
		//	1 bepaal werklijnen van v0 an v1 (snijpunt y-as, rico)
		v0.rico = Math.tan(v0.a);	v0.b = v0.y-v0.rico*v0.x;
		v1.rico = Math.tan(v1.a);	v1.b = v1.y-v1.rico*v1.x;
		var x=(v1.b-v0.b) / (v0.rico-v1.rico);	var y=v0.rico*x+v0.b;	//	hier grijpt de resultante aan
		// zet polair om naar cartesisch
		v0.vx = v0.m*Math.sin(v0.a); v0.vy = v0.m*Math.cos(v0.a);
		v1.vx = v1.m*Math.sin(v1.a); v1.vy = v1.m*Math.cos(v1.a);
		var xr=v0.vx+v1.vx;var yr=v0.vy+v1.vy;
		var rc = this.xy2pol({x:xr,y:yr});
		return {x:x,y:y,a:rc.a,m:rc.r};
		}
	xy2pol(xy){
		var rc = 	{	a:	Math.atan((xy.x) / (xy.y))
						,	r:	Math.sqrt((xy.x)*(xy.x)+(xy.y)*(xy.y))
						};
		if ((xy.y) > 0)
			rc.a = Math.PI-rc.a;
		else {
			rc.a = 2*Math.PI-rc.a;
			if (rc.a > 2*Math.PI)	rc.a -= 2*Math.PI;
			}
		return rc;
		}
	};
CInstrument.addInstrumentToSettings('./iazivector.js',			'azivector',	'canvas',	CInstrument_azivector,	'shows polar diagram of resulting AZI thrust vector');
