//	mfinstrument.js
// teken allerlei instrument uitlezingen
'use strict';
var createMFInstrument = function () {
	return {
		init: function(id_canvas){
			//	this init is called before page is loaded. Defer initialisation, you need the canvas for it, to after pageload event
			this.id_canvas = id_canvas;	
			window.addEventListener("load", this.init2.bind(this), false);	
			},
		init2: function(){
			//console.log('createMFInstrument.init()');
			var canvas = document.getElementById(this.id_canvas);
			if (undefined==canvas)	return;	//	sorry, no instrument possible
			this.cv = canvas.getContext("2d")
			this.midden = {x: canvas.width/2, y:canvas.height/2};
			this.cv.textAlign="center";
			this.cv.fillStyle = '#000';
			this.cv.font = '40px sans-serif';
			cbRxNmea('GGA',function(rxd,that){	//	install callback for position		todo kiezen van welke sentence je de latlon wilt (GGA, GLL, ...)
				if (undefined==rxd.data)	return;
				ship0.latlon.interpretLatLonGGA(rxd.data);
				that.draw(ship0);
				},this);
			cbRxNmea('ZDA',function(rxd,that){	//	install callback for time
				if (undefined==rxd.data)	return;
				ship0.utc = rxd.data.time;
				},this);
			},
		draw: function(ship) {
			this.cv.clearRect(0,0,2*this.midden.x,2*this.midden.y);
			this.cv.save();
			// this.cv.fillStyle = 'rgba(125,125,125, 1.0)';
			this.cv.fillStyle = 'rgba(0xB,0xB,0xB, 1.0)';
			this.cv.fillStyle = '#BBB';
			this.cv.fillRect(0,0,2*this.midden.x,2*this.midden.y);
			
			this.cv.translate(this.midden.x,this.midden.y);
			
			this.cv.fillStyle = '#FFF';
			var YY = -70;	//	todo query font height
			this.cv.fillText(ship.latlon.latdegmindec,0,YY);	YY+=40;
			this.cv.fillText(ship.latlon.londegmindec,0,YY);	YY+=40;
//			this.cv.fillText(zeroisim.prefixpad(ship.hdt.toFixed(1),3)+h.deg,0,YY);	YY+=40;
			this.cv.fillText(ship.utc,0,YY);														YY+=40;
			this.cv.fillText(zeroisim.prefixpad(ship.hdt.toFixed(1),3)+h.deg,0,YY);	YY+=40;
			this.cv.fillText(ship.sog.toFixed(2)+' knots',0,YY);							YY+=40;
//			this.cv.fillText(common.ship.time,0,YY);											YY+=40;
			this.cv.restore();
			},
		};
	};
