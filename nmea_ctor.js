//	nmea_ctor.js
//	construct nmea sentences in the tx-structure

'use strict';

var nmea_send=function(sentence,freq){
	this.s=sentence;
	this.f=freq;
	}
nmea_send.prototype.toString=function(){
	return JSON.stringify(this);
	}
nmea_send.prototype.parseArray=function(ar){
	var rc = new Array();
	ar.myforEach(function(el){
		var tmp = JSON.parse(el);
		rc.push(new nmea_send(tmp.s,tmp.f));
		});
	return rc;
	}
function nmea_ctor(that,sentence,subsentence){	
	switch (sentence){
	
		case 'DTM':	//	Datum reference
			tx[sentence].talker = nmeadefs[sentence][0].ti;
			tx[sentence].datum		='WGS-84';//',vartext,'local datum code','W84']
			tx[sentence].subdiv		='';	//',vartext,'local datum subdivision code','?']
			tx[sentence].latoff		=0.0;	//',floatingp,'latitude offset in minutes',0.0]
			tx[sentence].latoffns	='N';	//',literal('N|S'),'latitude offset, North/South','N']
			tx[sentence].lonoff		=0.0;	//',floatingp,'longitude offset',0.0]
			tx[sentence].lonoffew	='E';	// ,literal('E|W'),'longitude offset, East/West','E']
			tx[sentence].altoff		=0.0;//',floatingp,'altitude offset in meters',0.0]
			tx[sentence].refcode		='W84';//',vartext,'Reference datum code','W84']
			return true;
			break;
	
		case 'ETL':	//	Engine Telegraph Operation Status',	ti:'SS'}	
			tx[sentence].talker = nmeadefs[sentence][0].ti;
			tx[sentence].time='time';	//	,timestamp,'Event time UTC',]
			tx[sentence].msgtype='msgtype';	//	,literal('O|A'),'messagetype O=order status A=answerback',]
			tx[sentence].etposition='etposition';	//	,integer(),'Position indicator of engine telegraph',]
			tx[sentence].stposition='stposition';	//	,integer(),'Position indication of sub telegraph; 20=standby engine 30=full away - navigation full 40=finish with engine',]
			tx[sentence].oplocind='oplocind';	//	,vartext,'Operating location indicator',]
			tx[sentence].number='number';	//	,integer(1),'Number of engine or propeller shaft',]
			return true;
			break;
	
		case 'GLL':
			tx.GLL.talker = nmeadefs[sentence][0].ti;
			tx.GLL.lat = that.ship.latlon.latgll;	//	latgll2
			tx.GLL.NS = that.ship.latlon.latsgn;
			tx.GLL.lon = that.ship.latlon.longll;	//	longll2
			tx.GLL.EW = that.ship.latlon.lonsgn;
			tx.GLL.utc
				= zeroisim.prefixpad(that.ship.timestamp.getUTCHours(),2)
				+ zeroisim.prefixpad(that.ship.timestamp.getUTCMinutes(),2)
				+ zeroisim.prefixpad(that.ship.timestamp.getUTCSeconds(),2)
				;
			tx.GLL.status = 'A';
			tx.GLL.mode = 'A';
			return true;
			break;
		
		case 'GGA':
			tx[sentence].talker = nmeadefs[sentence][0].ti;
			tx[sentence].tim
				= zeroisim.prefixpad(that.ship.timestamp.getUTCHours(),2)
				+ zeroisim.prefixpad(that.ship.timestamp.getUTCMinutes(),2)
				+ zeroisim.prefixpad(that.ship.timestamp.getUTCSeconds(),2)
				;
			tx[sentence].lat = that.ship.latlon.latgll2;
			tx[sentence].NS = that.ship.latlon.latsgn;
			tx[sentence].lon = that.ship.latlon.longll2;
			tx[sentence].EW = that.ship.latlon.lonsgn;
			tx[sentence].qual = 1;	//	fix quality: 1=gps fix']
			tx[sentence].nsat = 10;	//	number of satellites']
			tx[sentence].hdop = 20;	//	hdop
			tx[sentence].alt = 0;	//	altitude']
			tx[sentence].altu = 'm';	//	unit of altitude']
			tx[sentence].hgeo = 0;	//	height of geoid above WGS84 ellipsoid']
			tx[sentence].hgeou = 'm';	//	unit of height']
			tx[sentence].tupd = 20;	//	Time since last DGPS update']
			tx[sentence].rstat = '101'	//	DGPS reference station id']
			return true;
			break;
		
		case 'HDT':
			tx.HDT.talker = nmeadefs[sentence][0].ti;
			tx.HDT.hdt = that.ship.hdt.toFixed(2);	//	Heading, true in degrees']
			tx.HDT.trdeg = 'T';		//	
			return true;
			break;
			
		case 'RMC':
			tx.RMC.talker = nmeadefs[sentence][0].ti;
			tx.RMC.hdgT = that.ship.hdt;	//	Heading, true in degrees']
			tx.RMC.trdeg = 'S';	//	[A|E|M|S|V],	'Autonomous, Estimated (DR), Manual, Simulator, V=data not valid (including standby)']
			tx.RMC.tfix  	//	time of fix
				= zeroisim.prefixpad(that.ship.timestamp.getUTCHours(),2)
				+ zeroisim.prefixpad(that.ship.timestamp.getUTCMinutes(),2)
				+ zeroisim.prefixpad(that.ship.timestamp.getUTCSeconds(),2)
				;
			tx.RMC.status = 'A'; //	Status A=active or V=Void']
			tx.RMC.lat = that.ship.latlon.latgll2;
			tx.RMC.slat = that.ship.latlon.latsgn;
			tx.RMC.lon = that.ship.latlon.longll2;
			tx.RMC.slon = that.ship.latlon.lonsgn;
			tx.RMC.sog=that.ship.stwL.toFixed(2);
			tx.RMC.cog=that.ship.hdt.toFixed(2);
			tx.RMC.date
					= zeroisim.prefixpad(that.ship.timestamp.getUTCDay(),2)
					+ zeroisim.prefixpad(that.ship.timestamp.getUTCMonth()+1,2)
					+ zeroisim.prefixpad(that.ship.timestamp.getFullYear(),4)
					;									
			tx.RMC.mvar='0.0';	//	Magnetic Variation
			tx.RMC.svar='E';		//	sense of magnetic variation E=East W=West
			return true;
			break;
					
		case 'ROT':
			tx[sentence].talker = nmeadefs[sentence][0].ti;
			tx[sentence].rot = that.ship.rot.toFixed(2);	//	rate of turn in degrees/minute. Negativ means bow turns to port
			tx[sentence].A = 'A';	//	status A=valid V=invalid
			return true;
			break;
			
		case 'RSA':
			tx[sentence].talker = nmeadefs[sentence][0].ti;
			tx[sentence].srud = that.ship.rsa.s.toFixed(2);	//	Starboard (or single) rudder sensor, "-" means bow turns to port
			tx[sentence].sstat = 'A';								//	status A=valid V=invalid
			tx[sentence].prud = that.ship.rsa.p.toFixed(2);	//	Port rudder sensor
			tx[sentence].pstat = 'A';								//	status A=valid V=invalid
			return true;
			break;
			
		case 'THS':
			tx.THS.talker = nmeadefs.THS[0].ti;
			tx.THS.hdgT = that.ship.hdt.toFixed(2);	//	Heading, true in degrees']
			tx.THS.trdeg = 'S';	//	[A|E|M|S|V],	'Autonomous, Estimated (DR), Manual, Simulator, V=data not valid (including standby)']
			return true;
			break;
			
		case 'VDM':
			switch (subsentence){
				case '1':
					tx.VDM.talker = nmeadefs['VDM'][0].ti;
					tx.VDM.totns = 1;
					tx.VDM.sentnum = 1;
					tx.VDM.messid = 1;
					tx.VDM.aischan = 1;
					tx.VDM.msg = encodeVDM(1,{MessageID:1
						,RepeatIndicator:2
						,UserID:that.ship.mmsi
						,NavigationalStatus:"under way using engine"
						,ROT:that.precalc.rot.rot
						,SOG:that.precalc.vtg.sogN
						,PositionAccuracy:"low"
						,Longitude:that.ship.latlon.londeg
						,Latitude:that.ship.latlon.latdeg
						,COG:that.precalc.rmc.cog
						,TrueHeading:that.ship.hdt
						,TimeStamp:that.ship.timestamp.getSeconds()
						,ResforRegApps:0
						,Spare:0
						,RAIMFlag:0
						,CommunicationState:"utc indirect"
						});
					tx.VDM.nfillb = 0;
					tx.VDO = tx.VDM;
					tx.VDO.date = that.tim;
					return true;
					//todo hier VDO sturen zodat aistable goede cpa geeft						
					break;
					
			case '5':
					var vdm5 = 
						{MessageID:5
						,RepeatIndicator:0
						,UserID:that.ship.mmsi
						,AISversion:0
						,IMOnumber:that.ship.IMO
						,CallSign:that.ship.CallSign
						,Name:that.ship.name
						,ShipType:that.ship.shiptype
						,dimRefPoint:'(A,B,C,D) = ('+that.ship.RefPoint.A+','+that.ship.RefPoint.B+','+that.ship.RefPoint.C+','+that.ship.RefPoint.D+')'
						,PosFixDev:"GPS"
						,ETA:"16-01 14:16 UTC"
						,draught:5.9
						,destination:that.ship.destination
						,DTE:1
						,spare:0
						};
					tx.VDM.talker = nmeadefs['VDM'][0].ti;
					tx.VDM.totns = 1;
					tx.VDM.sentnum = 1;
					tx.VDM.messid = 1;
					tx.VDM.aischan = 1;
					tx.VDM.msg = encodeVDM(5,vdm5);	//	todo msg moet array kunnen zijn als hij te lang is voor 1 msg
					tx.VDM.nfillb = 0;	//	todo fillbits moet resultaat zijn van encoding
					return true;
					break;
				}	
			break;
			
		case 'VBW':	//	[{title:'Dual Ground/Water Speed'}	//	Water referenced and ground referenced speed data.
			tx[sentence].talker = nmeadefs[sentence][0].ti;
			tx[sentence].longwat = that.ship.stwL.toFixed(2);	//	floatingp,			'Longitudinal water speed, knots "-" = astern']
			tx[sentence].transwat = 0.0;	//	floatingp,			'Transverse water speed, knots "-" = port']
			tx[sentence].validwat = 'A';	//	literal('A|V'),	'Status: Water speed, A = Data valid']
			tx[sentence].longgrd = that.ship.stwL.toFixed(2);	//	floatingp,			'Longitudinal ground speed, knots "-" = astern']
			tx[sentence].transgrd = 0.0;	//	floatingp,			'Transverse ground speed, knots "-" = port']
			tx[sentence].statgrd	= 'A';	//	literal('A|V'),	'Status, Ground speed, A = Data valid']
			tx[sentence].sterntransw = 0.0;	//	floatingp,			'Stern transverse water speed, knots "-" = port']
			tx[sentence].statsterntransw = 'A';	//	literal('A|V'),	'Status, stern water speed, A = Data valid']
			tx[sentence].sterntransg = 0.0;	//	floatingp,			'Stern transverse ground speed, knots "-" = port']
			tx[sentence].statsterntransg = 'A';	//	literal('A|V'),	'Status, stern ground speed A = Data valid V = Invalid']
			return true;
			break;

		//	todo niet kompleet	
		case 'VLW':	//	 [{title:'Dual Ground/Water Distance'}	//	The distance traveled, relative to the water and over the ground.
			//$--VLW,x.x,N,x.x,N,x.x,N,x.x,N*hh<CR><LF> todo onderstaande lijkt me niet af
			tx[sentence].talker = nmeadefs[sentence][0].ti;
			tx[sentence].wdisttot = 12345.6;	//	floatingp,	'Total cumulative water distance, nautical miles']
			tx[sentence].wdisttrip = 987.6;	//	floatingp,	'Water distance since reset, nautical miles']
			tx[sentence].gdisttot = 12345.7;	//	floatingp,	'Total cumulative ground distance, nautical miles']
			tx[sentence].gdisttrip = 987.5;	//	floatingp,	'Ground distance since reset, nautical miles']
			return true;
			break;
					
		case 'VTG':	//	Vector track and Speed over the Ground',
			tx.VTG.talker = nmeadefs[sentence][0].ti;
			tx.VTG.cogT = that.ship.hdt.toFixed(3);	//	floatingp,		'course over ground, degrees true']
			tx.VTG.T = 'T';	//	literal('T'),	'True indicator']
			tx.VTG.cogM = that.ship.hdt.toFixed(3);	//	floatingp,		'course over ground, degrees magnetic']
			tx.VTG.M = 'M';	//	literal('M'),	'Magnetic indicator']
			tx.VTG.sogN = that.ship.stwL.toFixed(3);	//	floatingp,		'speed over ground in knots']
			tx.VTG.N = 'N';	//	literal('N'),	'Nautical miles indicator']
			tx.VTG.sogK = (that.ship.stwL*1.852).toFixed(3);	//	floatingp,		'speed over ground in l,/h']
			tx.VTG.K = 'K';	//	literal('K'),	'Kilometer indicator']
			tx.VTG.mode = 'S';	//	literal('[A|D|E|M|S|N]'),	'mode indicator']
			return true;
			break;
		
		case 'ZDA':	//	Vector track and Speed over the Ground',
			tx[sentence].talker	= nmeadefs[sentence][0].ti;
			tx[sentence].time		//	UTC time',]
				= zeroisim.prefixpad(that.ship.timestamp.getUTCHours(),2)
				+ zeroisim.prefixpad(that.ship.timestamp.getUTCMinutes(),2)
				+ zeroisim.prefixpad(that.ship.timestamp.getUTCSeconds(),2)
				;
			tx[sentence].day		= that.ship.timestamp.getUTCDay();	//	',integer(2),'UTC day, 01 to 31',]
			tx[sentence].month	= that.ship.timestamp.getUTCMonth();	//	',integer(2),'UTC month 01 to 12',]
			tx[sentence].year		= that.ship.timestamp.getUTCFullYear();	//	',integer(4),'UTC year',]
			tx[sentence].hzone	= that.ship.timestamp.getTimezoneOffset() / 60;	//	that.ship.timestamp.getUTCDay();	//	',integer(),'Local zone hours, 00 to ± 13 hrs',]
			tx[sentence].mzone	= that.ship.timestamp.getTimezoneOffset() % 60;	//	that.ship.timestamp.getUTCDay();	//	',integer(),'Local zone minutes, 00 to +59',]
			return true;
			break;
		
		default:
			console.log('support for  no transmission '+sentence+' implemented yet.')
			break;
		}
	return false;	
	}