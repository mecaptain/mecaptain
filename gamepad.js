'use strict';
var gamepadSupport = {
	
	ticking: false,	// Whether we�re requestAnimationFrameing like it�s 1999.
	gamepads: [],	// The canonical list of attached gamepads, without �holes� (always starting at [0]) and unified between Firefox and Chrome.

	// Remembers the connected gamepads at the last check; used in Chrome to figure 
	// out when gamepads get connected or disconnected, since no events are fired.
	prevRawGamepadTypes: [],
	prevTimestamps: [],	// Previous timestamps for gamepad state; used in Chrome to not bother with
								// analyzing the polled data if nothing changed (timestamp is the same as last time).
	init: function(that) {	// Initialize support for Gamepad API.
		this.caller = that; 
		var gamepadSupportAvailable = navigator.getGamepads ||
			!!navigator.webkitGetGamepads ||
			!!navigator.webkitGamepads;

		if (!gamepadSupportAvailable) {
			// It doesn�t seem Gamepad API is available � show a message telling
			// the visitor about it.
			this.caller.showNotSupported();
			} 
		else {
			// Check and see if gamepadconnected/gamepaddisconnected is supported.
			// If so, listen for those events and don't start polling until a gamepad
			// has been connected.
			if ('ongamepadconnected' in window) {
				window.addEventListener('gamepadconnected',
				gamepadSupport.onGamepadConnect, false);
				window.addEventListener('gamepaddisconnected',
				gamepadSupport.onGamepadDisconnect, false);
				} 
			else {
				// If connection events are not supported just start polling
				// console.log('gamepad.js: connection events are not supported just start polling');
				gamepadSupport.startPolling();
				}
			}
		}
	
	,onGamepadConnect: function(event) {	// React to the gamepad being connected.
		gamepadSupport.gamepads.push(event.gamepad);	// Add the new gamepad on the list of gamepads to look after.
		this.caller.updateGamepads(gamepadSupport.gamepads);// Ask the this.caller to update the screen to show more gamepads.
		gamepadSupport.startPolling();// Start the polling loop to monitor button changes.
		}

	,onGamepadDisconnect: function(event) {	//	React to the gamepad being disconnected.
		// Remove the gamepad from the list of gamepads to monitor.
		for (var i in gamepadSupport.gamepads) {
			if (gamepadSupport.gamepads[i].index == event.gamepad.index) {
			gamepadSupport.gamepads.splice(i, 1);
			break;
			}
		}

			// If no gamepads are left, stop the polling loop.
		if (gamepadSupport.gamepads.length == 0) {
			gamepadSupport.stopPolling();
			}

		// Ask the this.caller to update the screen to remove the gamepad.
		this.caller.updateGamepads(gamepadSupport.gamepads);
		}

	,startPolling: function() {	//	Starts a polling loop to check for gamepad state.
		// Don�t accidentally start a second loop, man.
		if (!gamepadSupport.ticking) {
			gamepadSupport.ticking = true;
			gamepadSupport.tick();
			}
		}

	// Stops a polling loop by setting a flag which will prevent the next
	// requestAnimationFrame() from being scheduled.
	,stopPolling: function() {
		gamepadSupport.ticking = false;
		},

	// A function called with each requestAnimationFrame(). Polls the gamepad
	// status and schedules another poll.
	tick: function() {
		gamepadSupport.pollStatus();
		gamepadSupport.scheduleNextTick();
		}

	,scheduleNextTick: function() {
		// Only schedule the next frame if we haven�t decided to stop via stopPolling() before.
		if (gamepadSupport.ticking) {
			if (window.requestAnimationFrame) {
				window.requestAnimationFrame(gamepadSupport.tick);
				} 
			else if (window.mozRequestAnimationFrame) {
				window.mozRequestAnimationFrame(gamepadSupport.tick);
				} 
			else if (window.webkitRequestAnimationFrame) {
				window.webkitRequestAnimationFrame(gamepadSupport.tick);
				}
			// Note lack of setTimeout since all the browsers that support
			// Gamepad API are already supporting requestAnimationFrame().
			}
		}

   //	Checks for the gamepad status. Monitors the necessary data and notices
   //	the differences from previous state (buttons for Chrome/Firefox,
   //	new connects/disconnects for Chrome). If differences are noticed, asks
   //	to update the display accordingly. Should run as close to 60 frames per
   //	second as possible.
	,pollStatus: function() {
		// Poll to see if gamepads are connected or disconnected. Necessary
		// only on Chrome.
		gamepadSupport.pollGamepads();
		gamepadSupport.gamepads.myforEach(function(gamepad,gamepadId,ar,that){
			// Don�t do anything if the current timestamp is the same as previous
			// one, which means that the state of the gamepad hasn�t changed.
			// This is only supported by Chrome right now, so the first check
			// makes sure we�re not doing anything if the timestamps are empty
			// or undefined.
			if (gamepad.timestamp != gamepadSupport.prevTimestamps[gamepadId]) {
				gamepadSupport.prevTimestamps[gamepadId] = gamepad.timestamp;
				that.caller.updateGamepad(that.gamepads[gamepadId]);
				}
			}
		,this);
		}

	// This function is called only on Chrome, which does not yet support
	// connection/disconnection events, but requires you to monitor
	// an array for changes.
	,pollGamepads: function() {
		// Get the array of gamepads � the first method (getGamepads)
		// is the most modern one and is supported by Firefox 28+ and
		// Chrome 35+. The second one (webkitGetGamepads) is a deprecated method
		// used by older Chrome builds.
		var rawGamepads =
			(navigator.getGamepads && navigator.getGamepads()) ||
			(navigator.webkitGetGamepads && navigator.webkitGetGamepads());

		if (rawGamepads) {
			// We don�t want to use rawGamepads coming straight from the browser,
			// since it can have �holes� (e.g. if you plug two gamepads, and then
			// unplug the first one, the remaining one will be at index [1]).
			gamepadSupport.gamepads = [];

			// We only refresh the display when we detect some gamepads are new
			// or removed; we do it by comparing raw gamepad table entries to
			// �undefined.�
			var gamepadsChanged = false;

			for (var ii=0; ii<rawGamepads.length; ii++) {
				if (typeof rawGamepads[ii] != gamepadSupport.prevRawGamepadTypes[ii]) {
					gamepadsChanged = true;
					gamepadSupport.prevRawGamepadTypes[ii] = typeof rawGamepads[ii];
					}

				if (rawGamepads[ii]) {
					gamepadSupport.gamepads.push(rawGamepads[ii]);
					}
				}

			// Ask the this.caller to refresh the visual representations of gamepads
			// on the screen.
			if (gamepadsChanged) {
				this.caller.updateGamepads(gamepadSupport.gamepads);
				}
			}
		},
	};
