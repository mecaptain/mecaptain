//	aistable.js
'use strict';
var add2aistable = function(aissentence,me){
	var UserID = undefined;	
	var aispos;
	var obj = {};
	if (undefined==aissentence){
		return;	//	nothing to do
		}
	// aissentence.forEach(function(tupel){
	obj['ownship'] = me;
	for (var ii in  aissentence){
		var tupel = [ii,aissentence[ii]];
		switch (tupel[0]){	//	je kunt ook de numerieke [0] nemen, perform misschien beter maar minder leesbaar
			case 'MessageID':	break;
			case 'UserID':	//	MMSI is unique
				UserID = tupel[1];
				obj['MMSI'] = tupel[1];
				if (undefined!=me && me)	ship0.mmsi = tupel[1];
				break;
			case 'Longitude':	aispos = new zeroisim.LatLon({londeg:tupel[1]});			obj['lon_dmd'] = aispos.londegmindec;			obj[tupel[0]] = tupel[1];	break;	//	nice formatted position
			case 'Latitude':	aispos = new zeroisim.LatLon({latdeg:tupel[1]});			obj['lat_dmd'] = aispos.latdegmindec;			obj[tupel[0]] = tupel[1];	break;
			// case 'Longitude':	
			// case 'Latitude':	
			case 'ROT':			obj[tupel[0]] = zeroisim.roundNumber(tupel[1],2);	break;
			case 'NavigationalStatus': 
				obj[tupel[0]] = tupel[1];
				break;
			case 'TrueHeading':
				obj['HDGt'] = tupel[1];
				break;
			case 'CallSign':	
				if (undefined!=me && me)	ship0.CallSign = tupel[1];
				obj[tupel[0]] = tupel[1];
				break;
			case 'Name':	
				if (undefined!=me && me)	ship0.Name = tupel[1];
				obj[tupel[0]] = tupel[1];
				break;
			case 'ShipType':	
			case 'COG':	
			case 'IMOnumber':	
			case 'dimRefPoint':	
			case 'ETA':	
			case 'draught':	
			case 'destination':	
			case 'SOG':	
			case 'TimeStamp':	
				obj[tupel[0]] = tupel[1];
				break;
			case 'PositionAccuracy':	break;
			case 'ResforRegApps':	break;
			case 'Spare':	break;
			case 'RAIMFlag':	break;
			case 'CommunicationState':	break;
			case 'RepeatIndicator':	break;
			case 'AISversion':	break;
			case 'PosFixDev':	break;
			case 'DTE':	break;
			case 'spare':	break;
			}
		}
		// });
	if (undefined!=UserID){
		if (undefined==aislist[UserID]){
			aislist[UserID] = obj;
			}
		else {	//	exists, i need a union
			for (var attrname in obj) { 
				aislist[UserID][attrname] = obj[attrname]; 
				}
			}
		}
	}	//	add2aistable
