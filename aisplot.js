//	aisplot.js
// plot de ontvangen ais sentences in een soort radarschermpositie op een middelbreedtekaartje
//
'use strict';
var autorefresh = false;	//	todo get this out of global space
var aisplot_tab_page = {
	html: function(){
		this.idcv = 'ais';
		this.hasFocus = true;	//	to force initial draw
		window.addEventListener("load", this.onload.bind(this), false);
		this.aismsgs = new Array();
		var hpw = ' style: width=100%;';
		return h.table(h.tr(h.td(h.canvas(h.attributes(
			{id:this.idcv
			,width:settings.aisgraph_diameter
			,height:settings.aisgraph_diameter
			,style:'border:0px solid #000;'
			})))
		+h.td(h.table
			(h.tr(h.td(h.table(h.tr(h.td('range',h.attributes({colspan:3,style:"text-align:center;"})))+h.tr
					(h.td(h.button('&#43','id=btn_ais_rng_plus'+hpw))
					+h.td(h.div('rng','id=txt_ais_rng'))
					+h.td(h.button('&#45','id=btn_ais_rng_min'+hpw))
					),hpw)))
					
			+h.tr(h.td(h.fieldset("cursor",h.table
				(h.tr(h.td('lat<br/>lon')+h.td(h.div("00"+h.deg+"00.00' N"+h.br+"000"+h.deg+"00.00' E"	,'id=aisplot_display_pos')))
				+h.tr(h.td('brg')+h.td(h.div('0.0'+h.deg																,'id=aisplot_display_brg')))
				+h.tr(h.td('rng')+h.td(h.div("0.0'"																		,'id=aisplot_display_rng')))
				))))
				
			+h.tr(h.td(h.table
				(h.trtd(h.button('ais CPA matrix','onclick=getaiscpamatrix("aistable")'))
				+h.trtd(h.button('refresh aislist','onclick=getaistable("aistable")'))
				+h.trtd(h.input('autorefresh','type="checkbox" id="aislist_autorefresh" onclick="check(this);autorefresh=this.checked;" unchecked'))
				)))
			)))			
		+ h.tr(h.td(h.div('aislist','id="aistable"'),'colspan=2'))
		);
		}
			
	,ranges:		[0.25,0.5,0.75,1.5,3,6,12,24,48]
	,onfocus:function(){	this.hasFocus=true;		}
	,onblur:function(){	this.hasFocus=false;		}
	,onload: function(){
		// install mouse event hanglers
		var canvas = document.getElementById(this.idcv);
		canvas.that = this;
		canvas.onmousedown= function(e){	e.currentTarget.that.mousedown(e);	}
		canvas.onmouseup	= function(e){	e.currentTarget.that.mouseup(e);		}
		canvas.onmousemove= function(e){	e.currentTarget.that.mousemove(e);	}
		// canvas.onwheel		= function(e){	e.currentTarget.that.mousewheel(e);	}
		canvas.addEventListener('wheel', function(e){	e.currentTarget.that.mousewheel(e);	}, {passive: true});
		
		document.getElementById('btn_ais_rng_plus').addEventListener("click", this.rangePlus.bind(this), false);
		document.getElementById('btn_ais_rng_min').addEventListener("click", this.rangeMin.bind(this), false);

		this.cv = canvas.getContext("2d");
		this.midden = {x: canvas.width/2, y: canvas.height/2};
		this.canvas = canvas;
		this.llmidden = new zeroisim.LatLon({latdeg: 0, londeg: 0});	//	assume midpoint
		// this.scale = 1;	//	verhouding boogminuut / pixels
		this.range = this.ranges[Math.trunc(this.ranges.length/2)];		setValue('txt_ais_rng',this.range);
		this.degtorad = Math.PI / 180.0;
		this.aisShipShape = [[-1,2],[1,2],[1,-2],[0,-3],[-1,-2],[-1,2]];
		// this.aisShipShape = [[-1,2],[1,2],[0,-3],[-1,2]];
		this.aistarget = [[0,-3],[1,-1],[1,2],[0,1],[-1,2],[-1,-1],[0,-3]];
		this.count = 0;
		this.mdown = false;
		this.fps = 3;
		this.vdo = false;	//als er worden VDO's worden ontvangen, bepalen die het middelpunt, zo niet, dan ship0
		var d = new Date();		
		this.lastRefresh = d.getTime();		
		autorefresh = false;	//	milliseconds	// todo make setting
		this.refresh_interval = 1000;	//	milliseconds	// todo make setting
		this.cursorpos = new zeroisim.LatLon({latdeg: 0, londeg: 0});	//	assume midpoint
		this.draw(0,0,0);
		this.hasFocus=false;
		
		cbRxNmea('VDO',function(rx,that){	//	install callback handler upon rx of VDO
			if (undefined==rx.data)	return;
			that.ais_rxd(rx,true);
			},this);
		cbRxNmea('VDM',function(rx,that){	//	install callback handler upon rx of VDM
			if (undefined==rx.data)	return;
			that.ais_rxd(rx,false);
			},this);	// maak daar bij de aanroep een that van
		}
	,ais_rxd: function(rx,vdo){	//	todo maak aan een rxd functie die json aanneemt als data
		if (settings.decode_6bit){
			var tim = rx.date.getTime();
			var data=rx.data;
			if (data.totns>1){			//	more than 1 sentences form a msg
				if (data.sentnum==1){	//	this is first msg
					this.aismsgs[data.messid] = new Object;	//	create a aismsg object
					this.aismsgs[data.messid].totns = data.totns;
					this.aismsgs[data.messid].msg = data.msg;
					}
				else {						//	this is a next message
					this.aismsgs[data.messid].msg += data.msg;
					}
				if (data.sentnum==this.aismsgs[data.messid].totns){	//	last message is received
					var aissentence = decodeVDM(this.aismsgs[data.messid].msg);
					// var test=JSON.stringify(aissentence);
					this.aismsgs[data.messid] = undefined;	// destruct
					this.ais_decoded(aissentence,vdo);
					// add2aistable(aissentence,vdo);			//	let this function assign mmsi to ship0
					}
				}
			else {	//	aismessage in one transmission
				var aissentence = decodeVDM([data.msg]);	//	defined in ais_decoder.js
				this.ais_decoded(aissentence,vdo);
				// add2aistable(aissentence,vdo);				//	let this function assign mmsi to ship0
				}
			}
		}
	,ais_decoded: function(aissentence,vdo){
		// todo deze functie aanroepen ipv add2aistable
		add2aistable(aissentence,vdo);				//	let this function assign mmsi to ship0
		// en hier de vdo processing doen, hier weet je zeker dat je een aissentence hebt
		if (autorefresh) {
			if ((tim-this.lastRefresh) > this.refresh_interval){	//	todo check refresh checkbox
				this.lastRefresh = tim;
				getaistable("aistable");
				}
			}
		if (vdo){
			// setShipCentre: function(tf){
			this.vdo = true;	//er worden VDO's ontvangen, dit bepaald het middelpunt, zo niet, dan ship0 nemen
			if (undefined==aissentence){
				var breakpoint = 0;
				}
			if (undefined!=aissentence.Latitude){
				this.llmidden.latdeg = aissentence.Latitude;
				this.llmidden.londeg = aissentence.Longitude;
				}					
			}
		else if (!this.vdo){
			this.llmidden = ship0.latlon;
			}
		if (this.hasFocus){
			aislist.myforEachSparse(function(aistarget,idx,arr,that){
				if (undefined!=aistarget.Latitude){	//	maw wacht op een VDM msg:5
					if (aistarget.ownship!=true){	//	todo switchable
						var xy = that.latlongToCanvasPoint(aistarget.Latitude,aistarget.Longitude);	// get center from the map (projected)
						var dist = Math.sqrt(xy.x*xy.x+xy.y*xy.y);	//	+that.pulselength/2;
						if (dist>that.midden.x)	return;	//	off screen
						that.drawShape(xy,aistarget.COG,6,that.aisShipShape);
						}
					}
				},this);
			}
		}
	,rangePlus: function(){
		this.range = this.ranges[(this.ranges.indexOf(this.range)+1)% this.ranges.length];
		setValue('txt_ais_rng',this.range);
		this.draw(this.course,this.lat,this.lon);
		}
	,rangeMin: function(){
		var idx = this.ranges.indexOf(this.range);
		if (0==idx) idx = this.ranges.length
		this.range = this.ranges[--idx];
		setValue('txt_ais_rng',this.range);
		this.draw(this.course,this.lat,this.lon);
		},
	draw: function(course,lat,lon) {
		if (!this.hasFocus)	return;
		this.course=course;
		this.lat=lat;
		this.lon=lon;
		this.cv.clearRect(0, 0, 2*this.midden.x, 2*this.midden.y);
		this.cv.strokeStyle = '#000';
		//	draw a cross
		this.cv.beginPath();
		this.cv.moveTo(this.midden.x,this.canvas.height);			this.cv.lineTo(this.midden.x,0);
		this.cv.moveTo(this.canvas.width,this.midden.y);			this.cv.lineTo(0,this.midden.y);
		this.cv.stroke();

		var rings=[1/3, 2/3, 3/3];
		for (var ring in rings) {
			this.cv.beginPath();
			this.cv.arc(this.midden.x, this.midden.y, this.midden.x * rings[ring], 0, 2 * Math.PI);
			this.cv.stroke();
			}
		// for (var aistarget in this.targets){
			// this.aisplot(this.targets[aistarget]);
			// }
		}
	,drawShape: function(cvxy,hdt,schaal,shape,color,linewidth){
		this.cv.save();
		this.cv.translate(this.midden.x+cvxy.x,this.midden.y+cvxy.y);
		this.cv.rotate(hdt*Math.PI/180.0);
		if (undefined==color) color = '#F00';
		if (undefined==linewidth) linewidth = 3;
		this.cv.strokeStyle = color;
		this.cv.lineWidth = linewidth;
		this.cv.beginPath();
		if (schaal<5)	schaal = 5;
		shape.myforEach(function(pp,idx,arr,that){
			if (0==idx)	that.cv.moveTo(schaal*pp[0], schaal*pp[1]);
			that.cv.lineTo(schaal*pp[0], schaal*pp[1]);
			},this);			
		this.cv.closePath();		
		this.cv.stroke();
		this.cv.restore();
		}
	,aisplot: function(aistarget){
		if (undefined==aistarget.HDGt) return;
		var course = aistarget.HDGt;
		var latlon = {lat: aistarget.Latitude, lon: aistarget.Longitude}
		var sinfi;
		this.cv.save();
		var straal=6;
		var xy = this.latlon2xy(latlon);
		this.cv.beginPath();
		var schaal = 3;	//	size of the schip figure
		var lijn;
		for (var idxlijn in this.aisShipShape) {
			lijn = this.aisShipShape[idxlijn];
			sinfi = Math.sin(course * this.degtorad);
			var cosfi = Math.cos(course * this.degtorad);
			var rot =
				{x: lijn[0]*cosfi - lijn[1]*sinfi
				,y: lijn[0]*sinfi + lijn[1]*cosfi};
			this.cv.lineTo( xy.x + schaal * rot.x, xy.y + schaal * rot.y );
			}
		this.cv.stroke();
		this.cv.restore();
		},
	windowToCanvas: function(x,y) {
		var bbox = this.canvas.getBoundingClientRect();
		return { x: x - bbox.left * (this.canvas.width  / bbox.width),
					y: y - bbox.top  * (this.canvas.height / bbox.height)
				 };
		},
	ggmmd: function(num,digits,decimals,posneg){
		var rc = {'degrees':0,'minutes':0,'sign':' '};
		rc.degrees = zeroisim.prefixpad(Math.floor(Math.abs(num)).toString(),digits);
		rc.minutes = zeroisim.prefixpad((60*(Math.abs(num)-Math.floor(Math.abs(num)))).toFixed(decimals),2);
		rc.sign = posneg[0];
		if (num<0)	rc.sign=posneg[1];
		return rc.degrees + h.deg + rc.minutes + "'" + rc.sign;
		},
	mousemove: function(evt){
		var pos = this.windowToPosition(evt);
		var bd = pos.beardist(this.llmidden);
		setValue('aisplot_display_pos',pos.latdegmindec+h.br+pos.londegmindec);
		setValue('aisplot_display_brg',bd.b + h.deg);
		setValue('aisplot_display_rng',bd.d + "'");
		},
	mousedown: function(evt){
		console.log('.mousedown()');
		this.mdown = this.xy2latlon(this.windowToCanvas(evt.clientX,evt.clientY));
		},
	mouseup: function(evt){
		console.log('.mouseup()');
		if (this.mdown!==false){
			this.mdown = false;
			}
		}
	,mousewheel: function(evt){
		}
	,windowToCanvas: function(x,y) {
		var bbox = this.canvas.getBoundingClientRect();
		return { x: x-bbox.left * (this.canvas.width  / bbox.width)
				 ,	y: y-bbox.top  * (this.canvas.height / bbox.height)
				 };
		}
	,windowToPosition: function(evt) {
		var posPpi = this.windowToCanvas(evt.clientX,evt.clientY);	//	offset in pixels from origin of canvas
		posPpi.x -= this.midden.x;	// offset in pixels from midpoint of canvas
		posPpi.y -= this.midden.y;	// offset in pixels from midpoint of canvas
		this.cursorpos.latdeg = this.llmidden.latdeg-posPpi.y*this.range/60/this.midden.y;
		this.cursorpos.londeg = this.llmidden.londeg+posPpi.x*this.range/60/this.midden.x/Math.cos(this.llmidden.latrad);
		return this.cursorpos;
		}		
	,latlongToCanvasPoint: function(lat,lon){
		var dlat = 60.0*(this.llmidden.latdeg-lat);
		var dlon = 60.0*(lon-this.llmidden.londeg);
		var ypixels = dlat * this.midden.y / this.range;
		var xpixels = dlon * this.midden.x / this.range * Math.cos(this.llmidden.latrad);
		return {x:xpixels, y:ypixels};
		}
	};
