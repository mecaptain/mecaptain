//	ecdis.js
// zeroisim in simulator mode, send some nmea sentences to an ecdis on a remote ip:port

var world=zeroisim.gup('world','abc')
var ship=zeroisim.gup('ship','Bliss');
settings.ws_addr=zeroisim.gup('ws_addr','ws://bert-toshsat:12120');
var sentences = eval(zeroisim.gup('sentences'));	
var ecdisadress = zeroisim.gup('ecdisadress','udp://localhost:1500');
var position = zeroisim.gup('position','maasgeul in');
var heading = zeroisim.gup('heading');

intern_simulator.setSimShip(ship);
var op = [{"address":ecdisadress,"sentences":sentences}];
intern_simulator.is.ship.NmeaOutputChannels = op;
intern_simulator.is.ship.latlon= new zeroisim.LatLon(positions[position]);
intern_simulator.is.ship.hdt= parseInt(heading);

setValue('inp_ws_addr',settings.ws_addr,'value',1);	
setValue('talkaddr',op[0].address,'value',1);
wsStartListenToNmeaShip0();
wsNmeaBridge({ws_addr:settings.ws_addr,world:world,getMMSI:1,getCallSign:1,verbose:0},intern_simulator.is.ship);
window.is_engage.click();	//	start de simulator
