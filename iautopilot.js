//	iautopilot.js

// todo AEC cmds komen niet binnen in extwin mode

'use strict';

class CInstrument_ap extends CInstrument {
	constructor(obj,exposedParms,active,idx){
		super(idx,obj);
		if (undefined!=exposedParms)
			this.exposedParms = exposedParms;
		else
			this.exposedParms =
				{nmea:	
					{compassHDG: {s:'THS',f:'hdgT'}
					,compassROT: {s:'ROT',f:'rot'}
					,compassRSA: {s:'RSA',f:'srud'}
					,compassAPN: {s:'APN',f:'mode'}
					,compassCOG: {s:'RMC',f:'cog'}	//	may be VTG.cogT or RMC.cog	//	fror TRK control
					}
				,colorBackground:	'rgba(255,255,255,0.0)'	//	gray	
				,colorModeKnobOff:'#EEE'
				,colorModeKnobOn:	'#0F0'
				,tableBorder:		'3px solid rgba(255,255,255,0.4)' // 3px solid #FFF;
				};

		this.ship = {hdt:0.0,rot:0.0};
		this.active = (undefined==active)? false: active;
		this.engaged = new zeroisim.boolean_value(false,this.engagefun.bind(this));
		this.setpoint = new zeroisim.new_ascal_value('angle',0,this.setpointfun.bind(this));// kan een angle zijn, [0,360> of [-200,+200] of een radius in mijlen of meter of een track-id
		this.mode = new zeroisim.new_enum_value('change_hdg'
			,this.setmodefun.bind(this)
			,['change_hdg','keep_hdg','change_rot','keep_rot','change_track','keep_track','change_radius','keep_radius']
			);

		this.style_control= 
			['xborder: 0px solid black'
			,'height: 45px'
			,'white-space:nowrap'
			,'text-align:center'
			,'-moz-border-radius: 8px'
			,'-webkit-border-radius: 8px'
			,'font-family: Arial, Helvetica, sans-serif'
			,'color: #EEE'
			,'text-align: center'
			,'min-width: 40px'
			,'border: 3px solid #FFF'
			,'margin: 0px'
			,'padding: 1px'
			,'unselectable: on'
			,'-webkit-user-select: none'	// Safari
			,'-moz-user-select: none'		// Firefox
			,'-ms-user-select: none'		// IE10+/Edge
			,'user-select: none'				// Standard
			];
			// ,'background-color: #88AAAA'
			// ,'font-size: 1.1em'
		
		this.active = (undefined==active)? false: active;
		if (active){
			this.autopilot = PID_controller();	//	todo er kan maar 1 autopilot zijn, deze of iautopilot
			this.autopilot.init();	//	misschien init in pid constructor
			}
		
		var cw='width:84px; background-color:#bbb; font-family:\'Lucida Sans\', \'Lucida Sans Regular\', \'Lucida Grande\', \'Lucida Sans Unicode\', Geneva, Verdana, sans-serif; color:#ffffff;  text-shadow: 1px 1px #000; font-weight:600;';
		var fs = 'font-size: 160%;';
		
		this.controlModes = i.table(i.tr(
		// [i.td('mode',{colspan:1,style:cw})	//	alleen in uitgebreide mode
			[i.td(zeroisim.ionoffswitch('ap_engage',this.apEngage.bind(this)),{style:'width:93px;border:'+this.exposedParms.tableBorder})
			,this.defaultControlMode
			=this.apbtn('heading','imode_heading'	,{style:"width:calc((100% - 93px) / 4);"})
			,this.apbtn('rot','imode_rot'				,{style:"width:calc((100% - 93px) / 4);"})
			,this.apbtn('track','imode_track'		,{style:"width:calc((100% - 93px) / 4);"})
			,this.apbtn('radius','imode_radius'		,{style:"width:calc((100% - 93px) / 4);"})
			]),{style:'width:100%;'});
		this.setPointText = i.div('000',{id:'iap_setpoint_txtvalue',class:'bigwhite_old',style:'width:100px;margin:auto;text-align:center;font-size: 300%;color:#ffffff;text-shadow: 1px 1px #000; font-weight:600;'});	
		this.controls = i.table(i.tr(
		// [i.td('setpoint',{title:'move setpoint up and down',style:fs})
			[this.apbtn_port('&minus;&minus;','iminusminus')
			,this.apbtn_port('&minus;','iminus')
			,i.td(this.setPointText,{style:'white-space:nowrap;border:'+this.exposedParms.tableBorder},{style:''})
			,this.apbtn_sbord('&plus;','iplus')
			,this.apbtn_sbord('&plus;&plus;','iplusplus')
			]),{style:'width:100%;border-collapse:collapse'});
		this.draw();
		if (this.active==true) {
			var d = new Date();
			this.lasttime = d.getTime();
			
			super.cbRxNmea(this.exposedParms.nmea.compassHDG.s,function(rxd,that){
				var d = new Date();
				var n = d.getTime();
				// var dt = n - this.lasttime;
				var dt = n - that.lasttime;
				// this.lasttime = n;
				that.lasttime = n;
				if (dt>100000) return;	//	useless, delta time is too long
				if (!(that.engaged.get()))	return;	
				if (that.mode.get()=='change_hdg' || that.mode.get()=='keep_hdg'){	
					that.ship.hdt = parseFloat(rxd.data[that.exposedParms.nmea.compassHDG.f]);
					// console.log('THS mode: setpoint='+that.setpoint.get()+' rot='+ship0.hdt+' errval='+zeroisim.deltaDirection(ship0.hdt,that.setpoint.get(),1));
					var ap_rudder = that.autopilot.tick(dt,zeroisim.deltaDirection(that.ship.hdt,that.setpoint.get(),1),that.ship.rot);	
					//	todo rudder should be filtered
					ap_rudder = Math.round(ap_rudder);
					if (that.old_filtered_output.get() != ap_rudder){
						that.rudder(ap_rudder,ap_rudder);
						that.old_filtered_output.change(ap_rudder);
						}
					}
				},this);
			super.cbRxNmea('ROT',function(rxd,that){	//	install callback for rot
				if (undefined==rxd.data)	return;
				if (!(that.engaged.get()))	return;	
				if (!(that.mode.get()=='change_rot' || that.mode.get()=='keep_rot'))	return;	
				var d = new Date();
				var n = d.getTime();	//	todo dit spul centraliseren
				var dt = n - that.lasttime;
				// if (isNaN(dt)){
					// var brk = 0;
					// }
				that.lasttime = n;
				if (dt>100000) return;	//	useless, delta time is too short
				var rot = that.ship.rot = parseFloat(rxd.data.rot);	//	
				
				// console.log('ROT mode: setpoint='+that.setpoint.get()+' rot='+ship0.rot+' errval='+(that.setpoint.get()-ship0.rot));
				var ap_rudder = that.autopilot.tick(dt,that.setpoint.get()-that.ship.rot,that.ship.rot);	
				//	todo rudder should be filtered
				ap_rudder = Math.round(ap_rudder);
				if (that.old_filtered_output.get() != ap_rudder){
					that.rudder(ap_rudder,ap_rudder);
					that.old_filtered_output.change(ap_rudder);
					}
				},this);
			super.cbRxNmea('AEC',function(rxd,that){	//	install callback for autopilot remote control commands
				if (undefined==rxd.data)	return;
				if (!(that.engaged.get()))	return;	
				if ((undefined!=rxd.data.AutoPilotMode) && !(that.mode.get()=='change_hdg' || that.mode.get()=='keep_hdg')){	
					that.mode.set('change_hdg');
					that.setpoint.setmode('angle');	
					}
				that.setpoint.set(Math.floor(rxd.data.Setpoint));
				},this);
			super.cbRxNmea('MC0',function(rxd,that){	//	install callback for autopilot remote control commands
				if ((100+mc0.getApK) == rxd.data.req){	//	answer upon request
					that.Kp_hdg=	new zeroisim.scalar_value( rxd.data.ApK.Kp_hdg,function(that){/*if (undefined!=autopilot)*/	that.autopilot.setvalues({Kp_hdg:that.get()});});
					that.Ki_hdg=	new zeroisim.scalar_value( rxd.data.ApK.Ki_hdg,function(that){/*if (undefined!=autopilot)*/	that.autopilot.setvalues({Ki_hdg:that.get()});});
					that.Kd_hdg=	new zeroisim.scalar_value( rxd.data.ApK.Kd_hdg,function(that){/*if (undefined!=autopilot)*/	that.autopilot.setvalues({Kd_hdg:that.get()});});
					that.Kp_rot=	new zeroisim.scalar_value( rxd.data.ApK.Kp_rot,function(that){/*if (undefined!=autopilot)*/	that.autopilot.setvalues({Kp_rot:that.get()});});
					that.Ki_rot=	new zeroisim.scalar_value( rxd.data.ApK.Ki_rot,function(that){/*if (undefined!=autopilot)*/	that.autopilot.setvalues({Ki_rot:that.get()});});
					that.Kd_rot=	new zeroisim.scalar_value( rxd.data.ApK.Kd_rot,function(that){/*if (undefined!=autopilot)*/	that.autopilot.setvalues({Kd_rot:that.get()});});
					that.Kp_trk=	new zeroisim.scalar_value( rxd.data.ApK.Kp_trk,function(that){/*if (undefined!=autopilot)*/	that.autopilot.setvalues({Kp_trk:that.get()});});
					that.Ki_trk=	new zeroisim.scalar_value( rxd.data.ApK.Ki_trk,function(that){/*if (undefined!=autopilot)*/	that.autopilot.setvalues({Ki_trk:that.get()});});
					that.Kd_trk=	new zeroisim.scalar_value( rxd.data.ApK.Kd_trk,function(that){/*if (undefined!=autopilot)*/	that.autopilot.setvalues({Kd_trk:that.get()});});
					that.Kp_rad=	new zeroisim.scalar_value( rxd.data.ApK.Kp_rad,function(that){/*if (undefined!=autopilot)*/	that.autopilot.setvalues({Kp_rad:that.get()});});
					that.Ki_rad=	new zeroisim.scalar_value( rxd.data.ApK.Ki_rad,function(that){/*if (undefined!=autopilot)*/	that.autopilot.setvalues({Ki_rad:that.get()});});
					that.Kd_rad=	new zeroisim.scalar_value( rxd.data.ApK.Kd_rad,function(that){/*if (undefined!=autopilot)*/	that.autopilot.setvalues({Kd_rad:that.get()});});
					that.rlimp=		new zeroisim.scalar_value( rxd.data.ApK.rlimp, function(that){/*if (undefined!=autopilot)*/	that.autopilot.setvalues({rlimp:that.get()});});
					that.rlims=		new zeroisim.scalar_value( rxd.data.ApK.rlims, function(that){/*if (undefined!=autopilot)*/	that.autopilot.setvalues({rlims:that.get()});});
					that.useRot=	new zeroisim.boolean_value(rxd.data.ApK.useRot,function(that){/*if (undefined!=autopilot)*/	that.autopilot.setvalues({useRot:that.get()}); if (that.get()) autopilot.clear();});
					that.old_filtered_output=	new zeroisim.scalar_value(0,		function(that){}),
					that.autopilot.init(rxd.data.ApK);
					that.defaultControlMode.click();
					}
				},this);
			super.busTX('MC0',{req:mc0.getApK});	//	request simulator for name, mmsi and callsign
			
			// todo alleen als debug vinkje op AP aan staat	
			if (1){
				setInterval(function(that){
					that.debug_update({});
					},100,this);
				}
			}
		}
	debug_update(evt){
		var pid_debug = this.autopilot.piddebug;
		var order={control:this.mode.get().toUpperCase(),setpoint:this.setpoint.get()};
		order = Object.assign(order,pid_debug);
		super.busTX('APN',order);
		}
	apEngage(evt){
		console.log('apEngage(evt)');
		this.engaged.toggle();
		}
	engagefun(that){
		if (that.get()) {	//	ap engaged
			this.autopilot.clear();
			switch (this.mode.get()){
				case 'change_rot':	case 'keep_rot':	this.setAP({mode:'rot', setpoint:this.setpoint.get()});	break;
				case 'change_hdg':	case 'keep_hdg':	this.setAP({mode:'hdg', setpoint:this.setpoint.get()});	break;
				default:	// todo set first mode true
					this.defaultControlMode.click();
					var test = this.mode.get();
					//	todo goto label test
					break;
				}
			}
		else {	//	ap disengaged
			if (settings.rudder0_on_ap_off) {
				this.rudder(0,0);
				}
			// this.setAP({mode:'off'});
			this.setAP({onoff:'off'});
			}	
		// setValue('aplegend','autopilot '+(that.get()?'engaged':'idle'));
		}
	setpointfun(evt){
		switch (this.mode.get()){
			case 'change_hdg':
			case 'keep_hdg':
				this.setPointText.innerHTML = this.setpoint.get();
				this.mode.set('change_hdg');		
				this.setAP({mode: 'hdg',setpoint:this.setpoint.get()});
				break;
			case 'change_rot':
			case 'keep_rot':
				this.setPointText.innerHTML = this.setpoint.get();
				this.mode.set('change_rot');		
				this.setAP({mode: 'rot',setpoint:this.setpoint.get()});
				break;
			default:
				console.log('ap not supported: "'+this.mode.get()+'"');
				break;
			}
		}
	setmodefun(that,thot){
		if (undefined!=this.autopilot)	
			this.autopilot.setvalues({mode:that.get()});
		//setValue('ap_mode',that.get());
		this.setAP({mode:that.get().substring(that.get().length-3)});	
		}
	_apbtn(txt,id,klass,xattr){
		if (id==undefined)	id = txt;
		if (xattr==undefined)	xattr = {};
		return i.td(i.center(txt),Object.assign(xattr,{class:klass,click:this.autopilotcommand.bind(this,'ap_'),id:id}));
		}
	apbtn(txt,id,xattr){			return this._apbtn(txt,id,'tg1old' ,{style:['font-size:1.1em','background-color:#8AA'].concat(this.style_control).join(';')});	}
	apbtn_port(txt,id,xattr){	return this._apbtn(txt,id,'tg_lold',{style:['font-size:300%', 'background-color:#E00'].concat(this.style_control).join(';')});	}
	apbtn_sbord(txt,id,xattr){	return this._apbtn(txt,id,'tg_rold',{style:['font-size:300%', 'background-color:#0E0'].concat(this.style_control).join(';')});	}
	autopilotcommand(prefix,evt){
		var that = evt.currentTarget;
		// setValue('autopilot_command','pilotcommand '+prefix+that.id);
		var sw = prefix+that.id;
		switch (sw){
			case 'ap_iminus':						this.setpoint.dec(1);	break;
			case 'ap_iplus':						this.setpoint.inc(1);	break;
			case 'ap_iminusminus':				this.setpoint.dec(10);	break;
			case 'ap_iplusplus':					this.setpoint.inc(10);	break;
			case 'ap_imode_rot':
			case 'ap_imode_track': 
			case 'ap_imode_radius': 
			case 'ap_imode_heading':		
				var modes = ['imode_rot','imode_track','imode_radius','imode_heading'];
				modes.myforEach(function(mode,dd,ar,that){
					var test = document.getElementById(mode);
					if (undefined!=test){
						test.style.color = that.exposedParms.colorModeKnobOff;	//	zet lichtje in alle knoppen uit
						}
					},this);
				var test=document.getElementById(that.id);
				if (undefined!=test){
					test.style.color = this.exposedParms.colorModeKnobOn;	//	zet lichtje in gekozen knop aan
					}
				switch (sw){
					case 'ap_imode_heading':this.mode.set('change_hdg');		this.autopilot.setvalues({Kp:this.Kp_hdg.get(),Ki:this.Ki_hdg.get(),Kd:this.Kd_hdg.get()});	this.setpoint.setmode('angle');	this.setpoint.set(Math.floor(this.ship.hdt));	break;
					case 'ap_imode_rot':		this.mode.set('change_rot');		this.autopilot.setvalues({Kp:this.Kp_rot.get(),Ki:this.Ki_rot.get(),Kd:this.Kd_rot.get()});	this.setpoint.setmode('scalar');	this.setpoint.set(Math.floor(this.ship.rot));	break;
					case 'ap_imode_track': 	this.mode.set('change_track');	this.autopilot.setvalues({Kp:this.Kp_trk.get(),Ki:this.Ki_trk.get(),Kd:this.Kd_trk.get()});	this.setpoint.setmode('scalar');	this.setpoint.set(0);	break;
					case 'ap_imode_radius': this.mode.set('change_radius');	this.autopilot.setvalues({Kp:this.Kp_rad.get(),Ki:this.Ki_rad.get(),Kd:this.Kd_rad.get()});	this.setpoint.setmode('scalar');	this.setpoint.set(0);	break;
					}
				break;
			}
		}
	setAP(set){	//	send APN sentence to compasses in this program
		var order = {};
		if (undefined!=set.mode){
			switch (set.mode){	//	todo overweeg de P,I & D waarden erbij te zenden zodat het kompas die kan tonen this.autopilot.integral
			//	case 'off':order={control:set.mode.toUpperCase()};									break;
				case 'hdg':order={control:set.mode.toUpperCase(),orderedhdg:set.setpoint};	break;
				case 'rot':order={control:set.mode.toUpperCase(),rot_rad:set.setpoint};		break;
				case 'rad':order={control:set.mode.toUpperCase(),rot_rad:set.setpoint};		break;
				case 'trk':order={control:set.mode.toUpperCase(),orderedhdg:set.setpoint};	break;	//	todo
				}
			if (this.engaged.get()){	
				super.busTX('APN',order);
				}
			}
		else if (undefined!=set.onoff){	
			switch (set.onoff){
				case 'on':	order={control:'ON'};	break;
				case 'off':	order={control:'OFF'};	break;
				}
			super.busTX('APN',order);
			}
		//	todo als nmeadefs['APN'][0].rxcb undefined is, draait bv in extern window, moet een ander comms kanaal gevonden worden.	
		//	denk bv aan local memory
		//	if not engaged than skip
		}
	rudder(srud,prud){
		var ror = 
			{srud:	srud				//	'Starboard (or single) rudder order in degrees, "-" = port:0.0]
			,svalid:	'A'				//	'Starboard rudder status, A=Data valid, V=Data invalid:'A']
			,prud:	prud				//	'Port rudder order in degrees , "-" = port:0.0]
			,pvalid:	'A'				//	'Port rudder status, A=Data valid, V=Data invalid:'A']
			,src:		'P'				//	Command source location 'B|P|S|C|E|W'
			,from:	'iautopilot'
			,talker: nmeadefs.ROR[0].ti
			};
		super.busTX('ROR',ror);
		}
	resize(w,h){
		}
	draw(){
		if (this.active!=true){
			zeroisim.replaceNode(this.div,i.span(i.center('AP')));
			return;
			}
		zeroisim.replaceNode(this.div,i.table(
			[i.tr(this.controlModes)
			,i.tr(this.controls)
			// ],{style:'width:100%;border:1px solid #FFF;'}));
			],{style:'width:100%;border:'+this.exposedParms.tableBorder}));
		console.log('AP table created in DOM');	
		}
	}
CInstrument.addInstrumentToSettings
	('./iautopilot.js'
	,'autopilot'
	,'div'
	,CInstrument_ap
	,'automated control of Heading, RateOfTurn, Radius or Track'
	,	['./smartvalues.js'
		,'./pid.js'	
		]
	);
