//	tab_plotlist
//	userinterface to add variables to logging or plotting
'use strict';
var plotlist = {
	html: function(){
		this.bSomeBoolean = false;
		window.addEventListener("load", this.onload.bind(this), false);	//	todo use bind() everywhere on load event
		return h.div('hier komen alle variabelen die op de een of andere manier geplot moeten worden, bv kompas, posplot, graphic, instrument...'
						,'id="divplotlist"');
		}
	,onfocus:function(){	
		this.hasFocus=true;
		}
	,onblur:function(){	
		this.hasFocus=false;	
		}
	,onload: function(evt){
		}
	,hasFocus: false
	};
