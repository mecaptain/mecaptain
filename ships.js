// ships.js
//	define simulatable ships
//	todo define abstract base class CShip and let a simulatable ship derive from it
//
'use strict';
var positions =	//	todo een default heading voor de launcher zou makkelijk zijn
	// {'slijkgat - inkomend':{latdeg:51+51.0/60.0,londeg:3+53.5/60.0,hdg:90}
	{'slijkgat - inkomend':{latrad:0.9050500669548432,lonrad:0.0681031668274009}
	,'haringvlietdam':{latrad:0.9046235259487938,lonrad:0.07064550778896382}
	,'Antwerpen':	{latdeg:51+18/60.0,londeg:4+17.17/60.0}
	,'haven ouddorp':	{latdeg:51+47.6/60.0,londeg:3+56.6/60.0}
	,'grevelingen - vlieger':{latdeg:51+45.7/60.0,londeg:3+59.6/60.0}
	// ,'scheveningen - aanloop':{latdeg:52+7.1561/60.0,londeg:4+14.4810/60.0}
	,'scheveningen - aanloop':{latdeg:52+6.64444/60.0,londeg:4+15.0783/60.0}
	,'nulnul':{latdeg:-0.1/60.0,londeg:-0.1/60.0}							//	test equator and meridian passage
	,'vuurtoren hellevoetsluis':{latdeg:51.8196905,londeg:4.1278264}
	,'maasgeul in':{latdeg:51+59.32/60,londeg:3+24/60}
	,'aanloop Rotterdam':{latdeg:52+0.26/60,londeg:3+59.4/60}
	,'aanloop Rotterdam LL116':{latdeg:51+57.75/60,londeg:4+8.09/60}
	,'Ouddorp Westhoofd':{latdeg:51+49.50/60,londeg:3+52.04/60}
	,'Ouddorp duinen':{latdeg:51+48.54/60,londeg:3+52.14/60}
	,'Sunter':{latdeg:-1*(6+(8+15.1/60)/60),londeg:106+(52+32.4/60)/60}	//	sunter maritime academy
	,'op NOAA 13003':{latdeg:(39+(42)/60),londeg:-1*(73+(29)/60)}	//	test van NOAA kaart
	,'New York USA harbor entry':{latdeg:(40+(30)/60),londeg:-1*(73+(56)/60)}	//	test van NOAA kaart	todo geef hier ook de kaart bij
	,'New York USA statue of lib':{latdeg:(40+(41.5)/60),londeg:-1*(74+2.1/60)}	//	test van NOAA kaart	todo geef hier ook de kaart bij
	,'Jakarta Tanjung Priok':{latdeg:-6.08,londeg:106.9}
	,'Singapore':{latdeg:1+13/60,londeg:103+52/60}
	};

// baseclass ship (see https://developer.mozilla.org/en/docs/Web/JavaScript/Reference/Global_Objects/Object/create)
class CShip {
	constructor(name){
		this.name=name;				//	timeconstant, smaller is a faster response on commanded rpm
		this.tcTelegraph=0.1;		//	timeconstant, smaller is a faster response on commanded rpm
		this.tcRudder=0.1;			//	timeconstant, smaller is a faster response on rudder order, might depend on how many steering pumps are running
		this.ror={p:0,s:0};this.rsa={p:0,s:0};
		this.cogT=0.0;
		this.cmdTelegraph=0;	this.actualTelegraph=0;
		this.maxRudder=35;	this.azi=false;
		this.eng=[];	//	dit mergen met rpm en pitch
		this.rpm=0, this.pitch= 25;
		this.rpm2thrust=341800;
		this.mass= 100000000;		//	kg
		this.stwL= 0.0;				//	longitudinal speed through water knots
		this.stwT= 0.0;				//	transversal speed through water knots
		this.doppler = {};	//	todo, maak speed een vector, deze is snelheid door het water. Doppler instrument moet ook stroom krijgen
		this.factor_speedresistance= 120000.0;	//
		this.stwL_tau= 20;			//	bigger=faster reponding
		this.rpm_tau=2;
		this.rudder_tau= 1/600;		//
		this.rot= 0;					//	degrees/minute
		this.drot= 0;					//	degrees/minute/second
		this.factor_ruddermoment= 80000.0;	
		this.factor_turnresistance= 10000.0;
		this.autopilot_values = 
			{Kp_hdg:1.7,	Ki_hdg:0.1,		Kd_hdg:12.6
			,Kp_rot:1.2, 	Ki_rot:0.7,	   Kd_rot:1.0	
			,Kp_trk:3.0,	Ki_trk:0.1,		Kd_trk:7.0	
			,Kp_rad:1.1, 	Ki_rad:1.001,  Kd_rad:12.0
			,useRot:false,mode:'change_hdg',rlimp:-35,rlims:+35
			};
		this.hdt=230;
		// this.vdm1_interval = 999;	//	send every .. milliseconds a vdm1 to server
		// this.vdm5_interval = 20 * this.vdm1_interval;	//	send every .. milliseconds a vdm5 to server (may 20 times more than vdm1)
		this.RefPoint = {A:62,B:52,C:7,D:7};	//	position of ais antenna
		this.NmeaOutputChannels = [];
		this.navlights = //	todo visible sectors and brightness, dit moet ieder schip hebben
				[{x:-50,y:20,z:-10,c:0x00ff00}	//	boordlicht SB
				,{x:-50,y:20,z:+10,c:0xff0000}	//	boordlicht BB
				,{x:-70,y: 8,z:  0,c:0xFFFFFF}	//	heklicht
				,{x:-55,y:35,z:  0,c:0xFFFFFF}	//	hoog toplicht
				,{x:+60,y:26,z:  0,c:0xFFFFFF}	//	laag toplicht
				];
		this.scaleMesh = 1.0;		
		}
	static calcSOG(stwL,stwT,hdt,current){
		var sog	= //	speed over ground	in NS and EW directions
			{ns:(				stwL * Math.cos(hdt				*zeroisim.pi_180)
			+	  mso.currentspd * Math.cos(mso.currentdir*zeroisim.pi_180))//	take current into consideration; mso.currentdir,mso.currentspd
			,ew:(				stwL * Math.sin(hdt				*zeroisim.pi_180)
			+	  mso.currentspd * Math.sin(mso.currentdir*zeroisim.pi_180))//	take current into consideration; mso.currentdir,mso.currentspd
			,pyt:0.0
			};
		sog.pyt = Math.sqrt(sog.ns*sog.ns+sog.ew*sog.ew);
		var alpha = hdt*zeroisim.pi_180 - Math.atan2(sog.ew,sog.ns);	//	angle between ships heading and direction of movement
		sog.L = +sog.pyt*Math.cos(alpha);	//	longitudinal
		sog.T = -sog.pyt*Math.sin(alpha);	//	transversal
		sog.pyt = Math.sqrt(sog.L*sog.L+sog.T*sog.T);
		sog.cogT = hdt*zeroisim.pi_180 + Math.atan(zeroisim.safediv(sog.T,sog.L));
		sog.cogT /= zeroisim.pi_180;
		return sog;
		}
	}
var simShips	= [];	//	the collection of shipx

class vos_patriot extends CShip {
	constructor(){
		super('vos patriot');
		this.mmsi=244750866;
		this.IMO=this.mmsi;
		this.CallSign='PHVG';
		this.shiptype=74;
		this.maxRudder=false;	this.azi=true;
		this.latlon= new zeroisim.LatLon(positions['New York USA statue of lib']);
		this.destination = 'Rotterdam';
		this.dae = {mesh:'./3d/ships/vos_patriot2.dae', distance:12, y_correction: -10, scaleMesh:1.0}
		this.scaleMesh = 1.0;
		}
	} simShips.push(new vos_patriot);

class poolzee extends CShip {
	constructor(){
		super('poolzee');
		this.mmsi=244750866;
		this.IMO=this.mmsi;
		this.CallSign='PHVE';
		this.shiptype=74;
		this.maxRudder=false;	this.azi=true;
		this.latlon= new zeroisim.LatLon(positions['New York USA statue of lib']);
		this.destination = 'Rotterdam';
		this.dae = {mesh:'./3d/ships/smit_poolzee.dae', distance:12, y_correction: -10, scaleMesh:1.0}
		this.stwL_tau= 3;			//	bigger=faster responding longitudinal speed to rpm
		this.rpm_tau=1;
		this.factor_ruddermoment=150000.0;	
		}
	} simShips.push(new poolzee);
class CocksRhib extends CShip {
	constructor(){
		super('CocksRhib');
		this.mmsi=244750467;
		this.IMO=this.mmsi;
		this.CallSign='PF4367';
		this.factor_ruddermoment= 120000.0;	
		this.shiptype=74;
		this.latlon= new zeroisim.LatLon(positions['haven ouddorp']);
		this.destination = 'Ossehoek';
		this.rpm2thrust=1341800;
		this.factor_speedresistance= 12000.0;	//
		this.dae = {mesh:'./3d/ships/rib/RibBoatvanCock.dae', distance:8, y_correction: 0, scaleMesh:0.025}
		// this.NmeaOutputChannels = [{"address":"udp://localhost:8889","sentences":["VDM","GLL"]}];	//	todo dit wordt aan de 'wereld' gekoppeld
		// this.NmeaOutputChannels = [{"address":"udp://192.168.1.19:8011","sentences":["VDM","GLL","HDT"]}];	//	todo dit wordt aan de 'wereld' gekoppeld
		}
	};	simShips.push(new CocksRhib);
class Tugboat extends CShip {
	constructor(){
		super('Tugboat');
		this.mmsi=244750863;
		this.IMO=this.mmsi;
		this.CallSign='PHVD';
		this.shiptype=74;
		this.maxRudder=false;	this.azi=true;
		this.latlon= new zeroisim.LatLon(positions['New York USA statue of lib']);
		this.destination = 'Hong Kong';
		this.dae = {mesh:'./3d/ships/tugboat/Tugboat.dae', distance:2, y_correction: 0, scaleMesh:'todo'}
		}
	}; simShips.push(new Tugboat);
class simple extends CShip {
	constructor(){
		super('simple');
		this.mmsi=244750864;
		this.IMO=this.mmsi;
		this.CallSign='PHVJ';
		this.shiptype=74;
		this.maxRudder=false;	this.azi=true;
		this.latlon= new zeroisim.LatLon(positions['New York USA statue of lib']);
		this.destination = 'Hong Kong';
		this.dae = {mesh:'./3d/ships/simple_10.dae', distance:2, y_correction: -10, scaleMesh:10.0}
		}
	}; simShips.push(new simple);
class liberty extends CShip {
	constructor(){
		super('liberty');
		this.mmsi=244750865;
		this.IMO=this.mmsi;
		this.CallSign='PHVF';
		this.shiptype=74;
		this.maxRudder=false;	this.azi=true;
		this.latlon= new zeroisim.LatLon(positions['New York USA statue of lib']);
		this.destination = 'Hong Kong';
		this.dae = {mesh:'./3d/ships/liberty/liberty.dae', distance:1, y_correction: -10, scaleMesh:22.0}
		}
	}; simShips.push(new liberty);
class Zingwind extends CShip {
	constructor(){
		super('Zingwind');
		this.mmsi=244750466;
		this.IMO=this.mmsi;
		this.CallSign='PF4364';
		this.shiptype=74;
		this.latlon= new zeroisim.LatLon(positions['grevelingen - vlieger']);
		this.destination = 'Ouddorp';
		this.hdt=311;
		// this.NmeaOutputChannels = [{"address":"udp://localhost:8888","sentences":[new nmea_send("VDM",1)]}];	//	todo dit wordt aan de 'wereld' gekoppeld
		}
	};	//	simShips.push(new Zingwind);
class Bliss extends CShip {
	constructor(){
		super('Bliss');
		this.mmsi=244750467;
		this.IMO=this.mmsi;
		this.CallSign='PF4365';
		this.factor_ruddermoment= 120000.0;	
		this.shiptype=74;
		this.latlon= new zeroisim.LatLon(positions['haven ouddorp']);
		this.destination = 'Ossehoek';
		this.rpm2thrust=1341800;
		this.factor_speedresistance= 12000.0;	//
		// this.NmeaOutputChannels = [{"address":"udp://localhost:8889","sentences":["VDM","GLL"]}];	//	todo dit wordt aan de 'wereld' gekoppeld
		// this.NmeaOutputChannels = [{"address":"udp://192.168.1.19:8011","sentences":["VDM","GLL","HDT"]}];	//	todo dit wordt aan de 'wereld' gekoppeld
		}
	};	//	simShips.push(new Bliss);

class Maassluis extends CShip {
	constructor(){
		super('Maassluis');
		this.mmsi=244750865;
		this.IMO=this.mmsi;
		this.CallSign='PHVA';
		this.shiptype=74;
		this.pennant='M856';
		this.latlon= new zeroisim.LatLon(positions['grevelingen - vlieger']);
		this.destination = 'Bahrain';
		this.hdt=300;
		// this.NmeaOutputChannels = [{"address":"udp://localhost:8887","sentences":["VDM"]}];	//	todo dit wordt aan de 'wereld' gekoppeld
		}
	};	//	simShips.push(new Maassluis);
