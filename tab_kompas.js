//	kompas_cv.js
// teken een kompas en zet de simulatiegegevens in localstorage
//	volgens tab_template maken
'use strict';
// var compass_tab_page = {	html: function(){
	

// todo hier new CInstrument_compass	
	
class CTabCompass extends C0isimTab {
	constructor(canvasName){
		super();
		// var kompas_dropdown=i.select2			//	plaatjes zie http://jsfiddle.net/8FydL/445/
		var kompas_dropdown=i.select			//	plaatjes zie http://jsfiddle.net/8FydL/445/
			( 'kompas_marks', 
			[i.option('zeilstreep')
			,i.option('rode en groene balk')
			,i.option('paars wybertje')
			,i.option('lee line')
			]
			);
		this.kompas_canvas 	= i.canvas(i.attributes({id:canvasName,width:"400",height:"400",style:"border:0px solid #000;"}));
		this.mfinstr_canvas	= i.canvas(i.attributes({id:"mfinstr_canvas",width:"320",height:"220",style:"border:0px solid #000;"}));
		this.engpanel_canvas	= i.canvas(i.attributes({id:"engpanel_canvas",width:"300",height:"140",style:"border:0px solid #000;",unselectable:"on"}));
		this.dopplerlog_canvas= i.canvas(i.attributes({id:"dopplerlog_canvas",width:"300",height:"220",style:"border:0px solid #000;"}));
		
		this.compass_kompas	= i.td(this.kompas_canvas,i.attributes({style:"border: 0px;",id:"compass_kompas"/*,onclick:h.ele("compass_kompas").appendChild(h.ele("kompas_canvas"))*/}));
		this.compass_mfinstr	= i.td(this.mfinstr_canvas,{style:"border: 0px;",id:"compass_mfinstr"/*,onclick:h.ele("compass_mfinstr").appendChild(h.ele("mfinstr_canvas"))*/});
		this.compass_enpanel	= i.td(this.engpanel_canvas,{style:"border: 0px;",id:"compass_enpanel"/*,onclick:h.ele("compass_engpanel").appendChild(h.ele("engpanel_canvas"))*/});
		this.compass_doppler	= i.td(this.dopplerlog_canvas,{style:"border: 0px;",id:"compass_doppler"/*,onclick:h.ele("compass_doppler").appendChild(h.ele("dopplerlog_canvas"))*/});
		this.thehtml = i.table(
			[i.tr(i.table(
				[i.td(i.table(
					[	i.tr([i.td(i.input('HDT.hdt',{type:"checkbox"})) 
							,	i.td(kompas_dropdown)
							,	i.td(i.button('observe'/*, {onclick:this.updateKompas.bind(this)}*/ ))
							])
					,	i.tr([i.td(i.input('APN.orderedheading',	{type:"checkbox"})),i.td(kompas_dropdown)])
					,	i.tr([i.td(i.input('RMB.bdest',				{type:"checkbox"})),i.td(kompas_dropdown)])
					,	i.tr([i.td(i.input('ROT.rot',					{type:"checkbox"})),i.td(kompas_dropdown)])
					,	i.tr([i.td(i.input('RMC.cog',					{type:"checkbox"})),i.td(kompas_dropdown)])
					,	i.tr([i.td(i.input('RMC.mvar',				{type:"checkbox"})),i.td(kompas_dropdown)])
					,	i.tr([i.td(i.input('THS.hdt',					{type:"checkbox"})),i.td(kompas_dropdown)])
					,	i.tr([i.td(i.input('VTG.cogT',				{type:"checkbox"})),i.td(kompas_dropdown)])
					,	i.tr([i.td(i.input('VTG.cogM',				{type:"checkbox"})),i.td(kompas_dropdown)])
					// todo lint, big card, small card, tenths, symbols after different directions, wind angles
					]))
				,this.compass_kompas
				,i.td(i.button('obsolete'/*,i.attributes({onclick:alert('obsolete I said!')})*/))
				]))
			,i.tr(i.td(i.table(i.tr(
					[this.compass_mfinstr
					,this.compass_enpanel
					,this.compass_doppler
					]))))
			]);
		}
	onload(evt){
		document.getElementById(this._tabdivname).appendChild(this.thehtml);
		}
	onfocus(){	
		this.hasFocus=true;
		// var div=document.getElementById('compass_mfinstr');
		// if (undefined==div) return
		this.compass_mfinstr.appendChild(this.mfinstr_canvas);
		this.compass_kompas.appendChild(this.kompas_canvas);
		this.compass_enpanel.appendChild(this.engpanel_canvas);
		this.compass_doppler.appendChild(this.dopplerlog_canvas);
		}
	onblur(){	
		this.hasFocus=false;	
		}
	}
var separate_window= function(){
	sepwdw.kompas = window.open
		(	"kompas.html", "_blank","height=250,width=250"
		+	",left=20,top=20,toolbar=no,location=no,scrollbars=no,status=no,resizable=yes,fullscreen=no"
		);  
	sepwdw.kompas.focus(); 
	}
