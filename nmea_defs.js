-//	nmea_defs.js
// (c) BGTuijl 2015
//	https://en.wikipedia.org/wiki/IEC_61162
//	https://www.nmea.org/Assets/may%2009%20rtcm%200183_v400.pdf
//	regular expression validators
'use strict';
var floatingp = '^[+-]*\\d+\\.\\d*$';
var number = '^[+-]*\\d+\\[.\\d|]*$';
var b64 = 'b64';	//	'^[A-Za-z0-9\+\/]+$';
var literal = function(lit){
	return '^'+lit+'$';
	}
var integer = function(digits){
	if (digits==undefined)
		digits='+';
	else if (digits==0)
		digits='*';
	else
		digits = '{'+digits+'}';
	return '^\\d'+digits+'$';
	}
var vartext = '^.*$';	//	'^.+$';
var fivechars = '^\\$\\w{5}$';
var timestamp = '^\\d{6}\\.\\d{2}$';	//	hhmmss.ms
var sqltime = '^\\d{4}\\-\\d{2}\\-\\d{2}\\ \\d{2}:\\d{2}:\\d{2}$';	//	yyyy-MM-dd hh:mm:ss
var todo = [['p1',vartext,'p1'],['p2',vartext,'p2'],['p3',vartext,'p3'],['p4',vartext,'p4'],['p5',vartext,'p5'],['p6',vartext,'p6'],['p7',vartext,'p7'],['p8',vartext,'p8'],['p9',vartext,'p9'],['p10',vartext,'p10'],['p11',vartext,'p11']];
var mc0=
	//	req				reqdir	fields							meaning
	{getnames:			 1	//	to sim	name,mmsi,callsign			get data
	,setship:			 2	//	to sim	ship								set simulated ship
	,setpos:				 3	//	to sim	latrad,lonrad					set position
	,sethdg:				 4	//	to sim	hdgt								set heading
	,startsim:			 5	//	to sim										start simulator
	,getOwnShip:		 6	//	to sim										get 3d particulars of own ship
	,get3d:				 7	//	to sim										get 3d particulars of ghost ship
	,getApK:				 8	//	to sim										autopilot constants 
	,setONS:				 9	//	to sim	sentences						set simulator output nmea sentences
	,giveObj:			10	//													propose an object to be created under the gizmo. Should be answered by teacherpage or localghosts.js
	,createObj:			11	//	to trainer of page						create a ship object
	,createdObj:		12	//													a ship object mesh is created, here is the index in the scene.children
	,getScene:			13	//	
	// ,listRoutes:		13	//													
	//:				//	to sim										todo thrusters, rudders, AP constants 
	,response:100	//	this is added to the request to denote a response
	};

//	Mijn bron was VSTEP BV NMEA 0183 Release Version 4 10.pdf
//	er zijn nog een aantal sentences todo, saai typewerk :-(
// zie ook http://aprs.gids.nl/nmea/
//	en http://www.catb.org/gpsd/gpsd_json.html
var nmeadefs = {	//	alle backslashes moeten dubbel
	AEC:[{title:'Autopilot External Controller'}		//	nautis specific
		,['SetPoint',floatingp,'value of set point autopilot parameter']
		,['AutoPilotMode',literal('[ROT|Compass]'),'value which represents an autopilot mode ( e.g: ROT , Compass )']
		,['Rudder',floatingp,'value of AutoPilot rudder parameter']
		,['Trim',floatingp,'value of autopilot trim parameter']
		,['Damping',floatingp,'value of damping range autopilot parameter']
		,['Sensitivity',floatingp,'value of autopilot parameter sensitivity']		
		],
	ALA:[{title:'Set Detail Alarm Condition'}
		,['time',timestamp,'Time of condition change or acknowledgement']
		,['asrc',literal('..'),'system indicator of original alarm source (see table 16 in nmea spec)']
		,['asub',literal('..'),'Sub system equipment indicator of original alarm source']
		,['inst',literal('..'),'Instance number of equipment/unit/item']
		,['type',integer(),'Type of alarm']
		,['cond',literal('[N|H|J|L|K|X]'),'Alarm condition']
		,['ack',literal('[A|V|B|H|O]'),'Alarm’s acknowledged state']
		,['desc','^.*$','Alarm’s description text']
		],
	ANC:[{title:'Anchoring'}	//	$GPANC,x 1 ,xx.xx 2 ,xxx 3 ,x 4 ,hhmmss.ms 5 *hh<CR><LF> nautis specific
		,['side',	literal('[0|1]'),	'0 = Port / 1 = Starboard']
		,['chlen',	floatingp,			'Chain Length (in shackles)']
		,['chpos',	integer(3),			'Chain Position (in degrees)']
		,['ground',	literal('[0|1|2|3|4|5|6|7]'),		'Bottom holding ground; 0 = sand, 1 = gravel, 2 = stones, 3 = rocks, 4 = soft mud, 5 = mud, 6 = clay, 7 = stiff clay']
		,['time',		'^\\d{4}\\.\\d{2}$','timestamp']
		],	//	ANC
	APB:[{title:'Heading/Track Controller (Autopilot) Sentence "B"'}
		,['status1',	literal('A|V'),'status: A=valid, V=SNR warning or loran-C blink or general warning flag meaning no reliable fix is available']
		,['status2',	literal('A|V'),'status: A=valid or not used, V = Loran-C Cycle Lock warning flag']
		,['xte',			floatingp,'Magnitude of XTE (cross-track-error)']
		,['dts',			literal('L|R'),'Direction to steer, L/R']
		,['xteunits',	literal('N'),'XTE units, Nautical miles']
		,['status_arr',literal('A'),'Status: A = arrival circle entered']
		,['status_wpp',literal('A'),'Status: A = perpendicular passed at waypoint']
		,['brng_dst',	floatingp,'Bearing origin to destination']
		,['brng_dstmt',literal('M|T'),'Bearing origin to destination, Magnetic/True']
		,['wpt',			vartext,'Destination waypoint ID']
		,['brng_pos',	floatingp,'Bearing, Present position to destination']
		,['brng_posmt',literal('M|T'),'Bearing, Present position to destination, Magnetic or True']
		,['hts',			floatingp,'Heading-to-steer to destination waypoint']
		,['htsmt',		literal('M|T'),'Heading-to-steer to destination waypoint, Mag or True']
		,['mode',		literal('V|A|D'),'Mode indicator; V=Invalid for all values of Indicator mode except for: Autonomous or Differential']
		],	//	APB	
	APN:[{title:'Auto Pilot'}	//	APN is a custom defined NMEA sentence that will allow the autopilot to send its settings to NAUTIS. When
										//	autopilot mode is selected in NAUTIS, it will apply these settings to control the ship.
	//	,['control',		literal('OFF|HDG|CRS|TRK'),	'OFF, HDG, CRS, TRK']
		,['control',		literal('OFF|HDG|ROT|RAD|TRK'),	'OFF, HDG, ROT, RAD, TRK']	//	met berts uitbreidingen
		,['pilotwatch',	floatingp,				'value of pilot watch parameter']
		,['rudderlimit',	floatingp,				'value of rudder limit parameter']
		,['weather',		integer(),				'value of weather parameter']
		,['rudder',			floatingp,				'value of rudder parameter']
		,['rate',			floatingp,				'value of rate parameter']
		,['hdgsource',		literal('G1|G2|M'),	'heading source; gyro 1 or 2 or magnetic compass']
		,['orderedhdg',	floatingp,				'value of ordered heading parameter']
		,['mode',			literal('ROT|RAD'),	'rate of turn mode or radius mode'	]
		,['rot_rad',		floatingp, 				'value of ROT/RAD parameter']
		],
	BAL:[{title:'Ballast', ti:'CM'}	//	nautis specific
		,['index',integer(),'number of ballast box (zero-based)',]
		,['type',integer(),'0=solid 1=fluid',]
		,['mode',literal('A|P'),'A=absolute P=percentage',]
		,['value',floatingp,'mass in metric tons or percentage fill',]
		,['CoG',floatingp,'comes back from Nautis, current Centre of Gravity',]
		],	//	BAL
	BOW:[{title:'Bow thruster thrust', ti:'DP'}	//	nautis specific
		,['angle',floatingp,'Bow Thruster Angle (Degrees)',]
		,['thrust',floatingp,'Bow Thruster Thrust (Percent)',]
		,['depth',floatingp,'Bow Thruster Depth',]
		],	//	BOW
	CAM:[{title:'camera rotation,position,frustrum', ti:'VC'}	//	zeroisim specific
		,['camid',integer(),'camera id',]
		,['rotX',floatingp,'rotation around x-axis (degrees)',]	//	rotation direction implementation defined
		,['rotY',floatingp,'rotation around y-axis (degrees)',]	//	rotation direction implementation defined
		,['rotZ',floatingp,'rotation around z-axis (degrees)',]	//	rotation direction implementation defined
		,['posX',floatingp,'shift in x-direction (meters)',]		
		,['posY',floatingp,'shift in y-direction (meters)',]		
		,['posZ',floatingp,'shift in z-direction (meters)',]		
		,['fov',floatingp,'field of view in degrees',]
		,['aspect',floatingp,'aspect ratio',]
		,['near',floatingp,'near clipping plane (meters)',]
		,['far',floatingp,'far clipping plane (meters)',]
		],	//	CAM
	DBT:[{title:'Depth Below Transducer', ti:'SD'}
		,['depthf',	floatingp,		'Water depth, feet',		0.0]
		,['feet',	literal('f'),	'feet',						'f']
		,['depthm',	floatingp,		'Water depth, meters',	0.0]
		,['meters',	literal('M'),	'meters',					'M']
		,['depthF',	floatingp,		'Water depth, fathoms',	0.0]
		,['feet',	literal('F'),	'fathoms',					'F']
		],	//	dbt
	DDC:[{title:'Display Dimming', ti:'xx'}	//	todo see https://webstore.iec.ch/p-preview/info_iec61162-1%7Bed3.0%7Den.pdf
		],	//	dbt
	DPT:[{title:'Depth',	ti:'SD'}
		,['depth',floatingp,'Water depth relative to the transducer, meters',0.0]
		,['troff',floatingp,'Offset from transducer, meters ("positive" = distance from transducer to water-line, "-" = distance from transducer to keel)',+4.0]
		,['scale',floatingp,'Maximum range scale in use',100.0]
//		,['',,'',]
		],	//	dpt
	DTM:[{title:'Datum reference',	ti:'GP'}
		,['datum',vartext,'local datum code','W84']
		,['subdiv',vartext,'local datum subdivision code','?']
		,['latoff',floatingp,'latitude offset in minutes',0.0]
		,['latoffns',literal('N|S'),'latitude offset, North/South','N']
		,['lonoff',floatingp,'longitude offset',0.0]
		,['lonoffew',literal('E|W'),'longitude offset, East/West','E']
		,['altoff',floatingp,'altitude offset in meters',0.0]
		,['refcode',vartext,'Reference datum code','W84']
		],	//	dtm
	ETL:[{title:'Engine Telegraph Operation Status',	ti:'SS'}	
		,['time',timestamp,'Event time UTC',]
		,['msgtype',literal('O|A'),'messagetype O=order status A=answerback',]
		,['etposition',integer(),'Position indicator of engine telegraph',]
		,['stposition',integer(),'Position indication of sub telegraph; 20=standby engine 30=full away - navigation full 40=finish with engine',]
		,['oplocind',vartext,'Operating location indicator',]
		,['number',integer(1),'Number of engine or propeller shaft',]
		],	/*	ETL
		Engine telegraph positions:
		00 = STOP ENGINE
		01 = [AH] DEAD SLOW
		02 = [AH] SLOW
		03 = [AH] HALF
		04 = [AH] FULL
		05 = [AH] NAV. FULL
		11 = [AS] DEAD SLOW
		12 = [AS] SLOW
		13 = [AS] HALF
		14 = [AS] FULL
		15 = [AS] CRASH ASTERN
		Nautis will take the shaft index (6) and use that to look up the mapped control set. To that shaft it will
		apply the main telegraph order (3) as telegraph order. During the simulation the engine the NMEA
		telegraph order will prevail over the hardware telegraph orders.
		*/
	FIR:[{title:'Fire Detection'}
		,['status',literal('S|E|F|D'),'status for S=Section, E=fire detector, F=fault in system, D=disabled']
		,['time','^[\\d{4}\\.\\d{2}|]$','Time of condition change or acknowledgemen']
		,['ind',literal('FD|FH|FS|FM|GD|GO|GS|GH|SF|SV|CO|OT'),'fire detection indicator (see table 17 in nmea spec)']
		,['1div','.*','first division indicator of detector']
		,['2div','.*','second division indicator of detector']
		,['det','^[.*]$','detector ID, number of activated fire detectors']
		,['act','^[A|V|X|]$','activation']
		,['ack','^[A|V|]$','acknowledged']
		,['desc','^[.*]$','description']
		],
	GGA:[{title:'Global Positioning System Fix Data',ti:'GP'}
		,['tim',		vartext, 				'time todo tiende seconden']
		,['lat',		floatingp,				'latitude']
		,['NS',		literal('[N|S]'),		'north or south']
		,['lon',		floatingp,				'longitude']
		,['EW',		literal('[E|W]'),		'east or west']
		,['qual',	literal('[1|0]'),		'fix quality: 1=gps fix']
		,['nsat', 	integer(),				'number of satellites']
		,['hdop',	floatingp,				'hdop']
		,['alt',		floatingp,				'altitude']
		,['altu',	vartext, 				'unit of altitude']
		,['hgeo',	floatingp,				'height of geoid above WGS84 ellipsoid']
		,['hgeou',	vartext, 				'unit of height']
		,['tupd',	vartext, 				'Time since last DGPS update']
		,['rstat',	vartext, 				'DGPS reference station id']
		],	//	gga
	GLL:[{title:'Geographic Position – Latitude/Longitude',ti:'GP'}
		,['lat',		floatingp,	'latitude']
		,['NS',		literal('[N|S]'),		'north or south']
		,['lon',		floatingp,				'longitude']
		,['EW',		literal('[E|W]'),		'east or west']
		,['utc',		timestamp,				'UTC of position']
		,['status',	literal('[A|V|D]'),	'status: Valid, Autonomous, Differential']
		,['mode',	literal('A|D|E|M|S|N'),'Autonomous, Differential, Estimated (DR), Manual input, Simulator, Not valid']
		],	//	gll
	GSA:[{title:'Satellite status'}
		,['selfix',literal('A|M'),'selection of 2D or 3D fix (A=Auto M = manual)']
		,['dfix',integer(1),'xD fix 1=no fix, 2=2D fix, 3=3D fix']
		,['prn01',integer(0),'PRN of satellite used for fix']
		,['prn02',integer(0),'PRN of satellite used for fix']
		,['prn03',integer(0),'PRN of satellite used for fix']
		,['prn04',integer(0),'PRN of satellite used for fix']
		,['prn05',integer(0),'PRN of satellite used for fix']
		,['prn06',integer(0),'PRN of satellite used for fix']
		,['prn07',integer(0),'PRN of satellite used for fix']
		,['prn08',integer(0),'PRN of satellite used for fix']
		,['prn09',integer(0),'PRN of satellite used for fix']
		,['prn10',integer(0),'PRN of satellite used for fix']
		,['prn11',integer(0),'PRN of satellite used for fix']
		,['prn12',integer(0),'PRN of satellite used for fix']
		,['pdop',floatingp,'dilution of precision']
		,['hdop',floatingp,'horizontal dilution of precision']
		,['vdop',floatingp,'vertical dilution of precision']
		],	//	gsa
	GSV:[{title:'Satellite information'}
		,['nmsg',	integer(),'Total number of messages of this type in this cycle']
		,['msgn',	integer(),'Message number']
		,['viss',	integer(),'Total number of SVs visible']

		,['prn1',	integer(),'SV PRN number']
		,['elev1',	integer(),'Elevation, in degrees, 90&deg; maximum']
		,['azim1',	integer(),'Azimuth, degrees from True North, 000&deg; through 359&deg;']
		,['snr1',	integer(),'SNR, 00 through 99 dB (null when not tracking)']

		,['prn2',	integer(),'SV PRN number']
		,['elev2',	integer(),'Elevation, in degrees, 90&deg; maximum']
		,['azim2',	integer(),'Azimuth, degrees from True North, 000&deg; through 359&deg;']
		,['snr2',	integer(0),'SNR, 00 through 99 dB (null when not tracking)']

		,['prn3',	integer(),'SV PRN number']
		,['elev3',	integer(),'Elevation, in degrees, 90&deg; maximum']
		,['azim3',	integer(),'Azimuth, degrees from True North, 000&deg; through 359&deg;']
		,['snr3',	integer(0),'SNR, 00 through 99 dB (null when not tracking)']

		,['prn4',	integer(),'SV PRN number']
		,['elev4',	integer(),'Elevation, in degrees, 90&deg; maximum']
		,['azim4',	integer(),'Azimuth, degrees from True North, 000&deg; through 359&deg;']
		,['snr4',	integer(0),'SNR, 00 through 99 dB (null when not tracking)']
		],
	HDG:[{title:'Heading, Deviation & Variation'}
		,['hdg',		floatingp,			'Magnetic sensor heading, degrees']
		,['dev',		floatingp,			'Magnetic deviation']
		,['devew',	literal('E|W'),	'sense of Magnetic deviation, East/West']
		,['var',		floatingp,			'Magnetic variation']
		,['varew',	literal('E|W'),	'sense of Magnetic variation, East/West']
		],	//	hdg
	HDT:[{title:'Heading, True',ti:'HE'}	//	This is a deprecated sentence and should not be used for new designs. This sentence has been replaced by THS.
		,['hdt',		floatingp,			'Heading']
		,['trdeg',	literal('[T]'),	'degrees True']
		],
	HTC:[{title:'Heading/Track control command'}	//	todo
		,['ovr',	literal('[A|V]'),		'Override A=in use, V=not in use']
		,['ruda',		floatingp,		'Commanded rudder angle, degrees']
		,['rudd',		literal('L|R'),		'Commanded rudder direction, L/R=port/starboard']
		,['',		literal(''),		'Selected steering mode']
		,['',		literal(''),		'Turn mode R=radius T=turnrate N=not controlled']
		,['',		literal(''),		'Commanded rudder limit, degrees (unsigned)']
		,['',		literal(''),		'Commanded off-heading limit, degrees (unsigned)']
		,['',		literal(''),		'Commanded radius of turnrate+']
		,['',		literal(''),		'']
		,['',		literal(''),		'']
		,['',		literal(''),		'']
		],	//	HTC
	MSO:[{title:'Sky and Ocean (weather)'}
		,['rain',			floatingp,	'Rain (from 0.0 to 1.0)']
		,['thunder',		floatingp,	'Thunder (from 0.0 to 1.0)']
		,['windspd',		floatingp,	'Wind speed (from 0.0 to 67.5 kts)']
		,['winddir',		floatingp,	'Wind direction (from 0 to 360 degrees)']
		,['altostratus',	floatingp,	'Clouds altostratus (from 0.0 to 1.0)']
		,['stratus',		floatingp,	'Clouds stratus (from 0.0 to 1.0)']
		,['cirrocumulus',	floatingp,	'Clouds cirromuls (from 0.0 to 1.0)']
		,['cirrus',			floatingp,	'Clouds cirrus (from 0.0 to 1.0)']
		,['waveheight',	floatingp,	'Wave height (from 0 to 20m)']
		,['wavedir',		integer(3),	'Wave direction (from 0 to 360 degrees)']
		,['fog',				floatingp,	'Fog (from 0.1 to 1.0)']
		,['tide',			floatingp,	'Tide (from -12.4h to +12.4h)']
		,['seastate',		integer(1),	'Sea state']
		,['currentdir',	floatingp,	'Current direction (degrees)']
		,['currentspd',	floatingp,	'Current Speed (kts)']
		,['time',			timestamp,	'hhmmss.ms = timestamp (simulation time)']
		],	//	mso
	MWD:[{title:'Wind Direction & Speed',	ti:'WI'}	//	The direction from which the wind blows across the earth’s surface, with respect to north, and the speed of the wind.
		,['dirT',		floatingp,		'Wind direction, 0 to 359 degrees True',	0.0]
		,['true',		literal('T'),	'true',								'T']
		,['dirM',		floatingp,		'Wind direction, 0 to 359 degrees Magnetic',	0.0]
		,['magn',		literal('M'),	'magnetic',							'M']
		,['spdk',		floatingp,		'Wind speed, knots',				0.0]
		,['knots',		literal('N'),	'knots',								'K']
		,['spdm',		floatingp,		'Wind speed, meters/second',	0.0]
		,['msec',		literal('M'),	'meters/second',					'M']
		],	//	mwd
	MWV:[{title:'Wind Speed & Angle',	ti:'WI'}	//	apparent (relative) or calculated wind
		,['wang',		floatingp,		'Wind angle, 0 to 359 degrees',			0.0]
		,['ref',			literal('R|T'),	'reference, Relative Theoretical',	'R']
		,['spd',			floatingp,		'Wind speed',									0.0]
		,['knots',		literal('K|M|N|S'),	'Kilomer per hour, Meters per second, kNots, Statute miles/hour',	'N']
		,['status',		literal('A|V'),	'Status; vAlid, inValid',				'M']
		],	//	mwv
	NAU:[{title:'Nautis specific events	',ti:'--'}	//	Nautis specific events	
		,['exe',			literal('EXE'),'exercise']
		,['stend',		literal('S|E'),'Start or End of exercise']
		,['x',			integer(1),		'0 for custom exercises or instructor session']
		,['time',		timestamp,		'no idea']
		],	//	nau
	PRC:[{title:'Propulsion Remote Control Status',ti:'GP'}
		,['ldpos',	number,				'Lever demand position (-100% to 100%)',100.0]
		,['ldstat',	literal('A|V'),	'Lever demand status, A=Data valid, V=Data invalid','A']
		,['rdval',	number,				'RPM demand value (-100% to 100%)',100.0]
		,['rmind',	literal('P|R|V'),	'RPM mode indicator; Percentage Rpm inValid','P']
		,['pdval',	number,				'Pitch demand value (-100% to 100%)',100.0]
		,['pmind',	literal('P|D|V'),	'Pitch mode indicator; Percentage Degrees inValid','P']
		,['oploc',	literal('B|P|S|C|E|W|'),		'Operating location indicator; Bridge Portwing Starboardwing engineControlroom Engineside Wing','W']
		,['engshft',integer(1),			'Number of engine or propeller shaft, zero based (numeric) 0=single or center, odd=starboard, even=port',0]
		],	//	PRC
	RMB:[{title:'Recommended Minimum Navigation Information'}	//	To be sent by a navigation receiver when a destination waypoint is active.
		,['stat',	literal('[A|V]'),	'Status, A= Active, V = Void']
		,['cte',		floatingp,			'Cross Track error - nautical miles']
		,['dst',		literal('[L|R]'),	'Direction to Steer, Left or Right']
		,['twpt',	vartext,				'TO Waypoint ID']
		,['fwpt',	vartext,				'FROM Waypoint ID']
		,['dLat',	floatingp,			'Destination Waypoint Latitude']
		,['dLatNS',	literal('[N|S]'),	'N or S']
		,['dLon',	floatingp,			'Destination Waypoint Longitude']
		,['dLonEW',	literal('[E|W]'),	'E or W']
		,['rDest',	floatingp,			'10 Range to destination in nautical miles']
		,['bDest',	floatingp,			'11 Bearing to destination in degrees True']
		,['sDest',	floatingp,			'12 Destination closing velocity in knots']
		,['astat',	literal('A|.'),	'13 Arrival Status, A = Arrival Circle Entered']
		,['mode',	literal('.'),		'14 FAA mode indicator (NMEA 2.3 and later)']
		],	//	rmb
	RMC:[{title:'Recommended Minimum sentence C',ti:'GP'}
		,['tfix',	'^\\d{4}\\.\\d{2}$','time of fix']
		,['status',	literal('A|V'),'Status A=active or V=Void']
		,['lat',		floatingp,'latitude']
		,['slat',	literal('N|S'),'sense of latitude N=North S=South']
		,['lon',		floatingp,'longitude']
		,['slon',	literal('E|W'),'sense of longitude E=East W=West']
		,['sog',		floatingp,'Speed over the ground in knots']
		,['cog',		floatingp,'Track angle in degrees True']
		,['date',	integer(6),'date DDMMYY']
		,['mvar',	'^[\d+\.\d*]|$','Magnetic Variation']
		,['svar',	literal('E|W|'),'sense of magnetic variation E=East W=West']
		,['mode',	literal('A|D|E|F|M|N|P|R|S'),'mode not null! See nmea0183 spec']
		],	//	rmc
	RME:[{title:'Estimated Position Error'}
		,['hpe',		floatingp,	'Estimated horizontal position error in metres (HPE)'	]
		,['uhpe',	floatingp,	'unit of HPE M=metres'	]
		,['vpe',		floatingp,	'Estimated vertical error (VPE)'	]
		,['uvpe',	floatingp,	'unit of VPE M=metres'	]
		,['sepe',	floatingp,	'Overall spherical equivalent position error'	]
		,['usepe',	floatingp,	'unit of SEPE M=metres'	]
		],	//	rme
	RMM:[{title:'Map datum'}
		,['datum',	vartext, 'Currently active horizontal datum']
		],	//	rmm
	RMV:[{title:'garmin specific (velocity)'}
		,['todo1',		floatingp,	'unknown'	]
		,['todo2',		floatingp,	'unknown'	]
		,['todo3',		floatingp,	'unknown'	]
		],	//	rmv
	ROR:[{title:'Rudder Order Status',ti:'GP'}
		,['srud',	floatingp,			'Starboard (or single) rudder order in degrees, "-" = port',0.0]
		,['svalid',	literal('A|V'),	'Starboard rudder status, A=Data valid, V=Data invalid','A']
		,['prud',	floatingp,			'Port rudder order in degrees , "-" = port',0.0]
		,['pvalid',	literal('A|V'),	'Port rudder status, A=Data valid, V=Data invalid','A']
		,['src',		literal('B|P|S|C|E|W'),		'Command source location','W']
		],	//	rot
	ROT:[{title:'Rate Of Turn',ti:'HE'}	//	Rate of turn
		,['rot',		floatingp,	'	']
		,['A',		literal('A|V'),	'status A=valid V=invalid']
		],	//	rot
	RPM:[{title:'Engine Telegraph Operation Status',	ti:'SS'}
		,['source',literal('S|E'),'source Shaft or Engine',]
		,['number',integer(1),'Engine or shaft number, numbered from centerline 0=single or on centerline, odd=starboard, even=port',]
		,['speed',floatingp,'Speed, rev/min, "-" = counter-clockwise',]
		,['pitch',floatingp,'Propeller pitch, % of max., "-" = astern',]
		,['status',literal('A|P'),'status',]
		],	//	RPM
	RSA:[{title:'Rudder sensor angle'}	//	Rate of turn
		,['srud',	floatingp,			'Starboard (or single) rudder sensor, "-" means Turn To Port']
		,['sstat',	literal('A|V'),	'Status, A means data is valid']
		,['prud',	floatingp,			'Port rudder sensor']
		,['pstat',	literal('A|V'),	'Status, A means data is valid	']
		],
	RTE:[{title:'Routes',	ti:'SS'}	//	nautis specific
		,['nsents',integer(),'number of sentences`',]
		,['nsent',integer(),'sentence number',]
		,['mode',literal('c|w'),'sentence mode c = complete route, w = working route',]
		,['idroute',vartext,'route id',]
		,['idwaypt',vartext,'waypoint id',]
		,['idwayptadd',vartext,'additional waypoint ids',]
		],	//	RTE
	SES:[{title:'Playback & session management', }	//	nautis specific
		,['Mode',literal('0|1'),'0=Recording Mode, 1=Playback Mode',]
		,['Speed',floatingp,'Exercise speed; 0 = Pauzed, 1 = Normal speed, 2 = Twice the speed, 0,5 = Half the speed etc.',]
		,['Length',floatingp,'Session length in minutes and seconds',]
		,['Session Name',vartext,'Session Name',]
		,['Scenario',vartext,'Scenario Name',]
		,['SystemID',literal('0|1|2|3'),'An ID to identify from what system the message originates from: 0=Debriefing App, 1=Instructor Station, 2=Assessment tool, 3=GMDSS',]
		,['Commando',literal('C|A|V'),'C=Command, A=Acknowledge/Valid, V=Reject/Invalid',]
		,['Type',literal('P|L|D|S'),'P=Play, L=Load, D=Discard, S=Save',]	
		],	//	SES
	SHR:[{title:'Ship motion',ti:'PA'}	//	nautis specific Ship motion in 6 degrees of freedom (movement plus rotation)
		,['utc','^\\d{6}\\.\\d{3}$','UTC time']
		,['hdg',floatingp,'Heading in degrees']
		,['true',literal('T'),'flag to indicate that the Heading is True Heading (i.e. to True North)']
		,['roll',floatingp,'Roll Angle in degrees']
		,['pitch',floatingp,'Pitch Angle in degrees']
		,['heave',floatingp,'Heave (vertical movement in meters)']
		,['surge',floatingp,'Surge, forward movement measured from the current position to the point of origin']
		,['sway',floatingp,'Sway, starboard movement measured from the current position to the point of origin']
		,['aroll',floatingp,'Roll Angle Accuracy Estimate (Stdev) in degrees']
		,['apitch',floatingp,'Pitch Angle Accuracy Estimate (Stdev) in degrees']
		,['aheading',floatingp,'Heading Angle Accuracy Estimate (Stdev) in degrees']
		,['astatus',literal('.'),'Aiding Status']
		,['istatus',literal('.'),'IMU Status']
		],	//	SHR
	SIG:[{title:'Signals',	ti:'--'}	//	nautis specific
		,['lightson',literal('0|1'),'Lights 0=off, 1=on',]
		,['lighttype',integer(),'LightType',]
		,['deckliton',literal('0|1'),'DeckLights 0=off, 1=on',]
		,['asound',literal('0|1'),'Sound automatic 0=off, 1=on',]
		,['tasound',integer(1),'Sound automatic type',]
		,['rasound',integer(),'Sound automatic repeat sec (60, 90, 120)',]
		,['tmsound',vartext,'Sound manual type',]
		,['rmsound',integer(),'Sound manual repeat type (0 = single, 1 = continuous off, 2 = continuous on)',]
		,['flaglineid',integer(),'FlaglineID',]
		,['flaglineoff',integer(),'OffsetFromTop',]
		,['flagid',integer(),'FlagID',]
		,['timestamp',timestamp,'Timestamp',]
		],	//	SIG
	STN:[{title:'Stern thruster thrust',	ti:'DP'}	//	nautis specific, conflicts with NMEA standard
		,['angle',	floatingp,'Bow Thruster Angle (Degrees)',]
		,['thrust',	floatingp,'Bow Thruster Thrust (Percent)',]
		,['depth',	floatingp,'Bow Thruster Depth',]
		],	//	STN
	THS:[{title:'True Heading and Status',ti:'HE'}	//	replaces HDT
		,['hdgT',	floatingp,			'Heading, true in degrees']
		,['trdeg',	literal('[A|E|M|S|V]'),	'Autonomous, Estimated (DR), Manual, Simulator, V=data not valid (including standby)']
		],
	TRC:[{title:'Thruster control data',ti:'XX'}
		,['not',		integer(),		'Number of thruster (thruster index)']
		,['rpm',		floatingp,		'RPM demand value (percentage)']
		,['rpmi',	literal('P'),	'RPM mode indicator (indicates percentage)']
		,['pitch',	floatingp,		'Pitch demand value (percentage)']
		,['ptm',		literal('P'),	'Pitch mode indicator (indicates percentage)']
		,['azi',		floatingp,		'Azimuth demand (0-360, follow-up)']
		,['oloc',	literal('.'),	'Operating location indicator']
		,['stat',	literal('A|V'),'Sentence status flag']
		],
	TRD:[{title:'Thruster response data'}
		,['not',		integer(),		'Number of thruster (thruster index) Odd=bow thruster, Even=stern thruster']
		,['rpm',		floatingp,		'RPM response value (-=port)']
		,['rpmi',	literal('P|R|V'),	'RPM mode indicator; P=percent, R=RPM, V=data invalid']
		,['pitch',	floatingp,		'Pitch response value (-=port)']
		,['ptm',		literal('P'),	'Pitch mode indicator (P=percent, D=degrees, V=data invalid)']
		,['azi',		floatingp,		'Azimuth response, direction of thrust in degrees, 0-360']
		],
	VBW:[{title:'Dual Ground/Water Speed',ti:'VD'}	//	Water referenced and ground referenced speed data.
		,['longwat',		floatingp,			'Longitudinal water speed, knots "-" = astern']
		,['transwat',		floatingp,			'Transverse water speed, knots "-" = port']
		,['validwat',		literal('A|V'),	'Status: Water speed, A = Data valid']
		,['longgrd',		floatingp,			'Longitudinal ground speed, knots "-" = astern']
		,['transgrd',		floatingp,			'Transverse ground speed, knots "-" = port']
		,['statgrd',		literal('A|V'),	'Status, Ground speed, A = Data valid']
		,['sterntransw',	floatingp,			'Stern transverse water speed, knots "-" = port']
		,['statsterntransw',literal('A|V'),	'Status, stern water speed, A = Data valid']
		,['sterntransg',	floatingp,			'Stern transverse ground speed, knots "-" = port']
		,['statsterntransg',literal('A|V'),	'Status, stern ground speed A = Data valid V = Invalid']
		],
	VDM:[{title:'AIS VHF Data-link Message',ti:'AI',dollar:'!'}
		,['totns',	integer(1),	'Total number of sentences needed to transfer the message, 1 to 9']
		,['sentnum',integer(1),	'Sentence number, 1 to 9']
		,['messid',	integer(1),	'Sequential message identifier, 0 to 9']
		,['aischan',integer(),	'AIS Channel']
		,['msg',		b64,			'Encapsulated ITU-R M.1371 radio message']
		,['nfillb',	integer(1),	'Number of fill-bits, 0 to 5']
		],	//	vdm
	VDO:[{title:'AIS VHF Data-Link Own-Vessel Report',ti:'AI',dollar:'!'}
		,['totns',	integer(1),	'Total number of sentences needed to transfer the message, 1 to 9']
		,['sentnum',integer(1),	'Sentence number, 1 to 9']
		,['messid',	integer(1),	'Sequential message identifier, 0 to 9']
		,['aischan',integer(),	'AIS Channel']
		,['msg',		b64,			'Encapsulated ITU-R M.1371 radio message']
		,['nfillb',	integer(1),	'Number of fill-bits, 0 to 5']
		],	//	vdo
	VDR:[{title:'Set & Drift'}	//	The direction towards which a current flows (Set) and speed (Drift) of a current.
		//	$--VDR,x.x,T,x.x,M,x.x,N*hh<CR><LF>
		,['curdirT',	floatingp,		'Direction, degrees True']
		,['true',		literal('T'),	'true']
		,['curdirM',	floatingp,		'Direction, degrees Magnetic']
		,['magn',		literal('M'),	'magnetic']
		,['curspd',		floatingp,		'Current speed, knots']
		,['knots',		literal('N'),	'knots']
		],
	VDS:[{title:'time,lat,lon eigen formaat'}	//	eigen formaat om VDM achtige dingen te doen
		,['timestamp',	vartext,			'timestamp, sqltime']	//	sqltime
		,['lat',			floatingp,		'latitude']
		,['lon',			floatingp,		'longitude']
		],
	VHW:[{title:'Water Speed and Heading'}	//	The compass heading to which the vessel points and the speed of the vessel relative to the water.
		//	$--VHW,x.x,T,x.x,M,x.x,N,x.x,K*hh<CR><LF>
		,['headingt',	floatingp,		'Heading, degrees True']
		,['true',		literal('T'),	'True']
		,['headingm',	floatingp,		'Heading, degrees Magnetic']
		,['magnetic',	literal('M'),	'Magnetic']
		,['speedkn',	floatingp,		'Speed knots']
		,['knots',		literal('N'),	'kNots']
		,['speedkm',	floatingp,		'Speed kilometer per hour']
		,['kmhr',		literal('K'),	'km/hr']
		],
	VLW:[{title:'Dual Ground/Water Distance'}	//	The distance traveled, relative to the water and over the ground.
		//$--VLW,x.x,N,x.x,N,x.x,N,x.x,N*hh<CR><LF> todo onderstaande lijkt me niet af
		,['wdisttot',	floatingp,	'Total cumulative water distance, nautical miles']
		,['wdisttrip',	floatingp,	'Water distance since reset, nautical miles']
		,['gdisttot',	floatingp,	'Total cumulative ground distance, nautical miles']
		,['gdisttrip',	floatingp,	'Ground distance since reset, nautical miles']
		],
	VTG:[{title:'Vector track and Speed over the Ground',ti:'GP'}
		,['cogT',	floatingp,		'course over ground, degrees true']
		,['T',		literal('T'),	'True indicator']
		,['cogM',	floatingp,		'course over ground, degrees magnetic']
		,['M',		literal('M'),	'Magnetic indicator']
		,['sogN',	floatingp,		'speed over ground in knots']
		,['N',		literal('N'),	'Nautical miles indicator']
		,['sogK',	floatingp,		'speed over ground in l,/h']
		,['K',		literal('K'),	'Kilometer indicator']
		,['mode',	literal('[A|D|E|M|S|N]'),	'mode indicator']
			//	NOTE, Positioning system Mode indicator,
			//	A = Autonomous mode
			//	D = Differential mode
			//	E = Estimated (dead reckoning) mode
			//	M = Manual input mode
			//	S = Simulator mode
			//	N = Data not valid
			//	The positioning system Mode indicator field shall not be a null field.
		],	//	vtg
	ZDA:[{title:'Time & Date',	ti:'GP'}	//	Time & Date UTC, day, month, year and local time zone
		,['time',timestamp,'UTC time',]
		,['day',integer(2),'UTC day, 01 to 31',]
		,['month',integer(2),'UTC month 01 to 12',]
		,['year',integer(4),'UTC year',]
		,['hzone',integer(),'Local zone hours, 00 to ± 13 hrs',]
		,['mzone',integer(),'Local zone minutes, 00 to +59',]
		],	//	ZDA
	ON_:[{title:'L3 TrackPilot On switch',	ti:'TP'}	//	only for L3 bristol
		,['TP_on',literal(54|154),'154=on 054=off',]
		],	//	ONO
	OFF:[{title:'L3 TrackPilot Off switch',	ti:'TP'}	//	only for L3 bristol
		,['TP_off',literal(54|154),'154=off 054=on',]
		],	//	ONO
		
		
	//	todo implement, make special IS tab page with cbRxNmea()
	//	https://vstepbv.atlassian.net/wiki/display/NAU/Sentences
	//	https://vstepbv.atlassian.net/wiki/display/NAU/NMEA+sentences	//	automatically generated
	EXE:[{title:'Exercise synchronisation'}	//	nautis specific
		,['type',literal('L|S|N|C'),'L=Load, S=Save, N=New, C=Close',]
		,['timestamp',timestamp,'Scenario timestamp',]
		,['status',literal(0|1|2),'0=Succeeded, 1=Failed, 2=InProgress',]
		],
	MOO:[{title:'Mooring'}	//	nautis specific
		,['chainid',integer(),'L=Load, S=Save, N=New, C=Close',]
		,['name',vartext,'mooring line name: Head line, Forward breast line, Forward spring, Aft spring, Aft breast line, Stern line',]
		,['length',vartext,'Mooring Line length (in meters)',]
		,['linedirv',floatingp,'Mooring Line direction (in degrees)',]
		,['tension',floatingp,'Mooring Line tension (in tonnes)',]
		,['status',literal(0|1),'0=snapped/removed, 1=alive',]
		,['time',timestamp,'simulation time',]
		],	//	For every mooring line a ship has, a MOO sentence will be sent frequently with the data of this mooring line. When a mooringline is removed, the MOO sentence of this mooringline is deleted as well.	
	MTW:[{title:'Water Temperature'}	//	nautis specific
		,['temp',floatingp,'The temperature of the water in degrees celcius',]
		,['celcius',literal('C'),'Displayed as C, degrees in celcius',]
		],	//	MTW
	OBE:[{title:'Object Event'}	//	nautis specific
		,['type',literal('.+'),'command: A=Add object, R=Remove object, RA=Remove All objects, TA=Trainee Assigned, TR=Trainee Released, N=set Name, L=List object, C=Collision, OE=Outside simulated Environment, UE=object Update Error.',]	
		,['InstanceID',integer(),'Unique object type ID of the object',]
		,['InstanceType',vartext,'Unique object type ID of the object',]
		,['Name',vartext,'Name of the object as visible to the user',]
		,['CallSign',vartext,'Call sign of the object',]
		,['MMSI',vartext,'MMSI needs to be set during Add object',]
		,['Command',literal('C|A|V'),'C=Command, A=Acknowledge/Valid, V=Reject/Invalid',]
		],	//	OBE
	OBU:[{title:'Object position and orientation'}	//	nautis specific
		,['InstanceID',integer(1),'unique object instance ID: 1 - 99',]
		,['PositionType',literal('W|G'),'W=World coordinates, G=Geographical',]
		,['Northing',floatingp,'W: Distance in meters North of origin; G: Latitude in DMM. Use "-" for south, (-3436.0991)',]
		,['Easting',floatingp,'W: Distance in meters East of origin; G: Longitude in DMM. Use "-" for west, (-05818.9142)',]
		,['Upward',floatingp,'Distance in meters upwards',]
		,['Yaw',floatingp,'Yaw rotation (clockwise) in degrees',]
		,['Pitch',floatingp,'Pitch rotation in degrees. Bow up is positive. Optional, if not set, pitch is calculated by receiver',]
		,['Roll',floatingp,'Roll rotation in degrees. Heeling over to starboard is positive. Optional, if not set, roll is calculated by receiver',]
		],	//	OBU
	SPA:[{title:'Special Actions'}	//	nautis specific
		,['uniqueSceneID',integer,'The uniqueSceneID of the object',]
		,['specialactionID',integer,'The special action ID that needs to be set',]
		,['automatic',integer,'If the special action should be automatic',]
		,['timervalue',floatingp,'The timer value of the special action',]
		],	//	SPA
	STM:[{title:'Steering Mode'}	//	nautis specific
		,['Steeringmode',integer,'0=FU, 1=NFU, 2=AP, 3=DPext, 4=DPint, 5=EngineRoom',]
		,['ControllerID',integer,'ID of controller, -1 if not required',]
		,['ThrusterID',integer,'Thruster ID, -1 for all thrusters',]
		,['SentencyType',literal('C|R|S|A'),'C=Command give controller the steeringmode<br/>R=Request status update<br/>S=Status report. Current steering mode is<br/>A=Acknowledge received command, steeringmode command, accepted or unaccepted',]
		,['Status',literal('A|V'),'A=vAlid, V=inValid',]
		],
	TOW:[{title:'Tow line'}	//	nautis specific, deprecated from 2.14
		,['chainid',integer(),'L=Load, S=Save, N=New, C=Close',]
		,['name',vartext,'mooring line name: Head line, Forward breast line, Forward spring, Aft spring, Aft breast line, Stern line',]
		,['linedirv',floatingp,'Mooring Line direction (in degrees)',]
		,['length',vartext,'Mooring Line length (in meters)',]
		,['tension',floatingp,'Mooring Line tension (in tonnes)',]
		,['status',literal(0|1),'0=removed, 1=alive, 2=snapped',]
		,['time',timestamp,'simulation time',]
		],	//	The TOW sentence will be sent frequently for every towline with the name, direction, length, tension and status of the current towline. When the towline has been removed or has been snapped, the TOW sentence will be sent once more and then deleted.
	XDR:[{title:'Transducer Measurements'}	//	nautis specific
		,['type', vartext, 'Transducer type, Transducer #1, see table 18',]
		,['data', floatingp, 'Measurement data, Transducer #1',]
		,['units',vartext, 'Units of measure, Transducer #1, see table 18',]
		,['id',vartext,'Transducer #1 ID',]
		,['vardata',vartext,'Data for variable # of transducers (1,2,3 and 4)',]
		,['transducerdata',vartext,'Transducer n Sets of the four fields \'Type-Data-Units-ID"',]
		],	//	XDR	
	// mecaptain specifics	
	MC0:[{title:'Ships particulars or request for ships particulars'}	//	mecaptain specific
		,['req',		vartext, 'request/answer',]
		//	req	ans	reqdir	fields							meaning
		//	  1	101	to sim	name,mmsi,callsign			get data
		//	  2	102	to sim	ship								set simulated ship
		//	  3	103	to sim	latrad,lonrad					set position
		//	  4	104	to sim	hdgt								set heading
		//	  5	105	to sim										start simulator
		//	  .	10.	to sim										todo autopilot constants 
		//	  .	10.	to sim										todo thrusters, rudders, AP constants 
		// ,['mmsi',		vartext, 'ships mmsi',]
		// ,['callsign',	vartext, 'ships callsign',]
		],	//	MC0
	};

//		NEWSENTENCE:[{title:'Object Event'}].concat(todo),	//	the concat thing eneables this sentence without providing the details
var _counter_cbRxNmea = 0;	//	todo in nmeadefs stoppen
function cbRxNmea(key,fun,payload,bExternalWindow){	//	registrate a callback upon reception of a NMEA sentence, typical HighLevel function
	var oldcallback = nmeadefs[key][0].rxcb;
	if (undefined==nmeadefs[key][0].rxcb){
		nmeadefs[key][0].rxcb = new Array();
		}
	nmeadefs[key][0].rxcb.push({counter:++_counter_cbRxNmea,fun:fun, payload:payload});	//	register the callback function
	if ((undefined!=bExternalWindow) && (1==bExternalWindow) && (undefined!=window.opener)){
		var busbuffer = {cmd:'cbRxNmea',parms:key};
		if (true && 'file://'==window.location.href.substr(0,7)){
			// console.log('todo, als domain file:// is dan doorsturen via localstorage');
			// localStorage.setItem('poep',JSON.stringify(busbuffer));
			window.opener.fromExternalInstrument(window,busbuffer);	//	todo hier nog naam van callback meegeven
			}
		else{	
			window.opener.fromExternalInstrument(window,busbuffer);	//	todo hier nog naam van callback meegeven
			}	
		}	
	return _counter_cbRxNmea;
	}
function cbRxNmeaUnsubscribe(counter,key,bExternalWindow){	//	
	// console.log('cbRxNmeaUnsubscribe('+counter+','+key+')');
	var test = nmeadefs[key][0].rxcb.findIndex(el => el.counter===counter);
	if (undefined!=test){
		// console.log('cbRxNmeaUnsubscribe('+counter+','+key+') index '+test+' found');
		nmeadefs[key][0].rxcb.splice(test,1);
		if ((undefined!=bExternalWindow) && (1==bExternalWindow) && (undefined!=window.opener)){
			window.opener.fromExternalInstrument(window, {cmd:'cbRxNmeaUnsubscribe',parms:key});
			}	
		}
	}
//	maakt het aanroepen van NMEA subscribers makkelijk en klein	
class CeachRxcb {
	constructor(key){
		this.key=key;
		}
	go(parm){	
		if (undefined!=nmeadefs[this.key][0].rxcb)
			nmeadefs[this.key][0].rxcb.myforEach(function(cb,el,ar,p){cb.fun(p,cb.payload)},parm);	
		}
	}
// als dit (als instrument) in een external window draait, moet een verbinding met een ander window gemaakt worden	
class CeachRxcbExternalWindow {
	constructor(key){
		this.key=key;
		}
	go(parm,wid){	
		// if (undefined!=nmeadefs[this.key][0].rxcb)	//	misschien zijn er in het instrument zelf ook subscribers (onwaarschijnlijk)
		//	hier weet je niet of er geabboneerden zijn, laat dat aan het main programma over	
		if ('file://'==window.location.href.substr(0,7)){
			console.log('nmeadefs.js CeachRxcbExternalWindow::go() via localstorage; wid='+wid);
			localStorage.setItem(wid,JSON.stringify({cmd:'rxcb',parms:parm}));
			}
		else {
			window.opener.fromExternalInstrument(window, {cmd:'rxcb',parms:parm});
			}
		}
	}
// install ease of use functions to call all subscribed nmea callbacks	
Object.keys(nmeadefs).myforEach(function(key,el,ar,p){
	if (1==zeroisim.gup('extwin',0))
		nmeadefs[key][0].eachRxcb = new CeachRxcbExternalWindow(key);
	else
		nmeadefs[key][0].eachRxcb = new CeachRxcb(key);
	});	
	
	