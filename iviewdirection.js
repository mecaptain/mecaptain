//	ishipshape.js

'use strict';

class CInstrument_viewdir extends CInstrument {
	constructor(obj,exposedParms,active,idx){
		super(idx,obj);
		if (undefined!=exposedParms)
			this.exposedParms = exposedParms;
		else
			this.exposedParms = //	todo deze moeten door de conning designer kunnen worden gezet, nier overrulen hier dan
				{test:['a','b']
				,globalAlpha:					1.0
				,shapeStrokeWidth: 10	
				,color:
					{pullPointStroke:'rgba(40,40,40,0.6)'
					,pullPointStrokeWidth:1.5
					,pullPointFill:'rgba(255,255,255,0.6)'
					,pullPointFillOpacity:0.6
					}
				,hdgStrokeColor:'#f00'	
				,hdgStrokeWidth:100
				,hdgStrokeOpacity:0.6
				,posStrokeRadius:40
				,posStrokeColor:'#f00'
				,posStrokeOpacity:0.6
				
				,hdgRadius:			80
				,hdgStartAngle:	-40
				,hdgEndAngle:		40

				};
				
		this.active = (undefined==active)? false: active;
		var wh = {w:this.div.offsetWidth,h:this.div.offsetHeight};
		if (this.active){
			this.gizmo = new CGizmo(wh.w,wh.h,this.exposedParms)
				.onRotate(this.rotate.bind(this))
				.onPosition(this.move.bind(this))
				.onExit(this.sendPostionReady.bind(this))
				;
			zeroisim.replaceNode(this.div,this.gizmo.svg());
			// this.div.appendChild(this.gizmo.svg());
			this.draw();
			}
		}
	destruct(){
		// todo this.gizmo.destruct();
		}
	resize(width,height){
		// console.log('viewdirection resize('+width+','+height+')');
		// todo this.gizmo.resize(width,height);
		}
	rotate(direction){
		console.log('rotate:'+direction);
		}
	move(x,y){
		console.log('rotate:');
		}
	sendPostionReady(){}
	draw(ship) {
		}
	};

CInstrument.addInstrumentToSettings('./iviewdirection.js',		'viewdir',		'div',		CInstrument_viewdir,		'shows and defines where you are looking at');
	
