//	tab_astro.js
//	astronomical calculations
//	maak subtabs
//	teken een sphere, kijk bv naar http://learningthreejs.com/blog/2013/09/16/how-to-make-the-earth-in-webgl/
//	teken hoogtelijnen op de zeekaart
//	http://tdc-www.harvard.edu/catalogs/bsc5.html

// add ring shape
// var ring = new THREE.Mesh(new THREE.RingGeometry(30, 70, 32)
// 			, new THREE.MeshLambertMaterial({ color: this.getRandColor() }));
// ring.rotation.y = -Math.PI / 2;
// ring.position.x = -100;
// ring.position.y = 150;
// ring.position.z = 100;
// ring.castShadow = ring.receiveShadow = true;
// this.scene.add(ring);
//
//	https://threejs.org/examples/#svg_lines
'use strict';
var astrotabobj = {
	// verplichte drie members om de tabs interface te ondersteunen
	html: function(){
		// console.log('initting astrotabObj');
		this.bSomeBoolean = false;
		this.deg2rad = Math.PI/180.0;	//	hoeken naar radialen
		
		//	this init is called before page is loaded. Defer initialisation, you need the canvas for it, to after pageload event
		window.addEventListener("load", this.onload.bind(this), false);
		return 'astro tab' + h.canvas('id="astrotab_canvas" width="480" height="340"');
		}
	,onfocus:function(){	
		this.hasFocus=true;
		this.test();
		}
	,onblur:function(){	
		this.hasFocus=false;	
		}
	,onload: function(evt){
		var canvas = document.getElementById('astrotab_canvas');
		canvas.that = this;
		canvas.onmousedown	= function(e){	e.currentTarget.that.mousedown(e);	}
		canvas.onmouseup		= function(e){	e.currentTarget.that.mouseup(e);		}
		canvas.onmousemove	= function(e){	e.currentTarget.that.mousemove(e);	}
		// canvas.onwheel			= function(e){	e.currentTarget.that.mousewheel(e);	}	,{passive: true}
		canvas.addEventListener('wheel',function(e){	e.currentTarget.that.mousewheel(e);	},{passive: true});
		}
	,hasFocus: false
	,mousedown: function(evt){
		}
	,mouseup: function(evt){
		}
	,mousemove: function(evt){
		}
	,mousewheel: function(evt){
		}
	,draw: function(){
		//	todo if in focus, draw apparant places of stars and sun in spere
		}
	,cal2JulianDate: function(Y,M,D){
		if (M>2){
			var y = Y;
			var m = M-3;
			}
		else {
			var y = Y-1;
			var m = M+9;
			}
		var J = Math.trunc(365.25*(y+4712)) + Math.trunc(m*30.6+0.5) + 59 + D - 0.5;
		var G = 38 - Math.trunc(3*Math.trunc(49+y/100)/4);	//	gregorian
		return J+G;
		}
	,julianDate2Cal: function(jd){
		var JDN = jd+0.5;
		var G = Math.trunc(3*Math.trunc((JDN-4479.5)/36524.25)/4+0.5) - 37;
		var J1 = JDN+G;
		var J2 = J1 - 59.25;
		var J3 = Math.trunc(J2-365.25*Math.trunc(J2/365.25));
		var J4 = J3+0.5;
		var J5 = Math.trunc(J4/30.6)+2;
		var Y = Math.trunc(J1/365.25)-4712;
		var M = J5-12*Math.trunc(J5/12)+1;
		var D = Math.trunc(J4-30.6*Math.trunc(J4/30.6))+1
		return {Y:Y, M:M,D:D};
		}
	,sunRADECSD(Y,M,D,h){
		var JD = this.cal2JulianDate(Y,M,D);
		JD += h/24 - 2451545.0;
		//var JD = this.cal2JulianDate(Y,M,D) + h/24 - 2451545.0;	//	todo dit zou goed moeten zijn
		var t = JD/36525;	//	interval from 2000 january 1 at 12h UTC in Julian centuries of 36525 days
		if (Y>=-390 && Y<=948) {	//	long term changes in the rotation of the earth
			var deltaT = (28.43 + t*4.525 + t*t*1.404) * Math.pow(10,-8);
			}
		else {
			var deltaT = 0.808*(t-2)*(t-2) * Math.pow(10,-8);
			}
		var T = t+deltaT;	//	time interval in uniform time scale (Dynamical Time)
		var lambda_m = 280.46645 + T*(36000.76975 + T*0.0003132);	//	geometric mean ecliptic longitude of date
		lambda_m %= 360;	if (lambda_m<0) lambda_m += 360;
		
		var G = 357.529 + T*35999.05029 ;	//	mean anomaly
		G %= 360;	if (G<0) G += 360;
		
		G *= this.deg2rad;
		// todo remove multiples of 360
		var epsilon_0 = 23.4393 - T*(0.01301 - T*(0.0000001 + T*0.0000006));	//	mean obliquity of the ecliptic
		var C = (1.9147 - T*(0.00482 - T*0.000015))*Math.sin(G) + 0.01999*Math.sin(G*2);	//	equation of the centre
		var omega = 125.045 - T*1934.136;	//	
		omega %= 360;	if (omega<0) omega += 360;
		omega *= this.deg2rad;
		
		var epsilon = epsilon_0 + 0.0026*Math.cos(omega);
		epsilon  *= this.deg2rad;
		var lambda = lambda_m + C - 0.0057 - 0.0048*Math.sin(omega);
		lambda  *= this.deg2rad;
		var x = Math.cos(lambda);
		var y = Math.cos(epsilon)*Math.sin(lambda);
		var z = Math.sin(epsilon)*Math.sin(lambda);
		var A = Math.atan(y/x);	A = Math.atan(Math.cos(epsilon)*Math.tan(lambda));
		var RA = A;
		if (x<0) RA = A+Math.PI;
		if (x>0 &&  y<0) RA = A+2*Math.PI;
		RA /= this.deg2rad;
		var DEC = Math.asin(z);	DEC = Math.asin(Math.sin(epsilon)*Math.sin(lambda));	//	in radialen
		DEC /= this.deg2rad;
		var SD = 0.2666/(1-0.017*Math.cos(G));
		var GHA = 100.4606 + t*(36000.76998 + t*0.000387) + 15*h - 0.0048*Math.sin(omega)*Math.cos(epsilon_0*this.deg2rad)-RA;
		GHA %= 360;	if (GHA<0) GHA += 360;

		var ET = GHA - (h*15-180);
		if (ET>10)	ET -= 360;
		return {GHA:GHA, DEC:DEC, RA:RA, SD:SD, ET:ET};
		}
	,stars: 
		// [{n:'aaa',l:0,mu:0,b:0,ma:0}
		// ,{n:'Acamar',l:23.2723,mu:-0.00152,b:-53.7402,ma:+0.00112}	//	1
		// ,{n:'aaa',l:0,mu:0,b:0,ma:0}
		// ,{n:'Vega',l:285.3164,mu:0.01403,b:61.7328,ma:0.00709}	//	56
		
		[{l:23.27230,mu:-0.00152,b:-53.7402,ma:+0.00112,n:'Acamar'}
		,{l:345.3117,mu:+0.00285,b:-59.3783,ma:-0.00275,n:'Achernar'}
		,{l:221.8701,mu:-0.00047,b:-52.8787,ma:-0.00070,n:'Acrux'}
		,{l:110.7630,mu:+0.00025,b:-51.3602,ma:+0.00010,n:'Adhara'}
		,{l:69.78920,mu:+0.00104,b:-5.46740,ma:-0.00550,n:'Aldebaran'}

		,{l:158.9334,mu:+0.00417,b:+5431880,ma:+0.00194,n:'AIioth'}
		,{l:176.9331,mu:-0.00430,b:+5438800,ma:-0.00230,n:'Alkaid'}
		,{l:315.9070,mu:+0.00184,b:+3291330,ma:-0.00536,n:'AINa\'ir'}
		,{l:83.46360,mu:-0.00002,b:-2450640,ma:-0.00007,n:'Alnilam'}
		,{l:147.2792,mu:-0.00074,b:-22.3825,ma:+0.00067,n:'Alphard'}

		,{l:222.2959,mu:+0.00568,b:+44.3236,ma:-0.00118,n:'Alphecca'}
		,{l:14.30850,mu:+0.00162,b:+25.6804,ma:-0.00575,n:'Alpheratz'}
		,{l:301.7765,mu:+0.01939,b:+29.3035,ma:+0.00733,n:'Altair'}
		,{l:345.4938,mu:-0.00100,b:-40.6331,ma:-0.01237,n:'Ankaa'}
		,{l:249.7623,mu:-0.00007,b:-4.56990,ma:-0.00061,n:'Antares'}

		,{l:204.2337,mu:-0.00768,b:+30.7363,ma:-0.06288,n:'Arcturus'}
		,{l:260.8962,mu:+0.00123,b:-46.1513,ma:-0.00075,n:'Atria'}
		,{l:173.1294,mu:-0.00250,b:-72.6798,ma:-0.00013,n:'Avior'}
		,{l:80.94640,mu:-0.00032,b:-16.8161,ma:-0.00037,n:'Bellatrix'}
		,{l:88.75470,mu:+0.00080,b:-16.0270,ma:+0.00026,n:'Betelgeuse'}

		,{l:104.9614,mu:+0.00308,b:-75.8239,ma:+0.00076,n:'Canopus'}
		,{l:81.85790,mu:+0.00126,b:+22.86430,ma:-0.01191,n:'Capella'}
		,{l:335.3293,mu:+0.00029,b:+59.90610,ma:-0.00002,n:'Deneb'}
		,{l:171.6176,mu:-0.01153,b:+12.2669,ma:-0.00849,n:'Denebola'}
		,{l:2.583500,mu:+0.00673,b:-20.7836,ma:-0.00191,n:'Diphda'}

		,{l:135.1975,mu:-0.00239,b:+49.6802,ma:-0.00343,n:'Dubhe'}
		,{l:82.57500,mu:+0.00037,b:+5.38510,ma:-0.00491,n:'Elnath'}
		,{l:267.9687,mu:-0.00080,b:+74.9223,ma:-0.00055,n:'Eltanin'}
		,{l:331.8850,mu:+0.00090,b:+22.0999,ma:-0.00029,n:'Enif'}
		,{l:333.8604,mu:+0.00716,b:-21.13570,ma:-0.00802,n:'Fomalhaut'}

		,{l:216.7397,mu:+0.00737,b:-47.8312,ma:-0.00543,n:'Gacrux'}
		,{l:190.7256,mu:-0.00449,b:-14.5009,ma:-0.00128,n:'Gienah'}
		,{l:233.7925,mu:-0.00036,b:-44.1375,ma:-0.00076,n:'Hadar'}
		,{l:37.66250,mu:+0.00364,b:+9.96510,ma:-0.00569,n:'Hamal'}
		,{l:275.0787,mu:-0.00106,b:-11.0519,ma:-0.00346,n:'KausAustralis'}

		,{l:133.3195,mu:+0.00112,b:+72.9876,ma:-0.00088,n:'Kochab'}
		,{l:353.4857,mu:+0.00125,b:+19.4060,ma:-0.00182,n:'Markab'}
		,{l:44.32010,mu:-0.00091,b:-12.5856,ma:-0.00197,n:'Menkar'}
		,{l:222.3086,mu:-0.00873,b:-22.0800,ma:-0.01871,n:'Menkent'}
		,{l:211.9692,mu:-0.01254,b:-72.2357,ma:-0.00329,n:'Miaplacidus'}

		,{l:62.08100,mu:+0.00051,b:+30.1255,ma:-0.00084,n:'Midak'}
		,{l:282.3853,mu:+0.00026,b:-3.44950,ma:-0.00156,n:'Nunki'}
		,{l:293.8176,mu:-0.00041,b:-36.2677,ma:-0.00244,n:'Peacock'}
		,{l:113.2156,mu:-0.01700,b:+6.68420,ma:-0.00436,n:'Pollux'}
		,{l:115.7855,mu:-0.01504,b:-16.0196,ma:-0.03143,n:'Procyon'}

		,{l:262.4487,mu:+0.00459,b:+35.8352,ma:-0.00609,n:'Rasalhague'}
		,{l:14982920,mu:-0.00648,b:+0.46490,ma:-0.00222,n:'Regulus'}
		,{l:76.82950,mu:-0.00003,b:-31.1228,ma:-0.00007,n:'Rigel'}
		,{l:239.4793,mu:-0.13521,b:-42.5959,ma:-0.02399,n:'RigilKentaurus'}
		,{l:257.9696,mu:+0.00084,b:+7.19780,ma:+0.00275,n:'Sabik'}

		,{l:37.78380,mu:+0.00105,b:+46.6222,ma:-0.00157,n:'Schedar'}
		,{l:264.5858,mu:+0.00007,b:-13.7884,ma:-0.00079,n:'Shaula'}
		,{l:104.0816,mu:-0.01524,b:-39.6053,ma:-0.03492,n:'Sirius'}
		,{l:203.8414,mu:-0.00075,b:-2.05450,ma:-0.00118,n:'Spica'}
		,{l:161.1877,mu:-0.00116,b:-55.8708,ma:+0.00011,n:'Suhail'}

		,{l:285.3164,mu:+0.01403,b:+61.7328,ma:+0.00709,n:'Vega'}
		,{l:225.0827,mu:-0.00226,b:+0.33300,ma:-0.00267,n:'Zubenelgenubi'}
		,{l:88.56760,mu:+0.00098,b:+66.1014,ma:-0.00118,n:'Polaris'}
		,{l:271.8706,mu:+0.00118,b:-65.8402,ma:-0.00042,n:'Octantis'}
		]
	,starEcliptic: function(IDstar,Y,M,D,h){
		var JD = this.cal2JulianDate(Y,M,D);
		JD += h/24;
		D = JD - 2451545.0;
		var t = D/36525;

		// calculate ecliptic lat & lon for epoch of date and equiniox J2000.0
		var lon1 = this.stars[IDstar].l + t*this.stars[IDstar].mu;
		var lat1 = this.stars[IDstar].b + t*this.stars[IDstar].ma;
		
		// abberation
		var lambda_sun = this.circular(280.460 + t*36000.770);
		var lon2 = lon1 - 0.0057*Math.cosdeg(lon1-lambda_sun)/Math.cosdeg(lat1);
		var lat2 = lat1 + 0.0057*Math.sindeg(lon1-lambda_sun)*Math.sindeg(lat1);
		
		// precession
		var a = t*(1.39697+t*0.000309);
		var b = t*(0.0131-t*0.00001);
		var c = 5.1236+t*0.2416;
		var lat3 = lat2+b*Math.sindeg(lon2+c);
		var lon3 = lon2+a-b*Math.cosdeg(lon2+c)*Math.tandeg(lat3);
		
		// nutation
		var omega = this.circular(125.045-t*1934.136);
		var epsilon_0 = 23.4393 - t*0.01301;	//	mean obliquity of the ecliptic
		var epsilon = epsilon_0 + 0.0026*Math.cosdeg(omega);
		var lon = lon3-0.0048*Math.sindeg(omega);
		var lat = lat3;
		
		// convert to apparent Right Ascention and declination
		var x = Math.cosdeg(lat)*Math.cosdeg(lon);
		var y = Math.cosdeg(epsilon)*Math.cosdeg(lat)*Math.sindeg(lon) - Math.sindeg(epsilon)*Math.sindeg(lat);
		var z = Math.sindeg(epsilon)*Math.cosdeg(lat)*Math.sindeg(lon) + Math.cosdeg(epsilon) * Math.sindeg(lat);
		var A = Math.atan(y/x);	A /= this.deg2rad;
		var RA = A;
		if (x<0)	RA += 180;
		if (x>0 && y<0) RA += 360;
		var dec = Math.asin(z);	dec /= this.deg2rad;
		
		// GHA
		var GHA = this.circular(100.4606 + t*(36000.77005 + t*0.000388) + 15*h - 0.0048*Math.sindeg(omega)*Math.cosdeg(epsilon)-RA);
		
		return {lon_ecl:lon,lat_ecl:lat,RA:RA,DEC:dec,GHA:GHA};
		}	
	,circular: function(arg){
		arg %= 360;	if (arg<0) arg += 360;
		return arg;
		}
	,test: function(){
		var Y = 1990, M=3, D=5;
		var jd = this.cal2JulianDate(Y,M,D);	//	2447955.5
		var cal = this.julianDate2Cal(jd);
		if (cal.Y!=Y || cal.M!=M || cal.D!=D)	alert('ERROR in julian date calculations.');

		Y = 1978, M=1, D=3;
		jd = this.cal2JulianDate(Y,M,D);	//	fout: 2442961.5, had 2443511.5 moeten zijn
		cal = this.julianDate2Cal(jd);		// 1976-07-02	dit gaat fout, 
		if (cal.Y!=Y || cal.M!=M || cal.D!=D)	alert('ERROR in julian date calculations.');

		Y = 1976, M=7, D=2;
		jd = this.cal2JulianDate(Y,M,D);	//	2442961.5
		cal = this.julianDate2Cal(jd);
		if (cal.Y!=Y || cal.M!=M || cal.D!=D)	alert('ERROR in julian date calculations.');
		
		var Y = 1978, M=1, D=3, h=7.5;
		var grds = this.sunRADECSD(Y,M,D,h);
		var EPSILON = 0.01;	//	todo dit moet beter
		if (Math.abs(grds.GHA-291.4086)>EPSILON	
		|| Math.abs(grds.DEC+22.8452)>EPSILON
		|| Math.abs(grds.RA-283.6628)>EPSILON
		|| Math.abs(grds.SD-0.2716)>EPSILON
		|| Math.abs(grds.ET+1.0914)>EPSILON
		)	{	alert('ERROR in sun calculations');}
		
		var Y = 1978, M=1, D=3, h=7.5;
		var star = this.starEcliptic(3,Y,M,D,h);
		var EPSILON = 0.0001;
		if (Math.abs(star.GHA-296.0292)>EPSILON
		|| Math.abs(star.DEC-38.7645)>EPSILON
		){alert('ERROR in star calculations');}

		}
	};
