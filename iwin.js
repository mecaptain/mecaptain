//	iwin.js
//	helper script for running instruments in external window

'use strict';
window.onload = function() {
	var idx = zeroisim.gup('idx');	
	var scripts = ['./nmea_defs.js','./cinstrument.js','ipilotHorizontal.js'];
	var Instrument = settings.instrumentList[idx];
	scripts = scripts.concat([Instrument.file]);
	var ls = new CLoadScript();
	ls.loadScripts(
		{idx:idx
		,settings:settings
		,cb:function(parms){				
			loadDependencies(parms.idx);
			}	
		,scripts:scripts
		});
	}
function loadDependencies(idx){	
	var Instrument = settings.instrumentList[idx];
	if (undefined==Instrument.dependencies || Instrument.dependencies.length==0) {
		loadInstrument(idx);
		}
	else {
		var ls = new CLoadScript();
		ls.loadScripts(
			{idx:idx
			,settings:settings
			,cb:function(parms){				
				loadInstrument(parms.idx);
				}	
			,scripts:Instrument.dependencies
			});
		}
	}
function loadInstrument(idx){	
	var defInstrument = settings.instrumentList[idx];
	setValue('dbg',JSON.stringify(defInstrument));
	var wh = {w:zeroisim.gup('w'),h:zeroisim.gup('h')};
	var container = CInstrument.makeContainer4(defInstrument,wh);
	var parent = document.body;
	zeroisim.replaceNode(parent,container);
	var wid = zeroisim.gup('wid',undefined);	//	haal key naar localstorage uit url
	var exposedParms = localStorage.getItem(wid);	//	haal met deze key data uit localstorage
	if (undefined!=exposedParms) {
		exposedParms = JSON.parse(exposedParms);
		}
	var instrument = new settings.instrumentList[idx].clas(parent,exposedParms,1,idx);	//	instantiate class	
	instrument.wid = wid;
	//	if in file:// domain, interwindow comms is forbidden, use localstorage
	if (true && 'file://'==window.location.href.substr(0,7)){
		console.log('iwin, installed evt handler for comms via localstorage');
		window.addEventListener('storage', function(evt) {  
			console.log('iwin, wid='+wid+' sto_event key='+evt.key);
			console.log('iwin, wid='+wid+' sto_event old='+evt.oldValue);
			console.log('iwin, wid='+wid+' sto_event new='+evt.newValue);		
			});
		}
	}
