//	igps.js
'use strict';
class CInstrument_gps extends CInstrument {
	constructor(obj,exposedParms,active,idx){
		super(idx,obj);
		var canvas = obj.querySelector("canvas");
		this.cv = canvas.getContext("2d")
		this.midden = {x: canvas.width/2, y:canvas.height/2};
		this.cv.textAlign="center";
		if (undefined!=exposedParms)
			this.exposedParms = exposedParms;
		else
			this.exposedParms = //	todo deze moeten door de conning designer kunnen worden gezet, nier overrulen hier dan
				{colorBackground:			'rgba(200,200,200,0.1)'	//	gray
				,colorText:					'rgba(20,20,20,0.7)'	//	black
				,offsetTextVertical:		-70
				,spacingTextVertical:	40
				,font:'40px sans-serif'
				//	todo keuze voor GGA, GLL, RMC, ZDA
				};
			
		this.ship0 = {latlon: new zeroisim.LatLon()};	//	geen gebruik meer maken van global		
		// super.cbRxNmea('GGA',function(rxd,that){	//	install callback for position		todo kiezen van welke sentence je de latlon wilt (GGA, GLL, ...)
			// if (undefined==rxd.data)	return;
			// that.ship0.latlon.interpretLatLonGGA(rxd.data);
			// that.draw(that.ship0);
			// },this);
		super.cbRxNmea('RMC',function(rxd,that){	//	install callback for position		todo kiezen van welke sentence je de latlon wilt (GGA, GLL, ...)
			if (undefined==rxd.data)	return;
			that.ship0.latlon.interpretLatLonRMC(rxd.data);
			that.ship0.cogT = parseFloat(rxd.data.cog);
			that.ship0.sog = parseFloat(rxd.data.sog);
			that.draw(that.ship0);
			},this);
		super.cbRxNmea('ZDA',function(rxd,that){	//	install callback for time
			if (undefined==rxd.data)	return;
			that.ship0.utc = rxd.data.time;
			},this);
		}
	draw(ship) {
		this.cv.clearRect(0,0,2*this.midden.x,2*this.midden.y);
		this.cv.save();
		this.cv.fillStyle = this.exposedParms.colorBackground;
		this.cv.fillRect(0,0,2*this.midden.x,2*this.midden.y);
		this.cv.translate(this.midden.x,this.midden.y);
		this.cv.fillStyle = this.exposedParms.colorText;
		this.cv.font = this.exposedParms.font;
		var YY = this.exposedParms.offsetTextVertical;
		this.cv.fillText(ship.latlon.latdegmindec,0,YY);								YY+=this.exposedParms.spacingTextVertical;
		this.cv.fillText(ship.latlon.londegmindec,0,YY);								YY+=this.exposedParms.spacingTextVertical;
		this.cv.fillText(ship.utc,0,YY);														YY+=this.exposedParms.spacingTextVertical;
	// this.cv.fillText(zeroisim.prefixpad(ship.hdt.toFixed(1),3)+h.deg,0,YY);	YY+=this.exposedParms.spacingTextVertical;
		this.cv.fillText(zeroisim.prefixpad(ship.cogT.toFixed(1),3)+h.deg,0,YY);YY+=this.exposedParms.spacingTextVertical;
		this.cv.fillText(ship.sog.toFixed(2)+' knots',0,YY);							YY+=this.exposedParms.spacingTextVertical;
		this.cv.restore();
		}
	};
	
CInstrument.addInstrumentToSettings('./igps.js'
	,'gps'
	,'canvas'
	,CInstrument_gps
	,'shows values measured by Global Positioning System'
	,['./latlon.js']
	);
