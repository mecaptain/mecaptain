//	environment.js
//	handles wind, clouds, current, precipitation, seawaves
'use strict';

class CEnvironment extends C0isimTab {
	constructor(settings){
		super();
		var _d = new Date();
		var environment_vars = [
			[
				{weather:[
					{wind:[
						['wind speed (kts)',0,67.5,'windspd',0.5],
						['wind direction',0,359,'winddir',1],
						]},
					{precipitation:[
						['rain',0.0,1.0,'rain'],
						['thunder',0,1,'thunder'],
						['fog',0,1,'fog'],
						]},
					{clouds:[
						['cumulonimbus',0,100,'cumulonimbus'],	//	not in nmea
						['cumulus',0,100,'cumulus'],			// not in nmea
						['altostratus',0,1,'altostratus'],
						['stratus',0,1,'stratus'],
						['cirrocumulus',0,1,'cirrocumulus'],
						['cirrus',0,100,'cirrus'],
						]},
					]},
				],
			[
				{'light and sea':[
					{time:[
						['hour',0,23,'hour',1,_d.getHours()],
						['minute',0,59,'minute',1,_d.getMinutes()],
						['second',0,59,'second',1,_d.getSeconds()],
						]},
					]},
				{ocean:[
					{waves:[
						['sea state',0,9,'seastate',0.1],
						['swell height',0,20,'waveheight'],
						['swell direction',0,359,'wavedir',1],
						]},
					{current:[
						['speed (kts)',0,10,'currentspd'],
						['direction (&deg;)',0,359,'currentdir',1],
						]},
					{tide:[
						['height (m)',0,12,'htide'],				// not in nmea
						['phase',-12.4,12.4,'tide',0.1],
						]},
					]},
				],
			];

		this.environment = [];
		Object.keys(environment_vars).myforEach(function(ii,idx,that,pp){
			var column = environment_vars[ii];
			var fields = [];
			Object.keys(column).myforEach(function(jj,idx,that,pp){
				var fieldset=column[jj];
				var burp=[];
				Object.keys(fieldset).myforEach(function(kk,idx,that,pp){
					var groups = fieldset[kk];
					Object.keys(groups).myforEach(function(ll,idx,that,pp){
						var group = groups[ll];
						for (var groupname in group) break;
						var slidershtml = [];
						Object.keys(group).myforEach(function(mm,idx,that,pp){
							var sliders = group[mm];
							Object.keys(sliders).myforEach(function(nn,idx,that,pp){
								var slider = sliders[nn];
								if (undefined!=slider[5])
									mso[slider[3]] = slider[5];	//	fill default value
								else
									mso[slider[3]] = 0;
								if (slider.length>4){
									var step = slider[4];
									}
								else {
									var step = zeroisim.roundNumber((slider[2]-slider[1])/100,2+Math.log10(1/slider[2]-slider[1]));
									}	
								slidershtml.push(i.tr
									([i.td(i.span(slider[0]))
									,i.td(i.input('',
										{type:"range" 
										,id:slider[3]
										,input:pp.hslider.bind(pp) 
										,min:slider[1]
										,max:slider[2]
										,step:step
										,value:mso[slider[3]]
										,style:"width: 270px;"
										}))
									,i.td(i.output(i.span(mso[slider[3]].toString()),{for:'id_'+slider[3],id:'vol_'+slider[3]}),{style:"width: 30px;"})
									])
									);
								},pp);
							},pp);
						burp.push(i.trtd(i.fieldset(groupname,i.table(slidershtml)),{style:"border:0px"}));
						},pp);
					},pp);
				for (var legend in fieldset) break;
				fields.push(i.fieldset(legend,i.table(burp,{style:"border:0px"})));
				},pp);
			pp.environment.push(i.td(fields));
			},this);
		this.environment = i.table(
			[i.trtd(i.table(i.tr(this.environment),{style:"border:0px"}))
			,i.trtd(i.table(
				[i.trtd(i.div('MSO',{id:'theMSOSentence'}))
				,i.td(i.button('send',{click:this.commitweather.bind(this)}))
				]))
			]);
		}
	onload(evt){
		document.getElementById(this._tabdivname).appendChild(this.environment);
		}
	hslider(evt){
		var field = evt.target.id;
		var value = evt.target.value;
		document.querySelector('#vol_'+field).value = value;
		mso[field] = parseFloat(value);
		document.querySelector('#theMSOSentence').innerHTML = this.constructMSO();
		}
	constructMSO(){
		mso.time //	build time from hour:minute:second
			= zeroisim.prefixpad(mso.hour,2)
			+ zeroisim.prefixpad(mso.minute,2)
			+ zeroisim.prefixpad(mso.second,2)
			+ '.00';
		tx.MSO = mso;
		tx.MSO.talker = 'WI';
		return buildNMEA('MSO',1,1);
		}
	commitweather(evt){
		ws_talk_listen.sendNMEA(ship0.remote_protocol+'://'+ship0.remote_ip+':'+settings.talk_port,this.constructMSO());
		}
	}
