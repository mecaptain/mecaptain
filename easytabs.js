//	easytabs.js
//	(c)	Bert Tuijl
//	make jquery tabs and subtabs easy
// do not forget to include jquery style and script
//	example:
/*
see testjs/test_tabs.html
*/

'use strict';

var easytabs = function(tabses,id){
	this.tag=function(tagname,data,style_class){
		if (undefined==data)	data='';
		if (undefined==style_class)
			style_class = '';
		else
			style_class = " " + style_class;
		return "<"+tagname+style_class+">"+data+"</"+tagname+">";
		}
	this.tabhdrs = '';
	this.tabpages = '';
	this.id = id.replace(/ /g, '_').replace(/\+/g, '_plus_');
	for (var ii in tabses) {
		var tagid = this.id+'-'+ii.replace(/ /g, '_').replace(/\+/g, '_plus_');
		this.tabhdrs += this.tag('li',this.tag('a',ii,'href="#'+tagid+'"'));
		this.tabpages += this.tag('div',tabses[ii],'id="'+tagid+'"');
		}
	this.subtab=function(){
		return this.tag('ul',this.tabhdrs) + this.tabpages;		
		}
	this.creatediv=function(indiv){
		this.div = document.createElement('div');
		this.div.id = this.id;
		if (undefined==indiv)
			document.body.appendChild(this.div);
		else
			indiv.appendChild(this.div);
		return this;
		}
	this.setHTML=function(){
		this.div.innerHTML = this.subtab();
		return this;
		}
	this.tabs=function(tab){
		if (undefined==tab){
			$('#'+this.id).tabs();
			}
		else {
			$('#'+this.id+'-'+tab).tabs();
			}
		return this;
		}
	this.doit=function(indiv){
		this
			.creatediv(indiv)
			.setHTML()
			.tabs()
			;
		return this;
		}
	return this;//	return {hdrs: tabhdrs, pages: tabpages};
	}
