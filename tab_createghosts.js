//	tab_createghosts.js
//	provides skeleton to add a tab to zeroisim userinterfacereadout
'use strict';
var tabghosts = {
	// verplichte drie members om de tabs interface te ondersteunen
	html: function(){
		this.bSomeBoolean = false;
		this.aisfields = 
			{hdg:'degrees'
			,position:'latlon'
			,stw:'knots'
			,navStatus:'[1,2,3]'
			,name:'string'
			,mmsi:'number[9]'
			,callsign:'string'
			,IMO:'number[7]'
			,shipType:'[1-100]'
			,bow:'[1-100]',stern:'[1-100]',port:'[1-100]',starboard:'[1-100]'
			,draught:'knots'
			,posSystem:'[1,2,3,4,5]'
			,ETA:'date_eta'
			,destination:'string'
			};	//	todo alle eigenschappen die nodig zijn voor VDM1 en VDM5 hier toevoegen

		//	this init is called before page is loaded. Defer initialisation, you need the canvas for it, to after pageload event
		window.addEventListener("load", this.onload.bind(this), false);	//	todo use bind() everywhere on load event
		return 'Ghosts tab' + h.canvas('id="tabghosts" width="480" height="340"');
		}
	,onfocus:function(){	
		this.hasFocus=true;
		}
	,onblur:function(){	
		this.hasFocus=false;	
		}
	,onload: function(evt){
		var canvas = document.getElementById('tabghosts');
		canvas.that = this;
		canvas.onmousedown= function(e){	e.currentTarget.that.mousedown(e);	}
		canvas.onmouseup	= function(e){	e.currentTarget.that.mouseup(e);		}
		canvas.onmousemove= function(e){	e.currentTarget.that.mousemove(e);	}
		canvas.onwheel		= function(e){	e.currentTarget.that.mousewheel(e);	}
		}
	,hasFocus: false
	,mousedown: function(evt){
		}
	,mouseup: function(evt){
		}
	,mousemove: function(evt){
		}
	,mousewheel: function(evt){
		}
	,draw: function(){
		}
	};
