//	wsconnect.js
// todo subscriptions op 
//		onopen
//		onmessage
//		onclose
//		onerror

'use strict';
var wsconnect = function(ip,prot,cbconnect,cbdata,cbclose,that,divdebug){
	// open a websocket and post data to socket server
	//ip = 'ws://localhost:12120';
	//	ip = 'ws://192.168.1.14:12120';	//	BIJ WIM THUIS
	//ip = 'ws://82.173.162.184:12120';	//	server LH13
	//	ip = 'ws://192.168.1.12:12120';	//	direkt op lan adres
	var mythat = that;
	this.mythat = that;
	var mythis = this;
	this.savprotocol = prot;	//	voor identificatie

	if (divdebug!=undefined){
		var dbgdiv = document.getElementById('debug');
		dbgdiv.innerHTML = 'WS='+ip;
		}
	var ws = new WebSocket(ip,prot);
	ws.mythis = this;
	ws.mythat = that;
	ws.onopen = function(){
		console.log('Websocket "'+mythis.savprotocol+'" connected.');
		cbconnect(mythis,mythat);
		};
	ws.onmessage = function(evt){
		//	console.log('websocket rx: ' + evt.data );
		if (cbdata!=undefined){
			cbdata(mythis,evt.data,mythat);
			}
		};
	ws.onclose = function(mythis){					// websocket is closed.
		console.log('websocket "'+mythis.currentTarget.mythis.savprotocol+'" connection is closed...');
		if (cbclose!=undefined){
			cbclose(mythis,this.mythat);
			}
		};
	ws.onerror = function(){};	// todo implementeren
	this.zend = function(str){
		if (ws.readyState==1){
			ws.send(str);
			}
		}
	this.zendNMEA = function(toaddr,message){
		if (undefined!=toaddr){
		console.log('wsconnect.zendNMEA() '+JSON.stringify({send_addr:toaddr,message:message}));
		this.zend(JSON.stringify(
					{send_addr:		toaddr
					,message:		message
					}));
		// count_tx++;
		return 'msg sent';
		}
		else
			return 'ERROR no address given to send data to.'
		}
	// this.throttled_sendNMEA = _.throttle(this.zendNMEA.bind(this), 100,{trailing:1,leading:1});
	this.throttled_sendNMEA = _.throttle(this.zendNMEA, 100,{trailing:1,leading:1});	//	niet te snel aanroepen
	this.__defineGetter__('protocol', function(){	return this.savprotocol;	});
	this.__defineGetter__('ws', function(){	return ws;	});
	}
