//	azi.js
//	provides readout and control of azithrusters
'use strict';
var azitabobj = {
	// verplichte drie members om de tabs interface te ondersteunen
	html: function(){
		// console.log('initting aziTabOject');
		this.bAziShip = false;
		this.noThrusters = 2;
		this.deg2rad = Math.PI/180.0;
		this.aziScale = 0.009;	// 0.010 would be use full circle of azi picture
		this.colorCommand = '#0000FF';
		this.colorRespons = '#FF0000'
		this.colorPredict = '#888888'
		this.azi_readout = 'azi_readout';
		this.imgPropellor = new Image();
		this.imgPropellor.this = this;
		this.imgPropellor.src = './img/propellor_azi_push.png';
		this.imgPropellor.onload = function(evt) {	//	image loads slow
			// console.log('azitabobj image loaded');
			}
		//	this init is called before page is loaded. Defer initialisation, you need the canvas for it, to after pageload event
		// document.azitabobj = this;
		window.addEventListener("load", this.initAfterLoad.bind(this), false);

		cbRxNmea('TRC',function(rx,that){	//	install callback handler upon rx of TRC
			if (undefined==rx.data)	return;
				that.trc_rxd(rx);
				},this);
		cbRxNmea('TRD',function(rx,that){	//	install callback handler upon rx of TRD
			if (undefined==rx.data)	return;
				that.trd_rxd(rx);
				},this);
		var azif = 
			['rpm'
			,'rpmi'	
			,'pitch'	
			,'ptm'		
			,'azi'		
			,'oloc'	
			,'stat'
			]; var ii=0;			
		var azitable = h.table(h.tr
					(h.td(h.table
						( h.tr(h.td(h.nbsp)	+h.td('0','colspan=2'))
						+ h.tr(h.td(h.nbsp)	+h.td('command')+h.td(h.div('response')))
						+ h.tr(h.td(azif[ii+0])+	this.azirow(azif,0,ii+0))
						+ h.tr(h.td(azif[ii+1])+	this.azirow(azif,0,ii+1))
						+ h.tr(h.td(azif[ii+2])+	this.azirow(azif,0,ii+2))
						+ h.tr(h.td(azif[ii+3])+	this.azirow(azif,0,ii+3))
						+ h.tr(h.td(azif[ii+4])+	this.azirow(azif,0,ii+4))
						+ h.tr(h.td(azif[ii+5])+	this.azirow(azif,0,ii+5))
						+ h.tr(h.td(azif[ii+6])+	this.azirow(azif,0,ii+6))
						))
					+h.td(h.table
						( h.tr(h.td('1','colspan=2'))
						+ h.tr(h.td('command')+h.td(h.div('response')))
						+ h.tr(this.azirow(azif,1,ii+0))
						+ h.tr(this.azirow(azif,1,ii+1))
						+ h.tr(this.azirow(azif,1,ii+2))
						+ h.tr(this.azirow(azif,1,ii+3))
						+ h.tr(this.azirow(azif,1,ii+4))
						+ h.tr(this.azirow(azif,1,ii+5))
						+ h.tr(this.azirow(azif,1,ii+6))
						))
					+h.td(h.table
						( h.tr(h.td('2','colspan=2'))
						+ h.tr(h.td('command')+h.td(h.div('response')))
						+ h.tr(this.azirow(azif,2,ii+0))
						+ h.tr(this.azirow(azif,2,ii+1))
						+ h.tr(this.azirow(azif,2,ii+2))
						+ h.tr(this.azirow(azif,2,ii+3))
						+ h.tr(this.azirow(azif,2,ii+4))
						+ h.tr(this.azirow(azif,2,ii+5))
						+ h.tr(this.azirow(azif,2,ii+6))
						))
					+h.td(h.table
						( h.tr(h.td('3','colspan=2'))
						+ h.tr(h.td('command')+h.td(h.div('response')))
						+ h.tr(this.azirow(azif,3,ii+0))
						+ h.tr(this.azirow(azif,3,ii+1))
						+ h.tr(this.azirow(azif,3,ii+2))
						+ h.tr(this.azirow(azif,3,ii+3))
						+ h.tr(this.azirow(azif,3,ii+4))
						+ h.tr(this.azirow(azif,3,ii+5))
						+ h.tr(this.azirow(azif,3,ii+6))
						))
					));
		var canvasTag = '';
		for (var not=0; not<this.noThrusters; not++){
			canvasTag += h.canvas('id="'+this.azi_readout+not+'" width=300" height="300" style="border:0px solid #000;"')
			}
		return h.table(h.tr(h.td(h.fieldset('azi graphic',canvasTag))
				/*debugging, training*/
			+h.td(h.fieldset('resulting vector'
				,h.table
					(h.tr(h.td(h.canvas('id="vector_resultante" width=300" height="250" style="border:0px solid #000;"'),'colspan=2'))
					+h.tr(h.td('force:')+h.td(h.div('force','id="azi_res_force"')))
					+h.tr(h.td('azimuth:')+h.td(h.div('azimuth','id="azi_res_azimuth"')))
					)))
			/* end of debugging, training */
			))
			// + azitable
			+ h.table(h.tr(h.td(azitable)+h.td(h.fieldset('dopplerlog',h.nbsp,'id="azi_doppler"'))))
			;
		}
	,azirow:function(azif,idx,ii){
		if (ii>4)	
			return h.td(h.div(azif[ii+0],'id="div_aziC_'+idx+azif[ii+0]+'"'))+h.td(h.div(h.nbsp,'id="div_aziD_'+idx+azif[ii+0]+'"'))
		else
			return h.td(h.div(azif[ii+0],'id="div_aziC_'+idx+azif[ii+0]+'"'))+h.td(h.div(azif[ii+0]	,'id="div_aziD_'+idx+azif[ii+0]+'"'))
		}		
	,onfocus:function(){	this.hasFocus=true;
		for (var not=0; not<3; not++){
			if (undefined!=this.thrusterDataC[not]){
				setValue('div_aziC_'+not+'rpm',this.thrusterDataC[not].rpm);
				setValue('div_aziC_'+not+'rpmi',this.thrusterDataC[not].rpmi);
				setValue('div_aziC_'+not+'pitch',this.thrusterDataC[not].pitch);
				setValue('div_aziC_'+not+'ptm',this.thrusterDataC[not].ptm);
				setValue('div_aziC_'+not+'azi',this.thrusterDataC[not].azi);
				setValue('div_aziC_'+not+'oloc',this.thrusterDataC[not].oloc);
				setValue('div_aziC_'+not+'stat',this.thrusterDataC[not].stat);
				}
			if (undefined!=this.thrusterDataD[not]){
				setValue('div_aziD_'+not+'rpm',this.thrusterDataD[not].rpm);
				setValue('div_aziD_'+not+'rpmi',this.thrusterDataD[not].rpmi);
				setValue('div_aziD_'+not+'pitch',this.thrusterDataD[not].pitch);
				setValue('div_aziD_'+not+'ptm',this.thrusterDataD[not].ptm);
				setValue('div_aziD_'+not+'azi',this.thrusterDataD[not].azi);
				//setValue('div_aziD_'+not+'oloc',this.thrusterDataD[not].oloc);
				//setValue('div_aziD_'+not+'stat',this.thrusterDataD[not].stat);
				}
			}
		// document.getElementById("ap_kompas").appendChild(document.getElementById("kompas_canvas"));
		document.getElementById("azi_doppler").appendChild(document.getElementById("dopplerlog_canvas"));
		}
	,onblur:function(){	this.hasFocus=false;	}
	,hasFocus: false
	,thrusterDataC:[]
	,thrusterDataD:[]
	,trc_rxd:function(rx){	//	thruster command
		this.bAziShip = true;
		var data=rx.data;
		if (this.hasFocus) {
			// console.log('thruster data received; thruster: '+data.not);
			setValue('div_aziC_'+data.not+'rpm',data.rpm);
			setValue('div_aziC_'+data.not+'rpmi',data.rpmi);
			setValue('div_aziC_'+data.not+'pitch',data.pitch);
			setValue('div_aziC_'+data.not+'ptm',data.ptm);
			setValue('div_aziC_'+data.not+'azi',data.azi);
			setValue('div_aziC_'+data.not+'oloc',data.oloc);
			setValue('div_aziC_'+data.not+'stat',data.stat);
			}
		// else {
			//console.log('thruster data received; thruster: '+data.not+', but no focus');
			this.thrusterDataC[data.not] = {};
			this.thrusterDataC[data.not].rpm = data.rpm;
			this.thrusterDataC[data.not].rpmi = data.rpmi;
			this.thrusterDataC[data.not].pitch = data.pitch;
			this.thrusterDataC[data.not].ptm = data.ptm;
			this.thrusterDataC[data.not].azi = data.azi;
			this.thrusterDataC[data.not].oloc = data.oloc;
			this.thrusterDataC[data.not].stat = data.stat;
			// }
		if (this.bAziShip)	this.draw();
		}
	,trd_rxd:function(rx){	//	thruster response
		// this.bAziShip = true;
		var data=rx.data;
		if (this.hasFocus) {
			// console.log('thruster data received; thruster: '+data.not);
			setValue('div_aziD_'+data.not+'rpm',data.rpm);
			setValue('div_aziD_'+data.not+'rpmi',data.rpmi);
			setValue('div_aziD_'+data.not+'pitch',data.pitch);
			setValue('div_aziD_'+data.not+'ptm',data.ptm);
			setValue('div_aziD_'+data.not+'azi',data.azi);
			//setValue('div_aziD_'+data.not+'oloc',data.oloc);
			//setValue('div_aziD_'+data.not+'stat',data.stat);
			}
		// else {
			//console.log('thruster data received; thruster: '+data.not+', but no focus');
			this.thrusterDataD[data.not] = {};
			this.thrusterDataD[data.not].rpm = data.rpm;
			this.thrusterDataD[data.not].rpmi = data.rpmi;
			this.thrusterDataD[data.not].pitch = data.pitch;
			this.thrusterDataD[data.not].ptm = data.ptm;
			this.thrusterDataD[data.not].azi = data.azi;
			//this.thrusterDataD[data.not].oloc = data.oloc;
			//this.thrusterDataD[data.not].stat = data.stat;
			// }
		this.draw();
		}
	,initAfterLoad: function(){
		// console.log('initAfterLoad');
		this.canvas = [];
		this.cv = [];
		for (var not=0; not<this.noThrusters; not++){
			this.canvas[not] = document.getElementById(this.azi_readout+not);
			this.canvas[not].azitabobj = this;
			this.canvas[not].not = not;
			this.cv[not] = this.canvas[not].getContext("2d");

			this.canvas[not].addEventListener("mousedown", function(eEvent) {
				if (undefined != eEvent.currentTarget.azitabobj.mousedown)
					eEvent.currentTarget.azitabobj.mousedown(eEvent.currentTarget.azitabobj,eEvent);
				}, false);
			this.canvas[not].addEventListener("mouseup", function(eEvent) {
				if (undefined != eEvent.currentTarget.azitabobj.mouseup)
					eEvent.currentTarget.azitabobj.mouseup(eEvent.currentTarget.azitabobj,eEvent);
				}, false);
			this.canvas[not].addEventListener("mousemove", function(eEvent) {
				if (undefined != eEvent.currentTarget.azitabobj.mousemove)
					eEvent.currentTarget.azitabobj.mousemove(eEvent.currentTarget.azitabobj,eEvent);
				}, false);
			}
		//this.draw();
		}
	,mousedown: function(that,evt){
		//var azi = that.getRpmAzi(evt);
		}
	,mouseup: function(that,evt){
		// console.log('mouseup');
		var not = evt.currentTarget.not;
		var azi = that.getRpmAzi(evt);
		tx.TRC.not = not;
		tx.TRC.rpm = azi.rpm;
		tx.TRC.azi = azi.azi;
		tx.TRC.rpmi =	that.thrusterDataC[not].rpmi;
		tx.TRC.pitch = that.thrusterDataC[not].pitch;
		tx.TRC.ptm =	that.thrusterDataC[not].ptm;
		tx.TRC.oloc =	that.thrusterDataC[not].oloc;
		tx.TRC.stat =	that.thrusterDataC[not].stat;
		ws_talk_listen.sendNMEA(ship0.remote_protocol+'://'+ship0.remote_ip+':'+settings.talk_port,buildNMEA('TRC',1,1));
		that.showAziGraphic(this.colorCommand,that.cv[not],that.canvas[not].width/2,that.canvas[not].height/2,azi,this.aziScale,10);	//	command
		}
	,mousemove: function(that,evt){
		this.predict = that.getRpmAzi(evt);
		}
	,getRpmAzi: function(evt){
		// convert mouse coordinates in canvas to rpm and azi-angle
		var rect = evt.currentTarget.getBoundingClientRect();
		var pp = {x: evt.clientX - rect.left - evt.currentTarget.width/2, y: -1*(evt.clientY - rect.top - evt.currentTarget.height/2)};
		var rpm = Math.sqrt(Math.pow(pp.x,2) + Math.pow(pp.y,2));
		rpm /= evt.currentTarget.width/2;
		rpm /= this.aziScale;
		rpm = rpm.toFixed(1);

		var azi = Math.atan(pp.x/pp.y) / this.deg2rad;
		if (pp.x<0.0)	if (azi>0.0)	azi -= 180.0;
		if (pp.x>0.0)	if (azi<0.0)	azi += 180.0;
		azi = azi.toFixed(1);
		return {rpm: rpm, azi: azi};
		}
	,draw: function(){
		if (!this.hasFocus) return;
		var aziVectors = [];
		for (var not=0; not<this.noThrusters; not++){
			this.cv[not].clearRect(0,0,this.canvas[not].width,this.canvas[not].height);

			this.cv[not].beginPath();
			this.cv[not].strokeStyle = '#000000';
			this.cv[not].moveTo(this.canvas[not].width/2,this.canvas[not].height);			this.cv[not].lineTo(this.canvas[not].width/2,0);
			this.cv[not].moveTo(this.canvas[not].width,this.canvas[not].height/2);			this.cv[not].lineTo(0,this.canvas[not].height/2);
			this.cv[not].stroke();

			var rings=[3/3];
			for (var ring in rings) {
				this.cv[not].beginPath();
				this.cv[not].arc(this.canvas[not].width/2, this.canvas[not].height/2, this.canvas[not].width/2 * rings[ring], 0, 2 * Math.PI);
				this.cv[not].stroke();
				}
			this.showAziGraphic(this.colorCommand,	this.cv[not],this.canvas[not].width/2,this.canvas[not].height/2,this.thrusterDataC[not],this.aziScale,10);	//	command
			this.showAziGraphic(this.colorRespons,	this.cv[not],this.canvas[not].width/2,this.canvas[not].height/2,this.thrusterDataD[not],this.aziScale,10);	//	response
			}
		// teken de vectoren en hun resultante
		if (1){	//	todo posities vd azis moet verteld worden
			var cv = document.getElementById('vector_resultante');
			var wh={w:cv.width,h:cv.height};
			cv = cv.getContext("2d");
			cv.clearRect(0,0,wh.w,wh.h);
			// teken kruis
			cv.beginPath();
			cv.strokeStyle = '#000000';
			cv.moveTo(wh.w/2,wh.h);			cv.lineTo(wh.w/2,0);
			cv.moveTo(wh.w,wh.h/2);			cv.lineTo(0,wh.h/2);
			cv.stroke();
			
			if (this.thrusterDataC.length){
				var rc = this.showResultingVector(cv,wh,this.colorCommand,this.thrusterDataC);	//	command
				}
			if (this.thrusterDataD.length){
				var rr = this.showResultingVector(cv,wh,this.colorRespons,this.thrusterDataD);	//	respons
				setValue('azi_res_force',rr.m);
				setValue('azi_res_azimuth',rr.a/this.deg2rad);
				}
			}
		}
	,showResultingVector(cv,wh,color,azi){
		var vp0 = {x:9,y:+2,a:azi[0].azi*this.deg2rad,m:azi[0].rpm}; 
		var vp1 = {x:9,y:-2,a:azi[1].azi*this.deg2rad,m:azi[1].rpm};
		var rp = resultante(vp0,vp1);
		cv.save();	{
			cv.translate(wh.w/2-10*rp.y,wh.h/2+5*rp.x);
			// teken aangrijpingspunt voortstuwingsresultante
			cv.beginPath();	cv.strokeStyle = cv.fillStyle = color;	cv.arc(0,0,5,0,2*Math.PI);	cv.fill();	cv.stroke();
			cv.rotate(-rp.a);	
			// teken vector voortstuwingsresultante
			// cv.beginPath();	cv.strokeStyle = this.colorRespons;	cv.moveTo(0,0);cv.lineTo(0,rp.m);	cv.stroke();
			cv.beginPath();	cv.strokeStyle = color;	cv.moveTo(0,0);cv.lineTo(0,rp.m);	cv.stroke();
			}	cv.restore();
		return rp;	
		}		
	,showAziGraphic: function(color,cv,mx,my,thrusterData,aziscale,ballsize){
		if (undefined!=thrusterData){
			// with (cv){
				cv.beginPath();
				cv.strokeStyle = cv.fillStyle = color;
				cv.arc
				( mx*(1+aziscale*thrusterData.rpm * Math.sin(thrusterData.azi * this.deg2rad))
				, my*(1-aziscale*thrusterData.rpm * Math.cos(thrusterData.azi * this.deg2rad))
				, ballsize, 0, 2 * Math.PI);
				cv.fill();
				cv.stroke();
				// }
			}
		//this.cv.drawImage(this.imgPropellor,this.canvas.width/2,this.canvas.height/2);
		}
	};
function resultante(v0,v1){
	//	vector parameters moeten x,y,a,m bevatten
	//	1 bepaal werklijnen van v0 an v1 (snijpunt y-as, rico)
	v0.rico = Math.tan(v0.a);	v0.b = v0.y-v0.rico*v0.x;
	v1.rico = Math.tan(v1.a);	v1.b = v1.y-v1.rico*v1.x;
	// console.log('a_0:'+v0.a+'a_1:'+v1.a);
	// console.log('xy_0: ('+v0.x+','+v0.y+')  xy_1: ('+v1.x+','+v1.y+')');
	//console.log('b_0:'+v0.b+'  b_1:'+v1.b);
	// console.log('rico_0:'+v0.rico+' ('+180.9/Math.PI*Math.atan(v0.rico)+') rico_1:'+v1.rico+' ('+180.9/Math.PI*Math.atan(v1.rico));
	// console.log('y='+v0.rico+'x+'+v0.b+'\ny='+v1.rico+'x+'+v1.b);
	// console.log('y='+v0.rico+'x+'+v0.b+'\ny='+v1.rico+'x+'+v1.b);
	var x=(v1.b-v0.b) / (v0.rico-v1.rico);	var y=v0.rico*x+v0.b;	//	hier grijpt de resultante aan
	// zet polair om naar cartesisch
	v0.vx = v0.m*Math.sin(v0.a); v0.vy = v0.m*Math.cos(v0.a);
	v1.vx = v1.m*Math.sin(v1.a); v1.vy = v1.m*Math.cos(v1.a);
	var xr=v0.vx+v1.vx;var yr=v0.vy+v1.vy;
	var rc = xy2pol({x:xr,y:yr});
	return {x:x,y:y,a:rc.a,m:rc.r};
	}
function xy2pol(xy){
	var rc = 	{	a:	Math.atan((xy.x) / (xy.y))
					,	r:	Math.sqrt((xy.x)*(xy.x)+(xy.y)*(xy.y))
					};
	if ((xy.y) > 0)
		rc.a = Math.PI-rc.a;
	else {
		rc.a = 2*Math.PI-rc.a;
		if (rc.a > 2*Math.PI)	rc.a -= 2*Math.PI;
		}
	return rc;
	}
