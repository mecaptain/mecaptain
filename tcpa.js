//	tcpa.js
//	je kunt dit runnen onder cscript //E:javascript tcpa.js
// bereken de cpa en tcpa tussen twee schepen waarvan positie en KVvector bekend is
//	http://researchonline.ljmu.ac.uk/5162/1/1104-CPA%20Calculation%20Method%20Based%20on%20AIS%20Position%20Prediction.pdf
//	http://dkallen.org/CPA.htm

function intercept(x,y,s0,s1,v){
	/*
	1. bepaal eindpunt U van zijn vector vanuit zijn positie
	2. U is middelpunt van een cirkel met als straal mijn vaart
	3. snijpunt P van deze cirkel met verbindingslijn tussen (0,0) en (x,y) naar U levert intercept koers bij gegeven vaart op.
	4. vanuit U loodlijn op verbindingslijn tussen (0,0) en (x,y) levert ook P op. Dit is de minimale vaart voor intercept.
	5. tijdsduur tot intercept is vaart / (afstand P-(x,y)) uren.
	}
	*/	
	}
'use strict';
function tcpa(s0,s1){
	var deg2rad = Math.PI/180.0;	//	hoeken naar radialen
	var bd0 = beardist
		({latrad: s0.lat*deg2rad, lonrad: s0.lon*deg2rad}
		,{latrad: s1.lat*deg2rad, lonrad: s1.lon*deg2rad}
		// ,pos1
		);
	
	s0.hdt *= deg2rad;
	s1.hdt *= deg2rad;
	//	bepaal x en y van s1 op middelbreedtekaart, veronderstel s0 in de oorsprong
	x = 60*(s1.lon-s0.lon)/Math.cos(s0.lat*deg2rad);
	y = 60*(s1.lat-s0.lat);

	//var ic = intercept(x,y,s0,s1);
	var bearing = Math.atan(x/y) / deg2rad;
	if (bearing<0) bearing += 360.0;
	bearing = bearing.toFixed(1);
	
	var range = Math.sqrt(Math.pow(x,2)+Math.pow(y,2));
	range = range.toFixed(2);
	
	//	log('(x,y)=('+x+','+y+')');
	// voorbereiding van cosinusregel
	var a = s1.spd;
	var b = s0.spd;
	var gamma = s0.hdt+2*Math.PI-s1.hdt;
	//	log('gamma='+gamma/deg2rad);
	//	trek vector van s1 af van s0 met de cosinusregel
	var c = a*a + b*b - 2*a*b*Math.cos(gamma);	//	zie https://nl.wikipedia.org/wiki/Cosinusregel
	c = Math.sqrt(c);
	//	log('c='+c);

	var alpha = s0.hdt+Math.acos(-(a*a-b*b-c*c) / (2*b*c));	//	zie https://nl.wikipedia.org/wiki/Cosinusregel
	//	log('alpha='+alpha/deg2rad);

	var rico = Math.tan(Math.PI/2-alpha);
	//	log('rico='+rico);
	b = y-x*rico;	//	vergelijking van relatief pad van s1 tov s0
	//	log('b='+b);
	var rc = 
		{cpa: Math.abs(b/Math.sqrt(1+rico*rico))
		, tcpa: 0
		, bearing: bearing
		, range: range
		};	//	zie http://www.davdata.nl/algebra/alg3.html#apl
	//	stel lijn op loodrecht op relatief pad
	var rico2 = -1/rico;
	// bereken snijpunt van deze lijn met vorige
	var xcpa = -b/(rico-rico2);
	var ycpa = xcpa*rico2;
	//	log('cpa(x,y)=('+xcpa+','+ycpa+')');
	// de afstand van x,y tot xcpa,ycpa gedeeld door c is het aantal uren tussen nu en tcpa
	var afstand = Math.sqrt((xcpa-x)*(xcpa-x)+(ycpa-y)*(ycpa-y));
	//	log('afstand='+afstand);
	rc.tcpa = afstand/c;
	
	return rc;
	}
function tcpa2(p0,p1){
	// cpa en tcpa op basis van twee peilingen
	//	p0 = peiling0: 
	// p0.wp	ware peiling
	// p0.t	tijd 
	// p0.d	afstand
	var gamma = p0.wp-p1.wp;	//	log ('gamma='+gamma*180/Math.PI);
	var C = Math.sqrt(p0.d*p0.d + p1.d*p1.d - 2*p0.d*p1.d*Math.cos(gamma));	//	log ('C='+C.valueOf());	//	afstand tussen waargenomen posities
	var sinbeta = Math.asin(p1.d * Math.sin(gamma) / C);	//	sinus verre hoek
	var beta = Math.asin(sinbeta);	//	log ('beta='+beta*180/Math.PI);	//	verre hoek
	var cpa = p0.d*sinbeta;	log('cpa='+cpa.valueOf());
	var E = cpa / Math.tan(beta);	log('E='+E.valueOf());
	var tcpa = E*(p1.t-p0.t)/C;	log('tcpa='+tcpa.valueOf());
	var wpcpa = p0.wp-(Math.PI/2-beta);	log('wpcpa='+wpcpa*180/Math.PI);
	return {cpa: cpa,tcpa: tcpa, bearing: wpcpa>=0? wpcpa: wpcpa+2*Math.PI};
	}
function beardist(pos0,pos1){
		var R = 6371.000; // mean radius of planet earth
		var d = Math.acos(	Math.sin( pos0.latrad ) * Math.sin( pos1.latrad )
			+	Math.cos( pos0.latrad ) * Math.cos( pos1.latrad ) * Math.cos( pos1.lonrad-pos0.lonrad ) );
		
		var b = Math.acos((Math.sin(pos0.latrad)-Math.sin(pos1.latrad)*Math.cos(d))/Math.sin(d)/Math.cos(pos1.latrad));
		if (Math.sin(pos0.lonrad-pos1.lonrad) < 0){
			b = 2*Math.PI - b;
			}
		// d *= 6000*pos0._radtodeg;	//	in nautical miles
		d *= R*180/Math.PI;	//	in nautical miles
		return {b: b, d: d};
		};
function verzeil(pos,cog,sog){
	//todo
	}	
if (0){
	function log(text){
		//WScript.stdout.WriteLine(text);	//	onder cscript
		}
	/*
	var schip0 = {hdt:  10, spd: 12, lat: 52.0, lon:4.0};	
	var schip1 = {hdt: 300, spd: 12, lat: 52.1, lon:4.1};	
	var cpa = tcpa(schip0,schip1);
	log('cpa='+cpa.cpa.toFixed(2)+'mijl, tcpa='+Math.floor(cpa.tcpa/60).toFixed(0)+':'+Math.floor((cpa.tcpa*60)%60).toFixed(0)+':'+Math.floor((cpa.tcpa*3660)%60).toFixed(0)+'uur na nu.');
	*/
	var peiling0 = {wp: 45*Math.PI/180, t:0, d:3};
	var peiling1 = {wp: 35*Math.PI/180, t:5, d:2};
	tcpa2(peiling0,peiling1);
	}