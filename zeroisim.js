//	nmea_presenter.js
// todo
// sleep nmea waarden naar instrumenten
// satellietplot
// radarplot
//	todo splice out rx and tx pages
//	todo make more plugin - modular
//	todo great hints at: http://html5-demos.appspot.com/static/gdd11-modern-web-apps/index.html#43
//
// function test() {
//	var options = getValue("options");
//	var regex = new RegExp(getValue("myregex"),options);
//	var tresultdiv = document.getElementById("tresult");
//	tresultdiv.innerHTML = regex.test(getValue("myinput"));
// }
'use strict';
function dissect() {
	var nmeastring = getValue("nmea") + "\n";
	var mresultdiv = document.getElementById("grr");
	mresultdiv.innerHTML = nmeastring;
	tablelize2(nmeastring,undefined,function(evt){
		mresultdiv.innerHTML = evt;
		});
	}
function nmea_sentence_received(data,that){
	talker_ids = that.talkerids;
	//	ship0 = that.ship0;	todo ik denk dat dit niet goed is, er is maar een globale ship0 tot nu toe
	var date = new Date();  // todo veel andere Date() 's kunnen nu weg
	tablelize2(data,date,function(evt){
		if (undefined!=evt.sentence && undefined!=nmeadefs[evt.sentence]){

			if (undefined==talker_ids[evt.sentence]){		//	add talker id
				for(var first in rx[evt.sentence]) break;	//	take first talker-id if received sentence
				talker_ids[evt.sentence] = first;
				}

			if (undefined!=nmeadefs[evt.sentence][0].rxcb){	//	kijk of er een geregistreerde callback voor deze sentence is
				nmeadefs[evt.sentence][0].rxcb.myforEach(function(cb,ii,ar,p){	//	roep iedere geregistreerde callback aan
					cb.fun(
						{key:p.evt.sentence, data:rx[p.evt.sentence][talker_ids[p.evt.sentence]],date: date,raw:rx[p.evt.sentence]['raw']}
							,cb.payload
						)},
					{evt:evt})
				}

			if (undefined!=presentNMEA) {	//	alleen als rx-tab is geladen
				presentNMEA(evt.sentence);
				}


			if (0==count_rx){	//	alleen doen na ontvangst eerste data
				var r = new RegExp('^(.+):\\/\\/(\\d+\\.\\d+\\.\\d+\\.\\d+):\\d+$','i');	//	must parse for example udp://192.168.1.13:8033
				if (r.test(rx[evt.sentence][talker_ids[evt.sentence]].from)){					// determine ip-address of ship or simulation
					var adres = r.exec(rx[evt.sentence][talker_ids[evt.sentence]].from);
					ship0.remote_protocol = adres[1];
					ship0.remote_ip = adres[2];
					}
				}
			}
		});
	count_rx++;
	}
function getQRCode(tekst,size) {
	console.log('getQRCode...');
	setValue('qrcodetxt',tekst);
	var tmpws = new wsconnect(settings.ws_addr
		,'qrcode'						//	connect to websocketserver 'qrcode' service
		,function(ws){					//	connected
			ws.zend(JSON.stringify({url:tekst,sz:size}));
			}
		,function(ws,data,that){	//	data received, data is de qrc
			setValue('qrcodepng',h.img('','src="'+data+'"'));
			}
		,function(evclose,thatws){	//	disconnected
			thatws = undefined;
			}
		);
	}
function wsStartListenToNmeaShip0(ws_addr,listen_addr,ws_verbose){		//	todo should be a C0isimTab
	console.log('wsStartListenToNmeaShip0...');
	// open connection to socketserver and he will give you the nmea coming from settings.listen_addr
	ws_talk_listen = new wsconnect(ws_addr,'nmea-talk-listen'
		,function(ws){
			console.log('WebSocket protocol="nmea-talk-listen" connected to: '+ ws_addr);
			tab_settings.showConnected(true);
			ws.zend(JSON.stringify(
				{listen_addr:			listen_addr
				,verbose:				ws_verbose
				}));
			count_tx++;
			}
		,function(ws,data,that){
			data = JSON.parse(data);
			return nmea_sentence_received(data,that);
			}
		,function(/*ws*/){
			if (log) log.info('disconnected');
			tab_settings.showConnected(false);
			}
		,{		talkerids: talker_ids
			,	ship0: ship0
			}
		);
	}
function wsStartListenToNmeaShip0_new(ws_addr,listen_addr,ws_verbose){		//	todo should be a C0isimTab
	console.log('wsStartListenToNmeaShip0...');
	ws_talk_listen = new WebSocket(ws_addr,'nmea-talk-listen');
	ws_talk_listen.onmessage = function(ws,data,that){
		data = JSON.parse(data);
		return nmea_sentence_received(data,that);
		}.bind(this);
    ws_talk_listen.onclose = function (evt) {
        if (log) log.info('disconnected');
        tab_settings.showConnected(false);
    };
    ws_talk_listen.onerror = function (evt) {
        if (log) log.info('error connecting to websocketserver');
        tab_settings.showConnected(false);
    };
	ws_talk_listen.onopen = function(evt){
		var ws = evt.target;
		console.log('WebSocket protocol="'+ws.protocol+'" connected to: '+ ws.url);
		// open connection to socketserver and he will give you the nmea coming from settings.listen_addr
		tab_settings.showConnected(true);
		ws.send(JSON.stringify(
			{listen_addr:			listen_addr
			,verbose:				ws_verbose
			}));
		count_tx++;
		}.bind(this);
	}
function wsNmeaBridge(parm,ship){		//	todo should be a C0isimTab
	console.log('wsNmeaBridge(world='+parm.world+')');
	// open connection to socketserver and he will give you the nmea coming from settings.listen_addr
	ws_nmea_bridge = new wsconnect(parm.ws_addr,'nmea-bridge'
		,function(ws){
			console.log('WebSocket protocol="nmea-bridge" connected to: '+ parm.ws_addr);
			tab_settings.showConnected(true);
			ws.zend(JSON.stringify(parm));
			count_tx++;
			}
		,function(ws,data,that){	//	data received
			data = JSON.parse(data);
			if (undefined!=data.msg) {
				return nmea_sentence_received(data,that);
				}
			if (undefined!=data.MMSI) {
				that.theship.IMO=that.theship.mmsi=data.MMSI;
				}
			if (undefined!=data.CallSign) {
				that.theship.CallSign=data.CallSign;
				}
			}
		,function(/*ws*/){	//	disconnected
			if (log) log.info('disconnected');
			tab_settings.showConnected(false);
			}
		,{		talkerids: talker_ids
			,	theship: ship
			}
		);
	}

function handleFileSelect(evt){	//	todo should be a C0isimTab
//	if (log) log.info('handleFileSelect()');
	var files = evt.target.files; // FileList object
	readTextFiles(files);
	}
function readTextFiles(files){	//	todo file funktie in aparte tab
	// files is a FileList of File objects. List some properties.
	var output = [];
	for (var ii=0, f; f=files[ii]; ii++) {
		if (log) log.info('handleFileSelect('+f.name+', '+f.lastModifiedDate+')');
		//	break;
		var reader = new FileReader();
		reader.onload = (function(/*theFile*/) {
			return function(e) {
			//	splits in regels
				var sentences = e.target.result.split(/\r\n/);
				var test = settings.file_processing_time;
				switch (settings.file_processing_time){
					case 'afap':
						for (var ii=0, sentence; sentence=sentences[ii]; ii++){
							nmea_sentence_received(sentence
													,	{	grr: document.getElementById("grr")
														,	talkerids: talker_ids
														,	ship0: ship0
														});
							}
						break;
					case 't10':
						var filetimer = window.setInterval
							(function(sentences){
								sentence=sentences.shift();
								if (undefined==sentence) {
									clearInterval(filetimer);
									return;
									}
								nmea_sentence_received(sentence
													,	{	grr: document.getElementById("grr")
														,	talkerids: talker_ids
														,	ship0: ship0
														});
								}
							, 300, sentences);
						break;
					default:
						alert('Unsupported fileprocessing timing-mode: "'+settings.file_processing_time+'"');
						break;
					}
				};
			})(f);

		// Read in the image file as text.
		reader.readAsText(f);
		}
	//document.getElementById('filelist').innerHTML = '<ul>' + output.join('') + '</ul>';
	}
// globals
var lasttime = 0;
var rx = {};	//	namespace for all received nmea variables
var tx = {};	//	namespace for all nmea variables prone to transmission
var log = false;	//	log4javascript.getDefaultLogger();
// obsolete var ws = undefined;	//	WebSocket
var ws_talk_listen = undefined;	//	WebSocket
var ws_nmea_bridge = undefined;	//	WebSocket
var ws_loopback = undefined;		//	fake WebSocket
var count_rx = 0;
var count_tx = 0;
var talker_ids = {VTG: undefined, ROT: undefined, THS: undefined, HDT: undefined, RSA: undefined, VBW: undefined };	//	global
//	todo schip0 moet van het type CShip zijn, zie ships.js
var ship0 = undefined;	//	create when everything is loaded
// var kompas = undefined;
// var posplot = undefined;
var graphic = undefined;
// var mfi = undefined;	//	multifunctional instrument
// var dopplerlog = undefined;	//	doppler log
//var autopilot = undefined;
var plotlist = [];	//	list of variables that must be on the graphic plot
var sepwdw = {};	//	separate windows containing instruments and such
var presentNMEA = undefined;	//	maybe overruled by rxtx_tab.js
var mso = {};	//	maybe global? Yes, so simulator and tab_ocean can reach me.
startLoadingScripts();	//	dynamically load scripts
var aislist = [];

function check(obj,cb){	//	todo maak een checkbox a la onoffswitch, dan wordt deze kode veel eenvoudiger, doe dit in tab_settings
	switch (obj.id){
		case 'chinput':				settings.check_checksum = obj.checked;	break;
		case 'chpatt':					settings.check_pattern = obj.checked;	break;
		case 'chrxnmea_tojs':		settings.rxnmea_tojs = obj.checked;	break;
		case 'talkerid_tojs':		settings.talkerid_tojs = obj.checked;	break;
		case 'showraw':				settings.showraw = obj.checked;	break;
		case 'decode_6bit':			settings.decode_6bit = obj.checked;	break;
		case 'rudder0_on_ap_off':	settings.rudder0_on_ap_off = obj.checked;	break;
		default:
			if (undefined!=cb)	cb(obj);	//	this enables some object orientation
			settings[obj.id] = obj.checked;
			break;
		}
	}
wsconnect.prototype.sendNMEA = function (toaddr, message) {
    if (undefined == ws_talk_listen) {	// todo not sure if this should be ws_talk_listen or ws_nmea_bridge
        return 'no connection with WebSocket server...';
    }
    else {
        this.zend(JSON.stringify(
            {
                send_addr: toaddr
                , message: message
            }));
        count_tx++;
        return 'msg sent';
    }
}
function callscript(text){
	print('callscript('+text+')');
	}
function change_settings(setting,newvalue){
	settings[setting] = newvalue;
	if (setting=='aisgraph_diameter'){	//	todo dit werkt niet natuurlijk
		setValue('ais',newvalue,'style.width');
		setValue('ais',newvalue,'style.height');
		}
	}
function defd(variable,undefd){
	if (undefined==variable)
		return undefd;	//	return '';
	else
		return variable;
	}
function buildNMEA(key,bWithChecksum,bWithNewline){
	var nmeadef = nmeadefs[key];
	var rc;
	rc = nmeadef.myforEach(function(el,jj,ar,p){
		if (jj==0){
			p.rc = ['$' + defd(tx[p.key].talker,'--') + p.key];
			}
		else if (jj==1) {}	//	skip talker as we did it at jj=0
		else if (jj==ar.length-2){	//	checksum, laatste veld is timeRX
			p.rc = p.rc.join(',');
			if (undefined!=p.bWithChecksum){
				p.rc += '*'+nmeaChecksum(p.rc);
				}
			if (undefined!=p.bWithNewline){
				// p.rc += '\n';
				p.rc += '\r\n';
				}
			return p.rc;
			}
		else if (jj>ar.length-2){	//	voorbij de nmea definitie
			return p.rc;
			}
		else{
			p.rc.push(defd(tx[p.key][nmeadef[jj][0]],''));
			}
		},{rc:rc, key:key,bWithChecksum:bWithChecksum,bWithNewline:bWithNewline});
	//console.log('buildNMEA: '+rc);
	return rc;
	}
function handleslide(that,div_slideroutput,cb){
	if (undefined!=div_slideroutput)	setValue(div_slideroutput,that.value);
	if (undefined!=cb)					cb(that);
	}
function calcCheckSum(key){
	var sentence = buildNMEA(key);
	return nmeaChecksum(sentence);
	}
function getJson(key){
	return 'todo not yet implemented';
	}
function copyRX(key,that){
	if (undefined!=talker_ids[key]){
		if (undefined!=rx[key][talker_ids[key]]){
			var src = rx[key][talker_ids[key]];
			for (var ff in nmeadefs[key]) {
				if (ff>0){
					var field = nmeadefs[key][ff][0];
					var value = rx[key][talker_ids[key]][field];
					setValue('tx.'+key+'.'+field,value,'value');
					var txclass = {id: 'tx.'+key+'.'+field,value: value,className: 'check_input_nok'};
					changeTXvalue(txclass,key,ff);
					setValue(txclass.id,txclass.className,'class');
					}
				}
			}
		}
	// check all inputs
	return;
	}
function changeTXvalue(that,key,field){
	var fields = that.id.split('.');
	tx[fields[1]][fields[2]] = that.value;	//	add this nmea sentence to js variable space
	if (0==field)
		var patt = '^..$';
	else if (field==-1){
		var patt = '^[A-F0-9][A-F0-9]$';
		}
	else
		var patt = nmeadefs[key][field][1];
	var regex = new RegExp(patt,'');
	if (regex.test(that.value)){
		that.className = 'check_input_ok';
		}
	else{
		that.className = 'check_input_nok';
		}
	}
function setTXDefaultsOld(){
	for (var key in nmeadefs){
		for (var field in nmeadefs[key]) {
			if (field>0){
				if (undefined!=nmeadefs[key][field][3]){
					tx[key][nmeadefs[key][field][0]] = nmeadefs[key][field][3];
					}
				}
			}
		}
	}
function setTXDefaults(){
	for (var key in nmeadefs){
		if (settings.chrxnmea_tojs)	tx[key] = {};	//	add this nmea sentence to js variable space
		for (var field in nmeadefs[key]) {
			if (settings.chrxnmea_tojs)	tx[key][field] = '';	//	add this nmea sentence to js variable space
			if (field>0){
				if (undefined!=nmeadefs[key][field][3]){

					var txclass = {id: 'tx.'+key+'.'+field,value: tx[key][field],className: 'check_input_nok'};
					changeTXvalue(txclass,key,field);

					tx[key][nmeadefs[key][field][0]] = nmeadefs[key][field][3];
					}
				}
			}
		}
	}
function setDefaults(key){
	for (var field in nmeadefs[key]) {
		if (undefined!=nmeadefs[key][field][3]){
			if (0==field){
				setValue('tx.'+key+'.talker',nmeadefs[key][field][3],'value');	//	todo ooit nmeadefs veranderen zodat het eerste veld talker heet
				}
			else {
				setValue('tx.'+key+'.'+nmeadefs[key][field][0],nmeadefs[key][field][3],'value');
				}
			}
		}
	}
function rudder(angleP,angleS){	//	todo move to pilo
	if (undefined==ws_talk_listen)	return;
	var adrs = ship0.remote_protocol+'://'+ship0.remote_ip+':'+settings.talk_port;
	if (log) log.info('rudderangle starboard: '+angleS+'; port: '+angleP);
	if (false && tabmask & settings.tabmasks.show_azi) {
		var nots=[0,1];	//	support for azi-ships, assume for the time being not=[0,1]
		tx.ROR.srud = angleS;	tx.ROR.prud = angleP;
		tx.TRC.rpm = undefined;
		tx.TRC.rpmi = undefined;
		tx.TRC.pitch = undefined;
		tx.TRC.ptm = undefined;
		tx.TRC.oloc =	'B';
		tx.TRC.stat =	'C';	//	kweet niet waarom C, Nautis wil dit
		var trc0,trc1;
		// for (var not=0; not<2; not++){
		for (var not in nots){
			tx.TRC.not = not;
			switch (not){
				case 0:	tx.TRC.azi = -angleP;	trc0 = buildNMEA('TRC',1,1);	break;	// portside azi
				case 1:	tx.TRC.azi = -angleS;	trc1 = buildNMEA('TRC',1,1);	break;	// starboardside azi
				}
			}
		ws_talk_listen.throttled_sendNMEA(adrs,trc0);
		ws_talk_listen.throttled_sendNMEA(adrs,trc1);
		}
	tx.ROR.prud = ship0.ror.p = angleP;	tx.ROR.pvalid = 'A';	//	port rudder
	tx.ROR.srud = ship0.ror.s = angleS;	tx.ROR.svalid = 'A';	//	starboard rudder
	tx.ROR.src = 'S';	//	simulator
	// ws_talk_listen.zendNMEA(adrs,buildNMEA('ROR',1,1));
	ws_talk_listen.throttled_sendNMEA(adrs,buildNMEA('ROR',1,1));
	}
function telegraph(shaft0,shaft1){	//	todo move to pilo
	if (log) log.info('telegraph shaft0: '+shaft0+'; shaft1: '+shaft1);
	if (1){
		tx.PRC.rdval = tx.PRC.pdval = 1.0*shaft0;
		tx.PRC.engshft = 0;
		var nmea_shaft0 = buildNMEA('PRC',1,1);
		tx.PRC.rdval = tx.PRC.pdval = 1.0*shaft1;
		tx.PRC.engshft = 1;
		//	todo ws_talk_listen moet parameter zijn, niet meer global
		ws_talk_listen.sendNMEA(ship0.remote_protocol+'://'+ship0.remote_ip+':'+settings.talk_port,nmea_shaft0+buildNMEA('PRC',1,1));
		}
	else {
		tx.TRC.not	=	0;	//',		integer(),		'Number of thruster (thruster index)']
		tx.TRC.rpm	=	shaft0;	//',		floatingp,		'RPM demand value (percentage)']
		tx.TRC.rpmi	=	'P';	//',		literal('P'),	'RPM mode indicator (indicates percentage)']
		tx.TRC.pitch =	shaft0;	//',		floatingp,		'Pitch demand value (percentage)']
		tx.TRC.ptm	=	'P';	//',		literal('P'),	'Pitch mode indicator (indicates percentage)']
		tx.TRC.azi	=	undefined;	//',		floatingp,		'Azimuth demand (0-360, follow-up)']
		tx.TRC.oloc	=	'B';	//',		literal('.'),	'Operating location indicator']
		tx.TRC.stat	=	'A';	//',		literal('A|V'),'Sentence status flag']
		ws_talk_listen.sendNMEA(ship0.remote_protocol+'://'+ship0.remote_ip+':'+settings.talk_port,buildNMEA('TRC',1,1));
		}
	}
function azithruster(data){	//	todo move to pilo
	// if (tabmask & settings.tabmasks.show_azi) {
	if (log) log.info('azithruster');
	tx.TRC.not	= data.not;		//	integer(),		'Number of thruster (thruster index)']
	tx.TRC.rpm	= data.rpm;		//	floatingp,		'RPM demand value (percentage)']
	tx.TRC.rpmi	= data.rpmi;	//	literal('P'),	'RPM mode indicator (indicates percentage)']
	tx.TRC.pitch= data.pitch;	//	floatingp,		'Pitch demand value (percentage)']
	tx.TRC.ptm	= data.ptm;		//	literal('P'),	'Pitch mode indicator (indicates percentage)']
	tx.TRC.azi	= data.azi;		//	floatingp,		'Azimuth demand (0-360, follow-up)']
	tx.TRC.oloc	= data.oloc;	//	literal('.'),	'Operating location indicator']
	tx.TRC.stat	= data.stat;	//	literal('A|V'),'Sentence status flag']
	//	todo ws_talk_listen moet parameter zijn, niet meer global
	ws_talk_listen.sendNMEA(ship0.remote_protocol+'://'+ship0.remote_ip+':'+settings.talk_port,buildNMEA('TRC',1,1));
	}
function file_list(that){
	var wsfilelist = new wsconnect(settings.ws_addr,'file_list'
		,function(ws){
			if (log) log.info('WebSocket file_list connected');
			ws.zend('*.*');
			count_tx++;
			}
		,function(ws,data,mresultdiv){	//	rx data, show a filedialog
			var filelist = JSON.parse(data);
			var url = '';
			for (var ii in filelist){
				url += h.tr(h.td(h.div(filelist[ii],'onclick=this.returnValue=this.innerText')));
				}
			if (0){
			url = 'data:text/html,'
				+	h.title('Pick a file')
				+	h.style('* {font-family:courier;font-size:small;}')
				+	h.table(url)
				;
			window.open(url,
				'popUpWindow',
				'height=400,     \
				width=650,      \
				left=300,       \
				top=100,        \
				resizable=yes,  \
				scrollbars=yes, \
				toolbar=yes,    \
				menubar=no,     \
				location=no,    \
				directories=no, \
				status=no');
				}
			else {
				//	add dialog to document
				var dialog = document.createElement('DIALOG');
				dialog.id = 'filepicker';
				dialog.innerHTML = h.table(url);
				dialog.onclose = function(){alert('modal dialog closed, retval='+dialog.returnValue);}
				dialog.returnValue = '';
				document.body.appendChild(dialog);
				dialog.showModal();
				}
			}
		,function(ws){
			if (log) log.info('file_list disconnected');
			}
		);
	}
function isFunction(functionToCheck) {
	var getType = {};
	return functionToCheck && getType.toString.call(functionToCheck) === '[object Function]';
}

function buildTabs(tabses){
	var tabhdrs = '';
	var tabpages = '';
	var tabs_above = document.createElement('div');	tabs_above.id = 'tabs_above';	//document.body.appendChild(tabs_above);
	tabs_above.focusfuns = [];
	tabs_above.blurfuns = [];
	var tabbook = [];
	Object.keys(tabses).myforEach(function(tabname,ii,ar,p){
		var tab = tabses[tabname];
		var hdr = tabname;
		var content;
		if (undefined==tab.html){
            content = h.div(tab,'class="tababove" id="tababove-'+tabname+'"');
			}
		else {
			if (isFunction(tab.html)){
				content = h.div(tab.html('tababove-'+tabname),'class="tababove" id="tababove-'+tabname+'"');
				}
			else{
                content = h.div(tab.html,'class="tababove" id="tababove-'+tabname+'"');
				}
			}
		tabbook.push(
			{hdr:hdr
			,content:content
			,onfocus:(tab.onfocus)?tab.onfocus.bind(tab):undefined
			,onblur:(tab.onblur)?tab.onblur.bind(tab):undefined
			});
		});
	var tabbookContainer = document.body.appendChild(new CTabBook('zimtabs'
		,tabbook
        , { classContainer: 'container', classHeader: 'tabheader', classContent: 'tabcontent', headerDivname: 'tabheaderContainer' }).node);
        //wim , { classContainer: 'container', classHeader: 'tabheader', classContent: 'tabcontent' }).node);
      
}

function startLoadingScripts() {	//	dynamically load scripts
	var tabmask = parseInt(zeroisim.gup('tabmask')); 
	var ls = new CLoadScript(/*allScriptsFinishedLoading_new,burp_tabses*/);
	ls.loadStyle("./incl/leaflet/leaflet.css");	//	todo move under .show_ecdis
	ls.loadScripts({scripts:	
		['./incl/underscore_dev.js'
		,'./browserdetect.js'
		,'./latlon.js'
		,'./route.js'
		,'./settings.js'
		,'./gizmo.js'
		,'./nmea_defs.js'
		,'./nmea_parser.js'
		,'./nmea_ctor.js'
		,'./ais_codec.js'
		,'./incl/regexp-polyfill.js'	//	https://npm.runkit.com/regexp-polyfill/regexp-polyfill.js moz en edge ondersteunen geen regexp groups
		]
		,cb:function(parms){	//	when all scripts are loaded, expand tabbook
			settings.conning = zeroisim.gup('conning',settings.conning);
			for (var ii in nmeadefs) {	//	initialize all nmeadefs to have a talker	//	todo myforEach
				nmeadefs[ii].splice(1,0,['talker','^..$','talker identification','GP']);
				nmeadefs[ii].push(['checksum','^..$','checksum','FF']);
				nmeadefs[ii].push(['timeRX','^..$','timeRX','12:34:56']);
				}
			var scripts_to_load = [];
			if (tabmask & settings.tabmasks.show_rxtx)		scripts_to_load.push('./rxtx_tab.js');
			if (tabmask & settings.tabmasks.show_settings)	scripts_to_load.push('./tab_settings.js');
			if (tabmask & settings.tabmasks.show_simulator)	scripts_to_load = scripts_to_load.concat(['./ships.js','./intern_simulator.js']);	//	todo make a array.pushArray()
			if (tabmask & settings.tabmasks.show_ecdis)		scripts_to_load.push('./posplot_leaflet.js');
			if (tabmask & settings.tabmasks.show_radar)		scripts_to_load.push('./tab_radar.js');
			if (tabmask & settings.tabmasks.show_geoloc)		scripts_to_load.push('./geolocation.js');
			if (tabmask & settings.tabmasks.show_azi)			scripts_to_load.push('./azi.js');
			if (tabmask & settings.tabmasks.show_view)		scripts_to_load.push('./tab_ocean3.js');
			if (tabmask & settings.tabmasks.show_script)		scripts_to_load.push('./script.js');
			if (tabmask & settings.tabmasks.show_ais)			scripts_to_load = scripts_to_load.concat(['./aisplot.js','./tcpa.js','./aiscpamatrix.js','./aistable.js']);
			if (tabmask & settings.tabmasks.show_graph)		scripts_to_load = scripts_to_load.concat(['./incl/dygraph-combined.js','./graph.js']);
			if (tabmask & settings.tabmasks.show_usbcntrl)	scripts_to_load = scripts_to_load.concat(['./gamepad.js','./tab_usbcontrols.js']);
			if (tabmask & settings.tabmasks.show_fitcurv)	scripts_to_load = scripts_to_load.concat(['./incl/numeric.js','./tab_interpolate.js']);
			if (tabmask & settings.tabmasks.show_flags)		scripts_to_load.push('./tab_flags.js');
			if (tabmask & settings.tabmasks.show_astro)		scripts_to_load.push('./tab_astro.js');
			if (tabmask & settings.tabmasks.show_plotlist)	scripts_to_load.push('./tab_plotlist.js');
			if (tabmask & settings.tabmasks.show_vhf)			scripts_to_load.push('./vhf.js');
			if (tabmask & settings.tabmasks.show_createghosts)	scripts_to_load.push('./tab_createghosts.js');
			if (tabmask & settings.tabmasks.show_trainer)	scripts_to_load.push('./tab_trainer.js');
			if (tabmask & settings.tabmasks.show_controlif)	scripts_to_load.push('./control.js');
			if (tabmask & settings.tabmasks.show_env)			scripts_to_load.push('./environment.js');
			if (tabmask & settings.tabmasks.show_kompas)		scripts_to_load.push('./tab_kompas.js');
			if (tabmask & settings.tabmasks.show_conningdesign){
				var scripts = 
					['./incl/split.js'
					,'./cinstrument.js'
					,'./ipilotHorizontal.js'	//	needed for ipilotRudder and ipilotThruster
					,'./conningDesigner.js'
					,'./navmarks.js'			//	maybe move to websocket server
					];
				settings.instrumentList.myforEach(function(instr,ii,ar,scripts){
					scripts.push(instr.file);
					},scripts);
				scripts_to_load = scripts_to_load.concat(scripts);
				}
			if (tabmask & settings.tabmasks.show_pilot)		scripts_to_load.push('./pilot.js');
				
			ls.loadScripts(	//	when all scripts are loaded, expand tabbook
				{tabs:undefined
				,settings:settings
				,scripts:scripts_to_load
				,cb:function(parms){
					var burp_tabses = new Object();

					if (tabmask & settings.tabmasks.show_rxtx)			buildTXtabs(burp_tabses);
					if (tabmask & settings.tabmasks.show_settings)		burp_tabses.settings = tab_settings;
					if (tabmask & settings.tabmasks.show_simulator)		burp_tabses.simulator = intern_simulator;
					if (tabmask & settings.tabmasks.show_ecdis)			burp_tabses.posplot = posplotobj;
					if (tabmask & settings.tabmasks.show_radar)			burp_tabses.radar = radartabobj;
					if (tabmask & settings.tabmasks.show_geoloc)			burp_tabses.geoloc = geoloctabobj;
					if (tabmask & settings.tabmasks.show_azi)				burp_tabses.azi = azitabobj;
					if (tabmask & settings.tabmasks.show_script)			burp_tabses.script = script_tab_obj;
					if (tabmask & settings.tabmasks.show_ais)				burp_tabses.ais = aisplot_tab_page;
					if (tabmask & settings.tabmasks.show_graph)			burp_tabses.graph = {	//	todo see tab_example
																							html:graph_tab_page(),
																							onfocus:function(){graph_tab_onfocus()},
																							onblur:function(){graph_tab_onblur()}
																							};
					if (tabmask & settings.tabmasks.show_usbcntrl)		burp_tabses.usb_controls = tabobj_usbcontrols;
					if (tabmask & settings.tabmasks.show_fitcurv)		burp_tabses.interpolate = interpolatetabobj;
					if (tabmask & settings.tabmasks.show_flags)			burp_tabses.flags = flagstabobj;
					if (tabmask & settings.tabmasks.show_astro)			burp_tabses.astro = astrotabobj;
					if (tabmask & settings.tabmasks.show_plotlist)		burp_tabses.plotlist = plotlist;
					if (tabmask & settings.tabmasks.show_vhf)				burp_tabses.vhf = tabvhf;
					if (tabmask & settings.tabmasks.show_createghosts)	burp_tabses.ghosts = tabghosts;
					if (tabmask & settings.tabmasks.show_view)			burp_tabses.ocean = new COcean();
					if (tabmask & settings.tabmasks.show_trainer)		burp_tabses.trainer = new CTabTeacher();
					if (tabmask & settings.tabmasks.show_controlif)		wscontrol = new CControl();	//	wscontrol is a global defined in control.js
					if (tabmask & settings.tabmasks.show_env)				burp_tabses.env = new CEnvironment(settings);
					if (tabmask & settings.tabmasks.show_kompas)			burp_tabses.kompas = new CTabCompass();
					if (tabmask & settings.tabmasks.show_conningdesign)burp_tabses.ConningDesigner = new CTabConningDesigner(settings);
					if (tabmask & settings.tabmasks.show_pilot)			burp_tabses.pilot = new Cpilot_tab_page;			
					console.log('startLoadingScripts ready');
					
					var bootscript = zeroisim.gup('script',false);	//	todo find a better place to do this, ui gets messed up
					if (false!=bootscript){
						console.log('bootscript='+bootscript);
						ls.loadScripts({tabs:burp_tabses,scripts:JSON.parse(bootscript)});
						}
					
					buildTabs(burp_tabses);
					setTXDefaults();
					}
				});
		}});
	}
function bodyloaded() {
	console.log('zeroisim.js bodyloaded()');
	//	nu is het DOM aanwezig en kunnen verdere initialisaties gedaan worden.
	//	todo ik denk dat dit moet zijn: ship0 = new CShip('unnamed');
	ship0 = 
		{hdt:0.0
		,cogT:0.0
		,sog:0.0
		,rot:0.0
		,latlon:new zeroisim.LatLon()
		,ror:{p:0,s:0}
		,rsa:{p:0,s:0}
		,eng:[{pitch:0,rpm:0}]
		,mmsi:undefined
		,cmdTelegraph:0.0};
	
	cbRxNmea('RMC',function(rxd){	//	install callback for cog
		if (undefined==rxd.data)	return;
		var cog = parseFloat(rxd.data.cog);		if (undefined!=cog) ship0.cog = cog;
		var sog = parseFloat(rxd.data.sog);		if (undefined!=sog) ship0.sog = sog;
		});
	cbRxNmea('VTG',function(rxd){	//	install callback for cog
		if (undefined==rxd.data)	return;
		var cog = parseFloat(rxd.data.cogT);	if (undefined!=cog) ship0.cog = cog;
		var sog = parseFloat(rxd.data.sogN);	if (undefined!=sog) ship0.sog = sog;
		if (0 && settings.graph_on){
			// update graph
			var x = new Date();  // current time todo deze uit nmea ZDA halen
			graphicdata.push([x, ship0.sog]);
			graphic.updateOptions({ 'file': graphicdata });
			}
		});
	cbRxNmea('ROT',function(rxd){	//	install callback for rot
		if (undefined==rxd.data)	return;
		var rot = parseFloat(rxd.data.rot);	//
		if (undefined!=rot) ship0.rot = rot;

		if (0 && settings.graph_on){
			// update graph
			var x = new Date();  // current time todo deze uit nmea ZDA halen
			graphicdata.push([x, ship0.rot]);
			graphic.updateOptions({ 'file': graphicdata });
			}
		});
	cbRxNmea('RSA',function(rx){	//	rudder sensor angle
		if (undefined==rx.data)	return;
		ship0.rsa.s = rx.data.srud;
		ship0.rsa.p = rx.data.prud;
		});
	settings.listen_addr=zeroisim.gup('listen_addr',settings.listen_addr);
	settings.talk_port=zeroisim.gup('talk_port',settings.listen_addr);
	settings.ws_addr=zeroisim.gup('ws_addr',settings.ws_addr);
	console.log('settings.listen_addr='+settings.listen_addr);
	console.log('settings.talk_port='+settings.talk_port);
	console.log('settings.ws_addr='+settings.ws_addr);
	// setValue('inp_ws_addr',settings.ws_addr);
	// setValue('talkaddr',settings.listen_addr);
	
	if ('1'==zeroisim.gup('connect','0'))	wsStartListenToNmeaShip0(settings.ws_addr,settings.listen_addr,settings.ws_verbose);
	if ('qr' == zeroisim.gup('qrcode')) {
		getQRCode(window.location.href,7);
		}
	}
function DomFullyLoaded(){	//	$( document ).ready( handler ) see https://api.jquery.com/ready/
	console.log('DomFullyLoaded');
	document.body.onclick = function (e) {
		var isRightMB;
		e = e || window.event;

		if ("which" in e)  // Gecko (Firefox), WebKit (Safari/Chrome) & Opera
			isRightMB = e.which == 3;
		else if ("button" in e)  // IE, Opera
			isRightMB = e.button == 2;

		if (isRightMB)
			alert("Right mouse button " + (isRightMB ? "" : " was not") + "clicked!");
		}
	}
