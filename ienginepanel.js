//	ienginepanel.js
'use strict';

class CInstrument_engines extends CInstrument {
	constructor(obj,exposedParms,active,idx){
		// console.log('CInstrument_engines::constructor');
		super(idx,obj);
		var canvas = obj.querySelector("canvas");
		this.cv = canvas.getContext("2d")
		this.midden = {x: canvas.width/2, y:canvas.height/2};
		this.cv.textAlign ="center";
		if (undefined!=exposedParms)
			this.exposedParms = exposedParms;
		else
			this.exposedParms = //	todo deze moeten door de conning designer kunnen worden gezet, nier overrulen hier dan
				{colorBackground:			'rgba(255,255,255,0.0)'	//	gray
				,colorText:					'rgba(0,0,0,0.9)'	//	black
				,font:						'20px sans-serif'
				,offsetTextVertical:		-40
				,spacingTextVertical:	25
				};
		this.ship0 = {eng:[]};		
		super.cbRxNmea('TRD',function(rx,that){	//	todo also TRC
			if (undefined==rx.data)	return;
			var not = rx.data.not;	//	number of thruster
			that.ship0.eng[not] = undefined;	//	is dit nodig tegen geheugenlek?
			that.ship0.eng[not] = {rpm:rx.data.rpm, pitch:rx.data.pitch};	//	todo uitlaattemp, inlaattemp, oliedruk, ...
			that.draw(that.ship0);
			},this);
		//super.cbRxNmea('RPM',function(rx){	
		}
	resize(w,h){
		}
	draw(ship) {
		// if (!hasFocus)	return;
		this.cv.clearRect(0,0,2*this.midden.x,2*this.midden.y);
		this.cv.save();
		this.cv.fillStyle = this.exposedParms.colorBackground;
		this.cv.fillRect(0,0,2*this.midden.x,2*this.midden.y);
		this.cv.translate(this.midden.x,this.midden.y);
		this.cv.fillStyle = this.exposedParms.colorText;
		this.cv.font = this.exposedParms.font;
		this.YY = -this.exposedParms.spacingTextVertical;
		ship.eng.myforEach
			(function(engine,nengine,engines,p){
				if (undefined!=engine)	p.that.cv.fillText(nengine+': rpm='+engine.rpm+' pitch='+engine.pitch,0,p.that.YY);		
				p.that.YY+=p.that.exposedParms.spacingTextVertical;
				}
			,{ship:ship,that:this}
			);
		this.cv.restore();
		}
	};
CInstrument.addInstrumentToSettings('./ienginepanel.js',			'engines',		'canvas',	CInstrument_engines,		'shows commanded and actual propellor rpm and pitch');
	