//	websocketloader.js
//	original fileloader in three.js and three.min.js cannot work cross-domain or file-domain (because based on xmlHttp)
//	this loader get file from websocket server that contains simple filegetter

'use strict';

(function(zeroisim, THREE){
	zeroisim.ColladaLoader = function(manager){	//	misschien beter om dit Collada & Texture-loader te noemen
		var loader = new THREE.ColladaLoader;
		this.parse = loader.parse;
		this.manager = manager;
		this.idle = true;
		this.queue = [];
		// console.log('zeroisim.ColladaLoader');
		}
	zeroisim.ColladaLoader.prototype.manager = null;
	zeroisim.ColladaLoader.prototype.setWSip = function(value){
		this.wsip = this.crossOrigin = value;
		}
	zeroisim.ColladaLoader.prototype.load = function(url, callback, texturePath, payload){
		// console.log('zeroisim.ColladaLoader.load(\''+url+'\')');
		this.queue.push({url:url, cb:callback, tp:texturePath, pl:payload});
		if (this.idle){
			this.idle = false;
			// console.log('zeroisim.ColladaLoader.prototype.load() create WebSocket');
			var ws = new WebSocket(this.wsip.ip,'fxmlhttp');
			ws.onmessage = this.hwsMessage.bind(this);
			ws.onopen = this.hwsOpen.bind(this);
			}
		}
	zeroisim.ColladaLoader.prototype.getJob = function(evt){
		var job = this.queue.shift();
		if (undefined===job){
			evt.target.close(1000,'Ready loading collada objects. Thank you, dear wsServer');
			this.idle = true;	//	dwing heropening vd websocket af
			// console.log('zeroisim.ColladaLoader.prototype.getJob() queue empty, idle.');
			}
		else {	
			this.url = job.url;	
			this.callback = job.cb;
			this.texturePath = job.tp;
			this.payload = job.pl;
			this.manager.itemStart(this.url);
			// console.log('zeroisim.ColladaLoader.prototype.getJob('+evt.target.url+','+evt.target.protocol+') connected, retrieving: "'+this.url+'"');
			evt.target.send(JSON.stringify({fname:this.url,typ:this.payload.typ}));
			}
		}
	zeroisim.ColladaLoader.prototype.hwsOpen = function(evt){
		// console.log('zeroisim.ColladaLoader.prototype.hwsOpen()');
		this.getJob(evt);	//	start eating from queue
		}
	zeroisim.ColladaLoader.prototype.hwsMessage = function(evt){	//	adapted to handle dae, jpg and png
		// todo THREE.Cache.add( this.url, response );
		//	maybe check status here because data may be chunked
		if ( this.callback ) {
			console.log(`zeroisim.ColladaLoader.hwsMessage() url=\'${this.url}\' typ=${this.payload.typ}`);
			switch (this.payload.typ){
				case 'jpg':
					var texture = new THREE.Texture();
					texture.image = new Image();
					texture.image.src = 'data:image/jpg;base64,'+evt.data;	//	overweeg dit aan de ws-kant te zetten
					texture.format = THREE.RGBFormat;	// JPEGs can't have an alpha channel, so memory can be saved by storing them as RGB.
					texture.needsUpdate = true;
					this.callback(texture);
					break;
				case 'png':
					var texture = new THREE.Texture();
					texture.image = new Image();
					texture.image.src = 'data:image/png;base64,'+evt.data;	//	overweeg dit aan de ws-kant te zetten
					texture.format = THREE.RGBAFormat;
					texture.needsUpdate = true;
					this.callback(texture);
					break;
				case 'dae':
				default:
					this.callback(this.parse(evt.data,this.url),this.url,this.payload);
					break;
				}
			}
		this.manager.itemEnd(this.url);
		this.getJob(evt);	//	start eating from queue
		}
	zeroisim.ColladaLoader.prototype.setCrossOrigin = function(value){
		// this.crossOrigin = value;
		return this;
		}
		
	zeroisim.LoadingManager = function(onLoad,onstart,onResolveURL){	//	todo misschien class van maken
		this.onLoad = onLoad;
		this.lman = new THREE.LoadingManager(this.onLoad);
		this.onStart = onstart;
		this.onResolveURL = onResolveURL;
		this.lman.that = this;
		// console.log('zeroisim.LoadingManager');
		}
	zeroisim.LoadingManager.prototype.resolveURL = function(url){
		// console.log( `LoadingManager.resolveURL: ${url}`);
		// return url;
		//	only resolve non-data urls
		var dataUriRegex = /^data:(.*?)(;base64)?,(.*)$/;
		var dataUriRegexResult = url.match( dataUriRegex );
			// Safari can not handle Data URIs through XMLHttpRequest so process manually
		if ( dataUriRegexResult ) 
			return url;

		if (this.onResolveURL)	
			return this.onResolveURL(url);
			
		return url;
		/*
		var regex_options = 'gim';
		var strSplitPath = '(.*\/)?(\..*?|.*?)(\.[^.]*?)?';
	//	var strSplitPath = '(.*\/)?(\..*?|.*?)(\.[^.]*?)?(#.*$|\?.*$|$)';
		var regex_SplitPath = new RegExp(strSplitPath,regex_options);
		var parts = regex_SplitPath.exec(url);
		var texturePath = parts[1];
		return texturePath;
		*/
		}
	zeroisim.LoadingManager.prototype.itemStart = function(url) {
		// console.log( 'LoadingManager.itemStart Started loading file: ' + url);
		if (undefined!==this.onStart)
			this.onStart(url)
		};
	zeroisim.LoadingManager.prototype.itemEnd = function( url) {
		// console.log( 'LoadingManager.itemEnd loading file: ' + url);
		if ( this.onLoad !== undefined ) {
			this.onLoad(url);
			}
		};
	zeroisim.LoadingManager.prototype.itemError = function( url) {
		console.log( 'LoadingManager.itemError loading file: ' + url);
		if ( this.onLoad !== undefined ) {
			this.onLoad(url);
			}
		};
	// todo overload hier alle loaders
	
	
	
	
	// var loading = {};
	var fileLoaderQueue = [];
	var fileLoaderIdle = true;
	var fileLoaderWs;

	zeroisim.FileLoader = function(manager){	//	misschien beter om dit Collada & Texture-loader te noemen
		console.log(`zeroisim.FileLoader constructed`);
	
		this.manager = ( manager !== undefined ) ? manager : zeroisim.LoadingManager;
		// this.idle = true;
		// this.queue = [];
		}

	// zeroisim.FileLoader.prototype.setWSip = function(value){
		// this.wsip = this.crossOrigin = value;
		// }
	zeroisim.FileLoader.prototype = {
		setWSip:function(value){
			this.wsip = this.crossOrigin = value;
			}
		,setResponseType: function(typ){
			this.responseType = typ;
			}
		,setCrossOrigin: function(ignore){
			}
		,load: function( url, onLoad, onProgress, onError ) {
			if ( url === undefined ) url = '';
			if ( this.path !== undefined ) url = this.path + url;
			url = this.manager.resolveURL( url );
			var scope = this;
			var cached = THREE.Cache.get( url );
			if ( cached !== undefined ) {
				scope.manager.itemStart( url );
				setTimeout( function () {
					if ( onLoad ) onLoad( cached );
					scope.manager.itemEnd( url );
					}, 0 );
				return cached;
				}

			/*	
			// Check if request is duplicate
			if ( loading[ url ] !== undefined ) {
				loading[ url ].push( {
					onLoad: onLoad,
					onProgress: onProgress,
					onError: onError
					} );
				return;
				}
			*/
			
			// Check for data: URI
			var dataUriRegex = /^data:(.*?)(;base64)?,(.*)$/;
			var dataUriRegexResult = url.match( dataUriRegex );
			// Safari can not handle Data URIs through XMLHttpRequest so process manually
			if ( dataUriRegexResult ) {
				var mimeType = dataUriRegexResult[1];
				var isBase64 = !! dataUriRegexResult[2];
				var data = dataUriRegexResult[ 3 ];
				data = window.decodeURIComponent( data );
				if ( isBase64 ) data = window.atob( data );
				try {
					var response;
					var responseType = ( this.responseType || '' ).toLowerCase();
					switch ( responseType ) {
						case 'arraybuffer':
						case 'blob':
							var view = new Uint8Array( data.length );
							for ( var i = 0; i < data.length; i ++ ) {
								view[ i ] = data.charCodeAt( i );
								}
							if ( responseType === 'blob' ) {
								response = new Blob( [ view.buffer ], { type: mimeType } );
								} 
							else {
								response = view.buffer;
								}
							break;

						case 'document':
							var parser = new DOMParser();
							response = parser.parseFromString( data, mimeType );
							break;

						case 'json':
							response = JSON.parse( data );
							break;

						default: // 'text' or other
							response = data;
							break;
						}

					// Wait for next browser tick like standard XMLHttpRequest event dispatching does
					window.setTimeout( function () {
						if ( onLoad ) onLoad( response );
						scope.manager.itemEnd( url );
						}, 0 );

					} 
				catch ( error ) {
					// Wait for next browser tick like standard XMLHttpRequest event dispatching does
					window.setTimeout( function () {
						if ( onError ) onError( error );
						scope.manager.itemEnd( url );
						scope.manager.itemError( url );
						}, 0 );
					}
				} 
			else {
				if ('w'==url.substr(0,1) && 's'==url.substr(1,1)){
					//	todo in de toekomst moet ipv een ip natuurlijk de hostname worden gebruikt
					var regex = new RegExp('(?<ws>ws:\/\/)(?<wsip>[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+):(?<wsport>[0-9]+):(?<protocol>.+):(?<fname>.*)');	//	todo make case insensitive
					if (true===regex.test(url)){
						var wsdetails = regex.exec(url);
						var wsaddress = `${wsdetails.groups.ws}${wsdetails.groups.wsip}:${wsdetails.groups.wsport}`;
						this.url = wsdetails.groups.fname;
						if (undefined===this.responseType){
							var regex_SplitPath = new RegExp('(?<_11>.*\/)?(?<_22>\..*?|.*?)(?<_33>\.[^.]*?)?(?<_44>[a-z0-9_ ]*)\.(?<extension>.*)$','gim');
							var parts = regex_SplitPath.exec(this.url);
							if (parts.groups)	
								this.responseType = parts.groups.extension;
							}
						// this.queue.push({url:this.url, cb:onLoad, typ:this.responseType, /*tp:texturePath, pl:payload, */onProgress: onProgress, onError: onError});
						fileLoaderQueue.push({url:this.url, cb:onLoad, typ:this.responseType, /*tp:texturePath, pl:payload, */onProgress: onProgress, onError: onError});
						this.responseType = undefined;	//	dit responseType geldt alleen voor dit request
						
						// console.log(`zeroisim.fileLoader put in queue: ${this.url} of type: ${this.responseType}`);
						if (fileLoaderIdle){
							fileLoaderIdle = false;
							fileLoaderWs = new WebSocket(wsaddress,wsdetails.groups.protocol);
							fileLoaderWs.onmessage = this.hwsFLMessage.bind(this);
							fileLoaderWs.onopen = this.hwsFLOpen.bind(this);
							}
						}
					scope.manager.itemStart( url );
					
					/*
					Cache.add( url, response );
					scope.manager.itemEnd( url );
					scope.manager.itemError( url );
					*/
					}
				else {
					// invoke THREE.fileLoader or issue warning
					//	bgt	var loader = new THREE.FileLoader( scope.manager );
					}
				}
			// return request;
			return 0;
			}
		,hwsFLOpen: function(evt){
			console.log('zeroisim.FileLoader.hwsFLOpen');
			//this.manager.itemStart(this.url);
			// console.log('zeroisim.ColladaLoader.prototype.getJob('+evt.target.url+','+evt.target.protocol+') connected, retrieving: "'+this.url+'"');
			//evt.target.send(JSON.stringify({fname:this.url,typ:this.payload.typ}));
			this.getJob(evt);	//	start eating from queue
			}
		,hwsFLMessage:function(evt){
			// console.log(`zeroisim.fileLoader.hwsFLMessage file: ${this.url}`);
			// console.log(`zeroisim.ColladaLoader.hwsMessage() url=\'${this.url}\' typ=${this.typ}`);
			// var callback = loading[this.url][0].onLoad;
			var callback = this.callback;
			
			if ( callback ) {
				console.log(`zeroisim.ColladaLoader.hwsMessage() url=\'${this.url}\' typ=${this.typ}`);
				switch (this.typ){
					case 'jpg':
						// todo Cache.add( url, response );
						// this.manager.itemEnd(this.url);
						// this.getJob(evt);	//	start eating from queue
						var texture = new THREE.Texture();
						texture.image = new Image();
						texture.image.src = 'data:image/jpg;base64,'+evt.data;	//	overweeg dit aan de ws-kant te zetten
						texture.format = THREE.RGBFormat;	// JPEGs can't have an alpha channel, so memory can be saved by storing them as RGB.
						texture.needsUpdate = true;
						callback(texture);
						break;
					case 'png':
						// todo Cache.add( url, response );
						// this.manager.itemEnd(this.url);
						// this.getJob(evt);	//	start eating from queue
						var texture = new THREE.Texture();
						texture.image = new Image();
						texture.image.src = 'data:image/png;base64,'+evt.data;	//	overweeg dit aan de ws-kant te zetten
						texture.format = THREE.RGBAFormat;
						texture.needsUpdate = true;
						callback(texture);
						break;
					case 'gltf':
						// var test = evt.data;
						// callback(this.parse(evt.data,this.url),this.url,this.payload);
						// callback(atob(evt.data),this.url,this.payload);
						// todo Cache.add( url, response );
						// this.manager.itemEnd(this.url);
						// this.getJob(evt);	//	start eating from queue
						callback(evt.data,this.url/*,this.payload*/);
						break;
					case 'dae':
						// todo Cache.add( url, response );
						// this.manager.itemEnd(this.url);
						// this.getJob(evt);	//	start eating from queue
						callback(this.parse(evt.data,this.url),this.url/*,this.payload*/);
						break;
					case 'blob':
						var view = new Uint8Array(evt.data.length);
						for (var ii=0; ii<evt.data.length; ii++ ) {
							view[ii] = evt.data.charCodeAt(ii);
							}
						if (this.typ === 'blob'){
							callback(new Blob([view.buffer], {type:mimeType}));
							} 
						else {
							callback(view.buffer);
							}
						break;
					case 'arraybuffer':	//	create ArrayBuffer from base64 encoded stream atob btoa
						//	assume the data comes in base64 encoded
						this.loadText(evt.data,callback);
						/*
						var decoded = atob(evt.data);
						var arraybuffer = new ArrayBuffer(decoded.length);
						var dataView = new DataView(arraybuffer);
						for (var ii=0; ii<decoded.length; ii++ ) {
							dataView.setInt8(ii, decoded.charCodeAt(ii));
							}
						callback(arraybuffer);
						*/
						break;
					case 'bin':
						var view = new Uint8Array(evt.data.size);
						for (var ii=0; ii<evt.data.size; ii++ ) {
							view[ii] = evt.data[ii];
							view[ii] = evt.data.charCodeAt(ii);
							}
						callback(view.buffer);
						break;
					case 'document':
						var parser = new DOMParser();
						response = parser.parseFromString(evt.data,mimeType);
						break;
					case 'json':
						response = JSON.parse(evt.data);
						break;
					default:
						callback(this.parse(evt.data,this.url),this.url/*,this.payload*/);
						break;
					}
				// console.log(`zeroisim.ColladaLoader.hwsMessage() url=\'${this.url}\' typ=${this.typ} callback performed.`);
				}
			// todo Cache.add( url, response );
			this.manager.itemEnd(this.url);
			this.getJob(evt);	//	start eating from queue
			}
		,loadText(txtScene,cb){
			var decoded = atob(txtScene);
			var arraybuffer = new ArrayBuffer(decoded.length);
			var dataView = new DataView(arraybuffer);
			for (var ii=0; ii<decoded.length; ii++ ) {
				dataView.setInt8(ii, decoded.charCodeAt(ii));
				}
			cb(arraybuffer);
			}
		,getJob: function(evt){
			// var job = this.queue.shift();
			var job = fileLoaderQueue.shift();
			if (undefined===job){
				evt.target.close(1000,'Ready loading collada objects. Thank you, dear wsServer');
				fileLoaderIdle = true;	//	dwing heropening vd websocket af
				console.log('zeroisim.fileLoader.getJob() queue empty, idle.');
				}
			else {	
				this.url = job.url;	
				this.callback = job.cb;
				this.texturePath = job.tp;
				// this.payload = job.pl;
				this.typ = job.typ;
				this.manager.itemStart(this.url);
				// console.log(`zeroisim.fileLoader.getJob(${evt.target.url},${evt.target.protocol}) connected, retrieving: ${this.url}`);
				evt.target.send(JSON.stringify({fname:this.url,typ:this.typ}));
				}
			}
		,setPath: function(value){
			this.path = value;
			return this;
			}
		,setResponseType: function(value) {
			console.log(`zeroisim.fileLoader.setResponseType(${value})`);
			this.responseType = value;
			return this;
			}
		,setWithCredentials: function(value){
			console.log(`zeroisim.fileLoader.setWithCredentials(${value})`);
			this.withCredentials = value;
			return this;
			}
		,setMimeType: function(value){
			console.log(`zeroisim.fileLoader.setMimeType(${value})`);
			this.mimeType = value;
			return this;
			}
		,setRequestHeader: function(value){
			console.log(`zeroisim.fileLoader.setRequestHeader(${value})`);
			this.requestHeader = value;
			return this;
			}
		,setCrossOrigin: function(value){
			this.crossOrigin = value;
			return this;
			}
		}	//	zeroisim.FileLoader.prototype
	})(window.zeroisim = window.zeroisim || {}, THREE);
