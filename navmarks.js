//	navmarks.js
'use strict';

(function(global,factory){
	typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports) :
	typeof define === 'function' && define.amd ? define(['exports'], factory) :
	(factory(zeroisim));	//	add to zeroisim
	}(this, (function(exports) {

	class CNavMarks{
		static get lichtenlijnScheveningen(){	//	todo moet dynamisch uit een query komen
			return	{lo:{latdeg:52+6.00100/60,londeg:4+15.43981/60,h:45,color:0x00FF00}	//	raar maar hoogte lijkt wel negatief te werken
						,hi:{latdeg:52+5.86714/60,londeg:4+15.53605/60,h:15,color:0xFF0000}	//	volgens openseamap, hoogte onbekend
						// {hi:{latdeg:52+5.6249/60,londeg:4+15.7107/60,h:45}	//	volgens OpenCPN, hoogte kan ik niet in de ENC vinden
						// ,lo:{latdeg:52+5.8679/60,londeg:4+15.5352/60,h:15}
				};
			}
		static get slijkgat_boeienlijn_opencpn(){
			return	{SG1:	{latdeg:51+51.0017/60,londeg:3+54.0182/60,h:2,color:0x00FF00}
						,SG2:	{latdeg:51+51.3019/60,londeg:3+54.1082/60,h:2,color:0xFF0000}	//	ISO R 4s
						,SG3:	{latdeg:51+50.5972/60,londeg:3+55.0722/60,h:2,color:0x00FF00}	//	ISO G 8s
						,SG4:	{latdeg:51+51.1851/60,londeg:3+55.1263/60,h:2,color:0xFF0000}
						,SG5:	{latdeg:51+50.8739/60,londeg:3+56.0903/60,h:2,color:0x00FF00}	//	ISO G 4s
						,SG6:	{latdeg:51+51.0072/60,londeg:3+56.1535/60,h:2,color:0xFF0000}	//	ISO R 8s
						,SG7:	{latdeg:51+50.7238/60,londeg:3+56.8292/60,h:2,color:0x00FF00}	//	ISO G 2s
						,SG8:	{latdeg:51+50.8571/60,londeg:3+56.8292/60,h:2,color:0xFF0000}	//	ISO R 4s
						,SG9:	{latdeg:51+50.8181/60,londeg:3+57.6852/60,h:2,color:0x00FF00}	//	ISO G 8s
						,SG10:{latdeg:51+50.9517/60,londeg:3+57.7032/60,h:2,color:0xFF0000}	//	ISO R 8s
						,SG11:{latdeg:51+50.8294/60,londeg:3+58.5049/60,h:2,color:0x00FF00}	//	ISO G 4s
						,SG12:{latdeg:51+50.9582/60,londeg:3+58.4869/60,h:2,color:0xFF0000}	//	ISO R 4s
						,SG13:{latdeg:51+51.0017/60,londeg:3+59.5230/60,h:2,color:0x00FF00}	//	ISO G 8s
						,SG14:{latdeg:51+51.2073/60,londeg:3+59.4150/60,h:2,color:0xFF0000}	//	ISO R 8s
						,SG15:{latdeg:51+51.1518/60,londeg:4+ 0.5250/60,h:2,color:0x00FF00}
						,SG16:{latdeg:51+51.4965/60,londeg:4+ 0.2078/60,h:2,color:0xFF0000}
				};
			}

		static get slijkgat_boeienlijn(){
		return{SG1:			{latrad:0.90495452893945,lonrad:0.068074142532708,h:4,s57:{obj:'BOYLAT',colour:'#0F0'							}}
				,SG3:			{latrad:0.90496470787674,lonrad:0.068299408510226,h:4,s57:{obj:'BOYLAT',colour:'#0F0',lights:'Iso.G.8s'	}}
				,SG5:			{latrad:0.90494203642703,lonrad:0.068475239560009,h:4,s57:{obj:'BOYLAT',colour:'#0F0',lights:'Iso.G.4s'	}}
				,SG7:			{latrad:0.90486834969633,lonrad:0.068651819623849,h:4,s57:{obj:'BOYLAT',colour:'#0F0',lights:'Iso.G.2s'	}}
				,SG7A:		{latrad:0.90488489221711,lonrad:0.068863416094834,h:4,s57:{obj:'BOYLAT',colour:'#0F0'							}}
				,SG9:			{latrad:0.90490906912092,lonrad:0.069065462636598,h:4,s57:{obj:'BOYLAT',colour:'#0F0',lights:'Iso.G.8s'	}}
				,SG11:		{latrad:0.90491242375213,lonrad:0.069281365938409,h:4,s57:{obj:'BOYLAT',colour:'#0F0',lights:'Iso.G.4s'	}}
				,SG13:		{latrad:0.90491600972139,lonrad:0.069513185788922,h:4,s57:{obj:'BOYLAT',colour:'#0F0',lights:'Iso.G.8s'	}}
				,SG15:		{latrad:0.90499027030815,lonrad:0.069692387401960,h:4,s57:{obj:'BOYLAT',colour:'#0F0'							}}
				,P1:			{latrad:0.90506857176442,lonrad:0.069867469437686,h:4,s57:{obj:'BOYLAT',colour:'#0F0',lights:'Iso.G.4s'	}}
				,P1A:			{latrad:0.90506417691163,lonrad:0.070021953586857,h:4,s57:{obj:'BOYLAT',colour:'#0F0'  						}}
				,P3:			{latrad:0.90505966637924,lonrad:0.070168760341948,h:4,s57:{obj:'BOYLAT',colour:'#0F0',lights:'Q.G'	  		}}
				,P3A:			{latrad:0.90500391868650,lonrad:0.070270439000129,h:4,s57:{obj:'BOYLAT',colour:'#0F0',lights:'Iso.G.8s'	}}
				,P5:			{latrad:0.90492375998612,lonrad:0.070365189278287,h:4,s57:{obj:'BOYLAT',colour:'#0F0',lights:'Iso.G.4s'	}}
				,P7:			{latrad:0.90478586325818,lonrad:0.070404887023286,h:4,s57:{obj:'BOYLAT',colour:'#0F0'  						}}
				,P9:			{latrad:0.90465557952115,lonrad:0.070438779909346,h:4,s57:{obj:'BOYLAT',colour:'#0F0',lights:'Iso.G.8s'	}}
				,S_damGroen:{latrad:0.90462641892415,lonrad:0.070401329206517,h:4,s57:{obj:'BOYLAT',colour:'#0F0',lights:'F.G'			}}

				,SG2:			{latrad:0.90504069872875,lonrad:0.068099796264146,h:4,s57:{obj:'BOYLAT',colour:'#F00',lights:'Iso.R.4s'	}}
				,SG4:			{latrad:0.90501606323054,lonrad:0.068314763298386,h:4,s57:{obj:'BOYLAT',colour:'#F00'							}}
				,SG6:			{latrad:0.90497465534712,lonrad:0.068500331530905,h:4,s57:{obj:'BOYLAT',colour:'#F00',lights:'Iso.R.8s'	}}
				,SG8:			{latrad:0.90489935217723,lonrad:0.068677473355287,h:4,s57:{obj:'BOYLAT',colour:'#F00',lights:'Iso.R.4s'	}}
				,SG8A:		{latrad:0.90492665185635,lonrad:0.068863977855377,h:4,s57:{obj:'BOYLAT',colour:'#F00'							}}
				,SG10:		{latrad:0.90495059613289,lonrad:0.069055350946834,h:4,s57:{obj:'BOYLAT',colour:'#F00',lights:'Iso.R.8s'	}}
				,SG12:		{latrad:0.90495348790433,lonrad:0.069277808121640,h:4,s57:{obj:'BOYLAT',colour:'#F00',lights:'Iso.R.4s'	}}
				,SG14:		{latrad:0.90496065945157,lonrad:0.069486970296941,h:4,s57:{obj:'BOYLAT',colour:'#F00',lights:'Iso.R.8s'	}}
				,SG16:		{latrad:0.90502901720387,lonrad:0.069662052332668,h:4,s57:{obj:'BOYLAT',colour:'#F00'							}}
				,P2:			{latrad:0.90510222626918,lonrad:0.069847995072215,h:4,s57:{obj:'BOYLAT',colour:'#F00',lights:'Iso.R.4s'	}}
				,P2A:			{latrad:0.90510326710716,lonrad:0.070028320206338,h:4,s57:{obj:'BOYLAT',colour:'#F00'							}}
				,P4:			{latrad:0.90508938915401,lonrad:0.070191043510131,h:4,s57:{obj:'BOYLAT',colour:'#F00',lights:'Q.R'			}}
				,P4A:			{latrad:0.90502982682011,lonrad:0.070305455407274,h:4,s57:{obj:'BOYLAT',colour:'#F00',lights:'Iso.R.8s'	}}
				,P6:			{latrad:0.90494481255807,lonrad:0.070420616318474,h:4,s57:{obj:'BOYLAT',colour:'#F00',lights:'Iso.R.4s'	}}
				,P8:			{latrad:0.90479234216977,lonrad:0.070466867936468,h:4,s57:{obj:'BOYLAT',colour:'#F00'							}}
				,P10:			{latrad:0.90465372808684,lonrad:0.070485218780855,h:4,s57:{obj:'BOYLAT',colour:'#F00',lights:'Iso.R.4s'	}}
				,P12:			{latrad:0.90463405632793,lonrad:0.070460875824016,h:4,s57:{obj:'BOYLAT',colour:'#F00'							}}
				,S_damRood:	{latrad:0.90459482705656,lonrad:0.070400767445975,h:4,s57:{obj:'BOYLAT',colour:'#F00',lights:'F.R'			}}

				,SG:			{latrad:0.9052300655449086,lonrad:0.06731754470880054,h:4,s57:{obj:'BOYLAT',colour:'#F00',lights:'Iso.4s'	}}
				,HA10:		{latrad:0.9051734048430822,lonrad:0.06738102365009610,h:4,s57:{obj:'BOYLAT',colour:'#F00',lights:'Fl.Y.5s'	}}

				//	aanloop rotterdam
				,LL116lo:	{latrad:0.9068644665932357,lonrad:0.07234502068335470,h:10,s57:{colour:'#0F0',lights:'Oc.G.6s16M',pattern_sync:1}}	//	hoogte onbekend
				,LL116hi:	{latrad:0.9068269065619514,lonrad:0.07247057416458973,h:30,s57:{colour:'#0F0',lights:'Oc.G.6s16M',pattern_sync:1}}	//	hoogte onbekend

				,OD_westhoofd:	{latrad:0.9043106746407448,lonrad:0.06744066389435166,h:56,s57:{obj:'CATTOW',lights:'Fl(3)15s56m30M'}}
				,LP_Goeree:		{latrad:0.9062587860713363,lonrad:0.06403002838769284,h:56,s57:{obj:'OFSPLF',lights:'Fl(4)20s32m28M'}}
	/*			,ODAS_B:			{latrad:0.9060875707349290,lonrad:0.06463523174541336,h:56,color:'#FFF',pattern:'Fl(5)Y.20s'}

				//	mississippihaven
				,M1:		{latrad:0.9065390018568582,lonrad:0.07105596749197228,pattern:'LFl.G.8s'}
				,HK1_M2:	{latrad:0.9065218015513774,lonrad:0.07119846741623755,pattern:'Q.G'}
				,M3:		{latrad:0.9065221478699802,lonrad:0.07099454833933233,pattern:'LFl.G.5s'}
				,M4:		{latrad:0.9065063324975569,lonrad:0.07106327037902398,pattern:'LFl.R.5s'}
				,M6:		{latrad:0.9064852063557503,lonrad:0.07096589855166796,pattern:'LFl.R.8s'}
				,M8:		{latrad:0.9064654650195632,lonrad:0.07087320806216560,pattern:'LFl.R.5s'}
	*/			};
			}
		static get cirkel_360(){	//	todo overnieuw met een spreadsheet maken, centrum lijkt niet in 0,0 te zitten
		return {_000:{latrad: 0.00029136646388905,lonrad: -0.00000074901405658479, h:2,color:'#F00'}
				,_010:{latrad: 0.00028537435168552,	lonrad: 0.000050183941791181, 	h:2,color:'#FFF'}
				,_020:{latrad: 0.00027264111321923,	lonrad: 0.000099057108983338, 	h:2,color:'#00F'}
				,_030:{latrad: 0.00025129421333947,	lonrad: 0.00014530872697745, 		h:2,color:'#F00'}
				,_040:{latrad: 0.00022189541244222,	lonrad: 0.00018612999306132, 		h:2,color:'#FFF'}
				,_050:{latrad: 0.00018781527358458,	lonrad: 0.0002237679494047, 		h:2,color:'#00F'}
				,_060:{latrad: 0.00014624499402704,	lonrad: 0.00025297949761151, 		h:2,color:'#F00'}
				,_070:{latrad: 0.000099618869361162,lonrad: 0.00027488815876662, 		h:2,color:'#FFF'}
				,_080:{latrad: 0.000050371195284171,lonrad: 0.00028612336961539, 		h:2,color:'#00F'}
				,_090:{latrad:-0.000000187253514027,lonrad: 0.00029230273558221, 		h:2,color:'#F00'}
				,_100:{latrad:-0.000050464822040963,lonrad: 0.0002867787569149, 		h:2,color:'#FFF'}
				,_110:{latrad:-0.000099057108821299,lonrad: 0.0002721729828115, 		h:2,color:'#00F'}
				,_120:{latrad:-0.0001448405926856,	lonrad: 0.00025073245544176, 		h:2,color:'#F00'}
				,_130:{latrad:-0.00018603636523107,	lonrad: 0.00022189541426324, 		h:2,color:'#FFF'}
				,_140:{latrad:-0.00022170815893263,	lonrad: 0.00018631724657547, 		h:2,color:'#00F'}
				,_150:{latrad:-0.00025082607956861,	lonrad: 0.00014484059319208, 		h:2,color:'#F00'}
				,_160:{latrad:-0.00027245385971186,	lonrad: 0.000099337989254557, 	h:2,color:'#FFF'}
				,_170:{latrad:-0.00028743414025656,	lonrad: 0.0000504648220624, 		h:2,color:'#00F'}
				,_180:{latrad:-0.00029127283713581,	lonrad:-0.0000001872535141462, 	h:2,color:'#F00'}
				,_190:{latrad:-0.00028771502051606,	lonrad:-0.000050745702333619, 	h:2,color:'#FFF'}
				,_200:{latrad:-0.00027245385971186,	lonrad:-0.000098963482226265, 	h:2,color:'#00F'}
				,_210:{latrad:-0.00025251136114268,	lonrad:-0.00014558960724867, 		h:2,color:'#F00'}
				,_220:{latrad:-0.00022170815893263,	lonrad:-0.00018631724657547, 		h:2,color:'#FFF'}
				,_230:{latrad:-0.00018603636523107,	lonrad:-0.00022208266777739, 		h:2,color:'#00F'}
				,_240:{latrad:-0.00014587048700254,	lonrad:-0.00025279224409737, 		h:2,color:'#F00'}
				,_250:{latrad:-0.00009971249611751,	lonrad:-0.00027395189119589, 		h:2,color:'#FFF'}
				,_260:{latrad:-0.000050464822040963,lonrad:-0.00028696601042905, 		h:2,color:'#00F'}
				,_270:{latrad: 0.00000000000000000,	lonrad:-0.0029202185531099,  		h:2,color:'#F00'}
				,_280:{latrad: 0.000050932955825811,lonrad:-0.00028762139772856, 		h:2,color:'#FFF'}
				,_290:{latrad: 0.000098776228551589,lonrad:-0.00027207935605442, 		h:2,color:'#00F'}
				,_300:{latrad: 0.00014502784619808,	lonrad:-0.00025082608219883, 		h:2,color:'#F00'}
				,_310:{latrad: 0.00022226991946139,	lonrad:-0.00018650450008961, 		h:2,color:'#FFF'}
				,_330:{latrad: 0.00025466477648628,	lonrad:-0.00014090826939501, 		h:2,color:'#F00'}
				,_340:{latrad: 0.00027264111321923,	lonrad:-0.000099057108983338,		h:2,color:'#FFF'}
				,_350:{latrad: 0.00028715325999684,	lonrad:-0.0000504648220624,  		h:2,color:'#00F'}
				};
			}
		static createPattern(buoy,light){
			// if (undefined===buoy.pattern)	return null;
			if (undefined===buoy.s57)	return null;
			if (undefined==buoy.s57.lights)	return null;
			var colors = 
				{'R':new THREE.Color(1,0,0)
				,'G':new THREE.Color(0,1,0)
				,'W':new THREE.Color(1,1,1)
				,'Y':new THREE.Color(1,1,0)
				,'B':new THREE.Color(0,0,1)
				};
			var default_color = colors['W'];
			// var pattern_parsed = CNavMarks.ialaLightPatternParser(buoy.pattern);
			var pattern_parsed = CNavMarks.ialaLightPatternParser(buoy.s57.lights);
			switch (pattern_parsed.class){
					case 'F':
						light.material.color = (undefined==pattern_parsed.color)? default_color: colors[pattern_parsed.color];
						light.visible = true;
						break;
					case 'Q':
						light.material.color = (undefined==pattern_parsed.color)? default_color: colors[pattern_parsed.color];
						light.visible = true;
						console.warn(`navmarks.js createPattern Q not implemented`);
						break;
					case 'Fl':
					case 'LFl':
						light.material.color = (undefined==pattern_parsed.color)? default_color: colors[pattern_parsed.color];
						return CNavMarks.createPatternFlash(buoy,light,pattern_parsed);
						break;
					case 'Oc':
						light.material.color = (undefined==pattern_parsed.color)? default_color: colors[pattern_parsed.color];
						return CNavMarks.createPatternOcculting(buoy,light,pattern_parsed);
						break;
					case 'Iso':
						light.material.color = (undefined==pattern_parsed.color)? default_color: colors[pattern_parsed.color];
						return CNavMarks.createPatternIsophase(buoy,light,pattern_parsed);
						break;
					break;
				default:
					var dbg = 0;
					break;
				}
			var dbg = 0;
			}
		static ialaLightPatternParser(lightPattern){
			var regex_options = '';
			var strpatternClass = '^(?<class>Fl|F|LFl|Iso|Q|Oc)(?<lightPattern>.*)$';	//	dankzij de regexp polyfill werkt dit ook onder mozFF en msEdge
			var regex_patternClass = new RegExp(strpatternClass,regex_options);	//	todo make case insensitive
			if (!(true===regex_patternClass.test(lightPattern))){
				console.error('Light character is of unknown class: "'+lightPattern+'".');
				return null;
				}
			else {
				var patternClass = regex_patternClass.exec(lightPattern);
				switch (patternClass.groups.class){
					case 'F':
					case 'Q':
						var pattern = '^.(?<color>R|G|W|Y|B)$';	//	alleen chrom
						break;

					case 'Fl':
					case 'LFl':
					case 'Oc':
						var pattern = '^(\\((?<number_of_strokes>\\d)\\)|\.)((?<color>Y|R|G|W)\.)*(?<period>\\d+)(?<unit_period>.)(?<height>\\d+)*(?<unit_height>m)*(?<range>\\d+)*(?<unit_nomicalrange>M)*$';
						break;

					case 'Iso':
						var pattern = '^.((?<color>R|G|W|Y|BY).)*(?<period>\\d+)(?<unit_period>s)$';
						break;

					default:
						console.error('Light character:"'+patternClass.groups.class+'" is not supported.');
						return null;
						break;
					}
				var regex = new RegExp(pattern,regex_options);
				if (true===regex.test(patternClass.groups.lightPattern)){
					var exec = regex.exec(patternClass.groups.lightPattern);
					exec.groups.class = patternClass.groups.class;
					return exec.groups;
					}
				else {
					console.error(patternClass.groups.class+' character "'+lightPattern+'" not understood.');
					return null;
					}
				}
			}
		static createPatternFlash(buoy,light,pattern){	//	todo die patterns moeten objecten worden
			var rc = {};
			rc.phase = 0;
			rc.flashes = (undefined!=pattern.number_of_strokes)? pattern.number_of_strokes: 1;
			rc.flashperiod = 120;	//	ms light on
			rc.period = (undefined!=pattern.period)? pattern.period: 1;	//	ms light on
			rc.period *= ('s'==pattern.unit_period)? 1: 1;	//	todo for now, assume pattern.unit_period=='s'
			rc.flashinterval = 2900;	//	interval between flashes
			rc.darkness = (1000*rc.period - rc.flashes*rc.flashinterval) / rc.flashinterval;
			rc.fun = CNavMarks.patternFlashFun;
			light.visible = false;

			if (undefined!=buoy.s57.pattern_sync){	//	pattern_sync is bedoeld voor lichtenlijnen en andere navmarks die een gelijktijdig patroon moeten hebben
				rc.intervalfn = setInterval(CNavMarks.patternFlashFun.bind(light), rc.flashinterval);
				}
			else{
				//	delay een random tijd
				var randomtijddelay = Math.floor(Math.random()*1000*rc.period+1);
				var pattern_noise = Math.floor(Math.random()*12+1);	//	noise within 12 ms to avoid regular pattern interference
				setTimeout(function(){(rc.intervalfn = setInterval(CNavMarks.patternFlashFun.bind(light), rc.flashinterval+pattern_noise))}, randomtijddelay);
				}
			return rc;
			}
		static patternFlashFun(pp){
			//	put light on, install timerfunction to put light off
			this.pattern.phase++;
			// console.log('patternFlashFun; this.pattern.phase='+this.pattern.phase)
			if ((this.pattern.phase % 2)==1){
				if (this.pattern.phase <= (2*this.pattern.flashes)){
					this.visible = true;
					setTimeout(function(ppp){
						ppp.pattern.phase++;
						ppp.visible = false;
						},this.pattern.flashperiod,this);
					}
				else{
					clearInterval(this.pattern.intervalfn);
					setTimeout(function(ppp){
						ppp.visible = false;
						ppp.pattern.phase = 0;
						ppp.pattern.intervalfn = setInterval(ppp.pattern.fun.bind(ppp), ppp.pattern.flashinterval)
						},1000*this.pattern.darkness,this);	//	be in dark phase for <this.pattern.darkness> seconds
					// console.log('patternFlashFun; dark for ms='+1000*this.pattern.darkness)
					}
				}
			else {
				this.visible = false;
				}
			}
		static createPatternIsophase(buoy,light,pattern){
			var rc = {};
			rc.phase = 0;
			pattern.period = parseInt(pattern.period);
			if (undefined!=buoy.s57.pattern_sync){	//	pattern_sync is bedoeld voor lichtenlijnen en andere navmarks die een gelijktijdig patroon moeten hebben
				rc.intervalfn = setInterval(CNavMarks.patternIsoFun.bind(light), 500*pattern.period);
				}
			else{
				var randomtijddelay = Math.floor(Math.random()*1000*pattern.period+1);	//	delay a random period, preventing all lights to flash in sync
				var pattern_noise = Math.floor(Math.random()*12+1);	//	noise within 12 ms to avoid regular pattern interference
				rc.intervalfn = setTimeout(function(){(setInterval(CNavMarks.patternIsoFun.bind(light), 500*pattern.period+pattern_noise))}, randomtijddelay);
				}
			return rc;
			}
		static patternIsoFun(pp){
			// console.log('patternFun: '+this.pattern_phase);
			if (this.pattern.phase==0){
				this.pattern.phase = 1;
				this.visible = false;
				}
			else {
				this.pattern.phase = 0;
				this.visible = true;
				}
			}
		static createPatternOcculting(buoy,light,pattern){
			var rc = {};
			rc.phase = 0;
			pattern.period = parseInt(pattern.period);
			if (undefined!=buoy.s57.pattern_sync){	//	pattern_sync is bedoeld voor lichtenlijnen en andere navmarks die een gelijktijdig patroon moeten hebben
				rc.intervalfn = setInterval(CNavMarks.patternOccultingFun.bind(light), 1000*pattern.period);
				}
			else{
				var randomtijddelay = Math.floor(Math.random()*1000*pattern.period+1);	//	delay a random period, preventing all lights to flash in sync
				var pattern_noise = Math.floor(Math.random()*12+1);	//	noise within 12 ms to avoid regular pattern interference
				setTimeout(function(){(rc.intervalfn = setInterval(CNavMarks.patternOccultingFun.bind(light), 1000*pattern.period+pattern_noise))}, randomtijddelay);
				}
			return rc;
			}
		static patternOccultingFun(pp){	//	todo gebruik hier patternFlashFun() voor
			//	put light off, install timerfunction to put light on
			if (this.pattern.phase==0){
				this.pattern.phase=1;
				this.visible = false;
				setTimeout(function(ppp){
					ppp.pattern.phase=0;
					ppp.visible = true;
					},1000,this);	//	todo maak duistere periode circa 1/4 van de periode
				}
			else {
				this.pattern.phase=0;
				this.visible = true;
				}
			}
		};	//	class CNavMarks

	exports.navmarks = CNavMarks;
	})));
	
/*
//	unit tests
var flash0	= zeroisim.navmarks.ialaLightPatternParser('Fl(3)15s56m30M');
var flash1	= zeroisim.navmarks.ialaLightPatternParser('Fl(4)Y.20s32m28M');
var flash2	= zeroisim.navmarks.ialaLightPatternParser('Fl(5)Y.20s');
var flash3	= zeroisim.navmarks.ialaLightPatternParser('Fl.Y.5s');

var iso0		= zeroisim.navmarks.ialaLightPatternParser('Iso.R.8s');
var iso1		= zeroisim.navmarks.ialaLightPatternParser('Iso.4s');

var quick	= zeroisim.navmarks.ialaLightPatternParser('Q.R');

var navmarkLights = zeroisim.navmarks.slijkgat_boeienlijn;

for (var ii in navmarkLights) {
	var navmark = navmarkLights[ii];
	if (undefined!=navmark.pattern){
		var patt	= zeroisim.navmarks.ialaLightPatternParser(navmark.pattern);

		}
	}
var test = 0;
*/