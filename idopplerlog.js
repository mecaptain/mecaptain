//	idopplerlog.js

'use strict';

class CInstrument_doppler extends CInstrument {
	constructor(obj,exposedParms,active,idx){
		super(idx,obj);
		var canvas = obj.querySelector("canvas");
		this.cv = canvas.getContext("2d")
		this.midden = {x: canvas.width/2, y:canvas.height/2};
		if (undefined!=exposedParms)
			this.exposedParms = exposedParms;
		else
			this.exposedParms = //	todo deze moeten door de conning designer kunnen worden gezet, nier overrulen hier dan
				{extwindow: 0
				,globalAlpha:					1.0
				,arrow:
					{x:[1.0,1.0,2.0, 2.0, 1.0, 1.0]
					,y:[1.2,0.6,0.6,-0.6,-0.6,-1.2]
					}
				,colorBackground:				'rgba(200,200,200, 0.1)'	
				,colorTextModeWater:			'rgba(0,0,250,0.7)'	//	blue
				,colorTextModeGround:		'rgba(112,48,48,0.7)' //	brown	'#743'	
				,colorArrowForward:			'rgba(3,3,3,0.5)'
				,colorArrowBackward:			'rgba(250,0,0,0.7)'
				,colorArrowForeStarboard:	'rgba(0,250,0,0.7)'
				,colorArrowForePortside:	'rgba(250,0,0,0.7)'
				,colorArrowAftStarboard:	'rgba(0,250,0,0.7)'
				,colorArrowAftPortside:		'rgba(250,0,0,0.7)'
				,offsetTextVertical:		-100
				,spacingTextVerticalFirst:	45
				,spacingTextVertical:	75
				};
		this.ship0 = {doppler:{}};
		this.cv.globalAlpha = this.exposedParms.globalAlpha;
		this.cv.textAlign="center";
		this.fakeData();
		this.mode = 'GROUND';
		canvas.addEventListener("click", this.canvasClick.bind(this), false);
		/**/
		super.cbRxNmea('VBW',function(rxd,that){
			if (undefined==rxd.data)	return;
			this.errorTxt = 'VALID DATA';
			if ('A'==rxd.data.validwat) {
				that.ship0.doppler.longwat = rxd.data.longwat;
				that.ship0.doppler.transwat = rxd.data.transwat;
				}
			if ('A'==rxd.data.statgrd) {
				that.ship0.doppler.longgrd = rxd.data.longgrd;
				that.ship0.doppler.transgrd = rxd.data.transgrd;
				}
			if ('A'==rxd.data.statsterntransw) {
				that.ship0.doppler.sterntransw = rxd.data.sterntransw;
				}
			if ('A'==rxd.data.statsterntransg) {
				that.ship0.doppler.sterntransg = rxd.data.sterntransg;
				}
			that.draw(that.ship0);	//	that instrument is updated by a timer, not when data received
			},this);
		/**/	
		// setInterval(this.update.bind(this), 1000 / 2);
		}
	fakeData(){	
		this.ship0.doppler=
			{longwat: 12.3
			,transwat: -4
			,longgrd: -45.6
			,transgrd: 6
			,sterntransw: 7
			,sterntransg: -8
			};
		this.errorTxt = 'NO DATA';
		}
	canvasClick(evt){
		if (this.mode=='WATER')
			this.mode = 'GROUND';
		else if (this.mode=='GROUND')
			this.mode = 'WATER';
		}
	update() {	//	called by setInterval
		this.draw(this.ship0);
		}
	resize(w,h){
		}
	draw(ship) {
		this.cv.clearRect(0,0,2*this.midden.x,2*this.midden.y);
		this.cv.save();
		this.cv.fillStyle = this.exposedParms.colorBackground;	this.cv.fillRect(0,0,2*this.midden.x,2*this.midden.y);
		this.cv.translate(this.midden.x,this.midden.y);

		if (this.mode=='WATER'){
			this.cv.fillStyle = this.exposedParms.colorTextModeWater;	//	blue for water, brown for ground
			var trans		= ship.doppler.transwat		;
			var longs		= ship.doppler.longwat		;
			var sterntrans	= ship.doppler.sterntransw	;
			}
		else {	//	groundspeed
			this.cv.fillStyle = this.exposedParms.colorTextModeGround;	//	blue for water, brown for ground
			var trans		= ship.doppler.transgrd		;
			var longs		= ship.doppler.longgrd		;
			var sterntrans	= ship.doppler.sterntransg	;
			}
		this.cv.font = '10px sans-serif';
		var YY = this.exposedParms.offsetTextVertical;
		this.cv.fillText(this.mode,0,YY);					YY+=this.exposedParms.spacingTextVerticalFirst;
		this.cv.font = '40px sans-serif';
		this.cv.fillText(Math.abs(trans)			,0,YY);	YY+=this.exposedParms.spacingTextVertical;
		this.cv.fillText(Math.abs(longs)			,0,YY);	YY+=this.exposedParms.spacingTextVertical;
		this.cv.fillText(Math.abs(sterntrans)	,0,YY);	YY+=this.exposedParms.spacingTextVertical;
		// this.cv.fillText(this.errorTxt		,0,YY);	YY+=this.exposedParms.spacingTextVertical;
		if (longs == 0) {}	//	niks tekenen
		else if (longs > 0) {
			this.drawArrow(this.exposedParms.colorArrowForward,-80,-28,1,1,+Math.PI/2);	//	forward speed
			this.drawArrow(this.exposedParms.colorArrowForward,+80,-28,1,1,+Math.PI/2);	//	forward speed
			}
		else {
			this.drawArrow(this.exposedParms.colorArrowBackward,-80,+28,1,1,-Math.PI/2);	//	backward speed
			this.drawArrow(this.exposedParms.colorArrowBackward,+80,+28,1,1,-Math.PI/2);	//	backward speed
			}

		if (trans == 0){}	//	niks tekenen
		else if (trans > 0)
			this.drawArrow(this.exposedParms.colorArrowForeStarboard,+100,-76,-1,1);	//	starboard focsel, mirrored horizontal
		else
			this.drawArrow(this.exposedParms.colorArrowForePortside,-100,-76, 1,1);	//	port focsel

		if (sterntrans == 0){}	//	niks tekenen
		else if (sterntrans > 0)
			this.drawArrow(this.exposedParms.colorArrowAftStarboard,+100,+76,-1,1);	//	starboard poop, mirrored horizontal
		else
			this.drawArrow(this.exposedParms.colorArrowAftPortside,-100,+76, 1,1);	//	port poop

		this.cv.restore();
		}
	drawArrow(color,x,y,sx,sy,rotate){
		var thick = 5;
		this.cv.save();
		this.cv.translate(x,y);
		this.cv.scale(sx,sy);
		if (undefined!=rotate)
			this.cv.rotate(rotate);
		var scale = 20;
		this.cv.beginPath();
		this.cv.strokeStyle = color;
		this.cv.lineWidth = thick;
		this.cv.moveTo(0,0);
		for (var ii=0; ii<this.exposedParms.arrow.x.length; ii++){
			this.cv.lineTo
				(20*this.exposedParms.arrow.x[ii]
				,20*this.exposedParms.arrow.y[ii]);
			}
			
		this.cv.closePath();
		this.cv.stroke();
		this.cv.restore();
		}
	};
	
CInstrument.addInstrumentToSettings
	('./idopplerlog.js'
	,'doppler'
	,'canvas'
	,CInstrument_doppler
	,'shows longitudinal and dual transversal speed relative to both water and ground'
	);
