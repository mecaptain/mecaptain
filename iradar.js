//	tab_radar.js
'use strict';

class CInstrument_radar extends CInstrument {
	constructor(obj,exposedParms,active,idx){
		super(idx,obj);
//		window.addEventListener("load", this.onload.bind(this), false);	//	todo use bind() everywhere on load event
		this.hasFocus = false;
		if (undefined!=exposedParms)
			this.exposedParms = exposedParms;
		else
			this.exposedParms = //	todo deze moeten door de conning designer kunnen worden gezet, nier overrulen hier dan
				{aisplot: false
				,ranges:		[0.25,0.5,0.75,1.5,3,6,12,24,48]
				,range: 6.0	//	range in mijlen
				,l_pulse:	[  15, 15,  12, 10,6,4, 2, 2, 1]	//	in pixels
				,shipShape: [[-1,2],[1,2],[1,-2],[0,-3],[-1,-2],[-1,2]]
				,aisShipShape: [[-1,2],[1,2],[0,-3],[-1,2]]
				,pulselength: 20
				,dikte_gradenboog:37
				};	
		// console.log('initting radartabObj');
		this.active = (undefined==active)? false: active;
		if (this.active!=true){
			zeroisim.replaceNode(this.div,i.span(i.center('RADAR')));
			return;
			}
		window.addEventListener('resize',this.onWindowResize.bind(this),false);
		this.square = Math.min(this.div.offsetWidth,this.div.offsetHeight);
		this.canvas = i.canvas({id:'radar_canvas',width:this.square, height:this.square});	//	dependant on div.width and height
		this.ppi_midpoint = Math.min(this.canvas.width/2,this.canvas.height/2);	//	todo ppi wordt kleiner als er een gradenboog komt
		if (this.exposedParms.dikte_gradenboog > this.ppi_midpoint){
			this.dikte_gradenboog = 0;
			this.ppi_radius = this.ppi_midpoint;
			}
		else {	
			this.ppi_radius = this.ppi_midpoint - this.exposedParms.dikte_gradenboog;
			this.dikte_gradenboog = this.exposedParms.dikte_gradenboog;
			}
		this.init();
		}
	init(){
		var hpw = {style:'width=100%;'};
		this.ship = {hdt:0,latlon:new zeroisim.LatLon()};	//	geen gebruik meer maken van global		
		this.txt_radar_rng			=	i.div('rng',			{});
		this.radar_display_pos		=	i.div('latlon',		{});
		this.radar_display_brg		=	i.div('brg',			{});
		this.radar_display_rng		=	i.div('rng',			{});
		this.btn_radar_rng_plus		=	i.button('&#43',		{style:'width=100%;',click:this.rangePlus.bind(this)});
		this.btn_radar_rng_min		=	i.button('&#45',		{style:'width=100%;',click:this.rangeMin.bind(this)});
		this.check_radar_ais			=	i.input('ais',			{type:"checkbox",change:this.checkAis.bind(this)});
		this.check_radar_hdg			=	i.input('hdg',			{type:"checkbox",change:this.checkHdg.bind(this)});
		this.check_radar_rings		=	i.input('rings',		{type:"checkbox",change:this.checkRings.bind(this)});
		this.check_radar_navmarks	=	i.input('navmarks',	{type:"checkbox",change:this.checkNavmarks.bind(this)});
		
		this.html = i.table(i.tr(
			[i.td(this.canvas)
			,i.td(i.table(
				[i.tr(i.td(i.table(
					[i.tr(i.td('range',{colspan:3,style:"text-align:center;"}))
					,i.tr(
						[i.td(this.btn_radar_rng_plus)
						,i.td(this.txt_radar_rng)
						,i.td(this.btn_radar_rng_min)
						])
					],hpw)))
				,i.tr(i.td(i.fieldset("view",i.table(
					[i.tr(i.td([this.check_radar_ais,i.span('AIS')]))
					,i.tr(i.td([this.check_radar_hdg,i.span('hdg marker')]))
					,i.tr(i.td([this.check_radar_rings,i.span('rings')]))
					,i.tr(i.td([this.check_radar_navmarks,i.span('navmarks')]))
					]))))
				,i.tr(i.td(i.fieldset("cursor",i.table(
					[i.tr(i.td(this.radar_display_pos))
					,i.tr(i.td(this.radar_display_brg))
					,i.tr(i.td(this.radar_display_rng))
					]))))
				]))
			]));
		
		zeroisim.replaceNode(this.div,this.html);
		this.cv = this.canvas.getContext("2d");
		this.canvas.that = this;
		this.canvas.onmousedown	= function(e){	e.currentTarget.that.mousedown(e);	}
		this.canvas.onmouseup	= function(e){	e.currentTarget.that.mouseup(e);		}
		this.canvas.onmousemove	= function(e){	e.currentTarget.that.mousemove(e);	}
		// this.canvas.onwheel		= function(e){	e.currentTarget.that.mousewheel(e);	}
		this.canvas.addEventListener('wheel', function(e){	e.currentTarget.that.mousewheel(e);	}, {passive: true});
		this.canvas.style.backgroundColor = '#EEE';
		this.pi_180 = Math.PI / 180.0;
		
		this.ppi_pos = new zeroisim.LatLon({latdeg: 0, londeg: 0});	//	assume midpoint
		this.ppi_cursorpos = new zeroisim.LatLon({latdeg: 0, londeg: 0});	//	assume midpoint
		
		this.args_gradenboog =	//	looks for the compass card around the ppi
			{that:this
			,canvas: this.cv
			,p1:{ticks:360,short:5,long:10,ptext:10,angledivider:  1}
			,p2:{routside:0.9,rinside:0.915,r10s:1.030,r5s:1.017,rtexts:1.05,color:'#00F',font:'10px Verdana'}
			,m:{x:this.ppi_midpoint,y:this.ppi_midpoint}
			};
		
		//	do not let the text fields empty
		this.exposedParms.pulselength =  this.exposedParms.l_pulse[this.exposedParms.ranges.indexOf(this.exposedParms.range)];
		this.txt_radar_rng.innerHTML = this.exposedParms.range;
		this.radar_display_pos.innerHTML = this.ppi_pos.latdegmindec+h.br+this.ppi_pos.londegmindec;
		this.radar_display_brg.innerHTML = '000' + h.deg;
		this.radar_display_rng.innerHTML = '00.0' + "'";

		//	set defaults
		this.check_radar_hdg.click();	
		this.check_radar_rings.click();	
		this.check_radar_navmarks.click();	

		super.cbRxNmea('GGA',function(rxd,that){	//	install callback for position		todo kiezen van welke sentence je de latlon wilt (GGA, GLL, ...)
			if (undefined==rxd.data)	return;
			that.ship.latlon.interpretLatLonGGA (rxd.data);		
			that.update();
			},this);
		super.cbRxNmea('THS',function(rxd,that){	//	install callback for heading
			if (undefined==rxd.data)	return;
			that.ship.hdt = parseFloat(rxd.data.hdgT);
			that.update();
			},this);
		// this.aislist = [];//	assume aislist to be global
		this.hasFocus = true;
		if (this.ppi_radius > 1) this.draw();	
		}
	onfocus(){	this.hasFocus=true;	}
	onblur(){	this.hasFocus=false;	}
	resize(width,height){
		this.square = Math.min(width,height);
		this.canvas.width = this.square;
		this.canvas.height = this.square;
		if (undefined==this.html)
			this.init();
		this.ppi_midpoint = Math.min(this.canvas.width/2,this.canvas.height/2);	//	todo ppi wordt kleiner als er een gradenboog komt
		// this.ppi_radius = this.ppi_midpoint - this.exposedParms.dikte_gradenboog;
		if (this.exposedParms.dikte_gradenboog > this.ppi_midpoint){
			this.dikte_gradenboog = 0;
			this.ppi_radius = this.ppi_midpoint;
			}
		else {	
			this.ppi_radius = this.ppi_midpoint - this.exposedParms.dikte_gradenboog;
			this.dikte_gradenboog = this.exposedParms.dikte_gradenboog;
			}

		if (this.ppi_radius >1)	
			this.draw();

		// todo upon resizestop, whole new instrument is created, but conningdesigner does not rescale the canvas...
		}
	onWindowResize(evt){
		console.log('iradar.js onWindowResize()');
		this.resize(window.innerWidth-10,window.innerHeight-10);
		}	
	update(){
		this.ppi_pos = this.ship.latlon;
		this.draw();
		}
	rangePlus(){
		this.exposedParms.range = this.exposedParms.ranges[(this.exposedParms.ranges.indexOf(this.exposedParms.range)+1)% this.exposedParms.ranges.length];
		this.exposedParms.pulselength =  this.exposedParms.l_pulse[this.exposedParms.ranges.indexOf(this.exposedParms.range)];
		this.txt_radar_rng.innerHTML = this.exposedParms.range;
		}
	rangeMin(){
		var idx = this.exposedParms.ranges.indexOf(this.exposedParms.range);
		if (0==idx) idx = this.exposedParms.ranges.length
		this.exposedParms.range = this.exposedParms.ranges[--idx];
		this.exposedParms.pulselength =  this.exposedParms.l_pulse[this.exposedParms.ranges.indexOf(this.exposedParms.range)];
		this.txt_radar_rng.innerHTML = this.exposedParms.range;
		}
	checkAis(evt){
		this.aisplot = evt.target.checked;
		}
	checkHdg(evt){
		this.hdgFlash = evt.target.checked;
		}
	checkRings(evt){
		this.rings = evt.target.checked;
		}
	checkNavmarks(evt){	
		this.navmarks = evt.target.checked;
		}
	mousedown(evt){

	var dbg = 0;
		}
	mouseup(evt){
		var dbg = this.windowToPosition(evt);
		}
	mousemove(evt){
		var pos = this.windowToPosition(evt);
		var bd = pos.beardist(this.ppi_pos);
		this.radar_display_pos.innerHTML = pos.latdegmindec+h.br+pos.londegmindec;
		this.radar_display_brg.innerHTML = bd.b + h.deg;
		this.radar_display_rng.innerHTML = bd.d + "'";
		}
	mousewheel(evt){
		console.log('mousewheel');
		}
	drawShape(cvxy,hdt,schaal,shape,color,linewidth){
		this.cv.save();
		this.cv.translate(this.ppi_midpoint+cvxy.x,this.ppi_midpoint+cvxy.y);
		this.cv.rotate(hdt*Math.PI/180.0);
		if (undefined==color) color = '#F00';
		if (undefined==linewidth) linewidth = 3;
		this.cv.strokeStyle = color;
		this.cv.lineWidth = linewidth;
		this.cv.beginPath();
		if (schaal<5)	schaal = 5;
		shape.myforEach(function(pp,idx,arr,that){
			if (0==idx)	that.cv.moveTo(schaal*pp[0], schaal*pp[1]);
			that.cv.lineTo(schaal*pp[0], schaal*pp[1]);
			},this);			
		this.cv.closePath();		
		this.cv.stroke();
		this.cv.restore();
		}
	drawRings(color){
		this.cv.strokeStyle = color;
		this.cv.lineWidth = 1;
		for(var ring=1; ring<6; ring++){
			this.cv.beginPath();
			this.cv.arc(this.ppi_midpoint,this.ppi_midpoint,this.ppi_radius*ring/6,0,2*Math.PI);
			this.cv.stroke();
			}
		}
	drawEcho(dist,xy,color){
		// var dist = Math.sqrt(xy.x*xy.x+xy.y*xy.y)+this.exposedParms.pulselength/2;
		// if (dist>this.ppi_radius)	return;	//	off screen
		var bearing = Math.atan2(xy.y,xy.x);
		// var distance_discrimination = 0.1 /2;
		var distance_discrimination = 5 / dist;
		this.cv.strokeStyle = color;
		this.cv.lineWidth = this.exposedParms.pulselength;	//	afstand onderscheidingsvermogen, pulslengte
		this.cv.beginPath();
		this.cv.arc(this.ppi_midpoint,this.ppi_midpoint,dist,bearing-distance_discrimination,bearing+distance_discrimination);
		this.cv.stroke();
		this.cv.closePath();
		}
	drawHeadingFlash(hdt,color){
		this.cv.save();
		this.cv.translate(this.ppi_midpoint,this.ppi_midpoint);
		this.cv.rotate(hdt*zeroisim.pi_180);
		this.cv.beginPath();
		this.cv.lineWidth = 1;
		this.cv.strokeStyle = color;
		this.cv.moveTo(0,0);
		this.cv.lineTo(0,-this.ppi_radius);
		this.cv.closePath();
		this.cv.stroke();
		this.cv.restore();
		}
	draw(){
		if (!this.hasFocus)	return;
		// draw ppi
		this.cv.fillStyle = 'rgba(125,125,125,1.0)';
		this.cv.strokeStyle = 'rgba(0,255,0,1.0)';	//	groen
		this.cv.beginPath();
		this.cv.lineWidth = 1;
		this.cv.arc(this.ppi_midpoint,this.ppi_midpoint,this.ppi_radius,0,2*Math.PI);
		this.cv.stroke();
		this.cv.fill();
		zeroisim.gradenboog(this.args_gradenboog);
		if (this.hdgFlash) this.drawHeadingFlash(this.ship.hdt,'rgba(125,255,125,0.5)');
		if (this.rings) this.drawRings('rgba(0,200,0,1.0)');
		// treat AIS targets as radar echoes
		aislist.myforEachSparse(function(aistarget,idx,arr,that){
			if (undefined!=aistarget.Latitude){	//	maw wacht op een VDM msg:5
				if (aistarget.ownship!=true){	//	todo switchable
					var xy = that.latlongToCanvasPoint(aistarget.Latitude,aistarget.Longitude);	// get center from the map (projected)
					var dist = Math.sqrt(xy.x*xy.x+xy.y*xy.y)+that.exposedParms.pulselength/2;
					if (dist>that.ppi_radius)	return;	//	off screen
					that.drawEcho(dist,xy,'rgba(0,255,200,1.0)');
					if (that.aisplot){	//	if user wants ais targets being plotted
						that.drawShape(xy,aistarget.COG,6,that.exposedParms.aisShipShape);
						}
					}
				}
			},this);
		if (this.navmarks) {
			//	todo misschien moet je de navmarks halen uit de scene.children
			Object.keys(zeroisim.navmarks.slijkgat_boeienlijn).myforEach(function(key,ii,ar,that){
				var light = zeroisim.navmarks.slijkgat_boeienlijn[key];
			// Object.keys(zeroisim.navmarks.cirkel_360).myforEach(function(key,ii,ar,that){
				// var light = zeroisim.navmarks.cirkel_360[key];
				var xy = that.latlongToCanvasPoint(light.latrad/that.pi_180,light.lonrad/that.pi_180); 
				var dist = Math.sqrt(xy.x*xy.x+xy.y*xy.y)+that.exposedParms.pulselength/2;
				if (dist<that.ppi_radius)	//	if in range screen
					that.drawEcho(dist,xy,light.color);
				},this);
			}
		}
	windowToCanvas(x,y) {
		var bbox = this.canvas.getBoundingClientRect();
		return { x: x-bbox.left * (this.canvas.width  / bbox.width)
				 ,	y: y-bbox.top  * (this.canvas.height / bbox.height)
				 };
		}
	windowToPosition(evt) {
		var posPpi = this.windowToCanvas(evt.clientX,evt.clientY);	//	offset in pixels from origin of canvas
		posPpi.x -= this.ppi_midpoint;	// offset in pixels from midpoint of canvas
		posPpi.y -= this.ppi_midpoint;	// offset in pixels from midpoint of canvas
		this.ppi_cursorpos.latdeg = this.ppi_pos.latdeg-posPpi.y*this.exposedParms.range/60/this.ppi_midpoint;
		this.ppi_cursorpos.londeg = this.ppi_pos.londeg+posPpi.x*this.exposedParms.range/60/this.ppi_midpoint/Math.cos(this.ppi_pos.latrad);
		return this.ppi_cursorpos;
		}		
	latlongToCanvasPoint(lat,lon){
		var dlat = 60.0*(this.ppi_pos.latdeg-lat);
		var dlon = 60.0*(lon-this.ppi_pos.londeg);
		var ypixels = dlat * this.ppi_midpoint / this.exposedParms.range;
		var xpixels = dlon * this.ppi_midpoint / this.exposedParms.range * Math.cos(this.ppi_pos.latrad);
		return {x:xpixels, y:ypixels};
		}
	};
CInstrument.addInstrumentToSettings
	('./iradar.js'
	,'radar'
	,'div'
	,CInstrument_radar
	,'shows echoes from the radio beams sent out from this ship'
	,	['./latlon.js'
		,'./navmarks.js'	//	todo dit moet anders
		]
	);
