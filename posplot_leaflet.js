//	posplot_leaflet.js
// plot de positie op een middelbreedtekaartje
//	NB! posplot is aangepast zodat de callback van scale veranderd een that meegeeft.
//
//	todo
//	geef voorspelling rekening houdend met snelheid en rot v=w.r => r=v/w (bochtstraal is snelheid gedeeld door hoeksnelheid)
//	als autopilot aanstaat, kun je de voorspelling verbeteren met de HDG setpoint
//	laad tegels van openseamap	wget http://api06.dev.openstreetmap.org/api/0.6/map?bbox=4,52,4.5,52.3
//	maak mogelijkheid om een track te laten zien. bv gpx
//	op grote schaal scheepsfiguurtje laten zien
//	navmarks die in de simulator gedefinieerd zijn tonen in een aparte (uitzetbare) laag
//	
//	http://wiki.openstreetmap.org/wiki/Develop
//	http://leafletjs.com/examples/quick-start.html
//	http://wiki.openstreetmap.org/wiki/KAP-charts_from_OpenSeaMap//
//	https://leaflet-extras.github.io/leaflet-providers/
//	http://planet.openstreetmap.org/
//	http://www.klokan.cz/projects/gdal2tiles/
//	http://libbsb.sourceforge.net/
//	http://opencpn.org/ocpn/node/145
//https://webapiv2.navionics.com/examples/5000_LeafletSupport.html
		
'use strict';
var RingBuffer = function(length){
	var pointer = 0, buffer = [];
	return {
		get: function(key){
			if (key < 0)				return buffer[pointer+key];
			else if (key === false)	return buffer[pointer-1];
			else 							return buffer[key];
			},
		push: function(item){
			buffer[pointer] = item;
			pointer = (pointer +1) % length;
			},
		length: function(){
			return buffer.length;
			},
		foreach: function(cb,obj){
			for (var ptr=0; ptr<buffer.length; ptr++){
				cb(buffer[ptr],obj);
				}
			}
		};
	};
	
var posplotobj={
	html: function(tabdivname){
		console.log('posplotobj::html('+tabdivname+')');
		this._tabdivname = tabdivname;
		this.ship0 = {hdt:0,latlon:new zeroisim.LatLon(),rot:0.0,sog:0.0};	

		window.addEventListener("load", this.onload.bind(this), false);
		var _ShipPosition = i.tr(i.td(i.fieldset('ship position'
			,i.table(
				[i.tr(i.td(i.div('latitude', {id:"div_latitude_ship"})))
				,i.tr(i.td(i.div('lat_gll',  {id:"div_latitudegll_ship"})))
				]))));
		var _MousePosition = i.tr(i.td(i.fieldset('mouse position'
			,i.table(
				[i.tr(i.td(i.div('latitude',	{id:"div_latitude"})))
				,i.tr(i.td(i.div('latitude',	{id:"div_latitudegll"})))
				,i.tr(i.td(i.div('bearing',	{id:"div_beardist"})))
				]))));
		this._CenterAis = i.table(	//	checked:undefined,
				[i.tr(i.td(i.checkbox('ship in center',{click:this.check.bind(this),id:"posplot_centre"})))	//	todo zet waarde goed afh van variable
				,i.tr(i.td(i.checkbox('ais',				{click:this.check.bind(this),id:"posplot_ais"})))
				]);
		this.ppll_tab_routes		= i.div('reserved for trainer',{id:"ppll_tab_routes",	style:"overflow-y:scroll;"});				
		this.ppll_tab_targets	= i.div('reserved for trainer',{id:"ppll_tab_targets",style:"overflow-y:scroll;"});				
				
		this.radio = {onclick:"document.querySelector('#posplot_mouseaction').attributes.posplot_mode.value=this.id;",name:"posplot_settings_radio"};
		var btnSendGLLHelp = 'click on map, then shift and rotate using the gizmo. When ready press green button that appears here.';
		this.btnSendGLL = i.radio(i.span('set POS & HDG',{title:btnSendGLLHelp}), Object.assign(this.radio,{id:"div_posplot_mode_sendgll",title:btnSendGLLHelp}));
		this.btnSendGLLOK = i.button('send position',{click:this.sendPostionReady.bind(this),style:'background-color:#0F0'});
		this._MouseAction = i.fieldset('mouse action'
			,i.table(
				[i.tr(i.td(this.btnSendGLL,{id:'sendGLL_OK'}))
				,i.tr(i.td(i.radio('pan',				Object.assign(this.radio,{id:"div_posplot_mode_pan"}))))
				,i.tr(i.td(i.radio('select',			Object.assign(this.radio,{id:"div_posplot_mode_select"}))))
				,i.tr(i.td(i.radio('AP setpoint',	Object.assign(this.radio,{id:"div_posplot_mode_apsetpoint"}))))
				]),{id:"posplot_mouseaction",posplot_mode:"nothing"});
				
		// this.radio = {onclick:"document.querySelector('#posplot_prediction').attributes.posplot_predictionmode.value=this.id;",name:"posplot_predictionsettings_radio"};
		this.radio = {onclick:this.hRadioPosplotPrediction.bind(this),name:"posplot_predictionsettings_radio"};
		this._Prediction = i.tr(i.td(i.fieldset('prediction',i.table(
			[i.tr(i.td(
				[i.input('points',{type:"range",list:'ticks_predictiontime',input:this.handleslide.bind(this,'val_predictiontime',this.hPredictionTime.bind(this))})
				,i.datalist([i.option(1),i.option(5),i.option(6),i.option(10),i.option(12),i.option(20),i.option(30),i.option(60)],{id:"ticks_predictiontime"})
				,i.div(100,{id:"val_predictiontime"})
				]))
			,i.tr(i.td(i.radio('off',			Object.assign(this.radio,{id:"div_posplot_predictionmode_off"}))))
			,i.tr(i.td(i.radio('straight',	Object.assign(this.radio,{id:"div_posplot_predictionmode_straight"}))))
			,i.tr(i.td(i.radio('curved',		Object.assign(this.radio,{id:"div_posplot_predictionmode_curved"}))))
			])),{id:'posplot_prediction'}));	
			
		this._History = i.tr(i.td(i.fieldset('history',i.table(
			[i.tr(i.td(
				[i.input('points',{type:"range",min:"0",max:"100",value:10,id:"sliderhistpts",input:this.handleslide.bind(this)})
				,i.div(100,{id:"val_sliderhistpts"})
				]))
			,i.tr(i.td(
				[i.input('interval',{type:"range",min:"0",max:"100",value:10,id:"sliderhistint",input:this.handleslide.bind(this)})
				,i.div(100,{id:"val_sliderhistint"})
				]))
			]))));	
		this._Scale = i.tr(i.td(i.fieldset('scale (nycorrect)',i.table(i.tr(
			[i.td(i.input(' ',
				{type:"range"
				,min:"0",max:"19",value:18
				,id:"sliderscale"
				,input:this.handleslide.bind(this,'val_sliderscale',this.setScale.bind(this))	//	zoomend
				}))
			,i.td(i.div(2,{id:"val_sliderscale"}))
			])))));
		var _Chart = h.tr(h.td(h.fieldset('chart'	//	todo and use {click: this.check}
			,h.table
				(h.tr(h.td(h.input('none',	'type="radio" onclick="check(this,function(that){_tabses.posplot.loadOSM(that);})" name="posplot_radio_non" id="div_posplot_chart_non"')))
				+h.tr(h.td(h.input('openseamap',	'type="radio" onclick="check(this,function(that){_tabses.posplot.loadOSM(that);})" name="posplot_radio_osm" id="div_posplot_chart_osm"')))
				),'id="posplot_chart" posplot_predictionmode="nothing"')));


        //wim this.dimensions = { w: 900, h: 700 };
		//wim this.dimensions = {w:1500, h: 970};		//	msi vstep



        this.charttab = i.div(
            [i.div('', { id: "leafletmap", style: 'z-index: 1;' })
			,i.div('',{id:"leafletsvg",style:'z-index: -1;'})	
			//wim [i.div('',{id:"leafletmap",style:'width: '+this.dimensions.w+'px; height: '+this.dimensions.h+'px; position: absolute;z-index: 1;'})
			//wim ,i.div('',{id:"leafletsvg",style:'width: '+this.dimensions.w+'px; height: '+this.dimensions.h+'px; position: absolute;z-index: -1;'})	

                , i.table(
				[_MousePosition
				,_ShipPosition
				,i.tr(i.td(i.div('',{id:"detabs"})))
				],{style:"position: absolute; top:0; right:0; z-index:2; background-color:rgba(250,250,250,.7);"})
			],{style:"position: relative; height:100%;"});
		}
    , onfocus: function () {	
		this.focus = true;
		if (undefined===this.leafletmap)	return;
		this.draw(this.ship0.hdt,this.ship0.latlon.latdeg,this.ship0.latlon.londeg,this.ship0.rot,this.ship0.sog);	//	
		L.Util.requestAnimFrame(this.leafletmap.invalidateSize,this.leafletmap,!1,this.leafletmap._container);
		//	dit lost het probleem op dat ontstaat als de map geen focus heeft bij creatie, 
		//	zie http://stackoverflow.com/questions/10762984/leaflet-map-not-displayed-properly-inside-tabbed-panel
		//	todo, ook zoiets doen bij graph
		// this.canvaslayer.render();
		document.getElementById('posplot_centre').click();
		document.getElementById('posplot_centre').click();
		}
	,onblur:function(){	
		this.focus = false;
		}
	,onload: function(evt){
		var ls = new CLoadScript();
		ls.loadScript('./incl/leaflet/leaflet-src.js',function(){	//	leaflet-src.js als je wilt debuggen
			ls.loadScript('./incl/leaflet/canvaslayer/leaflet_canvas_layer.js',function(){
				this.init();
				}.bind(this));
			}.bind(this));
		}
	,init: function(){
		document.getElementById(this._tabdivname).appendChild(this.charttab);
		setValue('leafletmap',this,'that');	
		this.myCanvasLayer = this.extendCanvasLayer();
		this.canvaslayer = new this.myCanvasLayer();		
		this.canvaslayer.that = this;
		document.getElementById('detabs').appendChild(new CTabBook('pptabs',
			[{hdr:'AIS'
			,content:i.div
			('tabje2'
			,{id:"ppll_tab1"}
			)}
			,{hdr:'Routes',	content:this.ppll_tab_routes}
			,{hdr:'Targets',	content:this.ppll_tab_targets}	//	tab_trainer and localghosts.js fill this with functionality
			,{hdr:'settings',	content:i.table(
				[i.tr(i.td(this._CenterAis))
				,i.tr(i.td(this._MouseAction))
				,i.tr(i.td(this._Prediction))
				,i.tr(i.td(this._History))
				,i.tr(i.td(this._Scale))
				])}
			],{classContainer:'container',classHeader:'tabheader',classContent:'tabcontent'}).node);
			
		this.latlon = new zeroisim.LatLon({latdeg: 0, londeg: 0});
		this.zoom = 12;	//15; //18;
		this.scale = 1/(1000*Math.pow(2,19-this.zoom));
		this.degtorad = Math.PI / 180.0;
		this.radtodeg = 180.0 / Math.PI;
		this.schip = [[-1,2],[1,2],[1,-2],[0,-3],[-1,-2],[-1,2]];
		// this.history = new createRingBuffer(40);
		// this.historypersecond = 1/10;
		this.count = 0;
		this.mdown = false;
		this.panning = false;
		this.fps = 3;	//	todo
		this.shipcentre = true;
		this.aisplot = true;	//	false;
		this.mouselatlon = new zeroisim.LatLon();
		this.minpix = 1852*this.scale;	//	verhouding boogminuut / pixels
		
		this.leafletmap = L.map('leafletmap');
		if (1){
			L.tileLayer(settings.maptiles[0].url,settings.maptiles[0].options).addTo(this.leafletmap);
            L.tileLayer(settings.maptiles[1].url, settings.maptiles[1].options).addTo(this.leafletmap);
			this.canvaslayer.addTo(this.leafletmap);
			} 
		else {
			//	draw NOAA chart, see http://tileservice.charts.noaa.gov/
			// todo first load and parse metadata 
			// var noaamap = '13003_1';
			// var noaamap = '14804_1';	//	New York Harbor
			// var noaamap = '12327';	//	New York Harbor
			//	http://www.charts.noaa.gov/OnLineViewer/12326/TileGroup0/5-4-2.jpg    http://www.charts.noaa.gov/OnLineViewer/12326.shtml
			L.tileLayer(
				// 'http://tileservice.charts.noaa.gov/tiles/'+noaamap+'/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpandmbXliNDBjZWd2M2x6bDk3c2ZtOTkifQ._QA7i5Mpkd_m30IGElHziw'
				'https://tileserver.maptiler.com/noaa/{z}/{x}/{y}.png'
				,	{maxZoom: 14
					,	attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, ' 
						+ '<a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' 
						+ 'Imagery © <a href="http://tileservice.charts.noaa.gov">NOAA</a>'
					, id: 'Tileserver'
					}
				).addTo(this.leafletmap);
			}

		if (0){	//	add navionics bathymetry
			var overlay=new JNC.Leaflet.NavionicsOverlay(	//	todo add JNC, obtain navKey
				{navKey: 'eyJrZXkiOiJOYXZpb25pY3NfaW50ZXJuYWxwdXJwb3NlXzAwMDAxIiwia2V5RG9tYWluIjoid2ViYXBwLm5hdmlvbmljcy5jb20iLCJyZWZlcmVyIjoid2ViYXBwLm5hdmlvbmljcy5jb20iLCJyYW5kb20iOjE1MzQzNjk0MDM2NTF9'
				,chartType: JNC.NAVIONICS_CHARTS.NAUTICAL
				,isTransparent: true
				,zIndex: 1
				});
			}
		//	install event handlers
		this.leafletmap.on('mousemove',this.cbmousemove);
		// this.leafletmap.on('mousemove',this.cbmousemove.bind(this));
		this.leafletmap.on('click',this.cbmouseclick.bind(this));
		// this.leafletmap.on("zoomstart", function (e) { console.log("ZOOMSTART", e); });
		this.leafletmap.on("zoomend", function(e,that) { 
			console.log("ZOOMEND", e.target._zoom); 
			that.zoom = e.target._zoom; //	this is hier iets onverwachts, todo pas leaflet aan zodat een that-pointer meekomt
			if (undefined!=that.cbscaling)	that.cbscaling(that.zoom);
			setValue('sliderscale',e.target._zoom,'value');	//	misschien event handler er af halen?
			setValue('val_sliderscale',e.target._zoom);	
			},this);
		// this.cbshipmove = pp.cbshipmove;this.cbscaling = pp.cbscaling;


		// gizmo stuff
        //wim
        this.dimensions = document.getElementById(this._tabdivname).getBoundingClientRect();
        this.dimensions.w = this.dimensions.width, this.dimensions.h = this.dimensions.height;

        this.gizmo = new CGizmo(this.dimensions.w, this.dimensions.h)
			.onRotate(this.rotateShip.bind(this))
			.onPosition(this.moveShip.bind(this))
			.onExit(this.sendPostionReady.bind(this))
            ;

		zeroisim.replaceNode(document.getElementById('leafletsvg'),this.gizmo.svg());
		
        this.drawMap();


        


        //wim
        //this.tabheaders = [].slice.call(document.querySelectorAll(".tabheader"));
        //this.tabheaders[0].classList.add("active");
        //let mainMenu = this.tabheaders[0].parentNode;
        //let mainMenu_tabheaders = [].slice.call(mainMenu.querySelectorAll(".tabheader"));
        //let last_tabheader = mainMenu_tabheaders[mainMenu_tabheaders.length -1];
        //let dummy_tabheader = document.createElement("div");
        ////dummy_tabheader.innerHTML = "dummy";
        //dummy_tabheader.style.display = "table-cell";
        //dummy_tabheader.style.borderBottom = "1px solid #000";
        //dummy_tabheader.style.width = "10000px";
        //last_tabheader.parentNode.insertBefore(dummy_tabheader, last_tabheader.nextSibling);

        cbRxNmea('GGA', function (rxd, that) {	//	install callback for position		todo kiezen van welke sentence je de latlon wilt (GGA, GLL, ...)
			if (undefined==rxd.data)	return;
			that.ship0.latlon.interpretLatLonGGA(rxd.data);		//	mfinstrument doet dit ook, todo wie vd twee moet dit doen?
			that.draw(that.ship0.hdt,that.ship0.latlon.latdeg,that.ship0.latlon.londeg,that.ship0.rot,that.ship0.sog);	//	todo
			},this);
		cbRxNmea('THS',function(rxd,that){	//	install callback for heading
			if (undefined==rxd.data)	return;
			that.ship0.hdt = parseFloat(rxd.data.hdgT);
			},this);
		cbRxNmea('ROT',function(rxd,that){	//	install callback for heading
			if (undefined==rxd.data)	return;
			that.ship0.rot = parseFloat(rxd.data.rot);
			},this);
		cbRxNmea('RMC',function(rxd,that){	//	install callback for cog
			if (undefined==rxd.data)	return;
			var cog = parseFloat(rxd.data.cog);		if (undefined!=cog) that.ship0.cog = cog;
			var sog = parseFloat(rxd.data.sog);		if (undefined!=sog) that.ship0.sog = sog;
			},this);
		cbRxNmea('VDS',function(rxd,that){	//	
			if (undefined==rxd.data)	return;
			if (undefined==that.vdsold){
				that.vdsold = new zeroisim.LatLon({latdeg: 0, londeg: 0});
				}
			var rot,sog;
			that.latlon.latdeg = rxd.data.lat;
			that.latlon.londeg = rxd.data.lon;
			var bd = that.latlon.beardist(that.vdsold);
			var hdt = bd.b;
			that.vdsold.latdeg = that.latlon.latdeg;
			that.vdsold.londeg = that.latlon.londeg;
			rot = sog = 0;		//	todo bepaal hdt, rot en sog
			that.draw(hdt,that.latlon.latdegmindecnew,that.latlon.londegmindecnew,rot,sog);
			},this);	
		cbRxNmea('MC0',function(rxd,that){
			if ((mc0.giveObj+mc0.response) == rxd.data.req){
				that.currentTarget = rxd.data.de_object;
				var etable = new CeditableTable(that.currentTarget,{id:'target'},undefined,that.parameterEdited.bind(that),'target');
				etable.style['font-size'] = '0.6em';	//	}{style:'font-size:0.6em;'}
				var ships = [];
				simShips.myforEach(function(ship,el,ar,p){
					ships.push({t:ship.name,a:{value:ship.dae.mesh}});
					});

				that.oldCreateObjectNode = zeroisim.replaceNode
					(de_target
					,i.table(
						[i.tr(i.td(i.table(i.tr(
							[i.td(i.button('Create',{click:that.createObject.bind(that),title:'Create object as below.'}),{width:'50%'})
							,i.td(i.button('Cancel',{click:that.cancelCreateObject.bind(that),title:'Cancel creating object.'}),{width:'50%'})
							]),{width:'100%'}),{width:'100%'}))
						// ,i.tr(i.td(etable.objAsTableEditable(that.currentTarget,{id:'target'},undefined,that.parameterEdited.bind(that),'target')))
						// ,i.tr(i.td(i.table(i.tr([i.td('route'),i.td(i.select('select_routes',[' - '].concat(JSON.parse(CRoute.getRoutesLocal()))))]),{width:'100%'})))
						,i.tr(i.td(i.table(i.tr([i.td('route'),i.td(i.select('select_routes',[' - '].concat(CRoute.getRoutesLocal())))]),{width:'100%'})))
						,i.tr(i.td(i.table(i.tr([i.td('ship'),	i.td(i.select('select_ships',	ships))]),{width:'100%'})))
						,i.tr(i.td(etable))
						]));
		 		that.gizmo
					.onRotate(that.rotateObject.bind(that))
					.onPosition(that.moveObject.bind(that))
					.onExit(that.createObject.bind(that))
					;
				var xy = that.leafletmap.latLngToContainerPoint(rxd.data.de_object.position.leaflet);
				that.gizmo.draw(0,xy.x,xy.y);	//	todo maak heading afhankelijk van de richting waarin de muis recent bewoog
				that.raiseZindex(document.getElementById('leafletsvg') //	put svg layer in front
					 ,document.getElementById('leafletmap')
					 );
				}
			},this);
		setValue('posplot_ais','checked',this.aisplot);
		if (this.focus)
			this.onfocus();
		},
	check(evt){
		// console.log('posplot_leaflet.js check');
		switch (evt.currentTarget.id){
			case 'posplot_centre':		this.setShipCentre(evt.currentTarget.checked);	break;
			case 'posplot_ais':			this.setAis(evt.currentTarget.checked);			break;
			}
		},
	rotateShip(heading){
		// sendheading(heading);
		zeroisim.busTX('MC0',{req:mc0.sethdg,hdgt:parseInt(heading)});	//	set heading
		},
	moveShip(x,y){
		var latlon = new zeroisim.LatLon();
		latlon.leaflet = this.leafletmap.containerPointToLatLng([x,y]);
		zeroisim.busTX('MC0',{req:mc0.setpos,latrad:latlon.latrad,lonrad:latlon.lonrad});		//	set position
		latlon = undefined;	//	destruct
		},
	sendPostionReady(heading,x,y){
		// console.log('sendPostionReady()');
		zeroisim.replaceNode(document.getElementById('sendGLL_OK'),this.btnSendGLL);
		this.raiseZindex(document.getElementById('leafletmap') //	put map layer in front
							 ,document.getElementById('leafletsvg')
							 );
		},
	polarToCartesian(centerX, centerY, radius, angleInDegrees) {
		var angleInRadians = (angleInDegrees-90) * Math.PI/180.0;
		return	{ x: centerX + (radius * Math.cos(angleInRadians))
					, y: centerY + (radius * Math.sin(angleInRadians)) 
					};
		},
	svgArc(x, y, radius, startAngle, endAngle, anticlockwise){
		var start= this.polarToCartesian(x, y, radius, endAngle);
		var end	= this.polarToCartesian(x, y, radius, startAngle);
		var arcSweep = (endAngle - startAngle <= 180)? "0" : "1";
		return ["M", start.x, start.y, "A", radius, radius, 0, arcSweep, 0, end.x, end.y].join(" ");
		},
	hRadioPosplotPrediction(evt){
		var test = 0;
		// this.radio = {onclick:"document.querySelector('#posplot_prediction').attributes.posplot_predictionmode.value=this.id;",name:"posplot_predictionsettings_radio"};
		},
	hPredictionTime(value){
		this.canvaslayer.predictiontime = 60*value;
		},
	handleslide(div_slideroutput,cb,evt){	//	that,div_slideroutput,cb){
		//	'val_sliderhistpts'
		//	'val_sliderhistint'
		//	_tabses.posplot.setScale(that.value);
		if (undefined!=div_slideroutput)	setValue(div_slideroutput,evt.target.value);
		if (undefined!=cb)					cb(evt.target.value);
		// if ('sliderscale'==evt.target.id) this.setScale(evt.target.value);
		},
	drawMap: function(){
		if (!this.focus)	return;
		//	todo hier checken welke map getekend moet worden aan de hand van de radiobuttons
		//	console.log("drawMap()", this.zoom); 
		this.leafletmap.setView([this.latlon.latdeg,this.latlon.londeg],this.zoom);
		},
	loadOSM: function(that){	//	todo wim gaat dit fixen
		if (!(that.id=='div_posplot_chart_osm' && that.checked)) return;
		return;
		},
	cbshipmove:	function(latlon){	//	callback function upon shipmove
		// console.log('cbshipmove');
		setValue('div_latitudegll_ship',latlon.latgll + latlon.latsgn + '; ' + latlon.longll + latlon.lonsgn);
		setValue('div_latitude_ship',latlon.latdegmindec + '; ' + latlon.londegmindec);
		// object.style.zIndex="-1"	//	put svg layer in front
		},
	cbmousemove: function(evt){	//	callback function upon mousemove
		// console.log('posplot_leaflet.js cbmousemove');
		var test = this;
		var that = evt.originalEvent.currentTarget.that;
		var latlon = new zeroisim.LatLon();
		latlon.leaflet = evt.latlng;
		setValue('div_latitudegll',latlon.latgll + latlon.latsgn + '; ' + latlon.longll + latlon.lonsgn);
		setValue('div_latitude',latlon.latdegmindec + '; ' + latlon.londegmindec);
		var bd = latlon.beardist(that.latlon);	//	calculate bearing and distance from ship to mouse
		setValue('div_beardist', bd.b + h.deg +' / ' + bd.d + "'");
		latlon = undefined;	//	destructor
		},
	cbmouseclick: function(evt){	//	callback function upon mouseclick 
		var latlon = new zeroisim.LatLon();
		latlon.leaflet = evt.latlng;
		console.log('posplot_leaflet.js cbmouseclick: '+latlon.latgll + latlon.latsgn + '; ' + latlon.longll + latlon.lonsgn);
		switch (document.querySelector('#posplot_mouseaction').attributes.posplot_mode.value){	//	query mouseaction checkbox what to do
			case 'div_posplot_mode_sendgll':
				// set SVG on top to show gizmo, after acknowledge, put a mc0.setpos on the busTX
				var xy = this.leafletmap.latLngToContainerPoint(latlon.leaflet);
				this.gizmo.draw(this.course,xy.x,xy.y);
				this.raiseZindex(document.getElementById('leafletsvg') //	put svg layer in front
									 ,document.getElementById('leafletmap')
									 );
				zeroisim.replaceNode(document.getElementById('sendGLL_OK'),this.btnSendGLLOK);
				zeroisim.busTX('MC0',{req:mc0.setpos,latrad:latlon.latrad,lonrad:latlon.lonrad});		//	set position
				break;
			case 'div_posplot_mode_apsetpoint':	//	should maybe be HDG mode setpoint
				var bd = latlon.beardist(evt.originalEvent.currentTarget.that.latlon);	//	calculate bearing and distance from ship to mouse
				//	ap.setpoint.set(Math.floor(bd.b)); evil, niet direct tegen de ap praten, daar hebben we nmea voor
				var order = {Setpoint:Math.floor(bd.b),AutoPilotMode:'HDG'};
				zeroisim.busTX('AEC',order);
				break;
			case 'div_posplot_mode_pan':		break;
			case 'div_posplot_mode_select':	break;
			case 'div_posplot_mode_route_new':	
				this.route = undefined;
		 		document.getElementById('div_posplot_mode_route').click();	//	set in appendmode
				//	fall through intended
			case 'div_posplot_mode_route':	
				document.querySelector('#de_route').disabled = false;
				if (undefined==this.route){
					this.route = new CRoute();	//	deTrainer is set in tab_trainer
					}
				this.route.addWaypoint(latlon);
				this.route.asTable('de_route');	//	mc0.createdWaypoint
				this.canvaslayer.render();
				break;
			case 'div_posplot_mode_target':	
				console.log('create a target in this position: '+latlon.latdegmindec +', '+latlon.londegmindec);
				var de_target = document.querySelector('#de_target');
				de_target.position = latlon;
				de_target.disabled = false;
				//	todo render them and create a fidget to alter heading speed, maybe properties like name and such in editable table
				zeroisim.busTX('MC0',{req:mc0.giveObj,position:latlon});
				break;
			case 'div_posplot_mode_radarrefl':
				console.log('radar reflectiveness tool not implemented yet');
				break;
			}
		latlon = undefined;	//	destructor
		},
	raiseZindex(up,down){
		var hig = Math.max(up.style.zIndex,down.style.zIndex);
		var low = Math.min(up.style.zIndex,down.style.zIndex);
		up.style.zIndex = hig;
		down.style.zIndex = low;
		},	
	parameterEdited(a,b,c){
		console.log('posPlot:parameterEdited('+a+','+b+','+c+')');
		this.currentTarget[b] = c;
		var pos = new zeroisim.LatLon(this.currentTarget.position);
		var xy = this.leafletmap.latLngToContainerPoint(pos.leaflet);
		this.gizmo.draw(this.currentTarget.hdg,xy.x,xy.y);	//	update gizmo
		pos = undefined;
		},
	rotateObject(hdg){	//	console.log('rotateObject('+hdg+')');
		hdg = Math.round(hdg);
		// var de_target = document.querySelector('#de_target');
		setValue('hdg',hdg,'value');
		this.currentTarget.hdg = hdg;
		},
	moveObject(x,y){
		if (0){
			var latlon = new zeroisim.LatLon();
			latlon.leaflet = this.leafletmap.containerPointToLatLng([x,y]);
			setValue('position',JSON.stringify({latdeg:latlon.latdeg,londeg:latlon.londeg}),'value');	//	kennelijk niet begrepen door editabletable
			this.currentTarget.position.latdeg = latlon.latdeg;
			this.currentTarget.position.londeg = latlon.londeg;
			latlon = undefined;	//	destruct
			}
		else{
			this.currentTarget.position.leaflet = this.leafletmap.containerPointToLatLng([x,y]);
			setValue('position',JSON.stringify({latdeg:this.currentTarget.position.latdeg,londeg:this.currentTarget.position.londeg}),'value');	//	kennelijk niet begrepen door editabletable
			}
		},
	cancelCreateObject(){
		zeroisim.replaceNode(de_target,this.oldCreateObjectNode);
		this.raiseZindex(document.getElementById('leafletmap') //	put map layer in front
							 ,document.getElementById('leafletsvg')
							 );
		},
	createObject(a,b,c){
		console.log('posPlot:createObject');
		this.currentTarget.route = new CRoute(getValue('select_routes'));
		this.currentTarget.route.loadLocal();
		this.currentTarget.mesh = getValue('select_ships');
		zeroisim.busTX('MC0',{req:mc0.createObj,obj:this.currentTarget});	//	send a busmsg to create this object
		// or ask editabletable for values
		this.cancelCreateObject();
		},
	draw: function(course,lat,lon,rot,sog) {
		if (undefined==lat)	return;
		if (undefined==lon)	return;
		if (undefined==this.latlon)	return;	//	nog geen init2() gedaan
		this.course = course;
		this.rot = rot;
		this.sog = sog;
		this.latlon.latdeg = lat;	
		this.latlon.londeg = lon;
		// this.canvaslayer.that = this;
		
		if (this.shipcentre==true){
			this.drawMap();
			}
		else {	
			this.plotpos(course,this.latlon,this.rot,this.sog);
			}
		if (undefined!=this.cbshipmove)	this.cbshipmove(this.latlon);
		},
	// plotpos: function(course,latlon,rot,sog,count){
	plotpos: function(){
		if (!this.shipcentre){
			this.canvaslayer.render();
			}
		},
	lines: function(){
		var upleft = this.osm.xy2pos(new zeroisim.LatLon(),{x:0,y:0});
		var dnright = this.osm.xy2pos(new zeroisim.LatLon(),{x: 2*this.midden.x, y: 2*this.midden.y});
		var minutes_standing = 180.0*60.0/Math.PI*(upleft.latrad - dnright.latrad);
		var minutes_laying = 180.0*60.0/Math.PI*(dnright.lonrad - upleft.lonrad);
		var metres_standing = 1852*minutes_standing;
		var metres_laying = 1852*minutes_laying;
		//var rims = {up:,right:,down:,left:};
		},			
	windowToCanvas: function(x,y) {
		var bbox = this.canvas.getBoundingClientRect();
		return { x: x - bbox.left * (this.canvas.width  / bbox.width),
					y: y - bbox.top  * (this.canvas.height / bbox.height)
				 };
		},
	setScale: function(invscale){
		//	invscale = googlemaps zoom, or openstreetmap http://wiki.openstreetmap.org/wiki/Zoom_levels
		// 0 <=invscale<=19
		// width of the map in degrees=360/(2^z)
		// scale=1/(1000*2^(19-z))
		if (0){
			invscale = Math.round(Math.pow(10,invscale/10));
			this.scale = 1/invscale;
			this.minpix = 1852*this.scale;	//	verhouding boogminuut / pixels
			if (undefined!=this.cbscaling)	this.cbscaling('1:' + this.minpix);
			}
		else {	//	new code
			this.zoom = invscale;
			this.scale = 1/(1000*Math.pow(2,19-this.zoom));
			this.minpix = 1852/this.scale;	//	verhouding boogminuut / pixels
			if (undefined!=this.cbscaling)	this.cbscaling('1:' + 1/this.scale + ' ['+this.zoom+']');
	/**/	this.drawMap();	
			}	
	//	this.plotpos(this.course,{lat: this.lat,lon: this.lon},1);	
		// this.plotpos(this.course,this.latlon,1);	
		},
	setShipCentre: function(tf){
		console.log('setShipCentre');
		this.shipcentre = tf;
		},
	setAis: function(tf){
		console.log('setAis');
		this.aisplot = tf;
		},	
	equals: function(a,b,delta){
		if (Math.sign(a)!=Math.sign(b))		return false;
		if (Math.abs(a+delta)<Math.abs(b))	return false;
		if (Math.abs(a-delta)>Math.abs(b))	return false;
		return true;
		},
	test: function(){
		//	latlon2xy() en xy2latlon() moeten korrekte waarden geven
		var x = 150; var y = 250; var delta = 0.000000001
		var latlon = new zeroisim.LatLon();
		latlon = this.xy2latlon(latlon,{x: x, y: y});
		var newxy = this.latlon2xy(latlon);
		if (this.equals(newxy.x,x,delta)){
			if (this.equals(newxy.y,y,delta)){
				//	now other way around
				var oldxy = this.latlon2xy(latlon);
				var newlatlon = new zeroisim.LatLon(); 
				newlatlon = this.xy2latlon(newlatlon,{x: oldxy.x, y: oldxy.y});
				if (this.equals(newlatlon._latrad,latlon._latrad,delta)){
					if (this.equals(newlatlon._lonrad,latlon._lonrad,delta)){
						return true;
						}
					}
				}
        }
		return false;	
		},
//	}
	extendCanvasLayer:function(){
	
var myCanvasLayer = L.CanvasLayer.extend({	//	'../includes/leaflet/canvaslayer/leaflet_canvas_layer.js'
	firsttime: true,
	latlon: new zeroisim.LatLon({latdeg: 0, londeg: 0}),
	degtorad: Math.PI / 180.0,
	radtodeg: 180.0 / Math.PI,
	schip: [[-1,2],[1,2],[1,-2],[0,-3],[-1,-2],[-1,2]],
	aisschip: [[-1,2],[1,2],[0,-3],[-1,2]],
	waypoint: [[0,-1],[-0.59,0.81],[0.95,-0.31],[-0.95,-0.31],[0.59,0.81]],
	that: undefined
	,first: function() {
		this.canvas = this.getCanvas();
		this.firsttime = false;
		this.scale = 1/(1000*Math.pow(2,19-this._map._zoom));
		this.wind = 305;
		this.history = new RingBuffer(40);
		this.historypersecond = 1/10;	//	todo configurable by user interface
		this.predictiontime = 60;
		}
	,renderOwnShip: function(){
		if (1) {
			//	plot own ship
			var schaal = 0.20*this.scale;	//	size of the schip figure
			var xy = this._map.latLngToContainerPoint(this.that.latlon.leaflet);	// get center from the map (projected)
			this.drawShape(xy,this.that.course*this.degtorad,schaal,this.schip,'#00F',4)
		//	this.prediction(xy,this.that.sog,this.that.rot,this.that.course*this.degtorad,60);	//	plot prediction
			this.prediction(xy,this.that.sog,this.that.rot,this.that.course*this.degtorad,this.predictiontime);	//	plot prediction
			}
		else {
			//	plot VDS targets
			this.drawpng({lat:this.that.latlon.latdeg,lon:this.that.latlon.londeg},  this.that.course, this.wind);
			}
		}
	,renderRoute(route){
		var routeshape = [];	//	preserve canvas coordinates of waypoints to draw the route between waypoints
		route.doForRoute(function(waypoint,idx_waypoint,theroute,that){
			var xy = that._map.latLngToContainerPoint(waypoint.leaflet);
			that.drawShape(xy,0,0.20*that.scale,that.waypoint);	//	draw waypoint shape
			routeshape.push(xy);
			},this);
		this.drawShapeSimple(routeshape);	//	draw lines between waypoints
		}
	,render: function() {
		if (this.firsttime){
			this.first();
			}
		// this._ctx.save();
		this._ctx.clearRect(0, 0, this._canvas.width, this._canvas.height);
		var straal=6;
		this.scale = 1/(1000*Math.pow(2,19-this._map._zoom));
		this.metresPerPixel = 40075016.686 * Math.abs(Math.cos(this._map.getCenter().lat * this.degtorad)) / Math.pow(2, this._map.getZoom()+8);
		this.renderOwnShip();
		/* test
		// this.drawpng({lat:0,lon:0}, this.zeilkoers--, wind);
		this.drawpng({lat:0,lon:0},  0, this.wind);
		this.drawpng({lat:0,lon:0}, 45, this.wind);
		this.drawpng({lat:0,lon:0}, 90, this.wind);
		this.drawpng({lat:0,lon:0},135, this.wind);
		this.drawpng({lat:0,lon:0},180, this.wind);
		this.drawpng({lat:0,lon:0},226, this.wind);
		this.drawpng({lat:0,lon:0},270, this.wind);
		this.drawpng({lat:0,lon:0},315, this.wind);
		*/
		// plot AIS targets
		var schaal = 0.20*this.scale;	//	size of the schip figure
		if (undefined!=aislist){
			if (aislist && this.that.aisplot){	//	if user wants ais targets being plotted
				for (var ii in aislist){		
					var aistarget = aislist[ii];
					if (undefined!=aistarget.Latitude){	//	maw wacht op een VDM msg:5
						if (!zeroisim.isFn(aistarget) && (aistarget.ownship!=true)){	//	todo switchable
							var ll = new L.LatLng(aistarget.Latitude,aistarget.Longitude);
							var xy = this._map.latLngToContainerPoint(ll);	// get center from the map (projected)
							this.drawShape(xy,aistarget.COG*this.degtorad,schaal,this.aisschip)
							// this.prediction(xy,aistarget.SOG,aistarget.ROT,aistarget.COG*this.degtorad,60);	//	plot prediction
							this.prediction(xy,aistarget.SOG,aistarget.ROT,aistarget.COG*this.degtorad,this.predictiontime);	//	plot prediction
							// delete ll;	todo dit moet waarschijnlijk wel 
							}
						}
					}
				}
			}
		//	draw my history
		if (1){
			// this.history.foreach(function(ship,that){
				// that.plotpos(ship[0],{latdeg:ship[1],londeg:ship[2]});
				// },this);
			}
			
		// this._ctx.restore();
		// this.redraw();	//	doe dit als je een animation wilt
		if (undefined!=this.that.route){	// plot current route
			this.renderRoute(this.that.route);
			}
		}
	,drawShape: function(xy,course,schaal,shape,color,linewidth){
		if (undefined==color) color = '#000';
		if (undefined==linewidth) linewidth = 3;
		var sinfi = Math.sin(course);
		var cosfi = Math.cos(course);
		this._ctx.strokeStyle = color;
		this._ctx.lineWidth = linewidth;
		this._ctx.beginPath();
		if (schaal<5 && schaal!=0)	schaal = 5;
		var lijn;
		for (var idxlijn in shape) {
			lijn = shape[idxlijn];
			var rotat =
				{x: lijn[0]*cosfi - lijn[1]*sinfi
				,y: lijn[0]*sinfi + lijn[1]*cosfi};
			this._ctx.lineTo( xy.x + schaal * rotat.x, xy.y + schaal * rotat.y );
			}
		this._ctx.closePath();		this._ctx.stroke();
		},
	drawShapeSimple: function(shape,color,linewidth){
		if (undefined==color) color = '#000';
		if (undefined==linewidth) linewidth = 3;
		this._ctx.strokeStyle = color;
		this._ctx.lineWidth = linewidth;
		this._ctx.beginPath();
		var punt;
		for (var idxlijn in shape) {
			punt = shape[idxlijn];
			this._ctx.lineTo( punt.x, punt.y );
			}
		this._ctx.closePath();		this._ctx.stroke();
		},
	prediction: function(xy,sog,rot,course,tpredict){
		// todo nu pak je het middelpunt van de cirkel dwars op het schip. Beter is om hem loodrecht op de vaart door het water vektor te zetten.
		if (undefined!=rot && undefined!=sog){
			var delta = 0.01;
			var sog = sog*1852/3600;	//	m/s
			// var metresPerPixel = 40075016.686 * Math.abs(Math.cos(this._map.getCenter().lat * this.degtorad)) / Math.pow(2, this._map.getZoom()+8);
			this._ctx.beginPath();
			if (Math.abs(rot)<delta){	//	schip draait niet, teken gewoon een rechte lijn
				var pixels = sog * tpredict / this.metresPerPixel;
				this._ctx.moveTo(xy.x,xy.y);
				this._ctx.lineTo(xy.x+pixels*Math.sin(course),xy.y-pixels*Math.cos(course));
				}				
			else {	
				//	todo hieronder misschien cog gebruiken ipv hdg want boot slipt nogal door het water. Anders corrigeren voor de slip BWK
				var rot = rot*this.degtorad/60;	//	rad/s
				var rr = Math.abs(sog) / rot;					//	meters, straal van de gebogen prediction lijn
				rr = rr / this.metresPerPixel;			//	verhouding boogminuut / pixels
				var m = {x:xy.x+rr*Math.cos(course),y:xy.y+rr*Math.sin(course)};	//	middelpunt van cirkelvormige prediction lijn
				if (rot>0){	
					this._ctx.arc(m.x,m.y,Math.abs(rr),course+Math.PI,course+Math.PI+tpredict*rot);
					}
				else{
					this._ctx.arc(m.x,m.y,Math.abs(rr),course+tpredict*rot,course);
					}				
				}				
			}				
		this._ctx.stroke();
		},	
	drawpng: function(pos,hdg,wind){
		sailboats = 
			{  '0':{name: 'sailboat2.png',			midx:  279}	//	330 - 030
			, '45':{name: 'sailboat_adw_sb.png',	midx:  57}	//	030 - 067
			, '90':{name: 'sailboat_hw_sb.png',		midx:  59}	//	067 - 112
			,'135':{name: 'sailboat_rw_sb.png',		midx:  58}	//	112 - 156
			,'179':{name: 'sailboat_vdw_sb.png',	midx: 151}	//	156 - 180
			,'181':{name: 'sailboat_vdw_bb.png',	midx: 235}	//	180 - 205
			,'225':{name: 'sailboat_rw_bb.png',		midx: 227}	//	205 - 247
			,'270':{name: 'sailboat_hw_bb.png',		midx: 157}	//	247 - 292
			,'315':{name: 'sailboat_adw_bb.png',	midx:  65}	//	292 - 330
			};
		hdg%=360;	
		if (hdg<0) hdg+=360;	
		var windkoers = hdg-wind;	if (windkoers<0)	windkoers+=360;
		var sailboat = sailboats['0'];
		if			(windkoers>340 && windkoers<= 20)	sailboat = sailboats[  '0'];
		else if	(windkoers> 30 && windkoers<= 67)	sailboat = sailboats[ '45'];
		else if	(windkoers> 67 && windkoers<=112)	sailboat = sailboats[ '90'];
		else if	(windkoers>112 && windkoers<=156)	sailboat = sailboats['135'];
		else if	(windkoers>156 && windkoers<=180)	sailboat = sailboats['179'];
		else if	(windkoers>180 && windkoers<=205)	sailboat = sailboats['181'];
		else if	(windkoers>205 && windkoers<=247)	sailboat = sailboats['225'];
		else if	(windkoers>247 && windkoers<=292)	sailboat = sailboats['270'];
		else if	(windkoers>292 && windkoers<=330)	sailboat = sailboats['315'];
		
		var xy = this._map.latLngToContainerPoint(new L.LatLng(pos.lat,pos.lon));	// get center from the map (projected)
		var drawing = new Image();
		drawing.things = {ctx: this._ctx, xy: xy, hdg: hdg, wind: wind, degtorad: this.degtorad, midx:sailboat.midx};
		drawing.onload = function(evt) {
			var ctx = evt.currentTarget.things.ctx;
			ctx.save();
			ctx.translate(evt.currentTarget.things.xy.x-20,evt.currentTarget.things.xy.y);	//	todo -20 is wonderlijke offset
			ctx.rotate(evt.currentTarget.things.hdg*evt.currentTarget.things.degtorad);
			ctx.scale(0.4,0.4);
			ctx.drawImage(drawing,-evt.currentTarget.things.midx,0);	//	neus van het bootje op de gewenste coordinaat
			// ctx.drawImage(drawing,-evt.currentTarget.things.midx,-H/2);	//	middelpunt van het bootje op de gewenste coordinaat
			ctx.restore();
			};
		drawing.src = './img/'+sailboat.name; 
		},		
	});
		return myCanvasLayer;
		}	//	extendCanvasLayer
	}	//	posplotobj

var createPosplot = function () {
	var createRingBuffer = function(length){
		var pointer = 0, buffer = [];
		return {
			get: function(key){
				if (key < 0)				return buffer[pointer+key];
				else if (key === false)	return buffer[pointer-1];
				else 							return buffer[key];
				},
			push: function(item){
				buffer[pointer] = item;
				pointer = (pointer +1) % length;
				},
			length: function(){
				return buffer.length;
				},
			foreach: function(cb,obj){
				for (var ptr=0; ptr<buffer.length; ptr++){
					cb(buffer[ptr],obj);
					}
				}
			};
		};
    return {};

	}
var myLayer = function(){
	this.addTo = function(map){}
	}		
var myOwnLeaflet = function(){
	this.degtorad = Math.PI / 180.0;
	this.radtodeg = 180.0 / Math.PI;
	this.map = function(idcanvas){
		this.canvas = document.getElementById(idcanvas);
		this.setView = function(pos,zoom){
			var lat = pos[0];
			var lon = pos[1];
			}
		this.latLngToContainerPoint = function(latlon){
			return {x:100, y:100};	//	todo hier de echte berekening zetten
			}
		this.containerPointToLatLng = function(xy){
			return {lat: 54, lng: 4};	//	todo hier de echte berekening zetten
			}	
		return this;
		}
	this.tileLayer = function(url,attr){
		return new myLayer();
		}	
	this.point = function(x,y){
		return [x,y];
		}
	this.latLng = function(latdeg,londeg){
		return [this.degtorad*latdeg,this.degtorad*londeg];
		}
	}
		
	if (undefined==L){
		var L = new myOwnLeaflet();
		}
	