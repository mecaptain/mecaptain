// ibutton_html.js
'use strict';

class CInstrument_ButtonHtml extends CInstrument {
	constructor(obj,exposedParms,active,idx){
		super(idx,obj);
		if (undefined!=exposedParms)
			this.exposedParms = exposedParms;
		else
			this.exposedParms =
				{latched:0
				,startPushed:	0
				,txtReleased:	'push<br/>me'
				,txtPushed:		'release<br/>me'
				,onPush:			'horn ON'
				,onRelease:		'horn OFF'
				,colorBackground:	'rgba(255,255,255,0.0)'	//	gray
				,colorModeKnobOff:'#EEE'
				,colorModeKnobOn:	'#0F0'
				};
		this.active = (undefined==active)? false: active;
		if (active){
			this.button = i.button(this.exposedParms.txtReleased,
				{class:'tg1'
				,mousedown:	this.buttonDown.bind(this)
				,mouseup:	this.buttonUp.bind(this)
				,style:'border-radius:20px;height:100%;width:100%;border:2px solid #999999;unselectable:on;unselectable: on;-webkit-user-select: none;-moz-user-select: none;-ms-user-select: none;user-select: none;'
				});
			this.button.style.color = this.exposedParms.colorModeKnobOff;	
			this.button.setAttribute('_down',false);
			if ('0'!=this.exposedParms.startPushed){
				this.buttonDown();
				}
			this.draw();
			}
		}
	buttonDown(evt){	
		console.log('buttonDown(evt):' + this.exposedParms.onPush);
		this.button.style.color = this.exposedParms.colorModeKnobOn;	//	zet lichtje in gekozen knop aan
		this.button.innerHTML = this.exposedParms.txtPushed;
		if ('0'!=this.exposedParms.latched){
			var down = this.button.getAttribute('_down');
			if ('true'==down){
				this.button.setAttribute('_down',false);
				this.buttonUp(evt);
				}
			else
				this.button.setAttribute('_down',true);
			}
		//		ws_talk_listen.sendNMEA(ship0.remote_protocol+'://'+ship0.remote_ip+':'+settings.talk_port,buildNMEA('TRC',1,1));
		}
	buttonUp(evt){	
		if ('0'!=this.exposedParms.latched){
			if ('true'==this.button.getAttribute('_down'))	return; // ignore
			}
		console.log('buttonUp(evt):' + this.exposedParms.onRelease);
		this.button.style.color = this.exposedParms.colorModeKnobOff;	//	zet lichtje in gekozen knop aan
		this.button.innerHTML = this.exposedParms.txtReleased;
		//		ws_talk_listen.sendNMEA(ship0.remote_protocol+'://'+ship0.remote_ip+':'+settings.talk_port,buildNMEA('TRC',1,1));
		}
	draw(){
		if (this.active!=true){
			zeroisim.replaceNode(this.div,i.span(i.center('BUTTON<br />html')));
			return;
			}
		zeroisim.replaceNode(this.div,this.button);
		}
	}
CInstrument.addInstrumentToSettings('./ibutton_html.js',			'button',		'div',		CInstrument_ButtonHtml,	'Button, you can assign an action to it');
