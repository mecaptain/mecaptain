//	gizmo.js
//	svg graphic for positioning and directioning

'use strict';

class CGizmo {
	constructor(width,height,settings){
		settings = settings || {};
		this.detectBrowser();

		this.nodeHeadingSettings = 
			{radius:			this.or(settings.hdgRadius,80)
			,startAngle:	this.or(settings.hdgStartAngle,-40)
			,endAngle:		this.or(settings.hdgEndAngle,40)
			
			,stroke:			this.or(settings.hdgStrokeColor,'#f00')
			,strokewidtht:	this.or(settings.hdgStrokeWidth,50)
			,strokeopacity:this.or(settings.hdgStrokeOpacity,.6)

			};
		// this.nodeSVG = i.svg('',{id:'mysvg',class:'gizmosvg',style:'position:absolute;width:'+width+'px;height:'+height+'px;left:0;top:0;border:0px;'});
        //wim this.nodeSVG = i.svg('', { id: 'mysvg', class: 'gizmosvg', style: 'position:relative;width:' + width + 'px;height:' + height + 'px;left:0;top:0;border:0px;' });
        this.nodeSVG = i.svg('', { id: 'mysvg', class: 'gizmosvg', style: 'position:absolute;width:100%;height:100%;left:0;top:0;border:0px;' });
		this.nodeHeading = i.path('',{id:'gizmopath',class:'arc',d:'M 0 0 A 0 0 0 0 0 0 0',fill:'none',stroke:this.nodeHeadingSettings.stroke,'stroke-width':this.nodeHeadingSettings.strokewidtht,'stroke-opacity':this.nodeHeadingSettings.strokeopacity});
		this.nodePosition = i.circle('',{id:"gizmocircle",cx:50,cy:50,r:this.or(settings.posStrokeRadius,40),stroke:"black",'stroke-width':0,fill:this.or(settings.posStrokeColor,'#f00'),'fill-opacity':this.or(settings.posStrokeOpacity,0.6)});
		this.nodeText = i.text('000'+h.deg,{id:"gizmotxt",x:0,y:0,fill:"#F00",selectable:"off"});
		this.nodeTextOK = i.text('OK',{id:"gizmotxtok",x:0,y:0,fill:"#00F",selectable:"off"});
		this.nodeLine = i.line('',{id:'gizmoline',x1:0,y1:-140,x2:0,y2:-300,stroke:'#f00','stroke-width':2,'stroke-opacity':.6});
		this.nodeExit = i.rect('',{width:50,height:40,fill:'#0E0','stroke-width':1,stroke:'#0E0','fill-opacity':0.5});
		
		this.nodeSVG.appendChild(this.nodeHeading);
		this.nodeSVG.appendChild(this.nodePosition);
		this.nodeSVG.appendChild(this.nodeText);
		this.nodeSVG.appendChild(this.nodeLine);
		this.nodeSVG.appendChild(this.nodeExit);
		this.nodeSVG.appendChild(this.nodeTextOK);	//	clearer text on top but now needs click handler too
		
		this.nodeHeading.addEventListener("mouseover",this.mouseOverGizmoRotation.bind(this));
		this._mouseDownGizmoRotation	= this.mouseDownGizmoRotation.bind(this);
		this._mouseMoveGizmoRotation	= this.mouseMoveGizmoRotation.bind(this);
		this._mouseUpGizmoRotation		= this.mouseUpGizmoRotation.bind(this);

		this.nodePosition.addEventListener("mouseover",this.mouseOverGizmoPosition.bind(this));
		this._mouseDownGizmoPosition	= this.mouseDownGizmoPosition.bind(this);
		this._mouseMoveGizmoPosition	= this.mouseMoveGizmoPosition.bind(this);
		this._mouseUpGizmoPosition		= this.mouseUpGizmoPosition.bind(this);
		
		this.nodeExit.addEventListener("click",this.mouseClickExit.bind(this));
		this.nodeTextOK.addEventListener("click",this.mouseClickExit.bind(this));
		
		this.rotation = 0;
		this.draw(this.rotation,width/2,height/2);
		return this;
		}
	svg(){
		return this.nodeSVG;
		}
	or(firstOption,secondOption){
		return (undefined!=firstOption)? firstOption: secondOption;
		}
	detectBrowser(){
		if (navigator.userAgent.indexOf('Edge') != -1){//MS Edge
			this.browser = 'edge';
			}
		else if (navigator.userAgent.indexOf('Firefox') != -1 && parseFloat(navigator.userAgent.substring(navigator.userAgent.indexOf('Firefox') + 8)) >= 3.6){//Firefox
			this.browser = 'ff';
			}
		else if (navigator.userAgent.indexOf('Chrome') != -1 && parseFloat(navigator.userAgent.substring(navigator.userAgent.indexOf('Chrome') + 7).split(' ')[0]) >= 15){//Chrome
			this.browser = 'chrome';
			}
		else if(navigator.userAgent.indexOf('Safari') != -1 && navigator.userAgent.indexOf('Version') != -1 && parseFloat(navigator.userAgent.substring(navigator.userAgent.indexOf('Version') + 8).split(' ')[0]) >= 5){//Safari
			this.browser = 'safari';
			}
		else {
			console.log('unsupported browser');
			}
		}
	polarToCartesian(centerX, centerY, radius, angleInDegrees) {
		var angleInRadians = (angleInDegrees-90) * Math.PI/180.0;
		return	{ x: centerX + (radius * Math.cos(angleInRadians))
					, y: centerY + (radius * Math.sin(angleInRadians)) 
					};
		}
	svgArc(x, y, radius, startAngle, endAngle, anticlockwise){
		var start= this.polarToCartesian(x, y, radius, endAngle);
		var end	= this.polarToCartesian(x, y, radius, startAngle);
		var arcSweep = (endAngle - startAngle <= 180)? "0" : "1";
		return ["M", start.x, start.y, "A", radius, radius, 0, arcSweep, 0, end.x, end.y].join(" ");
		}
	draw(direction,x,y){
		console.log(`CGizmo.draw(${direction},${x},${y})`);
		this.xy = {x:x,y:y};
		this.rotation = direction;
		// this.nodeHeading.setAttribute("d", this.svgArc(x, y, 80, -40, 40));
		this.nodeHeading.setAttribute("d", this.svgArc(x, y, this.nodeHeadingSettings.radius, this.nodeHeadingSettings.startAngle, this.nodeHeadingSettings.endAngle));
		this.nodePosition.setAttribute("cx",x);	this.nodePosition.setAttribute("cy",y);
		this.nodeLine.setAttribute("x1",x);			this.nodeLine.setAttribute("y1",y-140);
		this.nodeLine.setAttribute("x2",x);			this.nodeLine.setAttribute("y2",y-300);
		return this.rotate(this.rotation);
		}
	rotate(direction){
		this.rotation = direction;
		var transform = "rotate(" + direction + ", "+this.xy.x+", "+this.xy.y+")";
		this.nodeHeading.setAttribute("transform", transform);
		this.nodeLine.setAttribute("transform", transform);
		
		//	draw text
		if (this.rotation<  0) this.rotation+=360;
		if (this.rotation>360) this.rotation%=360;
		var wh = {w:14,h:-7};
		var xy = this.polarToCartesian(this.xy.x-wh.w, this.xy.y-wh.h, 120, this.rotation);
		this.nodeText.setAttribute("x",xy.x);			this.nodeText.setAttribute("y",xy.y);
		var test = 0.5+parseFloat(this.rotation);
		test = Math.floor(test).toString()+h.deg;
		test = test.toString();
		test = test+h.deg;
		this.nodeText.textContent = Math.floor(0.5+parseFloat(this.rotation)).toString()+h.deg;

		//	draw commit rectangle
		wh = {w:this.nodeExit.getAttribute('width')/2,h:this.nodeExit.getAttribute('height')/2};
		xy = this.polarToCartesian(this.xy.x-wh.w, this.xy.y-wh.h, -85, this.rotation);
		this.nodeExit.setAttribute("x",xy.x);			this.nodeExit.setAttribute("y",xy.y);
		this.nodeTextOK.setAttribute("x",xy.x+14);	this.nodeTextOK.setAttribute("y",xy.y+26);
		
		return this;
		}
	onRotate(cb){
		this.cb_onRotate = cb;
		return this;
		}
	onExit(cb){
		this.cb_onExit = cb;
		return this;
		}
	mouseClickExit(evt){
		this.mouseUpGizmoRotation(evt);		// uninstall event handlers
		this.mouseUpGizmoPosition(evt);		// uninstall event handlers
		if (undefined!=this.cb_onExit)
			this.cb_onExit(this.rotation,this.xy.x,this.xy.y);
		}
	mouseOverGizmoRotation(evt){		// console.log('mouseOverGizmoRotation');
		this.nodeHeading.addEventListener("mousedown", this._mouseDownGizmoRotation, false);
		}
	mouseDownGizmoRotation(evt){		// console.log('mouseDownGizmoRotation');
		this.nodeHeading.addEventListener("mousemove", this._mouseMoveGizmoRotation, false);
		this.nodeHeading.addEventListener("mouseup", this._mouseUpGizmoRotation, false);
		}
	mouseUpGizmoRotation(evt){		// console.log('mouseDownGizmoRotation');
		this.nodeHeading.removeEventListener("mousemove", this._mouseMoveGizmoRotation, false);
		this.nodeHeading.removeEventListener("mouseup", this._mouseUpGizmoRotation, false);
		}
	mouseMoveGizmoRotation(evt){
		if (undefined!=this.cb_onRotate) {	//	if no callback, do not rotate
			var XY = {X:evt.offsetX,Y:evt.offsetY};		
			switch (this.browser){
				case 'ff':
					// todo dit is nog niet goed
					var XY = {X:evt.offsetY,Y:evt.offsetX};		
					var test = 
						{p0:	this.xy.y - XY.Y
						,p1:	XY.X - this.xy.x
						};
					test = (180 / Math.PI) * Math.atan2
						(XY.Y - this.xy.y
						,XY.X - this.xy.x
						);
					var angle = 90 + (180 / Math.PI) * Math.atan2
						(this.xy.y - XY.Y
						,XY.X - this.xy.x
						);
					break;
				case 'edge':
				case 'safari':
					var angle = 90 + (180 / Math.PI) * Math.atan2
						(XY.Y - this.xy.y
						,XY.X - this.xy.x
						)+this.rotation;
					break;
				case 'chrome':
					var angle = 90 + (180 / Math.PI) * Math.atan2
						(XY.Y - this.xy.y
						,XY.X - this.xy.x
						);
					break;
				}
			this.rotate(angle);
			this.cb_onRotate(this.rotation);
			}
		}
	onPosition(cb){
		this.cb_onPosition = cb;
		return this;
		}
	mouseOverGizmoPosition(evt){		// console.log('mouseOverGizmoPosition');
		this.nodePosition.addEventListener("mousedown", this._mouseDownGizmoPosition, false);
		}	
	mouseDownGizmoPosition(evt){		// console.log('mouseDownGizmoPosition');
		this.nodePosition.addEventListener("mousemove", this._mouseMoveGizmoPosition, false);
		this.nodePosition.addEventListener("mouseup", this._mouseUpGizmoPosition, false);
		}
	mouseUpGizmoPosition(evt){			// console.log('mouseDownGizmoPosition');
		this.nodePosition.removeEventListener("mousemove", this._mouseMoveGizmoPosition, false);
		this.nodePosition.removeEventListener("mouseup", this._mouseUpGizmoPosition, false);
		}
	mouseMoveGizmoPosition(evt){		//console.log('mouseMoveGizmoPosition: '+JSON.stringify({x:evt.offsetX,y:evt.offsetY}));
		if (undefined!=this.cb_onPosition){
			this.draw(this.rotation,evt.offsetX,evt.offsetY);
			this.cb_onPosition(evt.offsetX,evt.offsetY);
			}
		}
	};	//	CGizmo
