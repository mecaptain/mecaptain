//	iview.js
// laat navigatielichten van andere schepen zien
//	probeer dit met three.js op te lossen
//	todo ColladaLoader moet nog een file:// mode ondersteunen. Beter zelfs is een websocket mode. Zie hiervoor 
//
//	http://learningthreejs.com/blog/2011/08/15/lets-do-a-sky/
//	https://threejs.org/examples/#webgl_multiple_canvases_circle
//
//	todo een peilings uitlezing op het buitenzicht
//	todo meerder camera presets, bv stuurhut brugvleugel, achterdek, bak, walegang

'use strict';

class CInstrument_view extends CInstrument {
	constructor(obj,exposedParms,active,idx){
		super(idx,obj);
		if (undefined!=exposedParms)
			this.exposedParms = exposedParms;
		else
			this.exposedParms = //	todo deze moeten door de conning designer kunnen worden gezet, nier overrulen hier dan
				{viewDirection: 0	//	with respect to heading of the ship that bears the camera
				,camid:	0			//	unique camera id, user is responsible for uniqueness. This id is used by the CAM nmea sentence
				,fov:	60				//	degrees field of view
				,aspect: 0			//	0=let canvas (or resizing determine) value=aspect (resize canvas) todo
				,planeNear:	1		//	meters near clipping plane
				,planeFar: 5000	//	meters far clipping plane
				,height:40			//	todo dit moet aan het schip gekoppeld worden
				,nmea:	
					{compassHDG:	{s:'THS',f:'hdgT'}
					,position: 		{s:'RMC',f:'rot'}	//	todo
					}
				};

		if (!active) return;
		this.canvas = obj.querySelector("canvas");
		this.canvas.style = 'border:0px solid #000;backgroundColor:#000;color:#000';
		if (this.canvas.width > 0)
			this.resize(this.canvas.width,this.canvas.height);
		else
			this.resize(1200,400);	//	todo dit moet eigenlijk met de exposedparms

 		this.onfocus = this.onfocus.bind(this);
		this.onblur = this.onblur.bind(this);
		this.ship = {hdt:90,latlon:new zeroisim.LatLon({latdeg:51+51.0/60.0,londeg:3+53.5/60.0})};	//	todo dit moet met GGA of GLL binnenkomen
		this.degtorad = Math.PI / 180.0;
		this.viewDirection = this.exposedParms.viewDirection;
		this.renderer = new THREE.WebGLRenderer({canvas:this.canvas});	//	zie http://threejs.org/docs/index.html#Reference/Renderers/WebGLRenderer
		window.addEventListener('resize',this.onWindowResize.bind(this),false);
		this.canvas.addEventListener("mousemove",this.mousemove.bind(this),false)
		
		// simple question, big answer, should I use smoothing here?
		super.cbRxNmea(this.exposedParms.nmea.compassHDG.s,function(rxd,that){	//	install callback for compass and autopilot
			if (undefined==rxd.data)	return;
			that.ship.hdt = parseFloat(rxd.data[that.exposedParms.nmea.compassHDG.f]);
			that.updateHeading();	//	todo nog iets verzinnen voor de view direction	
			},this);
		super.cbRxNmea(this.exposedParms.nmea.position.s,function(rxd,that){	//	install callback for compass and autopilot
			if (undefined==rxd.data)	return;
			that.ship.latlon.interpretLatLonRMC(rxd.data);
			that.updatePosition();
			},this);
		super.cbRxNmea('CAM',function(rxd,that){	//	view direction
			if (undefined==rxd.data)	return;
			if (that.exposedParms.camid==rxd.data.camid){
				if (undefined!=rxd.data.rotY){
					that.viewDirection = parseFloat(that.exposedParms.viewDirection)+parseFloat(rxd.data.rotY);
					console.log(`iview.js camid=${that.exposedParms.camid} viewDirection: ${that.exposedParms.viewDirection}+${rxd.data.rotY}=${that.viewDirection} (hdt=${that.ship.hdt*that.degtorad})`);
					that.updateHeading();
					}
				}
			},this);
		// this.camera = undefined;
		this.createCamera();
		this.installControls();
		
		// loadScene, experimenting...
		super.cbRxNmea('MC0',function(rxd,that){	//	view direction
			if (undefined==rxd.data)	return;
			if ((mc0.getScene+mc0.response) !== rxd.data.req)	return;
			// load scene from serialized
			if (window.location.href.substr(0,7) == 'file://'){	//	eigen loaders vanuit wss, cross domain is een probleem in file://-domein
				var wsloadingManager = new zeroisim.LoadingManager
					(that.funloadingManager.bind(that)
					,that.loadingStart.bind(that)
					,function(url){
						if (window.location.href.substr(0,7) == 'file://'){	//	eigen loaders vanuit wss, cross domain is een probleem in file://-domein
							return `ws://127.0.0.1:12120:fxmlhttp:${url}`;
							}
						else {
							return url;
							}
						}
					);
				if (window.location.href.substr(0,7) == 'file://'){	//	eigen loaders vanuit wss, cross domain is een probleem in file://-domein
					THREE.fileLoader = zeroisim.FileLoader;	//	kennelijk overtuig ik THREEjs zo om mijn eigen fileloader te gebruiken
					var loader = new THREE.GLTFLoader(wsloadingManager,zeroisim.FileLoader);
					}
				else
					var loader = new THREE.GLTFLoader(wsloadingManager);

				// var sceneToImport = 'incl/threejs/examples/models/gltf/DamagedHelmet/glTF/DamagedHelmet.gltf';
				// var sceneToImport = 'scene_test.gltf';
				// var sceneToImport = '3d/scenes/scene_test.gltf';
				// loader.load( sceneToImport, function(gltf){
				if (1){
					that.scene = zeroisim.scene;
					}
				else{
					loader.loadText(rxd.data.scene, function(gltf){
						/*
						gltf.scene.traverse(function(child) {
							if (child.isMesh){
								// child.material.envMap = envMap;	//	maakt een spiegelend materiaal
								}
							});
						*/	
						// var scene = new THREE.Scene();				scene.add(gltf.scene);					that.scene = scene;
						that.scene = gltf.scene;

						//	todo het lijkt me leuk om hier that.scene te vergelijken met zeroisim.scene
						//zeroisim.compareScenes(that.scene,zeroisim.scene);
						
						//	ambientlight is nog niet ondersteund door exporter (misschien ook niet door de importer)
						that.ambientLight = new THREE.AmbientLight( 0xcccccc, 0.4 );
						that.ambientLight.name = 'ambientlight';
						that.scene.add( that.ambientLight );

						var pos = JSON.parse(gltf.userData.position);
						that.scene.userData = 
							{position: new zeroisim.LatLon({latdeg:pos.a,londeg:pos.o})
							,getDiff:function(there){	//	function wordt niet netjes geexporteerd
								var diff = zeroisim.scene.userData.position.meters_diff(there);
								return diff;
								}
							}
						});
					}
				}
			},this);
		zeroisim.busTX('MC0',{req:mc0.getScene});
		this.onfocus();
		}
	onfocus(){	
		this.hasFocus=true;	
		if (!this.camera){
			this.createCamera();
			this.installControls();
			}
		this.draw();
		}
	onblur(){	
		this.hasFocus=false;	
		if (undefined!=this.controls)	this.controls.dispose();
		}
	funloadingManager(url){
		url;
		}
	loadingStart(url){
		url;
		}
	installControls(){
		// console.log('view.js installControls()');
		if (0){	//	do not install controls, view dir is determined by other things than the mouse
			if (0){
				//	die OrbitControls wijzigen de camera.rotation, sla deze op om straks terug te zetten
				var camrot = new THREE.Vector3(this.camera.rotation.x,this.camera.rotation.y,this.camera.rotation.z);
				// console.log('installControls camrot1='+JSON.stringify(camrot));
				this.controls = new THREE.OrbitControls(this.camera,this.renderer.domElement);
				// this.controls.maxPolarAngle = +10*Math.PI / 180.0;
				// this.controls.minPolarAngle = -10*Math.PI / 180.0;
				this.controls.minDistance = 10;
				this.controls.enableDamping = true;
				this.controls.dampingFactor = 0.25;
				this.controls.addEventListener('change',this.view_tab_obj_controlchange.bind(this));
				this.camera.rotation.x = camrot.x;
				this.camera.rotation.y = camrot.y;
				this.camera.rotation.z = camrot.z;
				}
			else{
				this.controls = new THREE.OrbitControls( this.camera );
				this.controls.target.set( 0, -0.2, -0.2 );
				this.controls.update();
				}
			}
		}
	view_tab_obj_controlchange(p1){
		// var camrot = new THREE.Vector3(this.camera.rotation.x,this.camera.rotation.y,this.camera.rotation.z);
		this.draw();
		}
	createCamera(){
		if (1){
			this.camera = new THREE.PerspectiveCamera
				(this.exposedParms.fov
				,this.canvas.width/this.canvas.height
				,this.exposedParms.planeNear
				,this.exposedParms.planeFar
				);
			this.camera.rotation.order = 'YXZ';	//	todo ???
			this.camera.position.set(0,this.exposedParms.height,0);
			}
		else{
			this.camera = new THREE.PerspectiveCamera( 45, window.innerWidth / window.innerHeight, 0.25, 20 );
			this.camera.position.set( -1.8, 0.9, 2.7 );
			}
		
		this.update();
		}
	updateHeading(){
		if (!this.scene)	return;
		// console.log('CInstrument_view::updateHeading() '+JSON.stringify({hdt:this.ship.hdt,viewdir:this.viewDirection}));
		if (1)
			this.camera.rotation.y = (this.ship.hdt+parseFloat(this.viewDirection))*-this.degtorad;
		// console.log(' rotdeg:'+this.camera.rotation.x/this.degtorad+','+this.camera.rotation.y/this.degtorad+','+this.camera.rotation.z/this.degtorad+')');
		this.draw();
		}
	updatePosition(){
		if (!this.scene)	return;
		// console.log('CInstrument_view::updatePosition()');
		// shift camera to new position
		/* nog even niet totdat er een goede scene is */
		var diff = this.scene.userData.getDiff(new zeroisim.LatLon(this.ship.latlon)); //	bepaal xyz coordinaten van dit navmark
		this.camera.position.set(diff.x,this.exposedParms.height,diff.z);
		/**/
		this.draw();
		}
	update(){
		//	once in a while update position of all visual elements
		if (!this.scene)	return;
		this.draw();
		}
	progress_load(oargs,that){
		// oargs.total / oargs.loaded
		// console.log('view::progress_load('+oargs.total+'/'+oargs.loaded+')');
		console.log('view::progress_load('+100*oargs.loaded/oargs.total+')');
		// var midden = {x: that.canvas.width/2, y:that.canvas.height/2};
		}
	fail_load(oargs,that){
		// todo give some message
		}
	resize(width,height){
		this.canvas.width = width;
		this.canvas.height = height;
		// this.whcanvas = {width:this.canvas.width,height:this.canvas.height};
		
		if (this.camera){
			this.camera.aspect = this.canvas.width / this.canvas.height;
			this.camera.updateProjectionMatrix();
			this.renderer.setSize( width, height );
			}
		// todo upon resizestop, whole new instrument is created, but conningdesigner does not rescale the canvas...
		}
	onWindowResize(evt){
		console.log('iview.js onWindowResize()');
		this.resize(window.innerWidth-10,window.innerHeight-10);
		// this.resize(window.offsetWidth,window.offsetHeight);
		}	
	mousemove(evt){
		//	todo show bearing indicator
		var rect = evt.currentTarget.getBoundingClientRect();
		var pp = {x: evt.clientX - rect.left - evt.currentTarget.width/2, y: -1*(evt.clientY - rect.top - evt.currentTarget.height/2)};
		console.log('CInstrument_view::mousemove(evt) pp='+JSON.stringify(pp));
	//	localStorage.setItem('mousemove',JSON.stringify(pp));
		//	draw a line on the canvas
		var cv = this.canvas.getContext("3d");
		if (cv){
			cv.beginPath();
			cv.strokeStyle = 'rgba(255,255,255,0.6)';
			var xx = this.canvas.width/2 + pp.x;
			cv.moveTo(xx,this.canvas.height);			cv.lineTo(xx,0);
			// this.cv.moveTo(this.canvas.width,this.canvas.height/2);			this.cv.lineTo(0,this.canvas.height/2);
			cv.stroke();
			}
		}
	draw() {
		if (!this.hasFocus) return;
		if (!this.scene)	return;
		
		if (0){	//	debugging		
			// console.log('view.js draw()');
			// this.camera.rotation.z = 0;
			console.log('lights: (x,y,z)=');
			console.log('	0:'+this.lights[0].position.x+','+this.lights[0].position.y+','+this.lights[0].position.z+')');
			console.log('	1:'+this.lights[1].position.x+','+this.lights[1].position.y+','+this.lights[1].position.z+')');
			// console.log('	2:'+this.lights[2].position.x+','+this.lights[2].position.y+','+this.lights[2].position.z+')');
			console.log('cam:'+this.camera.position.x+','+this.camera.position.y+','+this.camera.position.z+')');
			}		
		if (0){	//	debugging		
			// console.log('view.js draw() camera: (x,y,z)=');
			// console.log(' pos:'+this.camera.position.x+','+this.camera.position.y+','+this.camera.position.z+')');
			console.log(' rot:'+this.camera.rotation.x+','+this.camera.rotation.y+','+this.camera.rotation.z+')');
			}	
		this.scene.traverseVisible(function(obj3d,that){	//	turn all lookAt() objects in the scene	
			if (undefined!=obj3d.mc_lookat && true==obj3d.mc_lookat){
				obj3d.lookAt(that.camera.position);
				}
			},this);
		this.renderer.render(this.scene,this.camera);
		}
	};

CInstrument.addInstrumentToSettings
	('./iview.js'
	,'view'
	,'canvas'
	,CInstrument_view
	,'gives a view from a camera'
	,	['./incl/threejs/build/three.js'
		,'./incl/threejs/examples/js/controls/OrbitControls.js'
		,'./incl/threejs/examples/js/loaders/ColladaLoader.js'
		,'./myGLTFLoader.js'
		,'./comparescenes.js'
		,'./latlon.js'
		]
	);
