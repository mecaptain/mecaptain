//	comparescenes.js
//	original fileloader in three.js and three.min.js cannot work cross-domain or file-domain (because based on xmlHttp)
//	this loader get file from websocket server that contains simple filegetter

'use strict';

(function(zeroisim){
	zeroisim.compareScenes = function(sceneImported,sceneOriginal){
		//	loop door sceneOriginal heen en kijk of de waarde bestaat in sceneImported
		zeroisim.compareObjects(sceneImported,sceneOriginal,0);
		}
	zeroisim.compareObjects = function(obj1,obj2,indent,parentkey){
		// if ( Object.keys(obj1).length !== Object.keys(obj2).length ) return false;
		for ( var key in obj2) {
			if (undefined===obj1[key])
				console.log(`obj2 misses key: '${key}'`);
			else if (typeof obj2[key] === 'object'){
				switch (key){
					case 'parent':
						break;
					default:
						return zeroisim.compareObjects(obj1[key],obj2[key],1+indent,key);
						break;
					}
				}
			else if ( obj1[key] !== obj2[key] ) {
				console.log(`${parentkey?parentkey:''}${strIndent(indent)}${key} member differs 1:${obj1[key]}`);
				console.log(`${parentkey?parentkey:''}${strIndent(indent)}${key}                2:${obj2[key]}`);
				// return false;
				}
			}
		return true;
		}
	function strIndent(nn){
		return ' '.repeat(nn);
		}
	})(window.zeroisim);

	
	
	
	