//	pilot.js
//	show controls and instruments for ship or simulator
//	todo misschien tabs maken voor enkelschroef, dubbelschroef, azi's enz.
'use strict';
// var pilot_tab_page = {

class Cpilot_tab_page extends C0isimTab {
	constructor(){
		super();
		var rudders =  //	als je het aantal rudders verandert, moet je ook de colspan van verdere td aanpassen
			[{txt:'hard<br/>port',angle:-35}
			,{txt:'port<br/>25',angle:-25}
			,{txt:'port<br/>20',angle:-20}
			,{txt:'port<br/>15',angle:-15}
			,{txt:'port<br/>10',angle:-10}
			,{txt:'port<br/>5',angle:-5}
			,{txt:'midships',angle:0}
			,{txt:'starboard<br>5',angle:5}
			,{txt:'starboard<br>10',angle:10}
			,{txt:'starboard<br>15',angle:15}
			,{txt:'starboard<br>20',angle:20}
			,{txt:'starboard<br>25',angle:25}
			,{txt:'hard<br>starboard',angle:35}
			];
		var telegraphs = //	als je het aantal telegraphs verandert, moet je ook de rowspan van verdere td aanpassen
			[{txt:'full<br/>ahead',thrust:100}
			,{txt:'half<br/>ahead',thrust:50}
			,{txt:'slow<br/>ahead',thrust:10}
			,{txt:'deadslow<br/>ahead',thrust:5}
			,{txt:'stop',thrust:0}
			,{txt:'deadslow<br/>astern',thrust:-5}
			,{txt:'slow<br/>astern',thrust:-10}
			,{txt:'half<br/>astern',thrust:-50}
			,{txt:'full<br/>astern',thrust:-100}
			];
		this.divKompas =	i.div(h.nbsp,{id:"divpilot_kompas"});
		this.divGPS =		i.div(h.nbsp,{id:"divpilot_gps"});
		this.divDoppler =	i.div(h.nbsp,{id:"divpilot_doppler"});
		// ,fsKompas:	i.fieldset('compass',this.divKompas,{id:"pilot_kompas" 	,style:"border:0px",colspan:6,rowspan:9})
		var instruments =
			[{cb:function(that,ii,iii){return that.itg0(i.table(
				[i.tr(i.td(i.fieldset('gps'		,that.divGPS,		{id:"pilot_gps"})))		//	onclick:document.getElementById("pilot_gps").appendChild(document.getElementById("mfinstr_canvas"));'))
				,i.tr(i.td(i.fieldset('doppler'	,that.divDoppler,	{id:"pilot_doppler"})))	//	onclick:document.getElementById("pilot_doppler").appendChild(document.getElementById("dopplerlog_canvas"));'))
				]),{style:"border:0px",colspan:6,rowspan:9})}}
			,{cb:function(that,ii,iii){return that.itg1(telegraphs[0].txt,telegraphs[0].thrust)}}
			,{cb:function(that,ii,iii){return i.td(i.fieldset('compass',that.divKompas,{id:"pilot_kompas" 	,style:"border:0px",colspan:6,rowspan:9}),{id:"tdpilot_kompas"})}}
			,{cb:function(that,ii,iii){return that.itg0(i.fieldset('engines',i.div(h.nbsp),{id:"pilot_eng",style:"border:0px",colspan:6,rowspan:9}))}}
			];	
//		this.kompas_canvas 	= i.canvas(i.attributes({id:"kompas_canvas",			width:"400",height:"400",style:"border:0px solid #000;"}));
		// this.mfinstr_canvas	= i.canvas(i.attributes({id:"mfinstr_canvas",		width:"320",height:"220",style:"border:0px solid #000;"}));
		// this.engpanel_canvas	= i.canvas(i.attributes({id:"engpanel_canvas",		width:"300",height:"140",style:"border:0px solid #000;",unselectable:"on"}));
		// this.dopplerlog_canvas= i.canvas(i.attributes({id:"dopplerlog_canvas",	width:"300",height:"220",style:"border:0px solid #000;"}));
			
		this.dehtml = i.div(
			[i.table( 
				[i.tr(this.itimes(this,rudders.length,function(that,ii){return that.irud(rudders[ii].txt,rudders[ii].angle)}))
				,i.tr(this.itimes(this,4,function(that,ii){return that.itg0(h.nbsp)})
					.concat([this.irudplus('-10',-10), this.irudplus('-1',-1), this.irudplus('0',0), this.irudplus('+1',1), this.irudplus('+10',10)])
					.concat(this.itimes(this,4,function(that,ii){return that.itg0(h.nbsp)}))
					)
				,i.tr(this.itimes(this,instruments.length,function(that,ii){return instruments[ii].cb(that)}))
				,i.table(this.itimes(this,telegraphs.length,function(that,ii){if (ii==0) return;return i.tr(that.itg1(telegraphs[ii].txt,telegraphs[ii].thrust))}))
				])
			/*	
			, i.br
			,i.table(h.tr(
				[i.td(i.input('',{type:"text",value:settings.talk_port,id:"tx_talkaddr_pilot",style:"width:69%;"}))
				,i.td(i.div('send',{id:"pilot_command"}))
				]))
			*/	
			]);	
		}
	onload(evt){
		document.getElementById(this._tabdivname).appendChild(this.dehtml);
		var wh;
		wh = {w:400,h:400};this.putinInstrument('compass',	wh,this.divKompas);	
		wh = {w:300,h:200};this.putinInstrument('gps',		wh,this.divGPS);		
		wh = {w:300,h:200};this.putinInstrument('doppler',	wh,this.divDoppler);
		// mf		width:"320",height:"220"
		// eng	width:"300",height:"140"
		}
	findInstrument(name) {
		var idx = settings.instrumentList.findIndex(function(element){	//	find instrument in directory
			return (element.name==this);
			},name);
		if (-1===idx)	return undefined;	//	instrument not found
		var rc = settings.instrumentList[idx];
		rc.idx = idx;
		return rc;
		}
	putinInstrument(name,wh,parent){
		var instrument = this.findInstrument(name);
		return this.putinInstrument_(instrument.name,instrument.domt,instrument.clas,instrument.idx,wh,parent);
		}
	putinInstrument_(name,domt,clas,idx,wh,parent){
		zeroisim.replaceNode(parent,CInstrument.makeContainer4({name:name,domt:domt},wh));
		var instrument = new clas(parent,undefined,1,idx);	//	instantiate class	
		}
	onfocus(){	
		}
	onblur(){
		}
	times(that,n,cb){
		var rc = '';
		for (var ii=0; ii<n; ii++){
			rc +=cb(that,ii,n);
			}	
		return rc;	
		}	
	itimes(that,n,cb){
		var rc = [];
		for (var ii=0; ii<n; ii++){
			var elem = cb(that,ii,n);
			if (undefined!=elem)
				rc.push(cb(that,ii,n));
			}	
		return rc;	
		}	
	tg0(d,x){
		var attr = 'class="tg0"';
		if (undefined!=x)
			attr += ' ' + x;
		return h.td(d,attr);
		}
	itg0(d,x){
		var attr = {class:"tg0"};
		if (undefined!=x)
			attr = Object.assign(attr,x);
		return i.td(d,attr);
		}
	tg1(d,i){
		return h.td(h.center(d),'class="tg1" onclick=pilotcommand(this,"t"); id="'+i+'"');
		}
	itg1(d,ii){
		return i.td(i.center(d),{class:"tg1",onclick:ipilotcommand.bind({that:this,rt:"t"}),id:ii});
		}
	rud(d,i){
		return h.td(h.center(d),'class="tg1" onclick=pilotcommand(this,"r"); id="'+i+'"');
		}
	irud(d,ii){
		return i.td(i.center(d),{class:"tg1",onclick:ipilotcommand.bind({that:this,rt:"r"}),id:ii});
		}
	rudplus(d,i){
		return h.td(h.center(d),'class="tg1" onclick=pilotcommand(this,"rp"); id="'+i+'"');
		}
	irudplus(d,ii){
		return i.td(i.center(d),{class:"tg1",onclick:ipilotcommand.bind({that:this,rt:"rp"}),id:ii});
		}
	}
function ipilotcommand(parm){
	return	ipilotcommand(parm.that,parm.rt);
	}
function pilotcommand(that,rt){
	var bAzi = true;	//	may only be used if azitabobj is loaded
	/*
	var aziData = 
		{not: 0		//	integer(),		'Number of thruster (thruster index)']
		,rpm: 0		//	floatingp,		'RPM demand value (percentage)']
		,rpmi: 0		//	literal('P'),	'RPM mode indicator (indicates percentage)']
		,pitch: 0	//	floatingp,		'Pitch demand value (percentage)']
		,ptm: 0		//	literal('P'),	'Pitch mode indicator (indicates percentage)']
		,azi: 0		//	floatingp,		'Azimuth demand (0-360, follow-up)']
		,oloc: 0		//	literal('.'),	'Operating location indicator']
		,stat: 0		//	literal('A|V'),'Sentence status flag']
		};
	*/	
	setValue('pilot_command','pilotcommand '+rt+that.id);
	switch (rt){
		case 'rp':	// rudder plus
			var rud = [parseInt(tx.ROR.prud)+parseInt(that.id),parseInt(tx.ROR.srud)+parseInt(that.id)];
				if (bAzi){
				rud[0] *= -1;
				rud[1] *= -1;
				}
			rudder(rud[0],rud[1]);
			break;
		case 'r':	// rudder
			rudder(that.id,that.id);
			break;
		case 't':	// telegraph
			telegraph(that.id,that.id);
			break;
		}
	}
