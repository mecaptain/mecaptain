//	smartvalues.js
'use strict';

(function(global,factory){
	typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports) :
	typeof define === 'function' && define.amd ? define(['exports'], factory) :
	// (factory((global.LatLon = {})));
	(factory(zeroisim));
	}(this, (function(exports) {

'use strict';
var angle_value = function(startvalue,cb){
	return {	value: startvalue
			,	cb: cb
			,	inc: function(increment){this.value+=increment; if (this.value>=360)	this.value-=360;	if (undefined!=cb)	cb(this);}
			,	dec: function(increment){this.value-=increment; if (this.value<0)		this.value+=360;	if (undefined!=cb)	cb(this);}
			,	get: function(){return (this.value<100?(this.value<10?'0':'')+'0':'')+ this.value;} // prefixen zodat drie cijfers ontstaan
			};
	}
var scalar_value = function(startvalue,cb){
	return {	value: startvalue
			,	cb:	cb
			,	change: function(value){this.value=value;	if (undefined!=this.cb) this.cb(this);	return value;}
			,	get: function(){return this.value;}
			};
	}	
var ascal_value = function(mode,startvalue,cb){	// kan een hoek of een scalar zijn
	this.mode = mode;
	return {	value: startvalue
			,	cb: cb
			,	inc: function(increment){
				switch (this.mode){
					case 'angle':	this.value+=increment; if (this.value>=360)	this.value-=360;	if (undefined!=cb)	cb(this);	break;
					case 'scalar':	this.value+=increment; 														if (undefined!=cb)	cb(this);	break;
					}}
			,	dec: function(increment){
				switch (this.mode){
					case 'angle':	this.value-=increment; if (this.value<0)		this.value+=360;	if (undefined!=cb)	cb(this);	break;
					case 'scalar':	this.value-=increment; 														if (undefined!=cb)	cb(this);	break;
					}}
			,	get: function(){
				switch (this.mode){
					case 'angle':	return (this.value<100?(this.value<10?'0':'')+'0':'')+ this.value; break;	// prefixen zodat drie cijfers ontstaan
					case 'scalar':	return this.value; break;
					}}
			,	setmode: function(mode){this.mode=mode;if (undefined!=cb)	cb(this);	return this.mode;}
			,	set: function(value){this.value=value;	if (undefined!=cb)	cb(this);	return this.value;}
			};
	}	
var boolean_value = function(startvalue,cb){
	return {	value: startvalue
			,	cb:	cb
			,	toggle: function(value){this.value=this.value?false:true;	if (undefined!=this.cb) this.cb(this);	}
			,	get: function(){return this.value;}
			,	set: function(value){this.value=value;	if (undefined!=cb)	cb(this); return value;}
			};
	}	
var enum_value = function(startvalue,cb,values){
	return {	value: startvalue
			,	myvalues: values
			,	cb: cb
			,	set: function(value){if (this.myvalues.indexOf(value)!=-1) this.value=value;	if (undefined!=cb)	cb(this);	return value;}
			,	get: function(){return this.value;} 
			};
	}	
class new_ascal_value {	// kan een hoek of een scalar zijn
	constructor(mode,startvalue,cb){
		this.mode = mode;
		this.value = startvalue;
		this.cb = cb;
		}
	inc(increment){
		switch (this.mode){
			case 'angle':	this.value+=increment; if (this.value>=360)	this.value-=360;	if (undefined!=this.cb)	this.cb(this);	break;
			case 'scalar':	this.value+=increment; 														if (undefined!=this.cb)	this.cb(this);	break;
			}}
	dec(increment){
		switch (this.mode){
			case 'angle':	this.value-=increment; if (this.value<0)		this.value+=360;	if (undefined!=this.cb)	this.cb(this);	break;
			case 'scalar':	this.value-=increment; 														if (undefined!=this.cb)	this.cb(this);	break;
		}}
	get(){
		switch (this.mode){
			case 'angle':	return (this.value<100?(this.value<10?'0':'')+'0':'')+ this.value; break;	// prefixen zodat drie cijfers ontstaan
			case 'scalar':	return this.value; break;
			}}
	setmode(mode){
		this.mode=mode;
		if (undefined!=this.cb)	this.cb(this);	
		return this.mode;
		}
	set(value){
		this.value=value;	
		if (undefined!=this.cb)	this.cb(this);	
		return this.value;
		}
	};
class new_enum_value {
	constructor(startvalue,cb,values){
		this.value = startvalue;
		this.myvalues = values;
		this.cb = cb;
		}
	set(value){
		if (this.myvalues.indexOf(value)!=-1) 
			this.value=value;	
		if (undefined!=this.cb)	
			this.cb(this);	
		return value;
		}
	get(){
		return this.value;
		} 
	};
	exports.new_ascal_value = new_ascal_value;
	exports.boolean_value = boolean_value;
	exports.new_enum_value = new_enum_value;
	exports.scalar_value = scalar_value;
	})));
	