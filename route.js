//	route.js
'use strict';
	
class CRoute {
	//	todo waypoint now are hard turnpoints. maybe convert turns into circles or splines.
	constructor(/*trainer,*/name){
		this._points = new Array();
		// this.deTrainer = trainer;
		this._name = (name? name: '');
		this._prefix = 'route_';
		}
	addWaypoint(latlon){
		if (latlon instanceof zeroisim.LatLon) {
			this._points.push(latlon);
			}
		else{
			this._points.push(new zeroisim.LatLon({latrad:latlon.a,lonrad:latlon.o}));
			}
		}
	doForRoute(cb,payload){
		if (typeof cb === 'function') {
			this._points.myforEach(function(el,idx,that,thing){
				thing.cb(el,idx,that,thing.payload);
				},{cb:cb,payload:payload});
			}
		}
	setNameEvt(evt){
		return this.setName(evt.target.value);
		}
	setName(name){
		this._name = name;
		if (this._name.length>0) {
			document.querySelector('#ppllrsave').disabled = false;
			document.querySelector('#ppllrsavelocal').disabled = false;
			}
		else {
			document.querySelector('#ppllrsave').disabled = true;
			document.querySelector('#ppllrsavelocal').disabled = true;
			}
		}
	asTableDOM(divname){
		var rc = [];
		rc.push(i.tr(
			[i.td(i.span('Name: '))
			,i.td(i.input('',
				{type:'text'
				,id:'ppllrname'
				,value:(this._name.length>0)?this._name :''
				,placeholder:'type name of route'
				,change:this.setNameEvt.bind(this)
				,keyup:this.setNameEvt.bind(this)
				}),{colspan:2})
			]));
		this.doForRoute(function(wp,idx,that){
			rc.push(i.tr(
				[i.td(i.span('wp['+idx+']'))
				,i.td(i.span(wp.latdegmindec))
				,i.td(i.span(wp.londegmindec))
				]));	//	todo maybe make editable
			});
			
		rc.push(i.tr(
			[i.td(i.span('save:'))
			,i.td(i.button('on Server',
			// {click:this.deTrainer.saveRoute.bind(this)
				{click:this.saveRoute.bind(this)
				,title:'to save, this route must have a name. Route is saved on the server.'
				,disabled:null
				,id:'ppllrsave'
				}))
			,i.td(i.button('Local',
			//	{click:this.deTrainer.saveRouteLocal.bind(this)
				{click:this.saveRouteLocal.bind(this)
				,title:'to save, this route must have a name. Route is saved on local disk.'
				,disabled:null
				,id:'ppllrsavelocal'
				}))
			]));
		/* maybe save on local device	
		+h.td(h.a('save CSV','href="data:application/octet-stream,'+this.asAnchor()+'" download="myroute_.csv"'))
		+h.td(h.a('save JSON','href="data:application/octet-stream,'+this.asJSON()+'" download="myroute_.json"'))
		*/	
		return i.table(rc);
		}
	asTable(divname){
		var parentNode = document.getElementById(divname);
		while (parentNode.firstChild) {	// remove old domtree
			parentNode.removeChild(parentNode.firstChild);
			}
		parentNode.appendChild(this.asTableDOM());
		}	
	asJSON(){
		var now = new Date();
		var rc = zeroisim.merge_objects(
			{name:this._name
			,datecreated:now.toLocaleDateString(undefined,{year:'numeric',month:'long',day:'numeric'})
			}
			,{points:this._points});
		now = undefined;
		return JSON.stringify(rc);
		}
	asAnchor(){
		var comma = '%2C';
		var newline = '%0A';
		var rc = '\ufeff';	//	see https://stackoverflow.com/questions/3665115/create-a-file-in-memory-for-user-to-download-not-through-server
		rc += 'idx'+comma+'lat'+comma+'lon'+newline;
		this.doForRoute(function(wp,idx,that){
			// rc += idx+comma+wp.latdegmindec+comma+wp.londegmindec+newline;	//	readable
			rc += idx+comma+wp._latrad+comma+wp._lonrad+newline;	//	radians
			});
		return rc;	
		}
	clear(){
		this._points = undefined;
		}
	static getRoutesLocal(){	//	todo misschien moet deze fun wat meer info geven, zoals een FileDir, dus dateCreated en #waypoints, misschien zelfs een preview bitmap
		console.log('CRoute::getRoutesLocal');
		var rc = localStorage.getItem('routes');
		if (undefined==rc)	
			rc = [];
		else	
			rc = JSON.parse(rc);
		return rc;
		}
	saveRouteLocal(evt){
		console.log(`CRoute::saveRouteLocal(${this._name})`);
		localStorage.setItem(this._prefix+this._name,this.asJSON());	//	save route, assume ok to overwrite previous route with the same name
		var routes = CRoute.getRoutesLocal();
		if (-1==routes.indexOf(this._name)){
			routes.push(this._name);
			}
		localStorage.setItem('routes',JSON.stringify(routes));	//	save route
		}
	saveRoute(evt){
		console.log('CRoute::saveRoute');
		// zeroisim.busTX('MC0',{req:mc0.createdWaypoint,route:this.asJSON(),destination:'server'});
		}
	loadLocal(){
		var rout = JSON.parse(localStorage.getItem(this._prefix+this._name));
		if (rout){
			rout.points.myforEach(function(txtPoint,idx,rts,that){
				that.addWaypoint(JSON.parse(txtPoint));
				},this);
			}
		}
	static getAndListRoutes(divname,cb_load,tr_att){	//	a list of routes is received from server
		return CRoute.listRoutes(CRoute.getRoutesLocal(),divname,cb_load,tr_att);
		}
	static listRoutes(routes,divname,cb_load,tr_att){	//	a list of routes is received from server
		console.log('CRoute::listRoutes');
		var nroutes = [];
		routes.myforEach(function(route,idx,rts,that){	//	verrijk met knopje
			nroutes.push(
				{knop:i.button('load',{click:cb_load,route:route})
				,name:route
				// ,created:route.created
				});
			},this);
		zeroisim.setiValue(divname, zeroisim.iasTable(nroutes,undefined,tr_att));
		}
	getHdg(wp0){	//	bereken grootcirkel richting van dit waypoint naar het volgende
		var wp1 = (wp0+1) % this._points.length;
		if (wp0==wp1)	return undefined;	//	er is geen richting naar het volgende waypoint
		var bd = this._points[wp1].beardist(this._points[wp0]);
		return bd.b;
		}
	findQuadrant(hdg){	
		var q = [];
		q[0] = Math.abs(zeroisim.deltaDirection(hdg,0));
		q[1] = Math.abs(zeroisim.deltaDirection(hdg,90));
		q[2] = Math.abs(zeroisim.deltaDirection(hdg,180));
		q[3] = Math.abs(zeroisim.deltaDirection(hdg,270));
		var mi = Math.min(...q);	//	 spread operator https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Spread_syntax
		return q.indexOf(mi);
		/*		
		// makelijke gevallen	
		var q0 = findQuadrant(0);		//	0
		var q1 = findQuadrant(90);		//	1
		var q2 = findQuadrant(180);	//	2
		var q3 = findQuadrant(270);	//	3
		// grensgevallen
		var q45 = findQuadrant( 45);	//	
		var q135 = findQuadrant(135);	//
		var q225 = findQuadrant(225);	//	
		var q315 = findQuadrant(315);	//
		// voorbeelden
		var q316	= findQuadrant(316);	//	0	
		var q044	= findQuadrant( 44);	//	0	
		var q046	= findQuadrant( 46);	//	1
		var q134	= findQuadrant(134);	//	1
		var q136	= findQuadrant(136);	//	2
		var q224	= findQuadrant(224);	//	2
		var q226	= findQuadrant(226);	//	3
		var q314	= findQuadrant(314);	//	3
		*/
		}
	show(posplotObj){	//	niet zo netjes, misschien met zeroisim.busTX('MC0') doen
		if (undefined!=posplotObj && undefined!=posplotObj.canvaslayer){
			posplotObj.canvaslayer.renderRoute(this);
			}
		}
	}
