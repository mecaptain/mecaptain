//	geolocation.js
//	geolocation can provide position for map display
// todo give user the options to render position as ais-target or midpoint of the map
'use strict';
var geoloctabobj = {
	// verplichte drie members om de tabs interface te ondersteunen
	html: function(){
		// console.log('initting geoloctabobj');
		this.shouldInstallLocationWatcher = false;
		document.geoloctabobj = this;
		window.addEventListener("load", this.initAfterLoad.bind(this), false);
		return 'example tab' + h.div('waiting for geolocation','id="geoloc"');
		}
	,onfocus:function(evt){	
		this.hasFocus=true;
		if (this.shouldInstallLocationWatcher){
			this.shouldInstallLocationWatcher = false;
			var x = document.getElementById("geoloc");
			if (navigator.geolocation) {
				x.innerHTML = "waiting for geolocation...";	
				navigator.geolocation.getCurrentPosition(this.showPosition.bind(this));
				var options = {
					timeout: 10000,
					maximumAge: 10000,
					enableHighAccuracy: true
					};
				this.positionHandled = false;
				var watchId = navigator.geolocation.watchPosition(this.showPosition.bind(this), this.handleError, options);
				// var watchId = navigator.geolocation.watchPosition(function(pos){alert('position received!');}, this.handleError, options );
				console.log('watchPosition installed');
				} 
			else { 
				x.innerHTML = "Geolocation is not supported by this browser.";
				}
			}
		}
	,onblur:function(){	
		this.hasFocus=false;	
		}
	,hasFocus: false
	,initAfterLoad: function(){
		this.shouldInstallLocationWatcher = true;
		}
	,showPosition(position){
		// if (!this.hasFocus) return;	//	todo this is onbekend	
		var nl = "<br/>";
		var d = new Date(position.timestamp);
		var posstr =
			" lat:" + position.coords.latitude + nl +
			" lon:" + position.coords.longitude + nl +
			" accuracy:" + position.coords.accuracy + nl +
			" alt:" + position.coords.altitude + nl +
			" altaccuracy:" + position.coords.altitudeAccuracy + nl +
			" heading:" + position.coords.heading + nl +
			" speed:" + position.coords.speed + nl +
			" json:" + JSON.stringify( position ) + nl +
			" timestamp:" + d.getFullYear() + "-" + (1+d.getMonth()) + "-" + d.getDate() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();

		var div = document.getElementById( "geoloc" );
		div.innerHTML = posstr;
		
		//	send GLL
		//zeroisim.busTX('MC0',{req:mc0.setpos,latrad:position.coords.latitude*zeroisim.pi_180,lonrad:position.coords.longitude*zeroisim.pi_180});		//	set position
		//	send GGA
		var latlon = new zeroisim.LatLon({latrad:position.coords.latitude*zeroisim.pi_180,lonrad:position.coords.longitude*zeroisim.pi_180});
		var gga = {lat: latlon.latgll, NS: latlon.latsgn, lon: latlon.longll, EW: latlon.lonsgn};
		var ths = {hdgT: (null==position.coords.heading?0.00: position.coords.heading.toFixed(2))};
		var vtg = {cogT: (null==position.coords.heading?0.00: position.coords.heading.toFixed(2)), sogN: position.coords.speed};	//	todo unit type?
		
		if (false===this.positionHandled){
			this.positionHandled = true;
			if (undefined!=posplotobj)
				posplotobj.leafletmap.setView([latlon.latdeg,latlon.londeg],7);	// centre the map around this position
			}
		zeroisim.busTX('GGA',gga);
		zeroisim.busTX('THS',ths);
		zeroisim.busTX('VTG',vtg);
		}		
	,handleError(error) {
		// Update a div element with error.message.
		//	alert("handleError("+error+")");
		//$.post( "/maps/geolocation_log.php?position=error", "", rxajax );
		var div = document.getElementById( "geoloc" );
		div.innerHTML = error.message;
		}
	,mousedown: function(that,evt){
		}
	,mouseup: function(that,evt){
		}
	,mousemove: function(that,evt){
		}
	,draw: function(){
		}
	};

	
	/***
	if (navigator.geolocation) {
		navigator.geolocation.getCurrentPosition(showPosition);	// One-shot position request.
		var options = {
			timeout: 10000,
			maximumAge: 10000,
			enableHighAccuracy: true
			};
		var watchId = navigator.geolocation.watchPosition(showPosition, handleError, options );
		}
	else {
		// geolocation is not available... have you considered a forward-thinking browser?
		var div = document.getElementById( "text" );
		div.innerHTML = "geolocation is not available";
//		alert( "geolocation is not available" );
		}
	****/