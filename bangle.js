//	angle.js
//	Een hoek is een circulair ding. Een scalaire variabele die gebruikt wordt om een hoek voor te stellen, heeft als nadeel dat deze steeds binnen de waarden van de hoek gezet moet worden.
//	Toevallig zijn unsigned integers ook circulaire waarden, het ligt dus voor de hand om een hoek door een unsigned int voor te stellen en gebruik te maken van de circulaire eigenschappen.

//	Inzicht 2018 04 01:
//	waarschijnlijk kan dit niet met javascript efficient opgelost worden. Weak typing belet je om te forceren dat de ruwe waarde
//	in 16, 32, 64 of hoeveel dan ook bits geperst wordt.

'use strict';

class CBangle {
	constructor(rad){	
		this.bits = 31;	//	this implementation uses 32 bit signed integers
		this.maxuint = 1 << this.bits;
		// Number.MAX_SAFE_INTEGER
		this.maxuint--;
		this.maxuint--;
		this.maxuint--;
		this.maxuint--;
		this.maxuint--;
		this.maxuint--;
		
		var test;
		test = 0xFFFFFFFF;	//	ok
		test = 0xFFFFFFFFF;
		test = 0xFFFFFFFFFF;
		test = 0xFFFFFFFFFFFFFFFF;	//	18446744073709552000
		test = 0xFFFFFFFFFFFFFFFFF;	//	
		
		test = 1 << (this.bits-1);
		test--;
		// test++;
		this.rad = rad;	
		return this;
		}
	inc(){				this.bangle++;	return this;}
	
	get raw(){			return this.bangle;	}
	set raw(value){	this.bangle = parseInt(value);	return this;}
	
	get deg(){			return this.bangle * 360 / this.maxuint; 	}	
	set deg(degrees){	
	
		var test = parseFloat(degrees);
		test = (parseFloat(degrees) * this.maxuint);
		test = (parseFloat(degrees) * this.maxuint) / 360.0;
		test = parseInt((parseFloat(degrees) * this.maxuint) / 360.0);
		
		this.bangle = parseInt((parseFloat(degrees) * this.maxuint) / 360.0);	return this;}
	
	get rad(){			return this.bangle * Math.PI / this.maxuint;}
	set rad(radians){	this.bangle = parseInt(radians / Math.PI * this.maxuint);	return this;}
	
	sin(){				return Math.sin(this.rad);	}
	cos(){				return Math.cos(this.rad);	}
	tan(){				return Math.tan(this.rad);	}
	}
	
var bangle = new CBangle(0);
bangle.inc();	
for (var deg=0; deg<(2*360); deg++){
	bangle.deg = deg;
	var test = {deg:deg,b_deg:bangle.deg,raw:bangle.raw,sin:bangle.sin()};
	// console.log(deg+'deg='+bangle.deg+' sin='+bangle.sin());
	console.log(JSON.stringify(test));
	}
