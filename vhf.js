//	vhf.js
//	look at https://github.com/audiocogs/aurora.js
// https://www.pubnub.com/blog/2014-10-21-building-a-webrtc-video-and-voice-chat-application/
//	http://egmdss.com/en/gmdss-simulators
//	http://audior.ec/blog/recording-mp3-using-only-html5-and-javascript-recordmp3-js/
// https://webrtc.github.io/adapter/adapter-latest.js
//


// todo css animation om vertraagd RX knop te kleuren: keyframes
// rx.classlist.add / .remove / .contains
'use strict';

var microphone;
var tabvhf = {
	btns: ['1','2','3','4','5','6','7','8','9','DW','0','16']
	,btnid: 'vhfChanSel'
	,recorder: undefined	//	opname apparaat
	,audio_context: undefined	//	voor zowel opnemen als afspelen
	,bufferLen: 16384
	,vhfChannel: 16
	,rxflag: undefined
	,html: function(){
		// console.log('initting vhf');
		window.addEventListener("load", this.onload.bind(this), false);
		var attr = 'style="min-width:52px;min-height:40px;" id=';
		var btn = 0;
		var rc
			= h.tr(h.td(h.button(this.btns[btn],attr+this.btnid+btn++))
					+h.td(h.button(this.btns[btn],attr+this.btnid+btn++))
					+h.td(h.button(this.btns[btn],attr+this.btnid+btn++)))
			+ h.tr(h.td(h.button(this.btns[btn],attr+this.btnid+btn++))
					+h.td(h.button(this.btns[btn],attr+this.btnid+btn++))
					+h.td(h.button(this.btns[btn],attr+this.btnid+btn++)))
			+ h.tr(h.td(h.button(this.btns[btn],attr+this.btnid+btn++))
					+h.td(h.button(this.btns[btn],attr+this.btnid+btn++))
					+h.td(h.button(this.btns[btn],attr+this.btnid+btn++)))
			+ h.tr(h.td(h.button(this.btns[btn],attr+this.btnid+btn++))	//	*
					+h.td(h.button(this.btns[btn],attr+this.btnid+btn++))	//	0	
					+h.td(h.button(this.btns[btn],attr+this.btnid+btn++)))	//	#
			;
		rc = h.tr(h.td(h.table(rc))+h.td(h.table
			(h.tr(h.td(h.div(this.vhfChannel,'style="min-width:52px; min-height:40px;" id=vhfChannel')))
			// +h.tr(h.td(h.div('RX','id=vhfRX')))
			// +h.tr(h.td('TX'))
			+h.tr(h.td(h.div('PTT','style="min-width:52px; min-height:40px;" id=vhfPTTbtn'))))
			));
		rc += h.tr(h.td('mmsi','id=vhfMmsi colspan=4'));
		rc += h.tr(h.td('callsign','id=vhfCallsign colspan=4'));
		rc += h.tr(h.td('name','id=vhfName colspan=4'));
		return h.table(rc);	// return 'vhf tab' + h.canvas('id="vhf_canvas" width="480" height="340"');
		}
	,onfocus:function(){
		this.hasFocus=true;
		if (undefined==this.audio_context)	this.audioInit();
		this.recorder.onFocus();
		}
	,onblur:function(){
		this.hasFocus=false;
		}
	,onload: function(evt){	
		this.btns.myforEach(function(pp,btn,arr,that){	//	install click event handlers to the channel buttons
			document.getElementById(that.btnid+btn).addEventListener("click", that.vhfChanSelect.bind(that), false);
			},this);
		document.getElementById('vhfPTTbtn').addEventListener("mousedown", this.ptt_down.bind(this), false);
		document.getElementById('vhfPTTbtn').addEventListener("mouseup", this.ptt_up.bind(this), false);
		document.getElementById('vhfPTTbtn').addEventListener("mouseout", this.ptt_up.bind(this), false);	//	todo doe dit bij alle buttons
		// setValue('vhfRX','check_input_nok','className');
		// setInterval(function(){setValue('vhfRX','check_input_nok','className')},1000);
		this.rxflag = document.getElementById('vhfChannel');
		}
	,hasFocus: false
	,vhfChanSelect: function(evt){
		if ('DW'==evt.currentTarget.textContent){
			if (16==this.vhfChannel) {
				//	no dual watch possible if 16 is current channel
				}
			else{
				if (document.getElementById('vhfChanSel9').classList.contains('dualwatch')) {
					document.getElementById('vhfChanSel9').classList.remove('dualwatch');	//	hilite DW
					}
				else {
					document.getElementById('vhfChanSel9').classList.add('dualwatch');	//	hilite DW
					//	todo if receiving, hilite the '16' button
					}
				}
			}
		else {	//	the user types in a new channel
			//	todo als er weinig tijd verstreken is sinds laatste, voeg nummer toe, anders overschrijf wat er al staat
			this.vhfChannel += evt.currentTarget.textContent;
			this.vhfChannel = this.vhfChannel.substr(this.vhfChannel.length-2);
			setValue('vhfChannel',this.vhfChannel);
			if (16==this.vhfChannel){
				document.getElementById('vhfChanSel9').classList.remove('dualwatch');
				}
			}
		// post message to worker	
		var vhfChannels = [parseInt(this.vhfChannel)];
		if (document.getElementById('vhfChanSel9').classList.contains('dualwatch')){
			vhfChannels.push(16);
			}
		this.recorder.sendConfig({vhfChannels:vhfChannels});
		}
	,ptt: function(){
		var test = this.recorder.recording;
		}	
	,ptt_down: function(){
		this.recorder && this.recorder.pttOn();
		document.getElementById('vhfPTTbtn').classList.add('streaming');
		}
	,ptt_up: function(){
		document.getElementById('vhfPTTbtn').classList.remove('streaming');
		this.recorder && this.recorder.pttOff();
		}
	,__log: function(e, data) {
		console.log(e + " " + (data || ''));
		}
	,audioInit: function() {
		//	see https://developer.mozilla.org/en-US/docs/Web/API/AudioContext/createMediaStreamSource
		// try {
			window.AudioContext = window.AudioContext || window.webkitAudioContext;	// webkit shim
			navigator.getUserMedia = 
				(	navigator.getUserMedia 
				||	navigator.webkitGetUserMedia 
				||	navigator.mozGetUserMedia 
				//navigator.mediaDevices.getUserMedia	//	navigator.mozGetUserMedia //	include adapter.js <script src="https://webrtc.github.io/adapter/adapter-latest.js"></script>
				||	navigator.msGetUserMedia
				);
			//window.URL = window.URL || window.webkitURL;
			this.audio_context = new AudioContext;

			// als een aparte audio_context voor play gemaakt moet worden is dat hier
			this.__log('Audio context set up.');
			this.__log('navigator.getUserMedia ' + (navigator.getUserMedia ? 'available.' : 'not present!'));
			//todo enable the ptt button
			this.recorder = new Recorder(this,
				{numChannels: 1
				,sampleRate: this.audio_context.sampleRate
				,bufferLen: 16384
				});
				
			setValue('vhfName',h.h1(ship0.Name));
			setValue('vhfMmsi','mmsi: '+h.h2(ship0.mmsi));
			setValue('vhfCallsign','c/s: '+h.h2(ship0.CallSign));
			console.log('Recorder initialised.');
			// } 
		// catch (e) {
			// alert('No web audio support in this browser!');
			// }
		}
	};

	//	see https://www.chromium.org/Home/chromium-security/deprecating-powerful-features-on-insecure-origins
	var Recorder = function(that,cfg){
		var WORKER_PATH = './recorderWorker.js?'+(new Date()).toString();
		var config = cfg || {};
		var bufferLen = config.bufferLen || 16384;
		var numChannels = config.numChannels || 1;
		this.recording = false;
		this.that = that;
		var currCallback;
		this.onFocus = function(){
			if (undefined==this.stream){
				navigator.getUserMedia
					({audio: true, video: false}
					, this.keepStream.bind(this)		//	success, store the stream
					, function(e) {						//	geen success todo bind
						console.log('No live audio microphone: ' + e);	//	todo iets op het scherm zetten
						}
					);
				}
			}
		this.pttOn = function(){
			//	todo eerst posten kanaal, positie, antennehoogte, zendvermogen, ...
			this.startUserMedia(this.stream);		
			}
		this.pttOff = function(){
			microphone && microphone.disconnect();
			if (this.audioHandler){
				this.audioHandler.disconnect();
				this.audioHandler.onaudioprocess = undefined;	//	misschien
				}
			}
		this.keepStream = function(stream) {
			this.stream = stream;
			}
		this.startUserMedia = function(stream) {
			microphone = this.that.audio_context.createMediaStreamSource(stream);
			// console.log('Media stream created, input sample rate ' +microphone.context.sampleRate);
			//microphone.connect(microphone.context.destination);	// speel geluid meteen af todo listenbutton
			this.audioHandler = microphone.context.createScriptProcessor(bufferLen,numChannels,numChannels);
			this.audioHandler.onaudioprocess = function(e){
				// console.log('this.audioHandler.onaudioprocess()');	//	hier kun je de buffers posten naar een server
				worker.postMessage(
					{command: 'record'
					,sampleRate: microphone.context.sampleRate 
					,desiredSampleRate: 8000	//	make this a setting 
					,buffer: e.inputBuffer.getChannelData(0)
					});
				}
			microphone.connect(this.audioHandler);
			this.audioHandler.connect(microphone.context.destination);   
			}
		this.sendConfig = function(config) {	
			worker.postMessage({command: 'sendTXT',config: config});
			}
	
		// worker is gemaakt met truk: zie worker onderaan
		worker.recorder = this;
		worker.postMessage(
			{command: 'init'
			,config: 
				{sampleRate: 0	//	nog onbekend microphone.context.sampleRate
				,numChannels: numChannels
				}
			});
		//	todo aanzetten van vhf
		// if (ws && 'nmea-bridge'==ws.protocol)
			{
			//	connection is made, merely let know listening channel, position and so on
			console.log('connectWS '+settings.ws_addr);
			worker.postMessage(
				{command: 'connectWS'
				,config: 
					{wsaddress:settings.ws_addr
					,wsprotocol: 'listenVHF'
					,world: 'abc'		//	todo
					,sampleRate: that.audio_context.sampleRate
					,numChannels: numChannels
					,bufferLen: bufferLen
					,mmsi: ship0.mmsi
					,vhfChannels:[that.vhfChannel]	//	todo on every change channel, call this
					,hAntenna:25					//	todo from ship
					}
				});	
			}
		worker.onmessage = function(msg){
			switch (msg.data.done){
				case 'play':
					if (!this.recorder.that.rxflag.classList.contains('receiving')) this.recorder.that.rxflag.classList.add('receiving');					
					this.recorder.that.rxflag.classList.remove('stopreceiving');					
					// console.log('play!');
					var buffer = this.recorder.that.audio_context.createBuffer(1, bufferLen, 8000 /*this.recorder.that.audio_context.sampleRate*/);
					buffer.copyToChannel(msg.data.buffer,0);
					var speaker = this.recorder.that.audio_context.createBufferSource();
					speaker.rxflag = this.recorder.that.rxflag;
					speaker.onended = function(evt){
						// console.log('speaker onended');
						var rxflag = evt.target.rxflag;
						rxflag.classList.remove('receiving');					
						if (!rxflag.classList.contains('stopreceiving')) 
							rxflag.classList.add('stopreceiving');					
						}
					speaker.buffer = buffer;
					speaker.connect(this.recorder.that.audio_context.destination);
					speaker.start(0);
					break;
				default:
					console.log('Worker has done:' + msg.data.done);
					break;
				}
			}
		};
	
function runWorker(fn) {
	return new Worker(URL.createObjectURL(new Blob(['('+fn+')()'])));
	}
	
const worker = runWorker(function() {
	//	worker in same file: http://jsbin.com/fitagudaqe/edit?js,console
	//	when worker is in the same file, running under the file://-domain gives no problem with worker
	var recLength = 0,
		wsRxTx = undefined,	//	binary ws wordt hier gemaakt
		numChannels;

	this.onmessage = function(e){
		switch(e.data.command){
			case 'init':		init(e.data.config);		break;
			// case 'connect':	connect(e.data.config);	break;
			// case 'record':		record(e.data.buffer);	break;
			case 'record':		record(e.data.buffer,e.data.sampleRate,e.data.desiredSampleRate);	break;
			// case 'exportWAV':	exportWAV(e.data.type);	break;
			// case 'getBuffer':	getBuffer();				break;
			case 'switchOFF':	clear();							break;	//	todo when vhf is switched off
			case 'connectWS':	connectWS(e.data.config);	break;	//	connect to WSS
			case 'sendTXT'	:	sendTXT(e.data.config);		break;
			}
		};
	function init(config){
		numChannels = config.numChannels;
		// initBuffers();
		this.postMessage({done:'init'});
		}
	function connectWS(config){	//	is called after switching vhf on
		postMessage({done:'RX connecting'});
		wsRxTx = new WebSocket(config.wsaddress,config.wsprotocol);
		wsRxTx.binaryType = "arraybuffer";
		wsRxTx.onopen = function(){
			wsRxTx.send(JSON.stringify(config));
			postMessage({done:'RX connected'});
			};
		wsRxTx.onmessage = function(evt){						//	audio received
			var toplay = new Float32Array(evt.data);
			postMessage({done:'play',buffer:toplay}); 
			};
		wsRxTx.onclose = function(mythis,mythat){			// websocket is closed.
			postMessage({done:'RX disconnected'});
			wsRxTx = undefined;
			};
		wsRxTx.onerror = function(){
			postMessage({done:'RX error'});
			};	
		}
	function sendTXT(obj){
		wsRxTx.send(JSON.stringify(obj));
		}
	function record(inputBuffer,sampleRate,desiredSampleRate){
		if (undefined!=wsRxTx){
			if (wsRxTx.readyState==1){
				var factor = Math.trunc(sampleRate / desiredSampleRate);
				if (1==factor){
					wsRxTx.send(inputBuffer);
					this.postMessage({done:'sent: '+inputBuffer.length+' bytes to wss'});
					}
				else{	
					//	resample the data to make it more compact
					var resampled = new Float32Array(Math.floor(inputBuffer.length / factor));
					for (var ii = 0; ii < resampled.length; ii++) {
					// factor -=1;	
					// for (var ii = 0; ii < result.length; ii+factor) {
						resampled[ii] = inputBuffer[ii*factor];
						}
					wsRxTx.send(resampled);	//	onder firefox krijg ik vaak onvolledige pakketten, met wireshark uitzoeken wat hier het verschip met vivaldi is
					this.postMessage({done:'sent: '+resampled.length+' bytes to wss'});
					}				
				}
			}
		}
	function clear(){
		this.postMessage({done:'clear'});
		if (undefined!=wsRxTx){
			this.postMessage({done:'disconnecting'});
			wsRxTx.close();
			}
		else {
			this.postMessage({done:'wsRxTx is undefined'});
			}	
		}
	/*
	You can change the binaryType on the fly, but it only affects the datatype of received messages. 
	To send binary data you must either send a blob or a typed array. If you send a string it will always be sent (and delivered) as a string.
	In the wire protocol, a message can be either 'text' meaning the sender sent data encoded as a string, 
	or it can be 'binary' which means that the message contains a raw encoded byte stream. 
	The binaryType indicates how that raw data should be delivered to the application.

	In other words, when you set binaryType it means that the next time the message handler fires for receipt of a binary message, 
	that event data will be of type binaryType.
	*/	
	
	});

	