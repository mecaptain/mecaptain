//pseudocode from https://en.wikipedia.org/wiki/PID_controller
//pseudocode from https://en.wikipedia.org/wiki/PID_controller
//
//start:
//  error = setpoint - measured_value
//  integral = integral + error*dt
//  derivative = (error - previous_error)/dt
//  output = Kp*error + Ki*integral + Kd*derivative
//  previous_error = error
//  wait(dt)
//  goto start
//	todo maybe implement deadzone
//	todo apply kalman filter to calculated rot as it is very noisy (https://www.cs.utexas.edu/~teammco/misc/kalman_filter/)
//
//
'use strict';
var PID_controller = function(){	//	Kp,Ki,Kd
	// var Kp,Ki,Kd,output,previous_error,integral,useRot;
	aaaIdentifier: 'PID_controller';
	return {
		init: function(pid,cb,that){
			var divdbg = divdbg;
			this.cb = cb;
			this.cb_that = that;
			this.integral = 0;
			this.previous_error = 0;
			this.values =
				{Kp_hdg:0,		Ki_hdg:0,		Kd_hdg:0
				,Kp_rot:0,		Ki_rot:0,		Kd_rot:0
				,Kp_trk:0,		Ki_trk:0,		Kd_trk:0
				,Kp_rad:0,		Ki_rad:0,		Kd_rad:0
				,useRot:0,mode:'change_hdg',rlimp:0,rlims:0};
			this.output = 0;
			// Kp=Ki=Kd=output=previous_error=integral=useRot=0;
			// var mode = 'change_hdg';

			// var rlimp,rlims;	// rudder limits port and starboard
			this.clear();
			this.setvalues(pid);
			},
		clear: function(pid,cb){
			this.previous_error=this.integral=0;
			},
		debug: function(p,i,d,integral,derivate,mode){
			// todo send pid values
			this.piddebug = {P:p,I:i,D:d};	//	opvraagbaar in iautopilot.js
			/*
			if (undefined!=this.cb){
				switch (mode){
					case 'keep_hdg':
					case 'change_hdg':	this.cb(this.cb_that,this.values.Kp_hdg,this.values.Ki_hdg,this.values.Kd_hdg,this.previous_error,this.output,p,i,d,integral,derivate,this.values.useRot,mode);	break;
					case 'keep_rot':
					case 'change_rot':	this.cb(this.cb_that,this.values.Kp_rot,this.values.Ki_rot,this.values.Kd_rot,this.previous_error,this.output,p,i,d,integral,derivate,this.values.useRot,mode);	break;
					case 'keep_rad':
					case 'change_rad':	this.cb(this.cb_that,this.values.Kp_rad,this.values.Ki_rad,this.values.Kd_rad,this.previous_error,this.output,p,i,d,integral,derivate,this.values.useRot,mode);	break;
					case 'keep_trk':
					case 'change_trk':	this.cb(this.cb_that,this.values.Kp_trk,this.values.Ki_trk,this.values.Kd_trk,this.previous_error,this.output,p,i,d,integral,derivate,this.values.useRot,mode);	break;
					}
				}
			*/	
			},
		tick: function(dt,error,rot){
			switch (this.values.mode){
				case 'change_hdg':
					var Kp = this.values.Kp_hdg;
					var Ki = this.values.Ki_hdg;
					var Kd = this.values.Kd_hdg;
					this.integral = 0;
					if (Math.abs(error) < 1){
						this.values.mode = 'keep_hdg';
						// setValue('ap_mode',this.values.mode);
						}
					break;
				case 'keep_hdg':
					var Kp = this.values.Kp_hdg;
					var Ki = this.values.Ki_hdg;
					var Kd = this.values.Kd_hdg;
					this.integral += error * dt / 1000.0;
					break
				case 'change_rot':	//	todo hier nog eens naar kijken, error moet verschil in ROT's zijn, niet op integrator vertrouwen
					var Kp = this.values.Kp_rot;
					var Ki = this.values.Ki_rot;
					var Kd = this.values.Kd_rot;
					this.integral = 0;
					if (Math.abs(error) < 0.4){
						this.values.mode = 'keep_rot';
						// setValue('ap_mode',this.values.mode);
						}
					break;
				case 'keep_rot':
					var Kp = this.values.Kp_rot;
					var Ki = this.values.Ki_rot;
					var Kd = this.values.Kd_rot;
					this.integral += error * dt / 1000.0;
					break;
				}
			var dhdg = (error - this.previous_error)*1000.0*60.0/dt;
			if (undefined!=rot && this.values.useRot){
				var derivate = rot / 60.0;
				}
			else	{
				var derivate = dhdg / 60;
				}
			//setValue('ap_rothdg',zeroisim.roundNumber(dhdg,2));
			this.output = Kp*error + Ki*this.integral + Kd*derivate
			this.previous_error = error;
			this.debug(Kp*error,Ki*this.integral,Kd*derivate,this.integral,derivate,this.values.mode);
			//	apply rudder limits todo in case of rot, output is a rudder correction. In case of hdg, output is a rudder command
			this.output = Math.max(this.values.rlimp,this.output);
			this.output = Math.min(this.values.rlims,this.output);
			return this.output;
			},
		setvalues: function(pid){
			if (undefined==pid)	return;
			
			if (undefined!=pid.Kp_hdg)		{this.values.Kp_hdg = pid.Kp_hdg;}
			if (undefined!=pid.Ki_hdg)		{this.values.Ki_hdg = pid.Ki_hdg;}
			if (undefined!=pid.Kd_hdg)		{this.values.Kd_hdg = pid.Kd_hdg;}
                                                                        
			if (undefined!=pid.Kp_rot)		{this.values.Kp_rot = pid.Kp_rot;}
			if (undefined!=pid.Ki_rot)		{this.values.Ki_rot = pid.Ki_rot;}
			if (undefined!=pid.Kd_rot)		{this.values.Kd_rot = pid.Kd_rot;}
                                                                        
			if (undefined!=pid.Kp_rad)		{this.values.Kp_rad = pid.Kp_rad;}
			if (undefined!=pid.Ki_rad)		{this.values.Ki_rad = pid.Ki_rad;}
			if (undefined!=pid.Kd_rad)		{this.values.Kd_rad = pid.Kd_rad;}
                                                                        
			if (undefined!=pid.Kp_trk)		{this.values.Kp_trk = pid.Kp_trk;}
			if (undefined!=pid.Ki_trk)		{this.values.Ki_trk = pid.Ki_trk;}
			if (undefined!=pid.Kd_trk)		{this.values.Kd_trk = pid.Kd_trk;}

			if (undefined!=pid.useRot)		{this.values.useRot = pid.useRot;}
			if (undefined!=pid.mode)		{this.values.mode = pid.mode;}
			if (undefined!=pid.rlimp)		{this.values.rlimp = pid.rlimp;}
			if (undefined!=pid.rlims)		{this.values.rlims = pid.rlims;}
			
			this.debug(0,0,0,this.integral,0,this.values.mode	);
			},
		}
	}
