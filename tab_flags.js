//	tab_flags.js
//	see 
//	https://threejs.org/examples/#webgl_loader_sea3d_morph
// https://threejs.org/examples/#webgl_physics_cloth
// https://threejs.org/examples/?q=clo#webgl_animation_cloth

'use strict';
var flagstabobj = {
	// verplichte drie members om de tabs interface te ondersteunen
	html: function(){
		// console.log('initting flagstabObj');
		this.bSomeBoolean = false;
		this.hoisted = [];

		//	this init is called before page is loaded. Defer initialisation, you need the canvas for it, to after pageload event
		document.flagstabobj = this;
		window.addEventListener("load", this.initAfterLoad.bind(this), false);
		
		var ii = 0;
		var flag_tr = '';
		var flag_table = '';
		this.flags = 
			['alpha'
			,'bravo'
			,'charlie'
			,'delta'
			,'echo'
			,'foxtrot'
			,'golf'
			,'hotel'
			,'india'
			,'juliet'
			,'kilo'
			,'lima'
			,'mike'
			,'november'
			,'oscar'
			,'papa'
			,'quebec'
			,'romeo'
			,'sierra'
			,'tango'
			,'uniform'
			,'victor'
			,'whisky'
			,'xray'
			,'yankee'
			,'zulu'
			,'tack'
			];
		var flagsize = {w:100,h:80};	
		var tdsize = {w:160,h:130};	
		this.flags.myforEach(function(flag,el,ar,p){
			if (!(ii++ % 6)) {
				flag_table += h.tr(flag_tr);
				flag_tr = '';
				}
			flag_tr += h.td(h.div(h.figure(h.figcaption(flag)	//	+
				+h.img('','src="./img/flags/svg/'+flag+'.svg" width="'+flagsize.w+'%" height="'+flagsize.h+'%"')
				)	
				,'id="flag_'+flag+'" nato="'+flag+'"')		//	div
				,'width="'+tdsize.w+'px" height="'+tdsize.h+'px"');	//	td
			});
		flag_table += h.tr(flag_tr);

		var flagline_opts = ['','geus','vlaggestok','bakboord','midden','stuurboord'];	//	todo moet van schip komen
		var html = 
			h.table(h.tr(h.td(h.table(flag_table))
				+h.td(h.table
					(h.tr(h.td(h.table(h.tr(h.td(h.div('flagline'))) + h.tr(h.td(h.select('select_flagline',flagline_opts))))))
					+h.tr(h.td(h.div('dip/closeup'))) 
					+h.tr(h.td('clear/exec'))
					))
				))
				;
			
		return html;
		}
	,onfocus:function(){	
		this.hasFocus=true;
		}
	,onblur:function(){	
		this.hasFocus=false;	
		}
	,hasFocus: false
	,initAfterLoad: function(){
		this.flags.myforEach(function(flag,el,ar,that){	//	assign click handler to flags
			var tdflag = document.getElementById('flag_'+flag);
			tdflag.addEventListener('click', that.flag_clicked);
			tdflag.that = that;			
			},this);
		}
	,flag_clicked(evt){
		var tdflag = evt.currentTarget;
		var that = tdflag.that;
		that.hoisted.push(tdflag.getAttribute('nato'));
		}
	,draw: function(){
		}
	};
