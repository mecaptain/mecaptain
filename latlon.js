//	latlon.js
//	(c) Bert Tuijl
//	zeroisim project
/**/
(function(global,factory){
	typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports) :
	typeof define === 'function' && define.amd ? define(['exports'], factory) :
	// (factory((global.LatLon = {})));
	(factory(zeroisim));	//	add to zeroisim
	}(this, (function(exports) {
/**/
'use strict';
class LatLon {	
	constructor(latlon){
		this._leaflet = undefined;	//	conversion to and from leaflet latlng
		this._degtorad = Math.PI / 180.0;
		this._radtodeg = 180.0 / Math.PI;
		this._signmatrix =
			{'N': +1
			,'S': -1
			,'E': +1
			,'W': -1
			};
		if (undefined!=latlon)	{
			this.set(latlon);
			}
		else {
			this._latrad = undefined;	//	store latitude in radians
			this._lonrad = undefined;	//	store longitude in radians
			this._latdeg = this._londeg = 0.0;
			}
		}
	set(latlon){
		if (undefined!=latlon.leaflet){this.latdeg = latlon.leaflet.lat;	this.londeg = latlon.leaflet.lng;	}
		if (undefined!=latlon.latrad)	{this._latrad = latlon.latrad;		this._latdeg = this._radtodeg * this._latrad;}
		if (undefined!=latlon.lonrad)	{this._lonrad = latlon.lonrad;		this._londeg = this._radtodeg * this._lonrad;}
		if (undefined!=latlon.latdeg)	{this._latdeg = latlon.latdeg;		this._latrad = this._degtorad * this._latdeg;}
		if (undefined!=latlon.londeg)	{this._londeg = latlon.londeg;		this._lonrad = this._degtorad * this._londeg;}
		}
	toJSON(key){	//	for nice JSON.stringifycation, only latrad and lonrad are included in serialization
		// return '{"latrad":'+this._latrad+',"lonrad":'+this._lonrad+'}';	//	looks nice, readable
		return '{"a":'+this._latrad+',"o":'+this._lonrad+'}';	//	much smaller 
		}
	get latdeg(){	return this._latdeg;	}
	get londeg(){	return this._londeg;	}

	get latrad(){	return this._latrad;	}
	get lonrad(){	return this._lonrad;	}

	get latgll(){	return this.gllold(this.latdeg,4,5);	}
	get longll(){	return this.gllold(this.londeg,5,5);	}

	get latgll2(){	return this.gllold(this.latdeg,4,2);	}
	get longll2(){	return this.gllold(this.londeg,5,2);	}

	get latsgn(){	return this.gllsgn(this.latrad,['N','S']);	}
	get lonsgn(){	return this.gllsgn(this.lonrad,['E','W']);	}
	
	get latdegmindec(){	return this.ggmmd(this.latdeg,2,2,[' N',' S']);	}
	get londegmindec(){	return this.ggmmd(this.londeg,3,2,[' E',' W']);	}

	get latdegmindecnew(){	return this.ggmmdnew(this.latdeg);	}
	get londegmindecnew(){	return this.ggmmdnew(this.londeg);	}

	set latdeg(latdeg){	this._latdeg = latdeg;	this._latrad = this._degtorad * this._latdeg;}
	set londeg(londeg){	this._londeg = londeg;	this._lonrad = this._degtorad * this._londeg;}
	
	set latrad(latrad){	this._latrad = latrad;	this._latdeg = this._radtodeg * this._latrad;}
	set lonrad(lonrad){	this._lonrad = lonrad;	this._londeg = this._radtodeg * this._lonrad;}
	
	set latgll(latgll){	this.latdeg = latgll/100;}
	set longll(longll){	this.londeg = longll/100;}
	
	set latgga(latgll){	this.latdeg = Math.trunc(latgll/100)+this.Frac(latgll/100)/0.6;}
	set longga(longll){	this.londeg = Math.trunc(longll/100)+this.Frac(longll/100)/0.6;}
	
	set latdegmindec(latdegmindec){	this.latdeg = Math.sign(latdegmindec)*(Math.floor(Math.abs(latdegmindec)) + this.Frac(Math.abs(latdegmindec))/0.6);}
	set londegmindec(londegmindec){	this.londeg = Math.sign(londegmindec)*(Math.floor(Math.abs(londegmindec)) + this.Frac(Math.abs(londegmindec))/0.6);}
	
	get leaflet(){	return {lat: this._latdeg, lng: this._londeg}}
	set leaflet(latlng){	this._leaflet=latlng;this._latdeg=latlng.lat;this._londeg=latlng.lng;this._latrad=latlng.lat*this._degtorad;this._lonrad=latlng.lng*this._degtorad;}
	
	set beardist(bd){
		// bd.bearing,bd.distance
		// var cosdl = (Math.cos(bd.distance*this._degtorad/60.0)-Math.sin()) / ()
		this._latrad = 0.0;
		this._lonrad = 0.0;
		}

	interpretLatLonGGA(data){	//	works also for GLL sentences
		this.latgga = data.lat;
		this.longga = data.lon;
		this.latdeg*= this._signmatrix[data.NS];
		this.londeg*= this._signmatrix[data.EW];
		}
	interpretLatLonRMC(data){	//	works also for GLL sentences
		this.latgga = data.lat;
		this.longga = data.lon;
		this.latdeg*= this._signmatrix[data.slat];
		this.londeg*= this._signmatrix[data.slon];
		}
	Frac(num){
		var rc = num - Math.floor(num);
		return rc;	//	todo test for negative values
		}		
	gll(num,digits,decimals){
		var rc = 100*Math.abs(num);
		//	return (num<0?'-': '')+this.prefixpad(rc.toFixed(decimals),digits);
		return this.prefixpad(rc.toFixed(decimals),digits);
		} 
	gllold(num,digits,decimals){
		num = Math.abs(num);
		var rc = Math.floor(num) * 100;
		rc += 60*this.Frac(num);
		return this.prefixpad(rc.toFixed(decimals),digits);
		} 
	gllsgn(num,posneg){
		var rc = posneg[0];
		if (num<0)	rc = posneg[1];
		return rc;
		};
	ggmmd(num,digits,decimals,posneg){
		var rc = {'degrees':0,'minutes':0,'sign':' '};
		rc.degrees = this.prefixpad(Math.floor(Math.abs(num)).toString(),digits);
		rc.minutes = this.prefixpad((60*(Math.abs(num)-Math.floor(Math.abs(num)))).toFixed(decimals),2);
		rc.sign = posneg[0];
		if (num<0)	rc.sign=posneg[1];
		return rc.degrees + h.deg + rc.minutes + "'" + rc.sign;
		};
	ggmmdnew(num){
		var rc = {'degrees':0,'minutes':0,'sign':' '};
		rc.degrees = Math.floor(Math.abs(num));
		rc.minutes = 0.6*(Math.abs(num)-rc.degrees);
		if (num<0)	rc.degrees*=-1;
		return rc.degrees + rc.minutes;
		};
	prefixpad(num,digits){
		var pad = '';
		for (var power=1; power<digits; power++){
			var factor = Math.pow(10,power);
			if (Math.abs(num)<factor){
				pad += '0';
				}
			}
		return pad + num;
		};
	beardist(ll){
		var R = 6371.000; // mean radius of planet earth
		var d = Math.acos(	Math.sin( this.latrad ) * Math.sin( ll.latrad )
			+	Math.cos( this.latrad ) * Math.cos( ll.latrad ) * Math.cos( ll.lonrad-this.lonrad ) );
		
		var b = Math.acos((Math.sin(this.latrad)-Math.sin(ll.latrad)*Math.cos(d))/Math.sin(d)/Math.cos(ll.latrad));
		if (Math.sin(this.lonrad-ll.lonrad) < 0){
			b = 2*Math.PI - b;
			}
		b *= 10*this._radtodeg;	
		b = Math.round(b)/10; // prefixen zodat drie cijfers ontstaan
		b = (b<100?(b<10?'0':'')+'0':'')+b; // prefixen zodat drie cijfers ontstaan
			
		d *= 6000*this._radtodeg;
		d = Math.round(d)/100;
		
		//R * d;	//	kilometers
		//60*180/pi * d zeemijlen
		return {b: b, d: d};
		}
	meters_diff(ll){	//	assume x related to longitude, y related to height, z to latitude
		var rc = {x: (this.lonrad-ll.lonrad)*Math.cos((this.lonrad+ll.lonrad)/2)
					,y: 0
					,z: this.latrad-ll.latrad
					};
		rc.x *= this._radtodeg;	rc.z *= this._radtodeg;		
		rc.x *= -60*1852;			rc.z *= 60*1852;		
		return rc;
		}
	};	//	class LatLon 
	exports.LatLon = LatLon;
	})));
