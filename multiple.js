//	multiple.js
// zeroisim in simulator mode, create a world with this ship in it, other ships in this world are seen on radar, vhf and ais

var world=zeroisim.gup('world','abc')
var ship=zeroisim.gup('ship','Bliss');
settings.ws_addr=zeroisim.gup('ws_addr','ws://bert-toshsat:12120');
var sentences = eval(zeroisim.gup('sentences'));	
var ecdisadress = zeroisim.gup('ecdisadress','udp://localhost:1500');
var position = zeroisim.gup('position','maasgeul in');
var heading = zeroisim.gup('heading');

intern_simulator.setSimShip(ship);
var op = [{"address":ecdisadress,"sentences":['VDM'],ws:undefined}];
intern_simulator.is.ship.NmeaOutputChannels = op;
intern_simulator.is.ship.latlon= new LatLon(positions[position]);
intern_simulator.is.ship.hdt= parseInt(heading);

setValue('inp_ws_addr',settings.ws_addr,'value',1);	
setValue('talkaddr',op[0].address,'value',1);
// wsStartListenToNmeaShip0();
wsNmeaBridge({ws_addr:settings.ws_addr,world:world,getMMSI:1,getCallSign:1,verbose:0},intern_simulator.is.ship);

// todo op het connect event zorgen dat op.ws deze websocket is dmv callback

window.is_engage.click();	//	start de simulator
