//	tab_usbcontrols.js
//	receive events from USB 'game'-device
//	todo attach actions to individual axis/buttons
//	todo make userinterface for that
//	todo eleborate userinterface with graph
//	todo make calibration function in ws server
//
'use strict';
var tabobj_usbcontrols = {
	// verplichte drie members om de tabs interface te ondersteunen
	html: function(){
		document.tabobj_usbcontrols = this;
		window.addEventListener("load", this.initAfterLoad.bind(this), false);
		return h.div('sorry, no USB-controls found connected to this computer...','id="show_usb_controls"');
		}
	,onfocus:function(){	
		this.hasFocus=true;
		}
	,onblur:function(){	
		this.hasFocus=false;	
		}
	,hasFocus: false
	,initAfterLoad: function(that){
		this.controls = undefined;
		gamepadSupport.init(this);
		document.getElementById('show_usb_controls').addEventListener("mousedown", this.mousedown.bind(this), false);

		if (1==Object.keys(_tabses).length)	//	if only one member, focus is true
			this.hasFocus = true;
		}
	,mousedown: function(evt){
		this.onfocus();
		}
	,mouseup: function(that,evt){
		}
	,mousemove: function(that,evt){
		}
	,draw: function(){
		}
	,objAsTable: function(obj,att,firstrow,bGraphic){
		if (undefined==obj)	return '';
		var table='';
		if (undefined!=firstrow) {
			var row='';
			firstrow.myforEach(function(el,ii,ar,that){
				row += h.td(el);
				});
			table += h.tr(row);
			}
		for (var ii in obj) {
			if (!(['id','mapping','index','connected','timestamp'].find(function(el){return (el==ii);}))) {	//	no interest in showing every field
				var value=obj[ii];
				// var test = typeof value;
				if (typeof value!=="function") {
					var Graphic = '';
					if (typeof value==="object" && (undefined!=value.pressed) && (undefined!=value.value)){	//	GamepadButton
						value = value.value;	//	show only one column: value
						}
					if (typeof value==="object") {
						if ('axes'==ii) 
							// value = this.objAsTable(value,att,['axis id',h.nbsp+h.nbsp+h.nbsp+'value'+h.nbsp+h.nbsp+h.nbsp,'value animated','function'],{ww:300,wslider:12,ff:function(p0,p1,p2){return (p0-p1)*(p2+1)/2;}});//	assume [-1 <= value <= +1]
							value = this.objAsTable(value,att,['axis id',h.nbsp+h.nbsp+h.nbsp+'value'+h.nbsp+h.nbsp+h.nbsp,'value animated','filter','curve','output function','when to send; on...','destination'],{ww:300,wslider:12,ff:function(p0,p1,p2){return (p0-p1)*(p2+1)/2;}});//	assume [-1 <= value <= +1]
						else if ('buttons'==ii) 
							value = this.objAsTable(value,att,['button id',h.nbsp+h.nbsp+h.nbsp+'value'+h.nbsp+h.nbsp+h.nbsp,'value animated','filter','curve','output function','when to send','destination'],{ww:300,wslider:150,ff:function(p0,p1,p2){return (p0-p1)*p2;}});	//	assume value [0,1]
						else {
							value = this.objAsTable(value,att,[]);
							}
						}
					else {
						if (bGraphic) {	//	a graphic object is supplied, draw it and put in table data. Moreover, present functions to user so he can have the inputs really do something
							var lmargin = bGraphic.ff(bGraphic.ww,bGraphic.wslider,value);	//	assume value [0,1]
							Graphic = h.td(h.div(h.div(''
								,h.attributes({style:'position:absolute;width:'+bGraphic.wslider+'px;height:15px;background-color:red;margin-left:'+lmargin+'px;'}))
								,h.attributes({style:'position:relative;width:'+bGraphic.ww+'px;height:15px;border:1px solid silver;'}))
								);
							value = zeroisim.round(value,4);	
							Graphic += h.td(h.select3('sel_filter',['none','kalman','schmitt']));
							Graphic += h.td('curve todo');
							Graphic += h.td('outfun todo');
							// Graphic += h.td(h.table
								// (h.tr(h.td(h.input('on change','type=checkbox')))
								// +h.tr(h.td(h.input('on timer','type=checkbox')))
								// +h.tr(h.td(h.input('on demand','type=checkbox')))
								// ));
							Graphic += h.td(h.table(h.tr
								(h.td(h.input('change','type=checkbox'))
								+h.td(h.input('timer','type=checkbox'))
								+h.td(h.input('demand','type=checkbox'))
								,' style="font-family: Courier New; font-size: 0.6em"')));
							Graphic += h.td('dest todo');
							}
						else {
							}	
						}
					table += h.tr(h.td(ii)+h.td(value)+Graphic);
					}
				}
			}
		return h.table(table,att);
		}
	,updateGamepads: function(controls) {	//	rx the state of all connected controls after init
		//	create a table with a column for every supported controller
		// console.log('updateGamepads');
		var row0 = '';
		var row1 = '';
		controls.myforEach( function(el,idx,ar,that){
			row0 += h.td(h.div(el.id));
			row1 += h.td(h.div('press a button or change an axis on your control','id="btnsNaxisOfController_'+el.index+'"'));
			}
			,this);
		setValue('show_usb_controls',h.table(h.tr(row0)+h.tr(row1)));
		this.controls = controls;
		}
	,updateGamepad: function(control) {	//	here you only get the change in one control
		//	makkelijke manier om axis0 als ap_gui.setpoint te krijgen
		//	todo onder een button hangen, ap.setpoint.set(zeroisim.round(100*control.axes[6],0));
				
		if (!this.hasFocus)	return;		
		if (undefined==this.controls)	{
			return updateGamepads({0:control});
			}
		if (undefined!=control.index) {
			/**/ console.log('updateGamepad, control='+JSON.stringify(control));
			setValue('btnsNaxisOfController_'+control.index,this.objAsTable(control));
			var sin = control.axes[0];		var cos = control.axes[1];
			var angle = Math.atan2(sin,-cos) * 180.0/ Math.PI;	//	werkt op mijn MS SideWinder
			console.log('angle='+zeroisim.round(angle,0));
			}
		}
	};	//	tabobj_usbcontrols
	