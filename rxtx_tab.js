//	rxtx_tab.js
'use strict';
function buildNMEAtable(key,rxtx,div_without_key,bAdd2Namespace,bSenderPart,date){	//	bouw de tabs voor de verzending (tx) van nmea
	if (undefined==bAdd2Namespace)	bAdd2Namespace = false;
	if (undefined==bSenderPart)	bSenderPart = false;
	var page = '';
	var nmeadef = nmeadefs[key];
	if (rx==rxtx)	{
		var regex_options = '';
		page =	makerow(['def','value','valid','regex','description','plot']);	//	table header
		if (undefined==talker_ids[key]){		
			return;	//	no data received yet	
			}
		else {	//	tell where data is coming from
			var talkerid = talker_ids[key];		
			page  +=	makerow(['from',	rxtx[key][talkerid]['from'],	'','','address of sender of sentence',h.nbsp]);	//	network source of this sentence
			page  +=	makerow(['to',		rxtx[key][talkerid]['to'],		'','','address of receiver of sentence',h.nbsp]);		//	network endpoint of this sentence
			}
		}
	if (settings.check_checksum)	var strs = [];
	var last = nmeadef.length-1;
	for (var jj in nmeadef){	//	show all fields of this sentence 
		var field = nmeadef[jj][0];
		if (tx==rxtx){
			if (0==jj) continue;		//	eerste field is een omschrijving
			page+= h.tr
				(h.td(field)
				// +h.td(tdvalue = h.input('','id="tx.'+key+'.'+field+'" type="text" class="'+txclass.className+'" onkeyup=changeTXvalue(this,\''+key+'\','+jj+') onchange=changeTXvalue(this,\''+key+'\','+jj+') value="'+rxtx[key][field]+'"'))
				+h.td(h.input('','id="tx.'+key+'.'+field+'" type="text" class="check_input_nok" onkeyup=changeTXvalue(this,\''+key+'\','+jj+') onchange=changeTXvalue(this,\''+key+'\','+jj+') value="'+rxtx[key][field]+'"'))
				+h.td('mask')	//	mask
				// +h.td(nmeadef[jj][1])	//	mask
				+h.td(nmeadef[jj][2])	//	description
				);
			}
		else if (rx==rxtx){
			if (0==jj) continue;		//	eerste field is een omschrijving
			var valid = h.nbsp;
			var checkpattern = nmeadef[jj][1];
			if (last==jj) {
				if (settings.check_checksum){
					strs = strs.join(',');
					var chksum_calculated = nmeaChecksum(strs);
					valid = (parseInt('0'+chksum_calculated)==parseInt(rxtx[key][talkerid][field]));
					checkpattern = chksum_calculated;
					}
				}
			else {	
				if (settings.check_checksum){
					strs.push(rxtx[key][talkerid][field]);
					}
				if (settings.check_pattern) {
					var regex = new RegExp(nmeadef[jj][1],regex_options);
					valid = regex.test(rxtx[key][talkerid][field]);
					}
				else if (b64==nmeadef[jj][1]){	//	decode AIS, maybe have a decoding function in nmeadefs
					}
				}
			page += makerow(
				[field	//	field
				,rxtx[key][talkerid][field]	//	value
				,valid
				,checkpattern		//	checkpattern
				,nmeadef[jj][2]	//	summary
				,chboxplot(key,talkerid,field)
				]);
			}
		}
	if (true===settings.showraw){	//	todo show raw nmea
		page += makerow(
			['raw'	//	field
			,rxtx[key].raw	
			,h.nbsp
			,h.nbsp
			,'raw NMEA sentence'
			,h.nbsp
			]);
		}
				
	// todo add age of reception of sentence here
	// var age = age();
	page += makerow(
		['age'	//	field
		// ,'00:00:00'	//	value	todo div here must be put here on focuschange	date.getMinutes() date.getSeconds()
		,(undefined==date)?'00:00:00': date.toLocaleTimeString()
		,h.nbsp
		,h.nbsp
		,'time since last reception'
		,h.nbsp
		]);
	
	page = h.h3(nmeadef[0].title)+h.table(page);
	if (bSenderPart){	
		page+= h.br+ h.table
			( h.tr(h.td(h.button('defaults','onclick=setDefaults(\''+key+'\')'),'style="width:39%;"')
				+ h.td(h.nbsp))
			+ h.tr(h.td(h.button('copy rx','onclick=copyRX(\''+key+'\')'),'style="width:39%;"')
				+ h.td(h.div('','id="copyrx_tx_'+key+'"'),' style="width:60%;"'))
			+ h.td(h.input(' ','type="range" min="0" max="100" oninput=handleslide(this)'))
				+h.td(h.div(' ','id="slideroutput_'+key+'"'))
			+ h.tr(h.td(h.button('json','onclick=getJson(\''+key+'\')'),'style="width:39%;"')
				+ h.td(h.div('','id="json_tx_'+key+'"'),' style="width:60%;"'))
			+ h.tr(h.td(h.button('nmea','onclick=setValue(\'nmea_tx_'+key+'\',buildNMEA(\''+key+'\',1))'),'style="width:39%;"')
				+ h.td(h.div('','id="nmea_tx_'+key+'"'),' style="width:60%;"'))
			+ h.tr(h.td(h.button('send','onclick=commitTX(\''+key+'\');ws.sendNMEA(ship0.remote_protocol+"://"+ship0.remote_ip+":"+settings.talk_port,buildNMEA(\''+key+'\',1,1))'))
				+ h.td(h.div('','id="nmea_tx_'+key+'"'),' style="width:60%;"'))
			, 'style="width:99%;"');
		}	
	setValue(div_without_key+key,page);
	}
function presentNMEA(key){	//	bouw de tabs voor de verzending (tx) van nmea
	var rx_div = document.getElementById("tabrx-"+key);
	if (undefined!=rx_div){
		if (undefined!=rx_div.attributes['shown']) {	//	ter verbetering van de performance, zodra page actief is dan opbouwen
			if (1==rx_div.getAttribute('shown')) {
				buildNMEAtable(key,rx,'tabrx-');	//	rx is in global namespace
				}
			}
		}
	else {
		console.warning('sentence "'+key+'" cannot be shown');
		}
	}
function buildTXtabs(tabs){	//	bouw de tabs voor de verzending (tx) van nmea
	var rxtxp = {tabhdrs_rx:'',tabpages_rx:'',tabhdrs_tx:'',tabpages_tx:''};
	Object.keys(nmeadefs).myforEach(function(ii,idx,that,pp){
		var nmeadef = nmeadefs[ii];
		pp.tabhdrs_rx += h.li(h.link(ii,'href="#tabrx-'+ii+'" shown=0'));
		pp.tabhdrs_tx += h.li(h.link(ii,'href="#tabtx-'+ii+'"'));
		pp.tabpages_rx += h.div('(rx) '+ii+': '+nmeadef[0].title,'id="tabrx-'+ii+'"  shown="0"');	//	todo leuk om hier de mogelijkheid te bieden instrumenten naar de waarden te slepen, daarna kun je posities van het instrument opgeven, daar gaat het getoond worden.
		pp.tabpages_tx += h.div('(tx) '+ii+': '+nmeadef[0].title +' wat tekst','id="tabtx-'+ii+'"');// + '<br>Hier komt een tabel met alle variablen uit deze sentence, je kunt dan een setter erin slepen, bv een slider, inputbox, radiobuttons oid.';
		},	rxtxp);

	tabs.rx = h.ul(rxtxp.tabhdrs_rx) + rxtxp.tabpages_rx;
	tabs.tx = h.ul(rxtxp.tabhdrs_tx) + rxtxp.tabpages_tx;

	window.addEventListener("load", buildTXtabsAfterLoad, false);	//	defer till DOM loaded
	}
function buildTXtabsAfterLoad(){	//	bouw de tabs voor de verzending (tx) van nmea
	for (var key in nmeadefs){
		buildNMEAtable(key,tx,'tabtx-',true,true);
		}
	}
function makerow(el){
	if (settings.check_pattern) {
		return h.tr(h.td(el[0])+h.td(el[1])+h.td(el[2])+h.td(el[3])+h.td(el[4])+h.td(el[5]));
		}
	else {
		return h.tr(h.td(el[0])+h.td(el[1])+h.td(el[4])+h.td(el[5]));
		}
	}
function commitTX(key){	//	fill tx values of given sentence according to values on tx page.
	var rc;
	var nmeadef = nmeadefs[key];
	for (var jj in nmeadef){
		if (jj==0){}
		else if (jj==1) {}	//	skip talker as we did it at jj=0
		else if (jj==nmeadef.length-1){}	//	checksum, mabe automatically correct or not
		else{
			var field = nmeadef[jj][0];
			var divid = 'tx.'+key+'.'+field;
			tx[key][field] = getValue(divid);
			}
		}
	}	
function checkplot(that){
	var what = that.id;
	what = what.split('_');	//	what[1]=key, what[2]=talkerid, what[3]=field
	var tmp = plotlist.length;
	if (plotlist.length>4){	//	todo must be in settings
		//	todo delete first entry from array
		}
	else {	
		plotlist[what[1]] = [what[2],what[3],that.value];	
		}	
	update_plotlist();	
	onblur_chboxplot(that);
	}
function onfocus_chboxplot(p0){
	var sent = p0.id.split('_');
	var div=document.getElementById('tabrx-' + sent[1]);
	div.setAttribute('shown',0);//	todo schakel updaten uit zet shown attribute op 0
	}
function onblur_chboxplot(p0){
	var sent = p0.id.split('_');
	var div=document.getElementById('tabrx-' + sent[1]);
	div.setAttribute('shown',1);
	}
function chboxplot(key,talkerid,field){
	//	todo als men met de checkbox bezig is moet deze tabel niet geupdate worden
	var plot_options = 
		[h.nbsp
		,'linegraphic'
		,'compass'
		,'table'
		,'chart'
		,'polair'
		];
	var selectplot = '';
	plot_options.myforEach(function(option,ii,that,p){
		// selectplot += h.ta_g('option',option,'value="'+option+'"');
		selectplot += h.option(option);
		});
	var checked = '';
	var selidx=0;
	if (undefined!=plotlist[key]){
		if (plotlist[key][0]==talkerid){
			if (plotlist[key][1]==field){
			//	checked = ' checked=true';	//	todo maak verschillende plotlijsten
				checked = plotlist[key][2];
				selidx=plot_options.indexOf(plotlist[key][2]);
				}			
			}			
		}			
	var opts = 
		['onchange=checkplot(this)' 
		,'onfocus=onfocus_chboxplot(this)'
		,'onblur=onblur_chboxplot(this)'
		,'selectedIndex='+selidx
		,'id="plot_'+key+'_'+talkerid+'_'+field+'"'
		, checked
		];
	opts = opts.join(' ');	
	return h.select(selectplot, opts);	
	}
function update_plotlist(){
	var html=h.tr(h.td('sentence')+h.td('talkerid')+h.td('field')+h.td('plot'));
	Object.keys(plotlist).myforEach(function(p0,it){
		html += h.tr(h.td(it)
			+h.td(plotlist[it][0])
			+h.td(plotlist[it][1])
			+h.td(plotlist[it][2])
			);		
		});
	setValue('divplotlist',h.table(html));	
	}
