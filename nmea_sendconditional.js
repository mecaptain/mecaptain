// nmea_sendconditional.js
'use strict';
var CNmea_sendconditional = function(){
	this.init= function(){
		// open websocket connection 'nmea-websock'
		//	install callback handlers for ROR, ON_ and OFF
		
		this.WS_nmea_sendconditional = new wsconnect(settings.ws_addr
			,'nmea-websock'						//	connect to websocketserver 'qrcode' service
			,function(ws){					//	connected
				ws.zend(JSON.stringify({ipdest:'localhost',portdest: 8011,verbose: 0}));		
				}
			,function(ws,data,that){	//	data received, data is de qrc
				}
			,function(evclose,thatws){	//	disconnected
				ws = undefined;
				}
			,{		here: 'you might pass an object'
				}
				
			);	
		if (1){
		this.WS_serial_sendconditional = new wsconnect(settings.ws_addr
			,'serial-send'					//	connect to websocketserver 'serial-send' service
			,function(ws){					//	connected
				ws.zend(JSON.stringify({comport:'COM7',baudrate: 38400,verbose: 0}));		
				//	todo send state of TP_on here
				}
			,function(ws,data,that){	//	data received, data is de qrc
				}
			,function(evclose,thatws){	//	disconnected
				ws = undefined;
				}
			,{		here: 'you might pass an object'
				}
				
			);	
		}
		if (0){	//	the problem here is that haven the websocketserver fread()ing the COM-port is very slow. Solved this having the program COM2UDP take care of that part.
		this.WS_serial_readfromTP = new wsconnect(settings.ws_addr
			,'serial-read'					//	connect to websocketserver 'serial-send' service
			,function(ws){					//	connected
				ws.zend(JSON.stringify({comport:'COM9',baudrate: 38400,verbose: 0}));		
				}
			,function(ws,data,that){	//	data received, data is de qrc
				if (that.TP_on == true)	{
					console.log('read ROR ('+data+') from COM9 (TP) and sent it to Nautis');
					that.WS_nmea_sendconditional.zend(data);	// send the ROR from the TP over the websocket 
					}
				else {
					console.log('read ROR ('+data+') from COM9 (TP) but not in TP mode');
					}
				}
			,function(evclose,thatws){	//	disconnected
				ws = undefined;
				}
			,{		here: 'you might pass an object'
				}
				
			);	
		// on timer event read serial port
		setInterval(function(that){ 
			if (undefined!=that.WS_serial_readfromTP){
				that.WS_serial_readfromTP.zend(JSON.stringify({read: "bla"}));
				}
			},980,this);
		}	
		if (1){
		this.WS_udp_readfromTP = new wsconnect(settings.ws_addr
			,'nmea-talk-listen'					//	connect to com2udp udp://localhost:8889
			,function(ws){					//	connected
				ws.zend(JSON.stringify(
					{listen_addr:			'udp://localhost:8889'
					}));
				}
			,function(ws,data,that){	//	data received, data is de qrc
				if (that.TP_on == true)	{
				//	console.log('read ROR ('+data+') from COM2UDP (TP) and sent it to Nautis');
					that.WS_nmea_sendconditional.zend(data);	// send the ROR from the TP over the websocket 
					}
				else {
				//	console.log('read ROR ('+data+') from COM2UDP (TP) but not in TP mode');
					}
				}
			,function(evclose,thatws){	//	disconnected
				ws = undefined;
				}
			,this				
			);	
		}	
		this.TP_off = true;	
		this.TP_on = 'nix';	
		cbRxNmea('ROR',function(rxd,that){	// reads UDP coming from UDP2COM
			if (that.TP_on == false)	{
			//	console.log('read ROR ('+rxd.raw+') from UDP:8888 (TP) and sent it to Nautis');
				that.WS_nmea_sendconditional.zend(rxd.raw);	// send the ROR from controlbox over the websocket 
				}
			else {
			//	console.log('read ROR ('+rxd.raw+') from UDP:8888 (TP) but controlbox is in TP mode');
				}
			},this);

		cbRxNmea('ON_',function(rxd,that){	//	TrackPilot On switch
			//console.log('TrackPilot  ON: TP_on ='+rxd.data.TP_on+', that.TP_on='+that.TP_on);
			if (that.TP_on == 'nix'){
				that.TP_on = rxd.data.TP_on.substr(0,3)=='154'? false: true;	// set TP_on switch in wrong position so we force a ON or OFF switch to be sent hereunder
				}				
			//	The proprietary nmea sentence $AUTO,... has been agreed upon by George Berridge, Darren ... and myself (Bert Tuijl) on 12 oct 2016
			if (rxd.data.TP_on.substr(0,3)=='154')	{
				if (undefined!=that.WS_serial_sendconditional) that.WS_serial_sendconditional.zend(JSON.stringify({write: "$AUTO,1*12\r\n"}));	//	keeping the L3 TrackPilot switched ON
				}
			if (that.TP_on == false && rxd.data.TP_on.substr(0,3)=='154')	{
				//	switch L3 TrackPilot from OFF to ON
				that.TP_on = true;
				console.log('TrackPilot  ON');
				if (undefined!=that.WS_serial_sendconditional) that.WS_serial_sendconditional.zend(JSON.stringify({write: "$AUTO,1*12\r\n"}));
				}
			if (that.TP_on == true && rxd.data.TP_on.substr(0,3)=='054')	{
				//	switch L3 TrackPilot from ON to OFF
				that.TP_on = false;
				console.log('TrackPilot  OFF');
				if (undefined!=that.WS_serial_sendconditional) that.WS_serial_sendconditional.zend(JSON.stringify({write: "$AUTO,0*13\r\n"}));
				}
			
			},this);
		cbRxNmea('OFF',function(rxd,that){	//	TrackPilot On switch, not in use now and probably not needed
			// console.log('TrackPilot OFF: TP_off='+rxd.data.TP_off);
			},this);
		}
	}
		
nmea_sendconditional = new CNmea_sendconditional();
nmea_sendconditional.init();
		
	