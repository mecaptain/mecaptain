//	control.js
//	maintains contact with wsserver - control

'use strict';

var wscontrol = undefined;

class CControl extends C0isimTab {	//	is not really a tab, but 
	constructor(){
		super();
		this.counter = 0;
		this.callbacks = [];
		this.ws_controladdr = zeroisim.gup('ws_controladdr',undefined);
		if (undefined===this.ws_controladdr){
			console.log('control.js: ERROR: undefined===ws_controladdr');
			}
		else {	
			this.ccontrol = new wsconnect(this.ws_controladdr,'control'
				,function(ws){	//	cbconnect
					console.log('control.js cbconnect');
					// ws.zend(JSON.stringify({action:'enumerate_joysticks'}));
					ws.zend(JSON.stringify({action:'enumerate_hardware'}));
					}
				,function(ws,data,that){	//	cbdata
					// console.log('   control.js; data='+data);
					data = JSON.parse(data);
					if (undefined!=data.counter){
						if (undefined!=that.callbacks[data.counter]){
							that.callbacks[data.counter].call(that,data);	//	apply() werkt hier niet
							}
						}
					else if (undefined!=data.nHardware){
						if (parseInt(data.nHardware)>0){
							var test = data.hardware;
							}						
						}
					}
				,function(ws,that){	//	cbclose
					console.log('control.js cbclose');
					}
				,this	
				);
			}
		}
	onfocus(){}
	onblur(){}
	subscribe_mediumControlbox(n,cb){
		console.log('subscribe_mediumControlbox('+n+',..)')
		this.ccontrol.zend(JSON.stringify(
			{action:'subscribe_mediumControlbox'
			,mcbIndex:n
			,counter:this.counter
			}));
		this.callbacks[this.counter++] = cb;
		}	
	subscribe_azi(n,cb){
		console.log('subscribe_azi('+n+',..)')
		this.ccontrol.zend(JSON.stringify(
			{action:'subscribe_azi'
			,aziIndex:n
			,counter:this.counter
			}));
		this.callbacks[this.counter++] = cb;
		}
	subscribe_joystick_axis(n,a,cb){
		// console.log()
		this.ccontrol.zend(JSON.stringify(
			{action:'subscribe_joystick_axis'
			,joystick:n
			,axis:a
			,counter:this.counter
			}));
		this.callbacks[this.counter++] = cb;
		}
	}
			
