// autopilot.js
//	bert tuijl
// todo ap en kompas moeten member worden van ship0
//	todo useRot werkt niet zo goed als zelf de rot uitrekenen
//	todo als naar ROT-mode wordt overgeschakeld, zet het setpoint op de huidige ROT, net zoals dat bij HDG werkt.
//	todo TRK en RAD
//	todo alvast een setpoint instellen (arm setpoint)
// githubbed!!!

'use strict';
var autopilot_tab_page = function(){
	var cw='"width:84px; background-color:#bbb; font-family:\'Lucida Sans\', \'Lucida Sans Regular\', \'Lucida Grande\', \'Lucida Sans Unicode\', Geneva, Verdana, sans-serif; color:#ffffff;  text-shadow: 1px 1px #000; font-weight:600;"';
	var fs = '"font-size: 160%;"';
	cbRxNmea('THS',function(rxd){	//	install callback for compass and autopilot
		var d = new Date();
		var n = d.getTime();
		var dt = n - lasttime;
		lasttime = n;
		if (dt>100000) return;	//	useless, delta time is too short
		if (!(ap.engaged.get()))	return;	
		if (ap.mode.get()=='change_hdg' || ap.mode.get()=='keep_hdg'){	
			// console.log('THS mode: setpoint='+ap.setpoint.get()+' rot='+ship0.hdt+' errval='+zeroisim.deltaDirection(ship0.hdt,ap.setpoint.get(),1));
			var ap_rudder = autopilot.tick(dt,zeroisim.deltaDirection(ship0.hdt,ap.setpoint.get(),1),ship0.rot);	
			//	todo rudder should be filtered
			ap_rudder = Math.round(ap_rudder);
			if (ap.old_filtered_output.get() != ap_rudder){
				rudder(ap_rudder,ap_rudder);
				ap.old_filtered_output.change(ap_rudder);
				}
			}
		});
	cbRxNmea('ROT',function(rxd){	//	install callback for rot
		if (undefined==rxd.data)	return;
		if (!(ap.engaged.get()))	return;	
		if (!(ap.mode.get()=='change_rot' || ap.mode.get()=='keep_rot'))	return;	
		var d = new Date();
		var n = d.getTime();	//	todo dit spul centraliseren
		var dt = n - lasttime;
		lasttime = n;
		if (dt>100000) return;	//	useless, delta time is too short
		var rot = parseFloat(rxd.data.rot);	//	
		// console.log('ROT mode: setpoint='+ap.setpoint.get()+' rot='+ship0.rot+' errval='+(ap.setpoint.get()-ship0.rot));
		var ap_rudder = autopilot.tick(dt,ap.setpoint.get()-ship0.rot,ship0.rot);	
		//	todo rudder should be filtered
		ap_rudder = Math.round(ap_rudder);
		if (ap.old_filtered_output.get() != ap_rudder){
			rudder(ap_rudder,ap_rudder);
			ap.old_filtered_output.change(ap_rudder);
			}
		});
	cbRxNmea('AEC',function(rxd,that){	//	install callback for autopilot remote control commands
		if (undefined==rxd.data)	return;
		if (!(ap.engaged.get()))	return;	
		if ((undefined!=rxd.data.AutoPilotMode) && !(ap.mode.get()=='change_hdg' || ap.mode.get()=='keep_hdg')){	
			ap.mode.set('change_hdg');
			ap.setpoint.setmode('angle');	
			}
		ap.setpoint.set(Math.floor(rxd.data.Setpoint));
		},this);
	/*** todo support for APN
	cbRxNmea('APN',function(rx){
		if (undefined==rx.data)	return;
		var angleS,angleP;
		angleS = angleP = rx.data.pilotwatch;
		rudder(angleS,angleP);
		});
	***/
		
	return h.table
		(h.tr(h.td(h.tag('fieldset',h.legend('autopilot','id="aplegend"') 
			+	h.table(	
				h.tr(
					h.td('mode','colspan="1" style='+cw)
					+h.td(zeroisim.onoffswitch('ap_engage','ap.engaged.toggle()'))
					+apbtn('heading','mode_heading')
					+apbtn('rot','mode_rot')
					+apbtn('track','mode_track')
					+apbtn('radius','mode_radius')
					// +h.td(h.nbsp,'colspan="1" style='+cw)
					)
			/* PID tuning parameters */
				+	h.tr(h.td('Kp','title="low: ringing, high:"')
						+h.td(h.nbsp,'colspan="1" style='+cw)
						+h.td(h.input('','type="text" value="'+ap.Kp_hdg.get()+'" id="Kp_hdg" onchange="ap.Kp_hdg.change(this.value)" style='+cw),'colspan="1"')
						+h.td(h.input('','type="text" value="'+ap.Kp_rot.get()+'" id="Kp_rot" onchange="ap.Kp_rot.change(this.value)" style='+cw),'colspan="1"')
						+h.td(h.input('','type="text" value="'+ap.Kp_trk.get()+'" id="Kp_trk" onchange="ap.Kp_trk.change(this.value)" style='+cw),'colspan="1"')
						+h.td(h.input('','type="text" value="'+ap.Kp_rad.get()+'" id="Kp_rad" onchange="ap.Kp_rad.change(this.value)" style='+cw),'colspan="1"')
						)
				+	h.tr(h.td('Ki','title="low: undershoot, high: overshoot"')
						+h.td(h.nbsp,'colspan="1" style='+cw)
						+h.td(h.input('','type="text" value="'+ap.Ki_hdg.get()+'" id="Ki_hdg" onchange="ap.Ki_hdg.change(this.value)" style='+cw),'colspan="1"')
						+h.td(h.input('','type="text" value="'+ap.Ki_rot.get()+'" id="Ki_rot" onchange="ap.Ki_rot.change(this.value)" style='+cw),'colspan="1"')
						+h.td(h.input('','type="text" value="'+ap.Ki_trk.get()+'" id="Ki_trk" onchange="ap.Ki_trk.change(this.value)" style='+cw),'colspan="1"')
						+h.td(h.input('','type="text" value="'+ap.Ki_rad.get()+'" id="Ki_rad" onchange="ap.Ki_rad.change(this.value)" style='+cw),'colspan="1"')
						)
				+	h.tr(h.td('Kd','title="low:, high:"')
						+h.td(h.nbsp,'colspan="1" style='+cw)
						+h.td(h.input('','type="text" value="'+ap.Kd_hdg.get()+'" id="Kd_hdg" onchange="ap.Kd_hdg.change(this.value)" style='+cw),'colspan="1"')
						+h.td(h.input('','type="text" value="'+ap.Kd_rot.get()+'" id="Kd_rot" onchange="ap.Kd_rot.change(this.value)" style='+cw),'colspan="1"')
						+h.td(h.input('','type="text" value="'+ap.Kd_trk.get()+'" id="Kd_trk" onchange="ap.Kd_trk.change(this.value)" style='+cw),'colspan="1"')
						+h.td(h.input('','type="text" value="'+ap.Kd_rad.get()+'" id="Kd_rad" onchange="ap.Kd_rad.change(this.value)" style='+cw),'colspan="1"')
						)
			/* end of PID tuning parameters */
				+	h.tr(
					h.td('setpoint','title="move setpoint up and down" style='+fs)
					+apbtn_port('&minus;&minus;','minusminus')
					+apbtn_port('&minus;','minus')
					+h.td(h.div('000','id="ap_setpoint_txtvalue" class="bigwhite"'))
					+apbtn_sbord('&plus;')
					+apbtn_sbord('&plus;&plus;')
					)
				+	h.tr(h.td('hdg','title="heading in degrees" style='+fs)										+h.td(h.div(ap.setpoint.get(),'id="ap_hdg" class="big"'),'colspan="5"'))
				+	h.tr(h.td('rot','title="rot in degrees/min" style='+fs+' onclick=ap.useRot.toggle();')	+h.td(h.div('0.0','id="ap_rot" class="bigwhite"'),'colspan="5"'))
				+	h.tr(h.td('&Delta;hdg','title="in degrees/min" style='+fs)									+h.td(h.div('0.0','id="ap_rothdg" class="bigwhite"'),'colspan="5"'))
				+	h.tr(h.td('rudder','title="rudder in &deg;" style='+fs)										+h.td(h.div('0.0','id="ap_rudder" class="bigwhite"'),'colspan="5"'))
				+	h.tr(h.td('mode','title="show automatic mode of autopilot" style='+fs)					+h.td(h.div('',	'id="ap_mode" class="bigwhite"'),'colspan="5"'))
				// +	h.tr(h.td(h.div('..','id="autopilot_command"'),"colspan='6'"))							
				,'style="border:0px; "')
				,'style="background-color:#bbb;"')	//	fieldset
				)
		+	h.td(h.table
			(	h.tr(h.td(h.fieldset('compass',h.nbsp
				,'id="ap_kompas" onclick=document.getElementById("ap_kompas").appendChild(document.getElementById("kompas_canvas"));')
				,'style="border:0px"'))
			+	h.tr(h.td(h.fieldset('gps',h.nbsp
				,'id="ap_gps" onclick=document.getElementById("ap_gps").appendChild(document.getElementById("mfinstr_canvas"));')
				,'style="border:0px"'))
				))
		+	h.td(h.fieldset('debug AP',h.nbsp
			+	h.table
				(	h.tr(h.td('Kp')+h.td(h.div(h.nbsp,'id=apdbg_Kp')))
				+	h.tr(h.td('Ki')+h.td(h.div(h.nbsp,'id=apdbg_Ki')))
				+	h.tr(h.td('Kd')+h.td(h.div(h.nbsp,'id=apdbg_Kd')))
				+	h.tr(h.td('useRot')+h.td(h.div(h.nbsp,'id=apdbg_useRot')))
				+	h.tr(h.td('errorval')+h.td(h.div(h.nbsp,'id=apdbg_errorvalue')))
				+	h.tr(h.td('mode')+h.td(h.div(h.nbsp,'id=apdbg_mode')))
				+	h.tr(h.td('P')+h.td(h.div(h.nbsp,'id=apdbg_p')))	
				+	h.tr(h.td('I')+h.td(h.div(h.nbsp,'id=apdbg_i')))
				+	h.tr(h.td('D')+h.td(h.div(h.nbsp,'id=apdbg_d')))
				+	h.tr(h.td('output')+h.td(h.div(h.nbsp,'id=apdbg_output')))
				+	h.tr(h.td('integral')+h.td(h.div(h.nbsp,'id=apdbg_integral')))
				+	h.tr(h.td('derivate')+h.td(h.div(h.nbsp,'id=apdbg_derivate')))
				+	h.tr(h.td('command')+h.td(h.div('..','id="autopilot_command"'),"colspan='1'"))
				,	'style="border:0px '+settings.style_aistable+'"')
			),'style="border:0px" font-family:"Lucida Sans", "Lucida Sans Regular", "Lucida Grande", "Lucida Sans Unicode", Geneva, Verdana, sans-serif;')
		),'style="border:0px"; position:absolute;');
	}
var ap_kompas_settings = {};	//	onthoud oude afmetingen en canvas
var autopilot_tab_onfocus = function(){
	document.getElementById("ap_gps").appendChild(document.getElementById("mfinstr_canvas"));
	var cv_kompas = document.getElementById("kompas_canvas");
	ap_kompas_settings = cv_kompas.that.kompasresize(270,270);	//	redraw kompasroos
	ap_kompas_settings.cv_kompas = cv_kompas;
	document.getElementById("ap_kompas").appendChild(cv_kompas);
	//	todo mini autopilot controls moeten ook in een andere div passen als die de focus krijgt, bevoorbeeld op de kaartpagina
	}
var autopilot_tab_onblur = function(){
	//	todo maak de appenChilds ongedaan
	ap_kompas_settings.cv_kompas.that.kompasresize(ap_kompas_settings.width,ap_kompas_settings.height);	//	redraw kompasroos
	}
	
var ap = {	//	autopilot
	engaged:			new boolean_value(false,function(that){
		if (that.get()) {	//	ap engaged
			autopilot.clear();
			switch (ap.mode.get()){
				case 'change_rot':	case 'keep_rot':	kompas.setAP({mode:'rot', setpoint:ap.setpoint.get()});	break;
				default:	// todo
				case 'change_hdg':	case 'keep_hdg':	kompas.setAP({mode:'hdg', setpoint:ap.setpoint.get()});	break;
				}
			}
		else {	//	ap disengaged
			if (settings.rudder0_on_ap_off) {
				rudder(0,0);
				}
			kompas.setAP({mode:'off'});
			}	
		setValue('aplegend','autopilot '+(that.get()?'engaged':'idle'));
		}),
	setpoint:	new ascal_value('angle',0,function(that){	// kan een angle zijn, [0,360> of [-200,+200] of een radius in mijlen of meter of een track-id
		switch (ap.mode.get()){
			case 'change_hdg':
			case 'keep_hdg':
				setValue('ap_setpoint_txtvalue',ap.setpoint.get());
				ap.mode.set('change_hdg');		
				kompas.setAP({mode: 'hdg',setpoint:ap.setpoint.get()});
				// kompas.draw(ship0.hdt,ship0.rot,ship0.cog,ship0.ror,ap.setpoint.get());
				kompas.draw(ship0,ap.setpoint.get());
				break;
			case 'change_rot':
			case 'keep_rot':
				setValue('ap_setpoint_txtvalue',ap.setpoint.get());
				ap.mode.set('change_rot');		
				kompas.setAP({mode: 'rot',setpoint:ap.setpoint.get()});
				// kompas.draw(ship0.hdt,ship0.rot,ship0.cog,ship0.ror,ap.setpoint.get());
				kompas.draw(ship0,ap.setpoint.get());
				break;
			default:
				console.log('ap not supported: "'+ap.mode.get()+'"');
				break;
			}
		}),
	Kp_hdg:	new scalar_value( 1.600,function(that){
		if (undefined!=autopilot)	autopilot.setvalues({Kp_hdg:that.get()});
		setValue('autopilot_command','Kp_hdg='+that.get());
		setValue('Kp_hdg',that.get(),'value');
		}),
	Ki_hdg:	new scalar_value( 0.100,function(that){if (undefined!=autopilot)	autopilot.setvalues({Ki_hdg:that.get()});setValue('autopilot_command','Ki_hdg='+that.get());setValue('Ki_hdg',that.get(),'value');}),
	Kd_hdg:	new scalar_value(12.600,function(that){if (undefined!=autopilot)	autopilot.setvalues({Kd_hdg:that.get()});setValue('autopilot_command','Kd_hdg='+that.get());setValue('Kd_hdg',that.get(),'value');}),
	                                                                                                                                                                                  
	Kp_rot:	new scalar_value( 1.200,function(that){if (undefined!=autopilot)	autopilot.setvalues({Kp_rot:that.get()});setValue('autopilot_command','Kp_rot='+that.get());setValue('Kp_rot',that.get(),'value');}),
	Ki_rot:	new scalar_value( 0.700,function(that){if (undefined!=autopilot)	autopilot.setvalues({Ki_rot:that.get()});setValue('autopilot_command','Ki_rot='+that.get());setValue('Ki_rot',that.get(),'value');}),
	Kd_rot:	new scalar_value( 1.000,function(that){if (undefined!=autopilot)	autopilot.setvalues({Kd_rot:that.get()});setValue('autopilot_command','Kd_rot='+that.get());setValue('Kd_rot',that.get(),'value');}),
	                                                                                                                                                                                  
	Kp_trk:	new scalar_value( 3.000,function(that){if (undefined!=autopilot)	autopilot.setvalues({Kp_trk:that.get()});setValue('autopilot_command','Kp_trk='+that.get());setValue('Kp_rad',that.get(),'value');}),
	Ki_trk:	new scalar_value( 0.100,function(that){if (undefined!=autopilot)	autopilot.setvalues({Ki_trk:that.get()});setValue('autopilot_command','Ki_trk='+that.get());setValue('Ki_rad',that.get(),'value');}),
	Kd_trk:	new scalar_value( 7.000,function(that){if (undefined!=autopilot)	autopilot.setvalues({Kd_trk:that.get()});setValue('autopilot_command','Kd_trk='+that.get());setValue('Kd_rad',that.get(),'value');}),
	                                                                                                                                                                                  
	Kp_rad:	new scalar_value( 1.000,function(that){if (undefined!=autopilot)	autopilot.setvalues({Kp_rad:that.get()});setValue('autopilot_command','Kp_rad='+that.get());setValue('Kp_trk',that.get(),'value');}),
	Ki_rad:	new scalar_value( 1.001,function(that){if (undefined!=autopilot)	autopilot.setvalues({Ki_rad:that.get()});setValue('autopilot_command','Ki_rad='+that.get());setValue('Ki_trk',that.get(),'value');}),
	Kd_rad:	new scalar_value(12.000,function(that){if (undefined!=autopilot)	autopilot.setvalues({Kd_rad:that.get()});setValue('autopilot_command','Kd_rad='+that.get());setValue('Kd_trk',that.get(),'value');}),
	
	useRot:	new boolean_value(false,function(that){if (undefined!=autopilot)	autopilot.setvalues({useRot:that.get()}); if (that.get()) autopilot.clear();setValue('autopilot_command','useRot='+(that.get()?'yes':'no'))}),
	old_filtered_output: new scalar_value(0,function(that){setValue('ap_rudder',that.get());}),
	mode:		new enum_value('change_hdg',function(that){if (undefined!=autopilot)	
		autopilot.setvalues({mode:that.get()});
		setValue('ap_mode',that.get());
		kompas.setAP({mode:that.get().substring(that.get().length-3)});
		}
		,['change_hdg','keep_hdg','change_rot','keep_rot','change_track','keep_track','change_radius','keep_radius']),
	rudderlimit_p:	new scalar_value(-35,function(that){if (undefined!=autopilot)	autopilot.setvalues({rlimp:that.get()});/*setValue('ap_rlimp',that.get());*/}),
	rudderlimit_s:	new scalar_value(+35,function(that){if (undefined!=autopilot)	autopilot.setvalues({rlims:that.get()});/*setValue('ap_rlims',that.get());*/}),
	
	errorValue: 0,
	};
function change_ap(setting,newvalue){
	settings[setting] = newvalue;
	}
function apbtn(txt,id){
	if (id==undefined)	id = txt;
	return h.td(h.center(txt),'class="tg1" onclick=autopilotcommand(this,"ap_"); id="'+id+'"');
	}
function apbtn_port(txt,id){
	if (id==undefined)	id = txt;
	return h.td(h.center(txt),'class="tg_l" onclick=autopilotcommand(this,"ap_"); id="'+id+'"');
	}
function apbtn_sbord(txt,id){
	if (id==undefined)	id = txt;
	return h.td(h.center(txt),'class="tg_r" onclick=autopilotcommand(this,"ap_"); id="'+id+'" onselect="return false;"');	//	todo als dit werkt doe dit ook met andere knoppen
	}
function autopilotcommand(that,rt){
	setValue('autopilot_command','pilotcommand '+rt+that.id);
	var sw = rt+that.id;
	switch (sw){
		case 'ap_minus':						ap.setpoint.dec(1);	break;
		case 'ap_+':							ap.setpoint.inc(1);	break;
		case 'ap_minusminus':				ap.setpoint.dec(10);	break;
		case 'ap_++':							ap.setpoint.inc(10);	break;
		case 'ap_mode_rot':
		case 'ap_mode_track': 
		case 'ap_mode_radius': 
		case 'ap_mode_heading':		
			var modes = ['mode_rot','mode_track','mode_radius','mode_heading'];
			modes.myforEach(function(mode,dd,ar,p){
				document.getElementById(mode).style.color = '#EEE';	//	zet lichtje in alle knoppen aan 
				});
			/*var div=*/document.getElementById(that.id).style.color = '#0F0';	//	zet lichtje in gekozen knop aan
			switch (sw){
				case 'ap_mode_heading':	ap.mode.set('change_hdg');		autopilot.setvalues({Kp:ap.Kp_hdg.get(),Ki:ap.Ki_hdg.get(),Kd:ap.Kd_hdg.get()});	ap.setpoint.setmode('angle');		ap.setpoint.set(Math.floor(ship0.hdt));	break;
				case 'ap_mode_rot':		ap.mode.set('change_rot');		autopilot.setvalues({Kp:ap.Kp_rot.get(),Ki:ap.Ki_rot.get(),Kd:ap.Kd_rot.get()});	ap.setpoint.setmode('scalar');	ap.setpoint.set(Math.floor(ship0.rot));	break;
				case 'ap_mode_track': 	ap.mode.set('change_track');	autopilot.setvalues({Kp:ap.Kp_trk.get(),Ki:ap.Ki_trk.get(),Kd:ap.Kd_trk.get()});	ap.setpoint.setmode('scalar');	ap.setpoint.set(0);	break;
				case 'ap_mode_radius': 	ap.mode.set('change_radius');	autopilot.setvalues({Kp:ap.Kp_rad.get(),Ki:ap.Ki_rad.get(),Kd:ap.Kd_rad.get()});	ap.setpoint.setmode('scalar');	ap.setpoint.set(0);	break;
				}
			break;
		}
	}
function apdebug(Kp,Ki,Kd,previous_error,output,p,i,d,integral,derivate,useRot,mode){
	//	div strings moeten overeenkomen met wat in nmea_presenter.js gemaakt is.
	var div=document.getElementById('apdbg_Kp');	if (undefined==div) return;	//	misschien is het dom nog niet geladen
	var apdebug = {Kp:'apdbg_Kp',Ki:'apdbg_Ki',Kd:'apdbg_Kd',errorvalue:'apdbg_errorvalue',output:'apdbg_output',p:'apdbg_p',i:'apdbg_i',d:'apdbg_d',integral:'apdbg_integral',derivate:'apdbg_derivate',useRot:'apdbg_useRot',mode:'apdbg_mode'};	
	if (undefined!=Kp)					setValue(apdebug.Kp,Kp);
	if (undefined!=Ki)					setValue(apdebug.Ki,Ki);
	if (undefined!=Kd)					setValue(apdebug.Kd,Kd);
	if (undefined!=previous_error)	setValue(apdebug.errorvalue,previous_error);
	if (undefined!=p)						setValue(apdebug.p,p);
	if (undefined!=i)						setValue(apdebug.i,i);
	if (undefined!=d)						setValue(apdebug.d,d);
	if (undefined!=output)				setValue(apdebug.output,output);
	if (undefined!=integral)			setValue(apdebug.integral,integral);
	if (undefined!=derivate)			setValue(apdebug.derivate,derivate);
	if (undefined!=useRot)				setValue(apdebug.useRot,useRot);
	if (undefined!=mode)					setValue(apdebug.mode,mode);
	ap.errorValue = previous_error;	//	tbv plot
	}
/*
ervaring: combistar twee stuurpompen aan, ziegler-nichols Ku=7.20; Tu=34s

classic PID 	Kp=0.6 x 7.2 = 5.2
					Ki=34 / 2 = 17
					Kd=34/ 8 = 4.24
some overshoot Kp=0.33 x 7.2 = 2.3976
					Ki=34 / 2 = 17
					Kd=34/ 3 = 11.333
*/