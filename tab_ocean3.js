//	tab_ocean.js

// met hulp van https://threejs.org/examples/#webgl_shaders_ocean
// zeroisim.scene is global

// seealso:	https://www.sitepoint.com/how-to-build-a-coach-holiday-showcase-with-wrld/
'use strict';

class COcean extends C0isimTab {
	constructor(){
		super();
		this.container = i.div('',{id:'container'});
		}
	onload(evt){
		document.getElementById(this._tabdivname).appendChild(this.container);
		// if ( ! Detector.webgl ) Detector.addGetWebGLMessage();
		var threejs_dir = './incl/threejs/';
		var threeex_dir = threejs_dir+'examples/js/';
		var ls = new CLoadScript();
		ls.loadScripts({scripts:
			[threejs_dir+'build/three.js' 
			,threeex_dir+'controls/OrbitControls.js'
			,threeex_dir+'loaders/ColladaLoader.js'
			,threeex_dir+'objects/Water.js' 
			,threeex_dir+'objects/Sky.js' 
			,threeex_dir+'libs/stats.min.js' 
			,threeex_dir+'libs/dat.gui.min.js' 
			,'./myGLTFExporter.js'	//	THREE.GLTFExporter
			,'./myGLTFLoader.js'
			,'./websocketloader.js'
			],cb:function(){ 
				this.kompas = new THREE.Mesh
					(new THREE.SphereGeometry
						(200				// radius : Float
						,36				// widthSegments : Integer
						,36				// heightSegments : Integer
						,0					// phiStart : Float
						,2*Math.PI		// phiLength : Float
						,0					// thetaStart : Float
						,2*Math.PI		// thetaLength : Float
						)
					,new THREE.MeshBasicMaterial({wireframe:true,color:'#F00'})
					);
				
				var buoyshape_light = new THREE.SphereGeometry(4,3,2);	//	put a light on the navmark, todo a triangle, allways .lookAt() the camera
				this.navmarklightMesh = new THREE.Mesh(buoyshape_light,new THREE.MeshBasicMaterial({color:'#F00'}));
				this.lights = [];	//	todo ik weet niet of die nodig is
				var wsloadingManager = new zeroisim.LoadingManager(this.funloadingManager.bind(this),this.loadingStart.bind(this));
				if (window.location.href.substr(0,7) == 'file://'){	//	eigen loaders vanuit wss, cross domain is een probleem in file://-domein
					this.wsip = {ip:'ws://127.0.0.1:12120',protocol:'fxmlhttp'};
					this.loader = new zeroisim.ColladaLoader(wsloadingManager);
					this.loader.setWSip(this.wsip);
					this.textureLoader = this.loader;	//	to stay compatible with THREEJS
					}
				else {	//	cross domain is geen probleem in http://-domein
					this.loader = new THREE.ColladaLoader(wsloadingManager);
					this.textureLoader = new THREE.TextureLoader();
					}
				var buoyImageLatRed = '3d/textures/buoy_iala1_lateral_red_transparant2.png';
				// var buoyImageLatRed = '3d/textures/test256x257.jpg';
				var buoyImageLatGrn = '3d/textures/buoy_iala1_lateral_green_transparant.png';
				var buoyTexture = this.textureLoader.load(buoyImageLatRed, function(texture){
					var buoyMaterial = new THREE.MeshBasicMaterial({map:texture,side:THREE.FrontSide /*DoubleSide*/,transparent:true,opacity:1.0});
					var buoyGeometry = new THREE.PlaneGeometry(32, 56, 10, 10);
					this.buoyMeshLatRed = new THREE.Mesh(buoyGeometry, buoyMaterial);
					var buoyTexture = this.textureLoader.load(buoyImageLatGrn, function(texture){
						var buoyMaterial = new THREE.MeshBasicMaterial({map:texture,side:THREE.FrontSide,transparent:true,opacity:1.0});
						var buoyGeometry = new THREE.PlaneGeometry(32, 56, 10, 10);
						this.buoyMeshLatGrn = new THREE.Mesh(buoyGeometry, buoyMaterial);
						this.textureLoader.load('3d/textures/waternormals3.jpg', function(texture){
							texture.wrapS = texture.wrapT = THREE.RepeatWrapping;
							this.waterNormals = texture;
							this.init();	
							}.bind(this),undefined,{typ:'jpg'});
						}.bind(this),undefined,{typ:'png'});
					}.bind(this),undefined,{typ:'png'});
				}.bind(this)
			});
		}
	init() {
		this.exposedParms =	//	deze worden door de conning designer gezet
			{viewDirection: 0	//	with respect to heading of the ship that bears the camera
			,camid:	0			//	unique camera id, user is responsible for uniqueness. This id is used by the CAM nmea sentence
			,fov:	45				//	degrees field of view
			,aspect: 0			//	0=let canvas (or resizing determine) value=aspect (resize canvas) todo
			,planeNear:	1		//	meters near clipping plane
			,planeFar: 5000	//	meters far clipping plane
			,nmea:	
				{compassHDG:	{s:'THS',f:'hdgT'}
				,position: 		{s:'RMC',f:'rot'}	//	todo
				}
			};
	
		this.stats = new Stats();
		this.gui = new dat.GUI();	//	undefined;
		this.animateFunction = this.animate.bind(this);
		
		this.camera_drone = new THREE.PerspectiveCamera
			(this.exposedParms.fov
			,window.innerWidth/window.innerHeight	//	this.canvas.width/this.canvas.height
			,this.exposedParms.planeNear
			,this.exposedParms.planeFar
			);

		this.viewDirection = this.exposedParms.viewDirection;
		this.camera_drone.position.set( 20, 20, 20 );
		this.camera_drone.lookAt( new THREE.Vector3( 0, 3, 0 ) );
		this.camera_drone.rotation.order = 'YXZ';	//	todo ???
		
		if (undefined===zeroisim.scene)
			zeroisim.scene = new THREE.Scene();
		this.clock = new THREE.Clock();
		var pos = new zeroisim.LatLon(positions[zeroisim.gup('position',null)]);

		//	eigen schip
		this.ship = //	put own ship in centre of scene
			{hdt:90
			,latlon:new zeroisim.LatLon({latdeg:pos.latdeg,londeg:pos.londeg})	//	zeroisim.busTX('MC0',{req:mc0.setpos,latrad:pos.latrad,lonrad:pos.lonrad});		//	set position
			,newlatlon: new zeroisim.LatLon()	//	todo dit moet met GGA of GLL binnenkomen
			,trim:	0	//	degrees, positive is aft-deep, negative is fore-deep
			,list:	0	//	degrees, positive is heeling to SB, negative heeling to PS
			,draft:	0	//	metres, 0 is on plimsol waterline, positive is deeper
			//  X      	Y      	Z
			,posx:0,	draft:0,	posz:0
			,list:0,	trim:0,	rotz:0
			,old:[]
			};

		zeroisim.scene.userData = 
			{position:new zeroisim.LatLon({latdeg:pos.latdeg,londeg:pos.londeg})
			,getDiff:function(there){
				var diff = zeroisim.scene.userData.position.meters_diff(there);
				return diff;
				}
			};
			
		cbRxNmea('MC0',function(rxd,that){	//	when mesh arrives, load it
			var regex_options = 'gim';
			var strSplitPath = '(.*\/)?(\..*?|.*?)(\.[^.]*?)?';
		//	var strSplitPath = '(.*\/)?(\..*?|.*?)(\.[^.]*?)?(#.*$|\?.*$|$)';
			var regex_SplitPath = new RegExp(strSplitPath,regex_options);
			var parts = regex_SplitPath.exec(rxd.data.mesh);
			var texturePath = parts[1];
			
			if ((mc0.getOwnShip+mc0.response) == rxd.data.req){
				var diff = that.ship.latlon.meters_diff(new zeroisim.LatLon(rxd.data.latlon));	//	todo zeroisim.scene.userdata.getDiff()
				if (undefined!=rxd.data.distance)	that.distance = parseInt(rxd.data.distance); //	alleen bij eigen schip, bepaal de afstand van de camera tot middelpunt
				that.loader.load(rxd.data.mesh,that.shipLoader.bind(that),undefined,{typ:'dae',diff:diff,scaleMesh:rxd.data.scaleMesh,texturepath:texturePath});
				}
			else if ((mc0.get3d+mc0.response) == rxd.data.req){
				var diff = that.ship.latlon.meters_diff(new zeroisim.LatLon({latdeg:rxd.data.ghost.position.latdeg,londeg:rxd.data.ghost.position.londeg}));	//	todo zeroisim.scene.userdata.getDiff()
				that.loader.load(rxd.data.mesh,that.ghostLoader.bind({pos:diff,hdt:rxd.data.ghost.hdg,idGhost:rxd.data.idGhost,ghost:rxd.data.ghost,scaleMesh:rxd.data.scaleMesh}),undefined,{typ:'dae',texturepath:texturePath});
				}
			else if ((mc0.getScene) == rxd.data.req){
				if (1){
					zeroisim.busTX('MC0',{req:mc0.getScene+mc0.response,scene:btoa({ditis:'garbage'})});
					}
				else{
					that.exportGLTF(zeroisim.scene,function(scene_text){	// GLTFExporter
						zeroisim.busTX('MC0',{req:mc0.getScene+mc0.response,scene:btoa(scene_text)});
						});
					}
				}
			},this);
		
		zeroisim.busTX('MC0',{req:mc0.getOwnShip});	//	request simulator for mesh

		//	kompas
		this.kompas.visible = false;
		this.kompas.name = 'compass';
		zeroisim.scene.add(this.kompas);	//	liefst per camera, met als middelpunt de camera.position
			
		//	ambient light
		this.ambientLight = new THREE.AmbientLight( 0xcccccc, 0.4 );
		this.ambientLight.name = 'ambientlight';
		zeroisim.scene.add( this.ambientLight );

		//	sunlight
		this.sunLight = new THREE.DirectionalLight( 0xffffff, 0.8 );
		this.sunLight.position.set( 1, 1, 0 ).normalize();
		this.sunLight.name = 'sunlight';
		zeroisim.scene.add(this.sunLight);

		// Water
		var waterGeometry = new THREE.PlaneBufferGeometry( 10000, 10000 );
		this.water = new THREE.Water(waterGeometry,
			{textureWidth: 512
			,textureHeight: 512
			,waterNormals: this.waterNormals
			,alpha: 1.0
			,sunDirection: this.sunLight.position.clone().normalize()
			,sunColor: 0xffffff
			,waterColor: 0x001e0f
			,distortionScale:  8.0	//	3.7
			,fog: zeroisim.scene.fog !== undefined
			});
		this.water.rotation.x = - Math.PI / 2;
		this.water.speed = 0.1;	//	{value:1.0,get(){	return this.value;	}};
		this.water.name = 'water';
		this.water.reflectivity;
		zeroisim.scene.add(this.water);
		
		// sky
		this.sky = new THREE.Sky();
		this.sky.scale.setScalar(10000);
		this.sky.name = 'sky';
		zeroisim.scene.add(this.sky);
		var uniforms = this.sky.material.uniforms;
		uniforms.turbidity.value = 10;
		uniforms.rayleigh.value = 2;
		uniforms.luminance.value = 1;
		uniforms.mieCoefficient.value = 0.005;
		uniforms.mieDirectionalG.value = 0.8;
		
		//	sun
		this.sunparameters = 
			{distance: 400
			,inclination: 0.49
			,azimuth: 0.205
			,ambientLight: 0.6
			};
			
		//	navmarks
		this.navmarkLights = zeroisim.navmarks.slijkgat_boeienlijn;
		// Object.assign(this.navmarkLights,zeroisim.navmarks.cirkel_360);	//	todo alleen voor testen
		this.navmarkLoader();
		
		//	create renderer
		this.renderer = new THREE.WebGLRenderer({antialias:true});
		this.renderer.setPixelRatio(window.devicePixelRatio);
		this.renderer.setSize(window.innerWidth,window.innerHeight);
		
		this.controls = new THREE.OrbitControls( this.camera_drone, this.renderer.domElement );
		// this.controls.target.set( 0, 2, 0 );
		this.controls.update();
		
		this.container.appendChild( this.renderer.domElement );
		var styl = this.stats.dom.style;
		styl.top = '34px';
		styl.left = '11px';
		this.container.appendChild( this.stats.dom );
		this.settings = 
			{'show navmarks': true
			};
		
		if (this.gui){
			var folder = this.gui.addFolder('Sky');
			folder.add( this.sunparameters,	'inclination', 0, 0.5, 0.0001 ).onChange(this.updateSun.bind(this));
			folder.add( this.sunparameters,	'azimuth', 0, 1, 0.0001 ).onChange(this.updateSun.bind(this));
			folder.add( this.sunparameters,	'ambientLight', 0, 1, 0.001 ).onChange(this.updateSun.bind(this));
			folder.add( 
				{get 'compass'(){	return this.hetKompas.visible;}
				,set 'compass'(v){this.hetKompas.visible = v		}
				,hetKompas: this.kompas
				},'compass');
				
			// folder.open();

			var folder = this.gui.addFolder( 'Water' );
			folder.add( this.water.material.uniforms.distortionScale, 'value', 0, 8, 0.1 ).name( 'distortionScale' );
			folder.add( this.water.material.uniforms.size, 'value', 0.1, 10, 0.1 ).name( 'size' );
			folder.add( this.water.material.uniforms.alpha, 'value', 0.9, 1, .001 ).name( 'alpha' );
			folder.add( this.water, 'speed', 0.0, 1, .001 ).name( 'speed' );
			// folder.open();
			
			var folder = this.gui.addFolder('Settings');
			folder.add(this.settings, 'show navmarks' ).onChange(this.hShowNavmarks.bind(this));
			
			var folder = this.gui.addFolder('Ship');
			folder.add(this.ship, 'posx',  -30, +30,1.0).onChange(this.hShip);
			folder.add(this.ship, 'draft', -10,  +3,0.1).onChange(this.hShip);
			folder.add(this.ship, 'posz',  -30, +30,1.0).onChange(this.hShip);
			folder.add(this.ship, 'list', -180,+180,1.0).onChange(this.hShip);
			folder.add(this.ship, 'trim', -180,+180,1.0).onChange(this.hShip);
			folder.add(this.ship, 'rotz', -180,+180,1.0).onChange(this.hShip);
			
			styl = this.gui.domElement.style['margin-top'] = '34px';
			this.gui.close();
			}
		window.addEventListener('resize',this.onWindowResize.bind(this),false);
		
		this.updateSun(0);
		this.animate();
		}
	loadingStart(url){
		// console.log('COcean loadingStart(\''+url+'\')');
		//	todo mooi plekje om iets op het scherm te zetten
		}
	funloadingManager(url){
		// console.log('funloadingManager('+url+')');
		}	
	navmarkLoader(){
		console.log('tab_ocean3.navmarkLoader()');
		Object.keys(this.navmarkLights).myforEach(function(key,ii,ar,that){	//	leg de navmarks op hun juiste positie
			var navmark = that.navmarkLights[key];
			navmark.diff = zeroisim.scene.userData.getDiff(new zeroisim.LatLon(navmark)); //	bepaal xyz coordinaten van dit navmark
			if (undefined!=navmark.s57){
				switch (navmark.s57.obj){
					case 'BOYLAT':
						switch (navmark.s57.colour){
							case '#F00':	var navmarkMesh = that.buoyMeshLatRed.clone();	break;	//	todo ambient licht blijkt geen effekt te hebben op deze boeien
							case '#0F0':	var navmarkMesh = that.buoyMeshLatGrn.clone();	break;
							}
						navmarkMesh.position.set(navmark.diff.x,2.4,navmark.diff.z);
						navmarkMesh.scale.set(0.1,0.1,0.1);
						navmarkMesh.mc_lookat = true;
						navmarkMesh.name = JSON.stringify(navmark.s57);
						zeroisim.scene.add(navmarkMesh);
						break;
					}
				if (undefined!=navmark.s57.lights){
					//	je zou de navmarkshape_light kunnen vergroten afhankelijk van de afstand (pythagoras), je kunt dan verder kijken
					// that.lights[ii] = new THREE.PointLight(navmark.color, 2, 50, 2 );	//	shining color does not matter
				
					// var navmarkshape_light = that.navmarklightMesh.geometry;
					that.lights[ii] = new THREE.Mesh(that.navmarklightMesh.geometry,new THREE.MeshBasicMaterial());
					// that.lights[ii] = that.navmarklightMesh.clone();	//	ik krijg dit niet werkend, de kleur wordt wit
					// that.lights[ii].material.color = new THREE.Color(navmark.color);
					zeroisim.scene.add(that.lights[ii]);
					that.lights[ii].position.set(navmark.diff.x,navmark.h,navmark.diff.z);
					that.lights[ii].pattern = zeroisim.navmarks.createPattern(navmark,that.lights[ii]);	// install pattern
					}
				}
			// todo als navmark uit zicht verdwijnt: clearInterval(key.intervalfn);
			},this);
		}
	ghostLoader(collada,url){
		console.log('tab_ocean3.ghostLoader('+url+')');
		switch (url){
			case './3d/ships/rib/RibBoatvanCock.dae': var scaleMesh =  1.0;	break;
			case './3d/ships/tugboat/Tugboat.dae':		var scaleMesh =  1.0;	break;
			case './3d/ships/simple_10.dae':				var scaleMesh = 10.0;	break;
			case './3d/ships/vos_patriot2.dae':			var scaleMesh =  1.0;	break;
			case './3d/ships/smit_poolzee.dae':			var scaleMesh =  1.0;	break;
			case './3d/ships/liberty/liberty.dae':		var scaleMesh = 22.0;	break;

			default:	var scaleMesh = 1.0;	break;
			}
		var ghost = {Collada: collada.scene};
		ghost.Collada.rotation.order = 'XYZ';
		ghost.Collada.position.set(this.pos.x,this.pos.y,this.pos.z);
		ghost.Collada.rotation.z = (90-this.hdt)*zeroisim.pi_180;
		// ghost.Collada.scale.setScalar(scaleMesh);
		ghost.Collada.scale.multiplyScalar(scaleMesh);

		var idxNewObject = zeroisim.scene.children.length;
		ghost.Collada.name = `ghost: ${url}`;
		zeroisim.scene.add(ghost.Collada);
		zeroisim.busTX('MC0', 		//	ref naar dit object bewaren om de plek en het aspekt van de ghost te kunnen veranderen
			{req:mc0.createdObj
			,idGhost:this.idGhost
			,idxInScene:idxNewObject
			,ghost:this.ghost
			});
		}
	shipLoader(collada,url,payload){
		console.log('tab_ocean3.shipLoader('+url+')');
		/* werkt niet
		//	enable smooth shading
		for (var ii in collada.library.geometries){
			var geometry = collada.library.geometries[ii];
			//geometry.computeVertexNormals(true);
			}
		for (var ii in collada.library.materials){
			var material = collada.library.materials[ii];
			material.shading = THREE.SmoothShading;
			}
		*/	
		this.ship.Collada = collada.scene;
		this.ship.Collada.rotation.order = 'XYZ';	//	todo ???
		if (undefined!=payload){
			var position3d = payload.diff;
			if (undefined!=position3d){
				this.ship.Collada.position.set(position3d.x,position3d.y,position3d.z);
				}
			this.ship.Collada.scale.multiplyScalar(payload.scaleMesh);
			}
			
		this.ship.Collada.name = `own ship: ${url}`;
		zeroisim.scene.add(this.ship.Collada);
		this.camera_drone.position.set
			(this.distance*this.camera_drone.position.x
			,this.distance*this.camera_drone.position.y //* 0.7
			,this.distance*this.camera_drone.position.z
			);
		console.log('camera.position='+this.camera_drone.position.x+','+this.camera_drone.position.y+','+this.camera_drone.position.z);
		
		//	now ship is loaded, let the simulator control the movement
		/**/
		cbRxNmea(this.exposedParms.nmea.compassHDG.s,function(rxd,that){	//	install callback for compass and autopilot
			if (undefined==rxd.data)	return;
			that.ship.hdt = parseFloat(rxd.data[that.exposedParms.nmea.compassHDG.f]);
			that.ship.Collada.rotation.z = (90-that.ship.hdt)*zeroisim.pi_180;
			that.camera_drone.rotation.y = (that.ship.hdt+parseFloat(that.viewDirection))*-zeroisim.pi_180;
			that.controls.update();
			},this);
			
		cbRxNmea(this.exposedParms.nmea.position.s,function(rxd,that){	//	install callback for compass and autopilot
			if (undefined==rxd.data)	return;
			that.ship.newlatlon.interpretLatLonRMC(rxd.data);
			var diffpos = that.ship.latlon.meters_diff(that.ship.newlatlon);
			that.ship.latlon.set(that.ship.newlatlon);
			var oldpos = that.ship.Collada.position;
			oldpos.x += diffpos.x;	oldpos.z += diffpos.z;
			that.ship.Collada.position.set(oldpos.x,oldpos.y,oldpos.z);
			that.controls.update();
			},this);
		/*super.*/cbRxNmea('CAM',function(rxd,that){	//	view direction
			if (undefined==rxd.data)	return;
			if (that.exposedParms.camid==rxd.data.camid){
				if (undefined!=rxd.data.rotY){
					that.viewDirection = that.exposedParms.viewDirection+parseFloat(rxd.data.rotY);
					that.controls.update();
					// that.updateHeading();
					}
				}
			},this);
		}
	hShowNavmarks(tf){
		this.showNavmarks = true;
		}
	hShip(newValue){
		var ship = this.object.Collada;
		if (undefined==this.object.old[this.property])
			this.object.old[this.property] = 0;
		switch (this.property){
			case 'posx':	ship.position.x = newValue;	break;
			case 'draft':	ship.position.y = newValue;	break;
			case 'posz':	ship.position.z = newValue;	break;
			case 'list':	ship.rotateX((newValue-this.object.old[this.property]) * zeroisim.pi_180);	break;
			case 'trim':	ship.rotateY((newValue-this.object.old[this.property]) * zeroisim.pi_180);	break;
			case 'rotz':	ship.rotateZ((newValue-this.object.old[this.property]) * zeroisim.pi_180);	break;
			}
		this.object.old[this.property] = newValue;		
		}
	updateSun(parameter) {
		var theta = Math.PI * (this.sunparameters.inclination - 0.5);
		var phi = 2 * Math.PI * (this.sunparameters.azimuth - 0.5);

		this.sunLight.position.x = this.sunparameters.distance * Math.cos( phi );
		this.sunLight.position.y = this.sunparameters.distance * Math.sin( phi ) * Math.sin( theta );
		this.sunLight.position.z = this.sunparameters.distance * Math.sin( phi ) * Math.cos( theta );

		this.sky.material.uniforms.sunPosition.value = this.sunLight.position.copy( this.sunLight.position );
		this.water.material.uniforms.sunDirection.value.copy( this.sunLight.position ).normalize();
		this.ambientLight.intensity = this.sunparameters.ambientLight;
		}
	onWindowResize(){
		this.camera_drone.aspect = window.innerWidth / window.innerHeight;
		this.camera_drone.updateProjectionMatrix();
		this.renderer.setSize(window.innerWidth,window.innerHeight);
		}
	exportGLTF(input,cb){
		var gltfExporter = new THREE.GLTFExporter();
		var options =
			{trs: false	//	todo zoek uit wat  document.getElementById('option_trs').checked
			,onlyVisible: false	// document.getElementById('option_visible').checked
			,truncateDrawRange: false	//	todo document.getElementById('option_drawrange').checked
			,binary: false	//	todo misschien is dit sneller of beter document.getElementById('option_binary').checked
			,forceIndices: true	//	todo waarschijnlijk wel nodig document.getElementById('option_forceindices').checked
			,forcePowerOfTwoTextures: true	//	document.getElementById('option_forcepot').checked
			};
		gltfExporter.parse(input, function( result ) {
			if ( result instanceof ArrayBuffer ) {
				saveArrayBuffer( result, 'scene.glb' );	//	binary. btoa() doen
				} 
			else {
				var output = JSON.stringify( result, null, 2 );
				// console.log( output );
				// return output;
				cb(output);
				}
			}, options);
		}		
	animate(){
		requestAnimationFrame(this.animateFunction);
		if (this.hasFocus) {
			var time = performance.now() * 0.001;	//	todo realtime maken, zoek uit wat Performance is
			// todo update ship attitude and position here
			this.water.material.uniforms.time.value += 1.0 / 60.0 * this.water.speed;	//	todo rekenen met stroom
			zeroisim.scene.traverseVisible(function(obj3d,that){	//	turn all lookAt() objects in the scene	
				if (undefined!=obj3d.mc_lookat && true==obj3d.mc_lookat){
					obj3d.lookAt(that.camera_drone.position);
					}
				},this);
			this.renderer.render(zeroisim.scene, this.camera_drone);
			this.stats.update();
			}
		}
	};	//	COcean
