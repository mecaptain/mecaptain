// ipilotHorizontal.js
'use strict';

// todo maak een optie dat het knopje na een commando blijf blinken totdat de engine het benodigde rpm of pitch bereikt

class CInstrument_pilotHorizontal extends CInstrument {
	constructor(obj,exposedParms,active,idx){
		super(idx,obj);
		if (undefined!=exposedParms)
			this.exposedParms = exposedParms;
		else
			this.exposedParms =
				{nots:'[0]'	//	number of thruster; value or array if coupled, in JSON format, may be array
				,oploc:	'B'	//	operation location in NMEA
				,colorBackground:	'rgba(255,255,255,0.0)'	//	gray
				,colorModeKnobOff:'rgba(224,224,224)'
				,colorModeKnobOn:	'rgba(0,255,0)'
				,tableBorder:		'3px solid rgba(255,255,255,0.4)' // 3px solid #FFF;
				,stops:	//	todo dit misschien in JSON formaat
					[{txt:'-6',value:-6}
					,{txt:'-5',value:-5}
					,{txt:'-4',value:-4}
					,{txt:'-3',value:-3}
					,{txt:'-2',value:-2}
					,{txt:'-1',value:-1}
					,{txt: '0',value:0}
					,{txt: '1',value:1}
					,{txt: '2',value:2}
					,{txt: '3',value:3}
					,{txt: '4',value:4}
					,{txt: '5',value:5}
					,{txt: '6',value:6}
					]
				};
		var stops = [];
		this.exposedParms.stops.myforEach(function(button,ii,ar,that){
			var style
				='background-color: #88AAAA;'
				+'xborder: 0px solid black;'
				+'height: 45px;'
				+'white-space:nowrap;'
				+'text-align:center;'
				+'-moz-border-radius: 8px;'
				+'-webkit-border-radius: 8px;'
				+'font-family: Arial, Helvetica, sans-serif;'
				+'font-size: 1.1em;'
				+'color: #EEE;'
				+'min-width: 40px;'
				+'border: 3px solid #FFF;'
				+'margin: 0px;'
				+'padding: 1px;'
				+'unselectable: on;'
				+'-webkit-user-select: none;'	/* Safari */
				+'-moz-user-select: none;'		/* Firefox */
				+'-ms-user-select: none;'		/* IE10+/Edge */
				+'user-select: none;'			/* Standard */
				;
			stops.push(i.td(button.txt,{style:style,class:'eotBtn',click:that.pilotcommand.bind(that),value:button.value}));
			},this);
		this.html = i.table(i.tr(stops),{style:'height:100%;width:100%;border:'+this.exposedParms.tableBorder});
		this.active = (undefined==active)? false: active;
		this.draw();
		}
	resize(w,h){
		}
	pilotcommand(evt){
		var value = evt.currentTarget.getAttribute('value');
		console.log('pilotcommand('+value+')');
		Array.from(evt.currentTarget.parentNode.parentNode.querySelectorAll('td.eotBtn')).myforEach(function(stop,ii,ar,that){
			if (stop.getAttribute('value')==value)
				stop.style.color = that.exposedParms.colorModeKnobOn;	//	zet lichtje in gekozen knop aan
			else
				stop.style.color = that.exposedParms.colorModeKnobOff;	//	zet lichtje in gekozen knop uit
			},this);
		this.txValue(value);
		}
	/*	
	txValue(value){	//	to be overloaded
	*/
	draw(){
		if (this.active!=true){
			zeroisim.replaceNode(this.div,i.span(i.center('HORIZ<br/>pilot')));
			return;
			}
		zeroisim.replaceNode(this.div,this.html);
		}
	}
/*	
CInstrument.addInstrumentToSettings
	('./ipilotHorizontal.js'
	,'pilotHor'
	,CInstrument.domTypeDiv
	,CInstrument_pilotHorizontal
	,'rudder, bow- or sternthruster'
	);
*/