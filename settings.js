'use strict';
//	global settings object
var settings = 
	{	tabmasks:
		{	show_settings: 		1<< 0		
		,	show_rxtx:				1<< 1		
		,	show_kompas:			1<< 2		
		,	show_radar:				1<< 3		
		,	show_geoloc:			1<< 4		
		,	show_azi:				1<< 5		
		,	show_ecdis:				1<< 6		
		,	show_ais:				1<< 7		
		,	show_view:				1<< 8		
		,	show_flags:				1<< 9		
		,	show_astro:				1<<10		
		,	show_usbcntrl:			1<<11		
		,	show_graph:				1<<12		
		,	show_env:				1<<13		
		,	show_script:			1<<14		
		,	show_pilot:				1<<15		
		,	show_fitcurv:			1<<16		
		,	show_plotlist:			1<<17		
		,	show_apilot:			1<<18		
		,	show_simulator:		1<<19		
		,	show_vhf:				1<<20		
		,	show_createghosts:	1<<21		
		,	show_trainer:			1<<22		
		,	show_controlif:		1<<23		//	for now, only Robs Arduino
		,	show_conningdesign:	1<<24		
		,	show_ghostmenu:		1<<25	//	add local ghost menu to posplot, do not combine this with show_trainer
	// ,	show_controlbox:		1<<25		
		}		
	,unitCollada: 1	//	1: betekent 1 unit is 1 meter. 0.1 betekent: 1 unit is 0.1 meter enz	
	,shipCollada:{
		file:
			// './3d/ships/tugboat/Tugboat.dae', distance:2, y_correction: -40
			// './3d/ships/simple_100.dae', distance:8, y_correction: -10
			'./3d/ships/simple_10.dae', distance:2, y_correction: -10
			// './3d/ships/rib/boat_rib.dae', y_correction: -40
			//	'./3d/ships/rib/Rib Boat 1.dae', distance:10, y_correction: 0
			// './3d/ships/rib/RibBoatvanCock.dae', distance:10, y_correction: 0 
			// './3d/ships/cement_ship/cement_ship.dae', distance:10, y_correction: -200
			// './3d/ships/liberty/liberty.dae', distance:1,
			// './3d/ships/liberty/liberty_simple.dae', distance:1,
			// './3d/ships/liberty/liberty_meshlab.dae', distance:1,
			// './3d/ships/buoy_iala1_green.dae', distance:10,
			// './3d/ships/schip_10meter/schip_10meter.dae', distance:1,
			// './js/3dobjects/nike-air-max-low-poly.dae', distance:1,
			// './3d/ships/container_ship/container_ship.dae', distance:50,
			// './3d/ships/CostaSerena/CostaSerena.dae', distance:1,
			// './3d/ships/titanic/titanic.dae', distance:1,
			// './3d/ships/schoenendoos/schoenendoos.dae', distance:1,
			//	'./incl/three.js-master/examples/models/collada/elf/elf.dae', distance:1,
			}
	//	todo specify chart layers	
	,	maptiles:		
	// [	{url:'https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpandmbXliNDBjZWd2M2x6bDk3c2ZtOTkifQ._QA7i5Mpkd_m30IGElHziw'
		[	{url:'https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoiYmVydHVzZGVnb3VkdmlzIiwiYSI6ImNqa2Qzd2ZldDNtdnczcWtjcXc4bWs5OTUifQ.0Nk-NZ4sGSnUl1Q_0sw1ZA'
			,options:
				{maxZoom: 18
					,attribution: 'Map data &copy; '	+ h.a('OpenStreetMap','href="http://openstreetmap.org"')
									+ ', contributors '	+ h.a('CC-BY-SA','href="http://creativecommons.org/licenses/by-sa/2.0/"')
									+ ', Imagery © '		+ h.a('Mapbox','href="http://mapbox.com"')
				,id: 'mapbox.streets'
				}
			}
		,	{url:'http://tiles.openseamap.org/seamark/{z}/{x}/{y}.png'
			,options:
				{maxZoom: 18
					,attribution: 'Map data &copy; '	+ h.a('OpenStreetMap','href="http://openstreetmap.org"')
									+ ', contributors '	+ h.a('CC-BY-SA','href="http://creativecommons.org/licenses/by-sa/2.0/"')
									+ ', Imagery © '		+ h.a('Mapbox','href="http://mapbox.com"')
				,id: 'openseamap'
				}
			}
		]
	/*
	OAuth-gegevens voor zeroisim
	Gebruikerssleutel: Ek3KsQVhFHgs0PpImnvQK635C0ZuefSAcbTdDag0
	Gebruikersgeheim: iduG7E9A3wNaZcfeBt4BiBWKve4cQxpBWpj1O6gL
	URL voor tokenverzoek: https://www.openstreetmap.org/oauth/request_token
	URL voor toegangstoken: https://www.openstreetmap.org/oauth/access_token
	URL voor autorisatie: https://www.openstreetmap.org/oauth/authorize
	Van de gebruiker worden de volgende rechten gevraagd:
	HMAC-SHA-1- (aangeraden) en RSA-SHA1-handtekeningen worden ondersteund.	
	*/
	,instrumentList:	//	these instruments appear in the conning designer and can be run in a separate window
		[{file:'./idopplerlog.js'}
		,{file:'./ipilotRudder.js'}
		,{file:'./ipilotThruster.js'}
		,{file:'./iview.js'}
		,{file:'./iradar.js'}
		,{file:'./igraph.js'}
		// ,{file:'./ivhf.js'}
		,{file:'./ienginepanel.js'}
		,{file:'./igps.js'}
		,{file:'./icompass.js'}
		,{file:'./ieot_html.js'}
		,{file:'./iais.js'}
		// ,{file:'./iengordertelegraph.js'}
		,{file:'./iruddercmd.js'}
		,{file:'./iazi.js'}
		,{file:'./iazivector.js'}
		,{file:'./iautopilot.js'}
		,{file:'./ishipshape.js'}
		,{file:'./ibutton_html.js'}
		// ,{file:'./iviewdirection.js'}	gizmo versie
		,{file:'./iviewdirection2.js'}
		,{file:'./iturnknob.js'}
		]
	,	check_internsim: false
	,	check_checksum: false
	,	check_pattern: false
	,	chrxnmea_tojs: true
	,	talkerid_tojs:	true	//	include talkerid in rx. example: talkerid_tojs==true:	rx.GGA.GP.lat has a value; talkerid_tojs==false:	rx.GGA.lat has a value
	,	showraw:	false			//	show raw nmea sentence on rx-pages
	,	decode_6bit: true
	,	style_aistable: 'font-family: Courier New; font-size: 0.8em'
	// ,	ws_addr:	'ws://192.168.1.14:12120'		//	mijn raspberry PI in de meterkast
	// ,	ws_addr:	'ws://169.254.246.206:12120'	//	mijn raspberry PI in de meterkast
	// ,	ws_addr:	'ws://82.173.162.184:12120'	//	mijn raspberry PI in de meterkast
	// ,	ws_addr:	'ws://localhost:12120'
	,	ws_addr:	'ws://bert-toshsat:80'	//	overrulable on cmd line
	,	listen_addr: 'udp://:8890'	//	werkt beter als hostname weggelaten wordt, websocket server vraagt zijn eigen hostname op
	,	ws_verbose: true
	,	talk_port: '8011'
	,	aisgraph_diameter: 400
	//	todo font, colors, layout
	,	graph_on: false
	,	rudder0_on_ap_off:	true
	,	file_processing_time:	't10'	//	todo verzin iets om de goede radiobutton zijn check te geven
	
	,	nmea:	//	defines which sentence:field 's are used for several instruments and functions
		{compassHDG: {s:'THS',f:'hdgT'}
		// {compassHDG: {s:'VTG',f:'cogT'}
		,
		}
	};
