// ieot_html.js
'use strict';

// todo maak een optie dat het knopje na een commando blijf blinken totdat de engine het benodigde rpm of pitch bereikt

class CInstrument_EOThtml extends CInstrument {
	constructor(obj,exposedParms,active,idx){
		super(idx,obj);
		if (undefined!=exposedParms)
			this.exposedParms = exposedParms;
		else
			this.exposedParms =
				{nots:'[0]'	//	number of thruster; value or array if coupled, in JSON format, may be array
				,oploc:	'B'	//	operation location in NMEA
				,colorBackground:	'rgba(255,255,255,0.0)'	//	gray
				,colorModeKnobOff:'#EEE'
				,colorModeKnobOn:	'#0F0'
				,tableBorder:		'3px solid rgba(255,255,255,0.4)' // 3px solid #FFF;
				,stops:	//	todo dit misschien in JSON formaat
					[{txt:'full<br/>ahead',thrust:100}
					,{txt:'half<br/>ahead',thrust:50}
					,{txt:'slow<br/>ahead',thrust:10}
					,{txt:'deadslow<br/>ahead',thrust:5}
					,{txt:'stop',thrust:0}
					,{txt:'deadslow<br/>astern',thrust:-5}
					,{txt:'slow<br/>astern',thrust:-10}
					,{txt:'half<br/>astern',thrust:-50}
					,{txt:'full<br/>astern',thrust:-100}
					]
				};
		var stops = [];
		this.exposedParms.stops.myforEach(function(button,ii,ar,that){
			var style
				='background-color: #88AAAA;'
				+'xborder: 0px solid black;'
				+'height: 45px;'
				+'white-space:nowrap;'
				+'text-align:center;'
				+'-moz-border-radius: 8px;'
				+'-webkit-border-radius: 8px;'
				+'font-family: Arial, Helvetica, sans-serif;'
				+'font-size: 1.1em;'
				+'color: #EEE;'
				+'min-width: 40px;'
				+'border: 3px solid #FFF;'
				+'margin: 0px;'
				+'padding: 1px;'
				+'unselectable: on;'
				+'-webkit-user-select: none;'	/* Safari */
				+'-moz-user-select: none;'		/* Firefox */
				+'-ms-user-select: none;'		/* IE10+/Edge */
				+'user-select: none;'			/* Standard */
				;
			stops.push(i.tr(i.td(button.txt,{style:style,class:'eotBtn',click:that.pilotcommand.bind(that),thrust:button.thrust})));
			},this);
		this.html = i.table(stops,{style:'height:100%;width:100%;border:'+this.exposedParms.tableBorder});
		this.active = (undefined==active)? false: active;
		this.draw();
		}
	resize(w,h){
		}
	pilotcommand(evt){
		var thrust = evt.currentTarget.getAttribute('thrust');
		console.log('pilotcommand('+thrust+')');
		Array.from(evt.currentTarget.parentNode.parentNode.querySelectorAll('td.eotBtn')).myforEach(function(stop,ii,ar,that){
			if (stop.getAttribute('thrust')==thrust)
				stop.style.color = that.exposedParms.colorModeKnobOn;	//	zet lichtje in gekozen knop aan
			else
				stop.style.color = that.exposedParms.colorModeKnobOff;	//	zet lichtje in gekozen knop uit
			},this);
		this.telegraph(thrust);
		}
	telegraph(thrust){
		var nots = JSON.parse(this.exposedParms.nots);
		if (!Array.isArray(nots))	nots = [nots];
		nots.myforEach(function(not,ii,ar,that){
			/*
			var trc = 
				{not:		not		//	integer(),		'Number of thruster (thruster index)'
				,rpm:		thrust	//	floatingp,		'RPM demand value (percentage)'
				,rpmi:	'P'		//	literal('P'),	'RPM mode indicator (indicates percentage)'
				,pitch:	thrust	//	floatingp,		'Pitch demand value (percentage)'
				,ptm:		'P'		//	literal('P'),	'Pitch mode indicator (indicates percentage)'
				,azi:		undefined//	floatingp,		'Azimuth demand (0-360, follow-up)'
				,oloc:	that.exposedParms.oploc		//	literal('.'),	'Operating location indicator'
				,stat:	'A'		//	literal('A|V'),'Sentence status flag'
				,from:	'ieot_html'
				,talker: nmeadefs.TRC[0].ti
				};
			zeroisim.busTX('TRC',trc,that.wid);
			*/
			var prc = 
				{ldpos:	thrust	//	Lever demand position (-100% to 100%):100.0]
				,ldstat:	'A'	//	Lever demand status, A=Data valid, V=Data invalid:'A']
				,rdval:	thrust	//	RPM demand value (-100% to 100%):100.0]
				,rmind:	'P'	//	RPM mode indicator; Percentage Rpm inValid:'P']
				,pdval:	thrust	//	Pitch demand value (-100% to 100%):100.0]
				,pmind:	'P'	//	Pitch mode indicator; Percentage Degrees inValid:'P']
				,oploc:	'B'	//	Operating location indicator; Bridge Portwing Starboardwing engineControlroom Engineside Wing:'W']
				,engshft:not	//	Number of engine or propeller shaft, zero based (numeric) 0=single or center, odd=starboard, even=port:0]
				,from:	'ipilotThruster'
				,talker: nmeadefs.PRC[0].ti
				}
			zeroisim.busTX('PRC',prc,that.wid);
			},this);
		}
	draw(){
		if (this.active!=true){
			zeroisim.replaceNode(this.div,i.span(i.center('EOT<br />html')));
			return;
			}
		zeroisim.replaceNode(this.div,this.html);
		}
	}
CInstrument.addInstrumentToSettings
	('./ieot_html.js'
	,'eothtml'
	,CInstrument.domTypeDiv
	,CInstrument_EOThtml
	,'Engine Order Telegraph, might be connected to an USB-input'
	);
