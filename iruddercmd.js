//	iruddercmd.js

'use strict';

class CInstrument_rudder extends CInstrument {
	constructor(obj,exposedParms,active,idx){
		super(idx,obj);
		var canvas = obj.querySelector("canvas");
		this.cv = canvas.getContext("2d")
		this.midden = {x: canvas.width/2, y:canvas.height/2};
		this.deg2rad = Math.PI / 180.0;
		this.shiftkey = false;
		this.mdown = false;
		this.__id = 'CInstrument_rudder';
		
		if (undefined!=exposedParms)
			this.exposedParms = exposedParms;
		else
			this.exposedParms = //	todo deze moeten door de conning designer kunnen worden gezet, nier overrulen hier dan
				{globalAlpha:					0.9
				,colorBackground:			'#BBB'	//	gray
				,colorRings:				'#00F'	//	blue
				,colorCross:				'#0F0'	//	green
				// ,schaal: 10.0	//	dit is mooi bij een canvas van 158 bij 208
				,schaal: canvas.height/17.4
				,roer: [[0,11],[1,-2],[-1,-2],[0,11]]
				,actualRudderFont: 'bold 30px sans-serif'
				,cmdRudderFont: 'bold 12px sans-serif'
				};	
				
		this.ship0 = {ror:{}};
		this.cv.globalAlpha = this.exposedParms.globalAlpha;
		this.bMoveRight = false;
		this.bMoveLeft = false;
		this.bMoveUp = false;
		this.bMoveDown = false;
		this.rudderzero = false;
		super.cbRxNmea('RSA',function(rxd,that){	//	install callback weather
			if (undefined==rxd.data)	return;
			that.actualRudder = parseFloat(rxd.data.srud);
			that.draw();	
			},this);
		super.cbRxNmea('ROR',function(rxd,that){	//	install callback weather
			if (undefined==rxd.data)	return;
			if ('A'==rxd.data.svalid)		that.ship0.ror.s = rxd.data.srud;
			if ('A'==rxd.data.pvalid)		that.ship0.ror.p = rxd.data.prud;
			that.draw();	
			},this);
		}
	draw() {
		this.cv.clearRect(0,0,2*this.midden.x,2*this.midden.y);
		this.cv.save();
		this.cv.fillStyle = this.exposedParms.colorBackground;	this.cv.fillRect(0,0,2*this.midden.x,2*this.midden.y);

		this.cv.translate(this.midden.x,3*this.exposedParms.schaal);
		this.cv.lineWidth = 5;

		//	draw green SB and red BB arc
		this.cv.lineWidth = 1;
		this.cv.beginPath();this.cv.fillStyle="green";this.arc(0,0,9*this.exposedParms.schaal,this.exposedParms.schaal,180,140,false);	this.cv.fill();	this.cv.stroke();
		this.cv.beginPath();this.cv.fillStyle="red";	 this.arc(0,0,9*this.exposedParms.schaal,this.exposedParms.schaal,180,220,true);	this.cv.fill();	this.cv.stroke();

		//	color rudder and rudderangle according to port-red and starboard-green
		this.cv.lineWidth = 1;
		this.rudderColorActual = '#000';
		if (this.actualRudder>0)	this.rudderColorActual = 'green';	//	'#0F0';
		if (this.actualRudder<0)	this.rudderColorActual = 'red';		//	'#F00';
		var rudderColorCmd = '#000';
		if (this.ship0.ror.s>0)		rudderColorCmd = 'green';	//	'#0F0';
		if (this.ship0.ror.s<0)		rudderColorCmd = 'red';		//	'#F00';

		//	type actual rudderangle
		this.cv.textAlign="center";
		this.cv.fillStyle = this.rudderColorActual;
		this.cv.font = this.exposedParms.actualRudderFont;
		this.cv.fillText(Math.abs(this.actualRudder).toFixed(1), 0,this.exposedParms.schaal*13.8);

		//	type rudder command
		if (this.actualRudder != this.ship0.ror.s){
			this.cv.fillStyle = rudderColorCmd;
			this.cv.font = this.exposedParms.cmdRudderFont;
			this.cv.fillText(Math.abs(this.ship0.ror.s).toFixed(1)
				, this.exposedParms.schaal * 11 * Math.sin(this.ship0.ror.s * this.deg2rad)
				, this.exposedParms.schaal * 11 * Math.cos(this.ship0.ror.s * this.deg2rad) + this.exposedParms.schaal/1.1
				);
			}

		//	draw rudder command
		this.cv.rotate( -this.ship0.ror.s * this.deg2rad);
		this.cv.strokeStyle = rudderColorCmd;
		this.drawcanvaslines(this.cv,this.exposedParms.roer,this.exposedParms.schaal);
		this.cv.fillStyle=rudderColorCmd; this.cv.fill();
		this.cv.rotate( +this.ship0.ror.s * this.deg2rad);

		//	draw actual rudder
		this.cv.rotate( -this.actualRudder * this.deg2rad);
		this.cv.strokeStyle = 'rgba(0,0,0,1.0)';	//	zwart
		this.drawcanvaslines(this.cv,this.exposedParms.roer,this.exposedParms.schaal);
		this.cv.fillStyle='gray'; this.cv.fill();

		this.cv.restore();
		}
	keydown(evt) {
		this.shiftkey = evt.shiftKey;
		switch (evt.key){
			case 'ArrowRight':	this.bMoveRight = true;	break;
			case 'ArrowLeft':		this.bMoveLeft = true;	break;
			// case 'ArrowUp':		this.bMoveUp = true;	break;
			// case 'ArrowDown':		this.bMoveDown = true;	break;
			}
		}
	keyup(evt) {
		this.shiftkey = evt.shiftKey;
		switch (evt.key){
			case 'ArrowRight':	this.bMoveRight = false;	break;
			case 'ArrowLeft':		this.bMoveLeft = false;	break;
			// case 'ArrowUp':		this.bMoveUp = false;	break;
			// case 'ArrowDown':		this.bMoveDown = false;	break;
			case '0':				this.rudderzero = true;	break;
			}
		}
	drawcanvaslines(cv,lines,schaal){
		cv.beginPath();
		for (var idxlijn in lines) {
			cv.lineTo( schaal * lines[idxlijn][0], schaal * lines[idxlijn][1] );
			}
		cv.stroke();
		}
	arc(xm,ym,r,dik,starthoek,eindhoek,rolo){
		var boog = {a:-Math.PI/2*(1-starthoek/90), b:Math.PI/2*(eindhoek/90-1)};
		this.cv.arc(xm,ym,r+dik,boog.a,boog.b,!rolo);
		this.cv.arc(xm,ym,r,    boog.b,boog.a, rolo);
		}
	}
CInstrument.addInstrumentToSettings
	('./iruddercmd.js'
	,'rudder'
	,'canvas'
	,CInstrument_rudder
	,'shows commanded and actual rudder angle'
	);
