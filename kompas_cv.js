//	kompas_cv.js
// teken een kompas en zet de simulatiegegevens in localstorage
//	volgens tab_template maken
'use strict';
var createKompasCV = function () {
	return {
		ap_mode: 'off',
		ap_setpoint: 0,
		onfocus:function(){
			if (undefined==this.canvas) 
				this.init2(this);
			}
		,onblur:function(){
			}
		,init: function(id_canvas){
			/**/
			cbRxNmea(settings.nmea.compassHDG.s,function(rxd){	//	install callback for compass and autopilot
			// cbRxNmea('THS',function(rxd){	//	install callback for compass and autopilot
				if (undefined==rxd.data)	return;
				ship0.hdt = parseFloat(rxd.data[settings.nmea.compassHDG.f]);
				// ship0.hdt = parseFloat(rxd.data.hdgT);	//	hdt is global
				kompas.draw(ship0,180);
				});
			/**/	
			/*
			cbRxNmea('VTG',function(rxd){	//	install callback for compass and autopilot
				if (undefined==rxd.data)	return;
				ship0.hdt = parseFloat(rxd.data.cogT);	//	hdt is global
				kompas.draw(ship0,180);
				});
			*/
			cbRxNmea('APN',function(rxd,that){	//	install callback for compass and autopilot
				if (undefined==rxd.data)	return;
				switch (rxd.data.control){
					case 'OFF':	that.setAP({mode:'off',setpoint:0});	break;
					case 'HDG':	that.setAP({mode:'hdg',setpoint:rxd.data.orderedhdg});	break;
					case 'ROT':	that.setAP({mode:'rot',setpoint:rxd.data.rot_rad});		break;
					case 'RAD':	that.setAP({mode:'rad',setpoint:rxd.data.rot_rad});		break;
					case 'TRK':	that.setAP({mode:'trk',setpoint:rxd.data.orderedhdg});	break;	//	todo
					}
				that.draw(ship0,180);
				},this);
			this.idcv = id_canvas;	
			window.addEventListener("load", this.init2.bind(this), false);	
			},
		init2: function(init2_onrdy){
			this.canvas = document.getElementById(this.idcv);
			if (undefined==this.canvas) return;
			// this.cv = this.canvas.getContext("2d");
 			this.pi_180 = Math.PI / 180.0;
			this.cb_onreadyImgs = init2_onrdy;
			//	bouw de kompasrozen
			this.kompasresize(this.canvas.width,this.canvas.height,init2_onrdy);
			this.canvas.that = this;
			
			//	todo dit zou voor ieder instrument-canvas moeten gelden
			this.canvas.addEventListener("click", this.canvasClick.bind(this), false);	
			this.canvas.addEventListener("mousedown", this.canvasMouseDown.bind(this), false);	
			this.canvas.addEventListener("mousemove", this.canvasMouseMove.bind(this), false);	
			this.canvas.addEventListener("mouseup", this.canvasMouseUp.bind(this), false);	
			},
		canvasMouseUp: function(ev){
			if (ev.currentTarget.classList.contains('resizing')){
				var hw = 
					{w:ev.currentTarget.width-(ev.currentTarget.zeroisimStart.w-ev.offsetX)
					,h:ev.currentTarget.height-(ev.currentTarget.zeroisimStart.h-ev.offsetY)};
				ev.currentTarget.height = hw.h;
				ev.currentTarget.width = hw.w;
				this.kompasresize(ev.currentTarget.width,ev.currentTarget.height);
				ev.currentTarget.classList.remove('resizing');
				ev.currentTarget.zeroisimStart = undefined;
				console.log('Stop resizing ('+hw.w+','+hw.h+')');
				}
			if (ev.currentTarget.classList.contains('moving')){
				ev.currentTarget.classList.remove('moving');
				console.log('Stop moving');
				}
			},
		canvasMouseMove: function(ev){
			//	uitdaging: als de muis buiten het canvas beweegt, krijgt deze handler geen events meer
			if (ev.currentTarget.classList.contains('resizing')){
				//	todo draw rubberband
				var hw = 
					{w:ev.currentTarget.width-(ev.currentTarget.zeroisimStart.w-ev.offsetX)
					,h:ev.currentTarget.height-(ev.currentTarget.zeroisimStart.h-ev.offsetY)};
				console.log('resizing ('+hw.w+','+hw.h+')');
				}
			if (ev.currentTarget.classList.contains('moving')){
				console.log('moving');
				}
			},
		canvasMouseDown: function(ev){
			//	if down in resize position, resize
			//	if down in move position, move
			if ((ev.offsetX > ev.currentTarget.width-20)
			&&  (ev.offsetY > ev.currentTarget.height-20)){
				ev.currentTarget.classList.add('resizing');
				ev.currentTarget.classList.remove('moving');	//	vd zekerheid
				ev.currentTarget.zeroisimStart = {w:ev.offsetX,h:ev.offsetY};
				console.log('Start resizing');
				}
			else {
				ev.currentTarget.classList.add('moving');
				ev.currentTarget.classList.remove('resizing');	//	vd zekerheid
				console.log('Start moving');
				}			
			},
		canvasClick: function(ev){
			/*
			if (ev.currentTarget.height > 300){
				ev.currentTarget.height = ev.currentTarget.height/2;
				ev.currentTarget.width = ev.currentTarget.width/2;
				}
			else {
				ev.currentTarget.height = ev.currentTarget.height*2;
				ev.currentTarget.width = ev.currentTarget.width*2;
				}
			this.kompasresize(ev.currentTarget.width,ev.currentTarget.height);
			*/
			},
		kompasresize: function(w,h){
			var rc = {width:this.canvas.width, height:this.canvas.height};
			if (undefined!=this.cv) this.cv = undefined;
			this.cv = this.canvas.getContext("2d");
			if (undefined!=this.img360) delete this.img360;
			if (undefined!=this.img010) delete this.img010;

			this.canvas.width = this.w = w;
			this.canvas.height = this.h = h;
			this.midden = {x: this.canvas.width/2, y:this.canvas.height/2};
			this.r_half = this.canvas.width/2;	//	todo let op aspectratio
			//	define settings
			var colorscheme = [
				{back:'#BBB',c360:'#FFF',c010:'#DDD',hdgtxt:'#DDD',l360:'rgba(255,0,0,0.3)',l010:'rgba(255,0,0,0.3)',cv:'#EEE'},
				{back:'#FFF',c360:'#00F',c010:'#00E',hdgtxt:'#00D',l360:'rgba(255,0,0,0.5)',l010:'rgba(255,0,0,0.5)',cv:'#EEE'},
				];
			var cs = colorscheme[1];	//	choose here which colorscheme you gonna use
			this.settings = 
				{	card360:{routside:0.92,rinside:0.87,r10s:0.95,r5s:0.978,rtexts:0.87,color:cs.c360,font:'10px Verdana'}
				,	card010:{routside:0.42,rinside:0.37,r10s:0.90,r5s:0.950,rtexts:0.78,color:cs.c010,font:'10px Verdana'}
				,	heading:{color:cs.hdgtxt,font:'bold 20px sans-serif',pos:{x:0,y:-0.65*this.midden.y}}
				,	lubber360:{color:cs.l360,outside:-0.96*this.midden.y,inside:-0.82*this.midden.y,thick:3}
				,	lubber010:{color:cs.l010,outside:-0.46*this.midden.y,inside:-0.35*this.midden.y,thick:3}
				,	canvas:{bgcolor:cs.cv}
				,	diamond:	{cog:{size:10,color:'rgba(0,240,240,0.6)',thick:5}		//	course over ground
								,rsa:{size: 4,color:'#F2930F',thick:5}						//	rudder sensor angle
								,rorp:{size: 1,color:'#F2930F',thick:5}					//	ordered port rudder
								,rors:{size: 1,color:'#F2930F',thick:5}					//	ordered starboard rudder
								,ohdg:{size: 7,color:'rgba(240,0,240,0.6)',thick:5}	//	ordered heading
								,orot:{size: 7,color:'rgba(240,0,240,0.6)',thick:5}	//	ordered rot
								}
				,	rrot: 0.59,	drot: 0.04	//	radius and thickness of rot marker
				};
			// zet de kleur van background op canvas
			this.canvas.style.backgroundColor = this.settings.canvas.bgcolor;

			//	bouw de kompasrozen
			//	todo misschien geheugenlek eruithalen
			this.cv.clearRect(0,0,this.w,this.h);
			if (Math.min(this.w,this.h) < 120) {
				//	todo, wat ook lauk is is om het grote plaatje te houden en het te tonen met een translatie
				if (undefined!=this.img360)	this.img360 = undefined;
				if (undefined!=this.img010)	this.img010 = undefined;
				this.readyCreatingCards();
				}
			else if (Math.min(this.w,this.h) < 240) {
				//	todo, wat ook lauk is is om het grote plaatje te houden en het te tonen met een translatie
				zeroisim.createImages
					(this
					,this.canvas
					,[{name:'img360',fun:this.gradenboog_proxy,parm:{that:this,canvas: this.cv,p1:{ticks: 72,short:1,long: 2,ptext: 6,angledivider:0.2},p2:this.settings.card360,m:this.midden}}
					 ,{name:'img010',fun:this.gradenboog_proxy,parm:{that:this,canvas: this.cv,p1:{ticks:100,short:5,long:10,ptext:10,angledivider: 10},p2:this.settings.card010,m:this.midden}}
					 ]
					,this.readyCreatingCards.bind(this) 
					);
				}
			else if (Math.min(this.w,this.h) < 310) {
				//	todo, wat ook lauk is is om het grote plaatje te houden en het te tonen met een translatie
				zeroisim.createImages(this,this.canvas,
					[{name:'img360',fun:this.gradenboog_proxy,parm:{that:this,canvas: this.cv,p1:{ticks: 72,short:1,long: 2,ptext: 4,angledivider:0.2},p2:this.settings.card360,m:this.midden}}
					,{name:'img010',fun:this.gradenboog_proxy,parm:{that:this,canvas: this.cv,p1:{ticks:100,short:5,long:10,ptext:10,angledivider: 10},p2:this.settings.card010,m:this.midden}}
					 ]
					,this.readyCreatingCards.bind(this) 
					);
				}
			else {	
				//	todo misschien moeten de images zichzelf kunnen tekenen met een soort cb routine
				zeroisim.createImages(this,this.canvas,
					[{name:'img360',fun:this.gradenboog_proxy,parm:{that:this,canvas: this.cv,p1:{ticks:360,short:5,long:10,ptext:10,angledivider:  1},p2:this.settings.card360,m:this.midden}}
					,{name:'img010',fun:this.gradenboog_proxy,parm:{that:this,canvas: this.cv,p1:{ticks:100,short:5,long:10,ptext:10,angledivider: 10},p2:this.settings.card010,m:this.midden}}
					 ]
					,this.readyCreatingCards.bind(this) 
					);
				}
			return rc;	
			},
		readyCreatingCards:function(){
			this.imgsReady = true;
			this.draw(ship0,180);
			if ((undefined!=this.cb_onreadyImgs) && zeroisim.isFn(this.cb_onreadyImgs))
				this.cb_onreadyImgs.apply();
			},
		setAP: function(ap){
			if (undefined!=ap.mode)			this.ap_mode = ap.mode;
			if (undefined!=ap.setpoint)	this.ap_setpoint = ap.setpoint;
			},		
		draw: function(ship,ord) {
			if (true!==this.imgsReady)	return;
			this.hdg = ship.hdt;
			if (undefined==this.img010)	return;

			if (undefined!=ship.rot)	this.rot = ship.rot;		else	this.rot = 0;	//	rate of turn
			if (undefined!=ship.cog)	this.cog = ship.cog;		else	this.cog = 0;	//	course over ground
			if (undefined!=ship.rsa)	this.rsa = ship.rsa;		else	this.rsa = 0;	//	rudder sensor angle
			if (undefined!=ship.ror){
			if (undefined!=ship.ror.p)	this.rorp = ship.ror.p;	else	this.rorp = 0;	//	ordered rudder angle
			if (undefined!=ship.ror.s)	this.rors = ship.ror.s;	else	this.rors = 0;	//	ordered rudder angle
			}
			this.cv.save();
			this.cv.clearRect(0,0,this.w,this.h);
			this.cv.fillStyle = this.canvas.style.backgroundColor;
			this.cv.fillRect(0,0,this.w,this.h);
			this.cv.translate(this.midden.x,this.midden.y);

			//	draai naar de koers en zet plaatje op het canvas
			if (undefined!=this.img360){
				this.cv.save();
				this.cv.rotate( -this.hdg * this.pi_180);
				this.cv.drawImage(this.img360,-this.img360.m.x,-this.img360.m.y);
				this.cv.restore();	// this.cv.rotate( +this.hdg * this.pi_180);	//	todo uitvinden wat sneller is, terugroteren of save()-restore()
				}
			//	teken vergroting van het kompas
			if (undefined!=this.img010.m){
				this.cv.save();
				this.cv.rotate( 36 * -this.hdg * this.pi_180);
				this.cv.drawImage(this.img010,-this.img010.m.x,-this.img010.m.y);
				this.cv.restore();	// this.cv.rotate( 36 * +this.hdg * this.pi_180);
				}
			//	schrijf koers
			this.cv.textAlign="center";
			this.cv.fillStyle = this.settings.heading.color;
			this.cv.font = this.settings.heading.font;
			this.cv.fillText(zeroisim.prefixpad(this.hdg.toFixed(1),3)+h.deg,this.settings.heading.pos.x,this.settings.heading.pos.y);

			//	teken buitenste zeilstreep
			this.cv.beginPath();
			this.cv.strokeStyle = this.settings.lubber360.color;	this.cv.lineWidth = this.settings.lubber360.thick;
			this.cv.moveTo(0,this.settings.lubber360.outside);		this.cv.lineTo(0,this.settings.lubber360.inside);
			
			//	teken binnenste zeilstreep
			this.cv.strokeStyle = this.settings.lubber010.color;	this.cv.lineWidth = this.settings.lubber010.thick;
			this.cv.moveTo(0,this.settings.lubber010.outside);		this.cv.lineTo(0,this.settings.lubber010.inside);
			this.cv.stroke();
			this.cv.closePath();

			//	draw rot
			if (Math.round(Math.abs(this.rot))>0){
				this.cv.fillStyle = '#0F0';
				var rotX = +14;
				if (this.rot<0){
					this.cv.fillStyle = '#F00';
					rotX *= -1;
					}
				this.cv.strokeStyle = this.cv.fillStyle;
				this.cv.fillText(Math.abs(this.rot).toFixed(0), rotX,-0.48*this.midden.y);
				}
			
			// var boog = {straal:0.59*this.midden.y,dikte:0.04*this.midden.y};
			var boog = {straal:this.settings.rrot*this.midden.y,dikte:this.settings.drot*this.midden.y};
			this.cv.beginPath();		this.cv.fillStyle=this.cv.fillStyle;
			this.arc(this.cv,0,0,boog.straal,boog.dikte,0,this.rot,this.rot>0);
			this.cv.fill();		this.cv.stroke();
			
			//	draw marks around compasscard
			var laf;
			laf = this.settings.diamond.cog;	this.diamond(laf,this.settings.card360.routside,this.cog-this.hdg);	//	draw cog
			laf = this.settings.diamond.rsa;	this.diamond(laf,this.settings.card360.routside,this.rsa.s	);	//	draw rudder sensor angle
			laf = this.settings.diamond.rorp;this.diamond(laf,this.settings.card360.routside,this.rorp	);	//	draw rudder order
			laf = this.settings.diamond.rors;this.diamond(laf,this.settings.card360.routside,this.rors	);	//	draw rudder order
			
			if (this.ap_mode=='hdg'){
				var dhdg = zeroisim.deltaDirection(this.hdg,this.ap_setpoint,1);
				var rr = this.settings.card360.routside;
				if (Math.abs(dhdg)<8){	//	move marker to magnified ring
					rr = this.settings.card010.routside;
					dhdg *=36;
					}
				laf = this.settings.diamond.ohdg;	this.diamond(laf,rr,dhdg);	//	draw ordered heading
				// todo consider using tween (http://learningthreejs.com/blog/2011/08/17/tweenjs-for-smooth-animation/)
				}
			else if (this.ap_mode=='rot'){
				laf = this.settings.diamond.orot;	this.diamond(laf,this.settings.rrot,this.ap_setpoint);	//	draw ordered heading
				}
			else {
				// todo ondersteun ook track en radius
				}
			this.cv.restore();	// sluit het tekenen af en zet oude matrix aktief
			},
		gradenboog_proxy: function(){
			zeroisim.gradenboog(this);			
			},
		arc: function(cv,xm,ym,r,dik,starthoek,eindhoek,rolo){
			var boog = {a:-Math.PI/2*(1-starthoek/90), b:Math.PI/2*(eindhoek/90-1)};
			cv.arc(xm,ym,r+dik,boog.a,boog.b,!rolo);
			cv.arc(xm,ym,r,    boog.b,boog.a, rolo);
			},
		diamond: function(laf,r,angle){
			zeroisim.doPolair(this.cv,function(cv,x,y,laf,that){
				cv.beginPath();
				var path=[[1,-1],[0,-2],[-1,-1],];
				cv.moveTo(x,y);
				for (var ii in path){
					var node=path[ii];
					cv.lineTo(x+laf.size*node[0],y+laf.size*node[1]);
					}
				cv.lineTo(x,y);
				cv.strokeStyle = laf.color;
				cv.lineWidth = laf.thick;
				cv.closePath();
				cv.stroke();
				},angle,r,laf,this.midden,this);
			},
		};
	}
var fromChild = function(w,what) {	//	todo zie de globals collectie sepwdw
	// document.getElementById("dbg").innerHTML += what + "<br />";
	switch (typeof what){
		case 'string':
			console.log('fromChild, string: '+what);
			w.fromParent("hajeto, voortaan ben jij het kompas");
			break;
		case 'object':	//	expect cmd,parms
			console.log('fromChild, cmd: '+what.cmd);
			switch (what.cmd) {
				case 'cbRxNmea':	//	expect parms is key
					console.log('   key: '+what.parms);
					/**/
					cbRxNmea(what.parms,function(rxd,wdw){	
						wdw.cbNmeaTHS(rxd);	//	todo, later krijg je de naam van de childwindow callback
						},w);
					/**/	
					break;
				default:	
					console.log('fromChild(): ERROR unsupported cmd: '+what.cmd);
					break;
				}
			break;
		}
	}
