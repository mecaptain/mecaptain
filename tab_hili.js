//	tab_hili.js
'use strict';

class CHili extends C0isimTab {
	constructor(){
		super();
		settings.ws_addr=zeroisim.gup('ws_addr');
		settings.listen_addr=zeroisim.gup('listen_addr');
		this.rob_remote_ip_port=zeroisim.gup('rob_remote_ip_port');
		this.wsHili(settings.ws_addr,settings.listen_addr);
		
		var slidset={name:'dimmer',min:0,max:255,id:'dimmer',step:1};
		if (Object.keys(slidset).length>4)
			var step = slidset.step;
		else 
			var step = zeroisim.roundNumber((slidset.max-slidset.min)/100,2+Math.log10(1/slidset.max-slidset.min));
			
		this.dehtml = i.table(i.tr(
				[i.td(i.span(slidset.name))
				,i.td(i.input('',{type:"range",id:slidset.id,input:this.chslid.bind(this),min:slidset.min,max:slidset.max,step:step,value:0,style:"width: 270px;"}))
				,i.td(i.output(i.span('0'),{for:'id_'+slidset.id,id:'slid_'+slidset.id}),{style:"width: 30px;"})
				]));
		}
	onload(evt){
		document.getElementById(this._tabdivname).appendChild(this.dehtml);
		}
	wsHili(ws_addr,listen_addr){
		// open connection to socketserver and he will give you the nmea coming from settings.listen_addr
		ws_talk_listen = new wsconnect(ws_addr,'nmea-talk-listen'
			,function(ws){	//	cbconnect
				console.log('WebSocket protocol="nmea-talk-listen" connected to: '+ ws_addr);
				tab_settings.showConnected(true);
				ws.zend(JSON.stringify(
					{listen_addr:			listen_addr
					,verbose:				1
					}));
				}
			,function(ws,data,that){	//	cbdata
				console.log('WebSocket protocol="nmea-talk-listen" data rx: '+ data);
				data = JSON.parse(data);
				}
			,function(ws,that){	//	cbclose
				console.info('WebSocket protocol="nmea-talk-listen" disconnected');
				// setValue('div_trainertab','login was not successful' + h.br + that.loginpanel);
				}
			,this	
			);
		}
	chslid(evt){
		var field = evt.target.id;
		var value = parseFloat(evt.target.value);
		document.querySelector('#slid_'+field).value=value;
		var niks = '';
		var nmea = ['VSTEP_CTRL,',niks,niks,niks,niks,niks,niks,niks,niks,niks,1,niks,niks,niks,niks,niks,niks,value,niks,niks,niks,niks,niks,];
		nmea = nmea.join(',');	//	 + '*FF';
		console.log('ws_talk_listen.sendNMEA(\'udp://\''+this.rob_remote_ip_port+' '+nmea);
		ws_talk_listen.sendNMEA('udp://'+this.rob_remote_ip_port,nmea+"\r\n");
		}
	};	//	CHili
