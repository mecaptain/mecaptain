//	tab_interpolate.js
//	show polynomal interpolation of a graph with a given number of nodes
// Bert Tuijl 2106
//
//	http://www.numericjs.com/lib/numeric-1.2.6.min.js
//
'use strict';
var interpolatetabobj = {
	// verplichte drie members om de tabs interface te ondersteunen
	html: function(){
		// Defer initialisation, you need the canvas for it, to after pageload event
		window.addEventListener("load", this.initAfterLoad.bind(this), false);
		return 'interpolate'
			+ h.canvas('id="interpolate_canvas" width="480" height="340"')
			+	h.div(h.nbsp,"id='tab_interpol_mousepos'")
			+	h.div(h.nbsp,"id='tab_interpol_formula'")
			;
		}
	,onfocus:function(){	
		this.hasFocus=true;
		}
	,onblur:function(){	
		this.hasFocus=false;	
		}
	,hasFocus: false
	,initAfterLoad: function(){
		var canvas = document.getElementById('interpolate_canvas');
		this.ctx = canvas.getContext("2d");
		this.hctx = canvas.height;
		this.wctx = canvas.width;
		this.scale = {x:0.8,y:0.8};
		// this.scale = {x:100,y:100};
		this.trans = {x:this.wctx/2,y:this.hctx/2};
		this.mouseIsDown = false;
		this.dragging = false;
		canvas.that = this;
		canvas.onmousedown = function(e){	e.currentTarget.that.mousedown(e);	}
		canvas.onmouseup = function(e){		e.currentTarget.that.mouseup(e);		}
		canvas.onmousemove = function(e){	e.currentTarget.that.mousemove(e);	}
		// canvas.onwheel = function(e){			e.currentTarget.that.mousewheel(e);	}
		canvas.addEventListener('wheel',function(e){	e.currentTarget.that.mousewheel(e);	},{passive: true});
		this.points = 
			// [{x: 0,y:10}
			// ,{x:20,y:20}
			// ,{x:10,y:30}
		//	[{x:-160,y:-160}
		//	,{x:+160,y:+160}
			[{x:-100.000000,y:-100}
			,{x:+100.000000,y:+100}
			];
		this.draw();
		}
	,mousedown: function(evt){
		// console.log('mousedown');
		this.mouseIsDown = {x:evt.offsetX,y:evt.offsetY};
		}
	,mouseup: function(evt){
		//console.log('mouseup');
		if (false!==this.dragging) {
			console.log('old trans: ');
			console.log(this.trans);
			this.trans = 
				{x:this.trans.x-(this.dragging.x-evt.offsetX)
				,y:this.trans.y-(this.dragging.y-evt.offsetY)
				};
			console.log('new trans: ');
			console.log(this.trans);
			this.dragging = false;
			this.draw();
			}
		else {
			// create a point to draw graphic through
			var pt = 
				{x:(+evt.offsetX-this.trans.x)/this.scale.x
				,y:(-evt.offsetY+this.trans.y)/this.scale.y
				};
			// console.log('point: ');console.log(pt);
			this.points.push(pt);
			var pp2 = this.polynom(this.points);
			// var W2 = this.horner(pp2);
			// this.points.myforEach(function(el,idx,arr,that){
				// var y = W2(el.x);
				// },this);
			this.points.myforEach(function(el,idx,arr,fun){
				var y = fun(el.x);
				},this.horner(pp2));
			
			this.draw();
			}			
		this.mouseIsDown = false;
		}
	,mousemove: function(evt){
		// console.log('mousemove');
		if (false!==this.mouseIsDown){	//	start dragging if mouse is down
			this.dragging = this.mouseIsDown;
			}
		var pt = 
			{x:(+evt.offsetX-this.trans.x)/this.scale.x
			,y:(-evt.offsetY+this.trans.y)/this.scale.y
			};
		setValue('tab_interpol_mousepos','('+pt.x+' , '+pt.y+')');
		}
	,mousewheel: function(evt){
		this.scale.x += evt.deltaY/1000.0;	this.scale.y += evt.deltaY/1000.0;
		// console.log('scale: ');
		// console.log(this.scale);
		this.draw();
		}
	,draw: function(){
		this.ctx.clearRect(0,0,this.wctx,this.hctx);
		this.ctx.save();
		this.ctx.translate(this.trans.x,this.trans.y);
		this.ctx.scale(this.scale.x,this.scale.y);
		this.coords(this.wctx, this.hctx);
		var pp2 = this.polynom(this.points);
		// W2 = this.horner(pp2);
		// y = W2(0);
		// y = W2(0.5);
		// y = W2(1);
		// y = W2(1.5);
		// y = W2(2);
		// y = W2(2.5);
		// y = W2(3);
		
		// helper function
		function draw_function(that,color,f) {
			that.ctx.strokeStyle = color;
			that.drawFunction(f,that.wctx,that.hctx);
			}
		this.points.myforEach(function(el,idx,arr,that){
			that.drawPoint({x:el.x,y:el.y},[[5,0],[0,-5],[-5,0],[0,5]],that.wctx, that.hctx /*,{x:1,y:1}that.scale*/);
			},this);
		draw_function(this,"rgb(150,0,0)", this.horner(pp2) /*, this.scale {x:1,y:1}*/);
		this.ctx.restore();
		}
	,drawFunction: function(fun,width,height) {
		// var ch = height/2;
		// var cw = width/2;
		var ch = 0;
		var cw = 0;
		this.ctx.beginPath();
		this.ctx.moveTo(0, ch-fun(-width));
		for (var x=-width+1; x<width; x++) {
			var test = fun(x);
			this.ctx.lineTo(x+cw,ch-fun(x));
			}
		this.ctx.lineWidth = 2;
		this.ctx.stroke();
		}
	,drawPoint: function(point,path,width,height/*,scale*/) {
		this.ctx.beginPath();
		var xx = point.x;
		var yy = -point.y;
		this.ctx.moveTo(xx+path[0][0],yy+path[0][1]);
		for (var ii in path){
			this.ctx.lineTo(xx+path[ii][0],yy+path[ii][1]);
			}
		this.ctx.lineWidth = 1;
		this.ctx.closePath();
		this.ctx.stroke();
		}
	,coords: function(width, height) {
		this.ctx.beginPath();
		var cw = width/2;
		var ch = height/2;
		cw = 0;
		ch = 0;
		this.ctx.moveTo(cw, height-this.trans.y);	//	(cw,height)
		this.ctx.lineTo(cw, -this.trans.y);	//			(cw,0)
		this.ctx.moveTo(0-this.trans.x, ch);
		this.ctx.lineTo(width-this.trans.x, ch);
		this.ctx.strokeStyle = "rgb(0,0,0)";
		this.ctx.stroke();

		//y arrow
		this.ctx.beginPath();
		this.ctx.moveTo(cw-4, 14-this.trans.y);
		this.ctx.lineTo(cw+0,  0-this.trans.y);
		this.ctx.lineTo(cw+4, 14-this.trans.y);
		this.ctx.closePath();
		this.ctx.fill();

		//x arrow
		this.ctx.beginPath();
		this.ctx.moveTo(width-14-this.trans.x, ch-4);
		this.ctx.lineTo(width- 0-this.trans.x, ch);
		this.ctx.lineTo(width-14-this.trans.x, ch+4);
		this.ctx.closePath();
		this.ctx.fill();

		this.ctx.font = "12pt Arial";
		// this.ctx.fillText("y", cw+10, 15-this.trans.y);
		// this.ctx.fillText("x", width-10-this.trans.x, ch+20);
		this.ctx.fillText("y", cw+10, 15-(this.trans.y)/this.scale.y);
		this.ctx.fillText("x", (width-this.trans.x)/this.scale.x-10, ch+20);
		}
	,horner:	function(array) {
		function recur(x, i, array) {
			if (i == 0) {
				return array[0];
				} 
			else {
				return array[i] + x*recur(x, --i, array);
				}
			}
		return function(x) {
			return recur(x, array.length-1, array);
			};
		}
	,polynom: function(points){
		var order = points.length-1;
		var y = [];
		var xMatrix = [];
		var xTemp = [];
			 
		for (var j=0;j<points.length;j++){
			y.push(points[j].y);
			}
		var yMatrix = numeric.transpose([y]);
		for (j=0;j<points.length;j++){
			xTemp = [];
			for (var i=0;i<=order;i++){
				xTemp.push(1*Math.pow(points[j].x,i));
				}
			xMatrix.push(xTemp);
			}
		var xMatrixT = numeric.transpose(xMatrix);
		var dot1 = numeric.dot(xMatrixT,xMatrix);
		var dotInv = numeric.inv(dot1);
		var dot2 = numeric.dot(xMatrixT,yMatrix);
		var tsolution = numeric.dot(dotInv,dot2);
		var solution = [];
		var formula = 'y = ';
		var plus = '';
		tsolution.myforEach(function(el,idx,arr,p){
			var factor = arr[p-1-idx][0];	
			solution.push(factor);	
				if (idx > 0){
					if (factor>0)	plus = ' +';
					if (factor<0)	plus = ' ';
					}
				formula += plus + factor.toString();
				var macht = arr.length-idx-1;
				if			(0==macht)	formula += '';
				else if	(1==macht)	formula += '*X';
				else						formula += '*X'+h.sup(macht);
			},tsolution.length);
		// console.log("formula:"+formula);
		// console.log(solution);
		setValue('tab_interpol_formula',formula);
		return solution;
		}
	};
