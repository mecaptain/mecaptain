//	tab_radar.js
'use strict';

var radartabobj = {
	// verplichte drie members om de tabs interface te ondersteunen
	html: function(){
		// console.log('initting radartabObj');
		var hpw = ' style: width=100%;';
		this.ship = {hdt:0,latlon:new zeroisim.LatLon()};	//	geen gebruik meer maken van global		
		window.addEventListener("load", this.onload.bind(this), false);	//	todo use bind() everywhere on load event
		return h.fieldset("radar",h.table(h.tr
			(h.td(h.canvas('id=radartab_canvas width="740" height="740"'))
			+h.td(h.table
				(h.tr(h.td(h.table(h.tr(h.td('range','colspan=3 style="text-align:center;"'))+h.tr
					(h.td(h.button('&#43','id=btn_radar_rng_plus'+hpw))
					+h.td(h.div('rng','id=txt_radar_rng'))
					+h.td(h.button('&#45','id=btn_radar_rng_min'+hpw))
					),hpw)))
				+h.tr(h.td(h.fieldset("view",h.table
					(h.tr(h.td(h.input('ais','type="checkbox" id=check_radar_ais')))
					+h.tr(h.td(h.input('hdg','type="checkbox" id=check_radar_hdg')))
					+h.tr(h.td(h.input('rings','type="checkbox" id=check_radar_rings')))
					+h.tr(h.td(h.input('navmarks','type="checkbox" id=check_radar_navmarks')))
					))))
				+h.tr(h.td(h.fieldset("cursor",h.table
					(h.tr(h.td(h.div(h.nbsp,'id=radar_display_pos')))
					+h.tr(h.td(h.div('brg','id=radar_display_brg')))
					+h.tr(h.td(h.div('rng','id=radar_display_rng')))
					))))
				))
			)));
		}
	,onfocus:function(){	this.hasFocus=true;	}
	,onblur:function(){	this.hasFocus=false;	}
	,onload: function(evt){
		//	initialize canvas
		this.canvas = document.getElementById('radartab_canvas');
		this.canvas.that = this;
		this.canvas.onmousedown	= function(e){	e.currentTarget.that.mousedown(e);	}
		this.canvas.onmouseup	= function(e){	e.currentTarget.that.mouseup(e);		}
		this.canvas.onmousemove	= function(e){	e.currentTarget.that.mousemove(e);	}
		// this.canvas.onwheel		= function(e){	e.currentTarget.that.mousewheel(e);	}
		this.canvas.addEventListener('wheel', function(e){	e.currentTarget.that.mousewheel(e);	}, {passive: true});

		this.canvas.style.backgroundColor = '#EEE';
		
		this.cv = this.canvas.getContext("2d");
		this.pi_180 = Math.PI / 180.0;
		this.dikte_gradenboog = 37;
		this.ppi_midpoint = Math.min(this.canvas.width/2,this.canvas.height/2);	//	todo ppi wordt kleiner als er een gradenboog komt
		this.ppi_radius = this.ppi_midpoint - this.dikte_gradenboog;
		
		this.ppi_pos = new zeroisim.LatLon({latdeg: 0, londeg: 0});	//	assume midpoint
		this.ppi_cursorpos = new zeroisim.LatLon({latdeg: 0, londeg: 0});	//	assume midpoint
		
		this.args_gradenboog =	//	looks for the compass card around the ppi
			{that:this
			,canvas: this.cv
			,p1:{ticks:360,short:5,long:10,ptext:10,angledivider:  1}
			,p2:{routside:0.9,rinside:0.915,r10s:1.030,r5s:1.017,rtexts:1.05,color:'#00F',font:'10px Verdana'}
			,m:{x:this.ppi_midpoint,y:this.ppi_midpoint}
			};
		
		//	do not let the text fields empty
		this.range = 6.0;	//	range in mijlen
		this.pulselength =  this.l_pulse[this.ranges.indexOf(this.range)];
		setValue('txt_radar_rng',this.range);
		setValue('radar_display_pos',this.ppi_pos.latdegmindec+h.br+this.ppi_pos.londegmindec);
		setValue('radar_display_brg','000' + h.deg);
		setValue('radar_display_rng','00.0' + "'");

		//	install event handlers
		document.getElementById('btn_radar_rng_plus').addEventListener("click", this.rangePlus.bind(this), false);
		document.getElementById('btn_radar_rng_min').addEventListener("click", this.rangeMin.bind(this), false);
		document.getElementById('check_radar_ais').addEventListener("change", this.checkAis.bind(this), false);
		document.getElementById('check_radar_hdg').addEventListener("change", this.checkHdg.bind(this), false);
		document.getElementById('check_radar_rings').addEventListener("change", this.checkRings.bind(this), false);
		document.getElementById('check_radar_navmarks').addEventListener("change", this.checkNavmarks.bind(this), false);
	
		//	set defaults
		window.check_radar_hdg.click();	
		window.check_radar_rings.click();	
		window.check_radar_navmarks.click();	

		cbRxNmea('GGA',function(rxd,that){	//	install callback for position		todo kiezen van welke sentence je de latlon wilt (GGA, GLL, ...)
			if (undefined==rxd.data)	return;
			that.ship.latlon.interpretLatLonGGA(rxd.data);		
			that.update();
			},this);
		cbRxNmea('THS',function(rxd,that){	//	install callback for heading
			if (undefined==rxd.data)	return;
			that.ship.hdt = parseFloat(rxd.data.hdgT);
			that.update();
			},this);
			
		this.draw();
		}
	,hasFocus: false
	,aisplot: false
	,ranges:		[0.25,0.5,0.75,1.5,3,6,12,24,48]
	,l_pulse:	[  15, 15,  12, 10,6,4, 2, 2, 1]	//	in pixels
	,shipShape: [[-1,2],[1,2],[1,-2],[0,-3],[-1,-2],[-1,2]]
	,aisShipShape: [[-1,2],[1,2],[0,-3],[-1,2]]
	,pulselength: 20
	,update: function(){
		this.ppi_pos = this.ship.latlon;
		this.draw();
		}
	, rangePlus: function(){
		this.range = this.ranges[(this.ranges.indexOf(this.range)+1)% this.ranges.length];
		this.pulselength =  this.l_pulse[this.ranges.indexOf(this.range)];
		setValue('txt_radar_rng',this.range);
		}
	, rangeMin: function(){
		var idx = this.ranges.indexOf(this.range);
		if (0==idx) idx = this.ranges.length
		this.range = this.ranges[--idx];
		this.pulselength =  this.l_pulse[this.ranges.indexOf(this.range)];
		setValue('txt_radar_rng',this.range);
		}
	,checkAis: function(evt){
		this.aisplot = evt.target.checked;
		}
	,checkHdg: function(evt){
		this.hdgFlash = evt.target.checked;
		}
	,checkRings: function(evt){
		this.rings = evt.target.checked;
		}
	,checkNavmarks: function(evt){	
		this.navmarks = evt.target.checked;
		}
	,mousedown: function(evt){
		var dbg = 0;
		}
	,mouseup: function(evt){
		var dbg = this.windowToPosition(evt);
		}
	,mousemove: function(evt){
		var pos = this.windowToPosition(evt);
		var bd = pos.beardist(this.ppi_pos);
		setValue('radar_display_pos',pos.latdegmindec+h.br+pos.londegmindec);
		setValue('radar_display_brg',bd.b + h.deg);
		setValue('radar_display_rng',bd.d + "'");
		}
	,mousewheel: function(evt){
		console.log('mousewheel');
		}
	,drawShape: function(cvxy,hdt,schaal,shape,color,linewidth){
		this.cv.save();
		this.cv.translate(this.ppi_midpoint+cvxy.x,this.ppi_midpoint+cvxy.y);
		this.cv.rotate(hdt*Math.PI/180.0);
		if (undefined==color) color = '#F00';
		if (undefined==linewidth) linewidth = 3;
		this.cv.strokeStyle = color;
		this.cv.lineWidth = linewidth;
		this.cv.beginPath();
		if (schaal<5)	schaal = 5;
		shape.myforEach(function(pp,idx,arr,that){
			if (0==idx)	that.cv.moveTo(schaal*pp[0], schaal*pp[1]);
			that.cv.lineTo(schaal*pp[0], schaal*pp[1]);
			},this);			
		this.cv.closePath();		
		this.cv.stroke();
		this.cv.restore();
		}
	,drawRings: function(color){
		this.cv.strokeStyle = color;
		this.cv.lineWidth = 1;
		for(var ring=1; ring<6; ring++){
			this.cv.beginPath();
			this.cv.arc(this.ppi_midpoint,this.ppi_midpoint,this.ppi_radius*ring/6,0,2*Math.PI);
			this.cv.stroke();
			}
		}
	,drawEcho: function(dist,xy,color){
		// var dist = Math.sqrt(xy.x*xy.x+xy.y*xy.y)+this.pulselength/2;
		// if (dist>this.ppi_radius)	return;	//	off screen
		var bearing = Math.atan2(xy.y,xy.x);
		// var distance_discrimination = 0.1 /2;
		var distance_discrimination = 5 / dist;
		this.cv.strokeStyle = color;
		this.cv.lineWidth = this.pulselength;	//	afstand onderscheidingsvermogen, pulslengte
		this.cv.beginPath();
		this.cv.arc(this.ppi_midpoint,this.ppi_midpoint,dist,bearing-distance_discrimination,bearing+distance_discrimination);
		this.cv.stroke();
		this.cv.closePath();
		}
	,drawHeadingFlash: function(hdt,color){
		this.cv.save();
		this.cv.translate(this.ppi_midpoint,this.ppi_midpoint);
		this.cv.rotate(hdt*zeroisim.pi_180);
		this.cv.beginPath();
		this.cv.lineWidth = 1;
		this.cv.strokeStyle = color;
		this.cv.moveTo(0,0);
		this.cv.lineTo(0,-this.ppi_radius);
		this.cv.closePath();
		this.cv.stroke();
		this.cv.restore();
		}
	,draw: function(){
		if (!this.hasFocus)	return;
		// draw ppi
		this.cv.fillStyle = 'rgba(125,125,125,1.0)';
		this.cv.strokeStyle = 'rgba(0,255,0,1.0)';	//	groen
		this.cv.beginPath();
		this.cv.lineWidth = 1;
		this.cv.arc(this.ppi_midpoint,this.ppi_midpoint,this.ppi_radius,0,2*Math.PI);
		this.cv.stroke();
		this.cv.fill();
		zeroisim.gradenboog(this.args_gradenboog);
		if (this.hdgFlash) this.drawHeadingFlash(this.ship.hdt,'rgba(125,255,125,0.5)');
		if (this.rings) this.drawRings('rgba(0,200,0,1.0)');
		// treat AIS targets as radar echoes
		aislist.myforEachSparse(function(aistarget,idx,arr,that){
			if (undefined!=aistarget.Latitude){	//	maw wacht op een VDM msg:5
				if (aistarget.ownship!=true){	//	todo switchable
					var xy = that.latlongToCanvasPoint(aistarget.Latitude,aistarget.Longitude);	// get center from the map (projected)
					var dist = Math.sqrt(xy.x*xy.x+xy.y*xy.y)+that.pulselength/2;
					if (dist>that.ppi_radius)	return;	//	off screen
					that.drawEcho(dist,xy,'rgba(0,255,200,1.0)');
					if (that.aisplot){	//	if user wants ais targets being plotted
						that.drawShape(xy,aistarget.COG,6,that.aisShipShape);
						}
					}
				}
			},this);
		//	todo check if in range	
		if (this.navmarks) {
			Object.keys(zeroisim.navmarks.slijkgat_boeienlijn).myforEach(function(key,ii,ar,that){
				var light = zeroisim.navmarks.slijkgat_boeienlijn[key];
				var xy = that.latlongToCanvasPoint(light.latrad/that.pi_180,light.lonrad/that.pi_180); 
				var dist = Math.sqrt(xy.x*xy.x+xy.y*xy.y)+that.pulselength/2;
				if (dist<that.ppi_radius)	//	if in range screen
					that.drawEcho(dist,xy,light.color);
				},this);
			}
		}
	,windowToCanvas: function(x,y) {
		var bbox = this.canvas.getBoundingClientRect();
		return { x: x-bbox.left * (this.canvas.width  / bbox.width)
				 ,	y: y-bbox.top  * (this.canvas.height / bbox.height)
				 };
		}
	,windowToPosition: function(evt) {
		var posPpi = this.windowToCanvas(evt.clientX,evt.clientY);	//	offset in pixels from origin of canvas
		posPpi.x -= this.ppi_midpoint;	// offset in pixels from midpoint of canvas
		posPpi.y -= this.ppi_midpoint;	// offset in pixels from midpoint of canvas
		this.ppi_cursorpos.latdeg = this.ppi_pos.latdeg-posPpi.y*this.range/60/this.ppi_midpoint;
		this.ppi_cursorpos.londeg = this.ppi_pos.londeg+posPpi.x*this.range/60/this.ppi_midpoint/Math.cos(this.ppi_pos.latrad);
		return this.ppi_cursorpos;
		}		
	,latlongToCanvasPoint: function(lat,lon){
		var dlat = 60.0*(this.ppi_pos.latdeg-lat);
		var dlon = 60.0*(lon-this.ppi_pos.londeg);
		var ypixels = dlat * this.ppi_midpoint / this.range;
		var xpixels = dlon * this.ppi_midpoint / this.range * Math.cos(this.ppi_pos.latrad);
		return {x:xpixels, y:ypixels};
		}
	};
